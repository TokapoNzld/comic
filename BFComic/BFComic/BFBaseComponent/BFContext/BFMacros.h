//
//  BFMacros.h.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#ifndef BFMacros_h_h
#define BFMacros_h_h

// 屏幕bounds
#define kScreenBounds UIScreen.mainScreen.bounds
#ifndef kScreenSize
// 屏幕size
#define kScreenSize UIScreen.mainScreen.bounds.size
#endif
#ifndef kScreenWidth
// 屏宽
#define kScreenWidth kScreenSize.width
#endif
#ifndef kScreenHeight
// 屏高
#define kScreenHeight kScreenSize.height
// 导航栏高度
#define kNaviHeight self.navigationController.navigationBar.frame.size.height
// 状态栏高度
#define kStatusHeight [[UIApplication sharedApplication] statusBarFrame].size.height
// 判断是否有刘海
#define ISBangsPhone ({\
    BOOL isBangsScreen = NO; \
    if (@available(iOS 11.0, *)) { \
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject]; \
    isBangsScreen = window.safeAreaInsets.bottom > 0; \
    } \
    isBangsScreen; \
})
// 获取底部安全高度
#define kBottomSafeHeight (ISBangsPhone ? (34.0):(0))

//默认图
#define kDefaultImage [UIImage imageNamed:@"comic_placeholder"]

#endif

#endif /* BFMacros_h_h */
