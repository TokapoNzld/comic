//
//  BFColor.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#ifndef BFColor_h
#define BFColor_h

#import <UIKit/UIKit.h>

#define UIColorFromHexWithAlpha(hexValue,a) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:a]

#define UIColorFromHex(hexValue) UIColorFromHexWithAlpha(hexValue,1.0)
#define UIColorFromRGBA(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define UIColorFromRGB(r,g,b) UIColorFromRGBA(r,g,b,1.0)
#define UIColorFromRandom [UIColor colorWithRed:arc4random_uniform(255.0)/255.0 green:arc4random_uniform(255.0)/255.0 blue:arc4random_uniform(255.0)/255.0 alpha:1.0];

/// TabBar 未选中的颜色
static NSInteger const kColorTabBarNormal = 0x757A82;
/// TabBar 选中的颜色
static NSInteger const kColorTabBarSelected = 0x000000;

#define AppMainThemeColor UIColorFromRGB(47,47,47)
#define AppTabbarBackgroundColor UIColorFromRGB(36,36,36)
#define AppNavigationBarBackgroundColor UIColorFromRGB(36,36,36)

#define AppTabbarNormalColor  UIColorFromRGB(69,69,69)
#define AppTabbarSelectedColor [UIColor whiteColor]

#define AppMainTextLabelColor UIColorFromRGB(240,240,240)

#endif /* BFColor_h */
