//
//  BFNotification.h
//  BFComic
//
//  Created by majunwen on 2022/5/24.
//

#ifndef BFNotification_h
#define BFNotification_h

static NSString *kLoginViewModelLogoutNotificationName = @"kLoginViewModelLogoutNotificationName";
static NSString *kLoginViewModelLoginNotificationName = @"kLoginViewModelLoginNotificationName";
static NSString *kBookshelfAddToComicNotificationName = @"kBookshelfAddToComicNotificationName";

#endif /* BFNotification_h */
