//
//  UILabel+BFAd.m
//
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "NSString+BFAd.h"
#import "UILabel+BFAd.h"
#import <CoreText/CoreText.h>
#import <objc/runtime.h>

@implementation UILabel (BFAd)

+(void)load{
    Method boundsOriMethod = class_getInstanceMethod([self class], @selector(textRectForBounds:limitedToNumberOfLines:));
    Method boundsSwiMethod = class_getInstanceMethod([self class], @selector(bf_textRectForBounds:limitedToNumberOfLines:));
    class_replaceMethod([self class], boundsSwiMethod, method_getImplementation(boundsOriMethod), method_getTypeEncoding(boundsOriMethod));
    
    Method drawOriMethod = class_getInstanceMethod([self class], @selector(drawTextInRect:));
    Method drawSwiMethod = class_getInstanceMethod([self class], @selector(bf_drawTextInRect:));
    method_exchangeImplementations(boundsOriMethod, boundsSwiMethod);
    method_exchangeImplementations(drawOriMethod, drawSwiMethod);
}

+ (instancetype)bf_labelWithText:(NSString *)text textFont:(int)font textColor:(UIColor *)color frame:(CGRect)frame {
  UILabel *label = [UILabel new];
  label.text = text;
  label.font = [UIFont systemFontOfSize:font];
  label.textColor = color;
  label.textAlignment = YES;
  label.frame = frame;
  return label;
}

- (void)bf_setColumnSpace:(CGFloat)columnSpace {
  NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
  //调整间距
  [attributedString addAttribute:(__bridge NSString *)kCTKernAttributeName value:@(columnSpace) range:NSMakeRange(0, [attributedString length])];
  self.attributedText = attributedString;
}

- (void)bf_setRowSpace:(CGFloat)rowSpace {
  self.numberOfLines = 0;
  NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
  //调整行距
  NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
  paragraphStyle.lineSpacing = rowSpace;
  paragraphStyle.baseWritingDirection = NSWritingDirectionLeftToRight;
  paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
  [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.text length])];
  self.attributedText = attributedString;
}

- (void)bf_setTextFont:(UIFont *)font atRange:(NSRange)range {
  [self bf_setTextAttributes:@{ NSFontAttributeName : font } atRange:range];
}

- (void)bf_setTextColor:(UIColor *)color atRange:(NSRange)range {
  [self bf_setTextAttributes:@{ NSForegroundColorAttributeName : color } atRange:range];
}

- (void)bf_setTextLineSpace:(float)space atRange:(NSRange)range {
  NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.text];
  NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];

  [paragraphStyle setLineSpacing:space];  //调整行间距

  [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.text length])];
  self.attributedText = attributedString;
  [self sizeToFit];
}
- (void)bf_setTextLineSpace {
  NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.text];
  NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
  //    CGFloat space = [self.text sizeWithFont:self.font].height*0.2;
  [paragraphStyle setLineSpacing:4];  // UI 要求8像素间距

  [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.text length])];
  self.attributedText = attributedString;
  self.lineBreakMode = NSLineBreakByTruncatingTail;
  [self sizeToFit];
}
- (void)bf_setTextFont:(UIFont *)font color:(UIColor *)color atRange:(NSRange)range {
  [self bf_setTextAttributes:@{ NSFontAttributeName : font, NSForegroundColorAttributeName : color } atRange:range];
}

- (void)bf_setTextAttributes:(NSDictionary *)attributes atRange:(NSRange)range {
  NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
  [mutableAttributedString setAttributes:attributes range:range];
  self.attributedText = mutableAttributedString;
}

- (void)bf_setLabelAppointStrColorWithStr:(NSString *)str color:(UIColor *)color {
  if (str == nil) {
    str = @"";
  }
  NSRange range = [self.text rangeOfString:str];
  NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:self.text];
  [attributedStr addAttribute:NSForegroundColorAttributeName value:color range:range];
  self.attributedText = attributedStr;
}
+ (instancetype)bf_makeLabel:(void (^)(UILabel *))textLabel {
  UILabel *label = [[UILabel alloc] init];
  textLabel(label);
  return label;
}
- (UILabel * (^)(NSString *))bf_addText {
  return ^(NSString *text) {
    [self setText:text];
    return self;
  };
}
- (UILabel * (^)(CGFloat))bf_addTextFont {
  return ^(CGFloat font) {
    [self setFont:[UIFont systemFontOfSize:font]];
    return self;
  };
}
- (UILabel * (^)(UIColor *))bf_addTextColor {
  return ^(UIColor *textColor) {
    [self setTextColor:textColor];
    return self;
  };
}
- (UILabel * (^)(UIColor *))bf_addLabelBackgroudColor {
  return ^(UIColor *backgroudColor) {
    [self setBackgroundColor:backgroudColor];
    return self;
  };
}
- (UILabel * (^)(CGRect))bf_addLabelFrame {
  return ^(CGRect labelFrame) {
    [self setFrame:labelFrame];
    return self;
  };
}

-(void)setEdgeInset:(UIEdgeInsets)edgeInset{
    objc_setAssociatedObject(self, @selector(edgeInset), @(edgeInset), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self setNeedsDisplay];
}

-(UIEdgeInsets)edgeInset{
    return [objc_getAssociatedObject(self, _cmd) UIEdgeInsetsValue];
}


- (CGRect)bf_textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines {
    [self bf_textRectForBounds:bounds limitedToNumberOfLines:numberOfLines];
    
    CGRect rect = [self bf_textRectForBounds:UIEdgeInsetsInsetRect(bounds,
                                                                 self.edgeInset) limitedToNumberOfLines:numberOfLines];
    //根据edgeInsets，修改绘制文字的bounds
    rect.origin.x -= self.edgeInset.left;
    rect.origin.y -= self.edgeInset.top;
    rect.size.width += self.edgeInset.left + self.edgeInset.right;
    rect.size.height += self.edgeInset.top + self.edgeInset.bottom;
    return rect;
}

//绘制文字
- (void)bf_drawTextInRect:(CGRect)rect {
    if (self.text && ![self.text isEqualToString:@""]) {
        //令绘制区域为原始区域，增加的内边距区域不绘制
        [self bf_drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInset)];
//        self.hidden = NO;
    } else {
        [self bf_drawTextInRect:UIEdgeInsetsInsetRect(rect, UIEdgeInsetsZero)];
//        self.hidden = YES;
    }
}

@end
