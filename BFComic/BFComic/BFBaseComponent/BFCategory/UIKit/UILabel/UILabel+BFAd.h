//
//  UILabel+BFAd.h
//  BeautifulBreastClub
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (BFAd)

@property (assign, nonatomic) UIEdgeInsets edgeInset;

// 快速创建标签
+ (instancetype)bf_labelWithText:(NSString *)text textFont:(int)font textColor:(UIColor *)color frame:(CGRect)frame;

/**
 *  设置字间距
 */
- (void)bf_setColumnSpace:(CGFloat)columnSpace;

/**
 *  设置行距
 */
- (void)bf_setRowSpace:(CGFloat)rowSpace;

/**
 指定范围字体大小设置
 */
- (void)bf_setTextFont:(UIFont *)font atRange:(NSRange)range;

/**
 指定范围更改颜色
 */
- (void)bf_setTextColor:(UIColor *)color atRange:(NSRange)range;

/**
 指定范围更改间距
 */
- (void)bf_setTextLineSpace:(float)space atRange:(NSRange)range;

/**
 指定范围更改字体大小与颜色
 */
- (void)bf_setTextFont:(UIFont *)font color:(UIColor *)color atRange:(NSRange)range;

/**
 传入一个字典更改Attributes属性
 */
- (void)bf_setTextAttributes:(NSDictionary *)attributes atRange:(NSRange)range;

/**
 默认间距
 */
- (void)bf_setTextLineSpace;

/**
 更改指定字符串颜色

 @param str 需要更改的字符
 @param color 颜色
 */
- (void)bf_setLabelAppointStrColorWithStr:(NSString *)str color:(UIColor *)color;

+ (instancetype)bf_makeLabel:(void(^)(UILabel *label))textLabel;
- (UILabel * (^)(NSString *text))bf_addText;
- (UILabel * (^)(UIColor *textColor))bf_addTextColor;
- (UILabel * (^)(CGFloat font))bf_addTextFont;
- (UILabel * (^)(UIColor *backgroudColor))bf_addLabelBackgroudColor;
- (UILabel * (^)(CGRect labelFrame))bf_addLabelFrame;


@end
