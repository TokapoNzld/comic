//
//  UIButton+BFAd.h
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ButtonEdgeInsetsStyle) {
    ButtonEdgeInsetsStyleTop, // image在上，label在下
    ButtonEdgeInsetsStyleLeft, // image在左，label在右
    ButtonEdgeInsetsStyleBottom, // image在下，label在上
    ButtonEdgeInsetsStyleRight // image在右，label在左
};


@interface UIButton (BFAd)

@property (nonatomic, assign) int imagePosition;
@property (nonatomic, assign) CGFloat spacingBetweenImageAndTitle;
/**
 *  设置button的titleLabel和imageView的布局样式，及间距
 *
 *  @param style titleLabel和imageView的布局样式
 *  @param space titleLabel和imageView的间距
 */
- (void)bf_setButtonEdgeInsetsStyle:(ButtonEdgeInsetsStyle)style
                 imageTitleSpace:(CGFloat)space;


/**
 *  增大按钮的点击范围
 *
 *  @param top    上
 *  @param right  右
 *  @param bottom 下
 *  @param left   左
 */
- (void)bf_setEnlargeEdgeWithTop:(CGFloat)top Right:(CGFloat)right Bottom:(CGFloat) bottom Left:(CGFloat)left;

/**
 *  利用 UIButton 的 titleEdgeInsets 和 imageEdgeInsets 来实现文字和图片的自由排列
 *  注意：这个方法需要在设置图片和文字之后才可以调用，且 button 的大小要大于 图片大小+文字大小+spacing
 *
 *  @param spacing 图片和文字的间隔
 */
- (void)bf_setImagePosition:(ButtonEdgeInsetsStyle)style spacing:(CGFloat)spacing;

#pragma mark - Count Down
/**
 根据倒计时间, 倒计标题, 倒计背景颜色, 结束标题及结束背景颜色, 修改Button(每秒修改)
 
 @param seconds                 倒计时间
 @param title                   倒计标题
 @param backgroundColor         倒计背景颜色
 @param finishedTitle           结束标题
 @param finishedBackgroundColor 结束背景颜色
 */
- (void)bf_changeWithCountDown:(NSInteger)seconds
                         title:(NSString *)title
               backgroundColor:(UIColor *)backgroundColor
                 finishedTitle:(NSString *)finishedTitle
       finishedBackgroundColor:(UIColor *)finishedBackgroundColor;

@end

NS_ASSUME_NONNULL_END
