//
//  UIButton+BFAd.m
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "UIButton+BFAd.h"
#import <objc/runtime.h>
#import "UIView+BFAd.h"

static char topNameKey;
static char rightNameKey;
static char bottomNameKey;
static char leftNameKey;
static const void *kImagePosition = &kImagePosition;
static const void *kSpacingBetweenImageAndTitle = &kSpacingBetweenImageAndTitle;

@implementation UIButton (BFAd)

- (void)bf_setButtonEdgeInsetsStyle:(ButtonEdgeInsetsStyle)style imageTitleSpace:(CGFloat)space {

  CGFloat imageWith = self.imageView.frame.size.width;
  CGFloat imageHeight = self.imageView.frame.size.height;

  CGFloat labelWidth = 0.0;
  CGFloat labelHeight = 0.0;
  if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
    // 由于iOS8中titleLabel的size为0，用下面的这种设置（这里遇到了坑点）
    labelWidth = self.titleLabel.intrinsicContentSize.width;
    labelHeight = self.titleLabel.intrinsicContentSize.height;
  }
  else {
    labelWidth = self.titleLabel.frame.size.width;
    labelHeight = self.titleLabel.frame.size.height;
  }

  UIEdgeInsets imageEdgeInsets = UIEdgeInsetsZero;
  UIEdgeInsets labelEdgeInsets = UIEdgeInsetsZero;

  //  根据style和space得到imageEdgeInsets和labelEdgeInsets的值
  /**
   ButtonEdgeInsetsStyleTop, // image在上，label在下
   ButtonEdgeInsetsStyleLeft, // image在左，label在右
   ButtonEdgeInsetsStyleBottom, // image在下，label在上
   ButtonEdgeInsetsStyleRight // image在右，label在左
   */
  switch (style) {
    case ButtonEdgeInsetsStyleTop: {
      imageEdgeInsets = UIEdgeInsetsMake(-labelHeight - space / 2.0, 0, 0, -labelWidth);
      labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith, -imageHeight - space / 2.0, 0);
    } break;
    case ButtonEdgeInsetsStyleLeft: {
      imageEdgeInsets = UIEdgeInsetsMake(0, -space / 2.0, 0, space / 2.0);
      labelEdgeInsets = UIEdgeInsetsMake(0, space / 2.0, 0, -space / 2.0);
    } break;
    case ButtonEdgeInsetsStyleBottom: {
      imageEdgeInsets = UIEdgeInsetsMake(0, 0, -labelHeight - space / 2.0, -labelWidth);
      labelEdgeInsets = UIEdgeInsetsMake(-imageHeight - space / 2.0, -imageWith, 0, 0);
    } break;
    case ButtonEdgeInsetsStyleRight: {
      imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth + space / 2.0, 0, -labelWidth - space / 2.0);
      labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith - space / 2.0, 0, imageWith + space / 2.0);
    } break;
    default:
      break;
  }
  self.titleEdgeInsets = labelEdgeInsets;
  self.imageEdgeInsets = imageEdgeInsets;
}

/**
 *  利用 UIButton 的 titleEdgeInsets 和 imageEdgeInsets 来实现文字和图片的自由排列
 *  注意：这个方法需要在设置图片和文字之后才可以调用，且 button 的大小要大于 图片大小+文字大小+spacing
 *
 *  @param spacing 图片和文字的间隔
 */
- (void)bf_setImagePosition:(ButtonEdgeInsetsStyle)style spacing:(CGFloat)spacing{
    CGFloat imageWith = self.imageView.image.size.width;
    CGFloat imageHeight = self.imageView.image.size.height;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    CGFloat labelWidth = [self.titleLabel.text sizeWithFont:self.titleLabel.font].width;
    CGFloat labelHeight = [self.titleLabel.text sizeWithFont:self.titleLabel.font].height;
#pragma clang diagnostic pop
    
    CGFloat imageOffsetX = (imageWith + labelWidth) / 2 - imageWith / 2;//image中心移动的x距离
    CGFloat imageOffsetY = imageHeight / 2 + spacing / 2;//image中心移动的y距离
    CGFloat labelOffsetX = (imageWith + labelWidth / 2) - (imageWith + labelWidth) / 2;//label中心移动的x距离
    CGFloat labelOffsetY = labelHeight / 2 + spacing / 2;//label中心移动的y距离
    
    switch (style) {
        case ButtonEdgeInsetsStyleLeft:
            self.imageEdgeInsets = UIEdgeInsetsMake(0, -spacing/2, 0, spacing/2);
            self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing/2, 0, -spacing/2);
            break;
            
        case ButtonEdgeInsetsStyleRight:
            self.imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth + spacing/2, 0, -(labelWidth + spacing/2));
            self.titleEdgeInsets = UIEdgeInsetsMake(0, -(imageHeight + spacing/2), 0, imageHeight + spacing/2);
            break;
            
        case ButtonEdgeInsetsStyleTop:
            self.imageEdgeInsets = UIEdgeInsetsMake(-imageOffsetY, imageOffsetX, imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(labelOffsetY, -labelOffsetX, -labelOffsetY, labelOffsetX);
            break;
            
        case ButtonEdgeInsetsStyleBottom:
            self.imageEdgeInsets = UIEdgeInsetsMake(imageOffsetY, imageOffsetX, -imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(-labelOffsetY, -labelOffsetX, labelOffsetY, labelOffsetX);
            break;
            
        default:
            break;
    }
}

#pragma mark - 设置button点击范围
- (void)bf_setEnlargeEdgeWithTop:(CGFloat)top Right:(CGFloat)right Bottom:(CGFloat)bottom Left:(CGFloat)left {
  objc_setAssociatedObject(self, &topNameKey, [NSNumber numberWithFloat:top], OBJC_ASSOCIATION_COPY_NONATOMIC);
  objc_setAssociatedObject(self, &rightNameKey, [NSNumber numberWithFloat:right], OBJC_ASSOCIATION_COPY_NONATOMIC);
  objc_setAssociatedObject(self, &bottomNameKey, [NSNumber numberWithFloat:bottom], OBJC_ASSOCIATION_COPY_NONATOMIC);
  objc_setAssociatedObject(self, &leftNameKey, [NSNumber numberWithFloat:left], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CGRect)bf_enlargedRect {

  NSNumber *topEdge = objc_getAssociatedObject(self, &topNameKey);
  NSNumber *rightEdge = objc_getAssociatedObject(self, &rightNameKey);
  NSNumber *bottomEdge = objc_getAssociatedObject(self, &bottomNameKey);
  NSNumber *leftEdge = objc_getAssociatedObject(self, &leftNameKey);
  if (topEdge && rightEdge && bottomEdge && leftEdge) {
    return CGRectMake(self.bounds.origin.x - leftEdge.floatValue, self.bounds.origin.y - topEdge.floatValue, self.bounds.size.width + leftEdge.floatValue + rightEdge.floatValue,
                      self.bounds.size.height + topEdge.floatValue + bottomEdge.floatValue);
  }
  else {
    return self.bounds;
  }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {

  CGRect rect = [self bf_enlargedRect];
  if (CGRectEqualToRect(rect, self.bounds)) {
    return [super hitTest:point withEvent:event];
  }
  return CGRectContainsPoint(rect, point) ? self : nil;
}


- (void)setImagePosition:(int)imagePosition{
    objc_setAssociatedObject(self, &kImagePosition, [NSNumber numberWithInt:imagePosition], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (int)imagePosition{
    return [objc_getAssociatedObject(self, &kImagePosition) intValue];
}

- (void)setSpacingBetweenImageAndTitle:(CGFloat)spacingBetweenImageAndTitle{
    objc_setAssociatedObject(self, &kSpacingBetweenImageAndTitle, [NSNumber numberWithFloat:spacingBetweenImageAndTitle], OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

- (CGFloat)spacingBetweenImageAndTitle{
    return [objc_getAssociatedObject(self, &kSpacingBetweenImageAndTitle) floatValue];
}

#pragma mark - Count Down
- (void)bf_changeWithCountDown:(NSInteger)seconds title:(NSString *)title backgroundColor:(UIColor *)backgroundColor finishedTitle:(NSString *)finishedTitle finishedBackgroundColor:(UIColor *)finishedBackgroundColor {
    [self bf_changeWithCountDown:seconds countDownHandler:^(UIButton *sender, NSInteger second, BOOL finished) {
        if (finished) {
            sender.backgroundColor = finishedBackgroundColor;
            [sender setTitle:finishedTitle forState:UIControlStateNormal];
            sender.enabled = YES;
        } else {
            sender.backgroundColor = backgroundColor;
            NSString *timeStr = [NSString stringWithFormat:@"%0.2ld", (long)second];
            [sender setTitle:[NSString stringWithFormat:@"%@(%@S)",title, timeStr] forState:UIControlStateDisabled];
            sender.enabled = NO;
        }
    }];
}

@end
