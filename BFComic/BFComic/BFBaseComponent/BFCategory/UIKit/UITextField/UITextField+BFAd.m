//
//  UITextField+BFAd.m
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "UITextField+BFAd.h"
#import <objc/runtime.h>

NSString * const GTTextFieldDidDeleteBackwardNotification = @"textfield_did_notification";

@implementation UITextField (BFAd)


+ (void)load {
    Method method1 = class_getInstanceMethod([self class], NSSelectorFromString(@"deleteBackward"));
    Method method2 = class_getInstanceMethod([self class], @selector(bf_deleteBackward));
    method_exchangeImplementations(method1, method2);
}

- (void)bf_deleteBackward {
    [self bf_deleteBackward];
    
    if ([self.delegate respondsToSelector:@selector(bf_textFieldDidDeleteBackward:)])
    {
        id <GTTextFieldDelegate> delegate  = (id<GTTextFieldDelegate>)self.delegate;
        [delegate bf_textFieldDidDeleteBackward:self];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:GTTextFieldDidDeleteBackwardNotification object:self];
}

- (CGFloat)bf_addDoneButtonCloseKeyBoard {
    
    CGFloat barHeight = 40;
    UIToolbar *toolBar = [[UIToolbar alloc] init];
    toolBar.bounds = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, barHeight);
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(bf_doneClick)];
    // 按添加顺序显示
    toolBar.items = @[flexible, doneBtn];
    // 添加工具条
    self.inputAccessoryView = toolBar;
    
    return barHeight;
}

- (void)bf_doneClick {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (BOOL)bf_textFieldCheckInputNumberWithRange:(NSRange)range CurrentString:(NSString *)string DecimalNumberCount:(NSInteger)numberCount {
    BOOL isHaveDian = YES;
    if ([self.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    if ([string length] > 0) {
        //当前输入的字符
        unichar single = [string characterAtIndex:0];
        if ((single >= '0' && single <= '9') || single == '.') {
            // 数据格式正确
            // 首字母不能为0和小数点
            if([self.text length] == 0){
                if(single == '.') {
                    // showTip 第一个数字不能为小数点
                    [self.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            // 小数点前面只能有一个0，不能连续输入多个0
            if ([self.text length] == 1) {
                unichar first = [self.text characterAtIndex:0];
                if (single == '0' && first == '0') {
                    // showTip 只能有一个0开头
                    [self.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            // 输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian) {
                    // text中还没有小数点
                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    // showTip 您已经输入过小数点了
                    [self.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                if (isHaveDian) {
                    // 存在小数点
                    // 判断小数点的位数
                    // 最多输入numberCount位小数
                    if (numberCount <= 0) {
                        numberCount = 0;
                    }
                    NSRange ran = [self.text rangeOfString:@"."];
                    if (range.location - ran.location <= numberCount) {
                        return YES;
                    }else {
                        
                        return NO;
                    }
                }else {
                    return YES;
                }
            }
        }else {
            // showTip 输入的数据格式不正确
            [self.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else {
        return YES;
    }
}

- (BOOL)bf_textFieldCheckInputNumberWithRange:(NSRange)range CurrentString:(NSString *)string {
    // 首字母不能为0
    if (range.length == 0 && range.location == 0 && [string isEqual: @"0"]) {
        return NO;
    }
    NSString *number = @"1234567890";
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:number] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    BOOL basic = [string isEqualToString:filtered];
    return basic;
}


- (BOOL)bf_textFieldlimitInputWithNumber:(NSInteger)number {
    
    BOOL flag = YES;
    
    NSInteger length = number;
    NSString *toBeString = self.text;
    NSString *lang = [self.textInputMode primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"] || [lang isEqualToString:@"zh-Hant"]) {
        // 简体中文输入，包括简体拼音，简体五笔，简体手写(zh-Hans)
        // 繁体中文输入，包括繁体拼音，繁体五笔，繁体手写(zh-Hant)
        UITextRange *selectedRange = [self markedTextRange];
        // 获取高亮部分（联想部分）
        UITextPosition *position = [self positionFromPosition:selectedRange.start offset:0];
        // 没有联想，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > length) {
                self.text = [toBeString substringToIndex:length];
                //                [self resignFirstResponder];
                flag = NO;
            }
        }
        // 有联想，则暂不对联想的文字进行统计
        else {
            
        }
    }
    // 中文输入法以外的直接对其统计限制即可，暂时不考虑其他语种情况
    else {
        if (toBeString.length > length) {
            self.text = [toBeString substringToIndex:length];
            //            [self resignFirstResponder];
            flag = NO;
        }
    }
    
    
    return flag;
}




@end
