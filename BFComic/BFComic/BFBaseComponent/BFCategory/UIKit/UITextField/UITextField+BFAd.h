//
//  UITextField+BFAd.h
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol GTTextFieldDelegate <UITextFieldDelegate>

@optional
- (void)bf_textFieldDidDeleteBackward:(UITextField *)textField;

@end


@interface UITextField (BFAd)

///在键盘上方添加完成栏
- (CGFloat)bf_addDoneButtonCloseKeyBoard;

/**
 *  只能输入 数字和小数点(最多只能x有几位小数，默认2位)
 *
 *  @param range             range
 *  @param string            当前输入的内容
 *  @param numberCount       当前输入的内容
 *
 *  @return 输入是否正确
 */
- (BOOL)bf_textFieldCheckInputNumberWithRange:(NSRange)range CurrentString:(NSString *)string DecimalNumberCount:(NSInteger)numberCount;

/**
 只能输入整数
 
 @param range 长度区间
 @param string 当前输入的内容
 @return 输入是否正确
 */
- (BOOL)bf_textFieldCheckInputNumberWithRange:(NSRange)range CurrentString:(NSString *)string;

/**
 限制输入的字符串长度

 @param number 限制输入的字数
 @return 返回YES：可以继续输入
 */
- (BOOL)bf_textFieldlimitInputWithNumber:(NSInteger)number;


@property (weak, nonatomic) id <GTTextFieldDelegate> delegate;

@end


/**
 *  监听删除按钮
 *  object:UITextField
 */
extern NSString * const GTTextFieldDidDeleteBackwardNotification;

NS_ASSUME_NONNULL_END
