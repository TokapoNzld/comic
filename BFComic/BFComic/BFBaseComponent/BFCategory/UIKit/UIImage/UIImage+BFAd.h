//
//  UIImage+BFAd.h
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface UIImage (BFAd)

/**
 附加字段 拓展名
 */
@property (nonatomic, copy) NSString *bf_externName;

/**
 图片二进制数据
 */
@property (nonatomic, copy) NSData *bf_imgData;


+ (UIImage *)bf_imagePathName:(NSString *)name Class:(Class)classs bundleName:(NSString *)bundle;


/**
 读取本地图片
 
 @param imageName 图片名
 @param targetClass 当前类【self class】
 @param display 资源文件所在的包
 */
+ (UIImage *)bf_imageNamed:(NSString *)imageName targetClass:(Class)targetClass display:(NSString *)display;


/**
 读取本地图片前提该工程有AppDelegate文件
 
 @param imageName 图片名
 @param display 资源文件所在的包
 */
+ (UIImage *)bf_imageNamed:(NSString *)imageName display:(NSString *)display;
+ (UIImage *)bf_imagePathWithName:(NSString *)imageName display:(NSString *)display;
/**
 加载资源文件前提是图片资源与targetClass是在同一个包
 */
+ (UIImage *)bf_imageNamed:(NSString *)imageName targetClass:(Class)targetClass;

/**
 *  根据指定的图片宽度等比例缩放图片, 文件大小, 返回ImageData
 *
 *  @param defaultWidth     图片宽度
 *  @param maxFileSize      图片最大大小
 *
 */
- (NSData *)bf_adjustImageDataWithDfaultWidth:(CGFloat)defaultWidth maxFileSize:(NSInteger)maxFileSize;

/**  根据指定的图片Size, 文件大小, 压缩图片
 *
 *   @param defaultWidth    图片宽度
 *   @param fileSize        图片最大大小
 *   @param complected      完成回调
 */
- (void)bf_adjustImageDataWithithDfaultWidth:(CGFloat)defaultWidth maxFileSize:(NSInteger)fileSize complected:(void(^)(NSString *encodedImageStr, NSData *imageData))complected;

/**
 *  根据指定的图片Size, 文件大小, 返回ImageData
 *
 *  @param newSize     图片Size
 *  @param maxFileSize 图片最大大小
 *
 */
- (NSData *)bf_adjustImageDataWithNewSize:(CGSize)newSize maxFileSize:(NSInteger)maxFileSize;

/**
 *  根据指定的图片Size, 文件大小, 压缩图片
 *
 *  @param newSize    图片Size
 *  @param fileSize   图片最大大小
 *  @param complected 完成回调
 */
- (void)bf_adjustImageDataWithNewSize:(CGSize)newSize maxFileSize:(NSInteger)fileSize complected:(void(^)(NSString *encodedImageStr, NSData *imageData))complected;


- (UIImage *)bf_adjustShareImageWithithDfaultWidth:(CGFloat)defaultWidth maxFileSize:(NSInteger)fileSize;

/**
 *  Return a full screen image with fileName in content files.
 *
 *  @param fileName image with fileName.
 */
+ (UIImage *)bf_fullScreenImageWithContentName:(NSString *)fileName;

/**
 Return an image with fileName in content files.
 
 @param fileName  image with fileName.
 */
+ (UIImage *)bf_imageWithFileName:(NSString *)fileName;

/**
 Create and return a 1x1 point size image with the given color.
 
 @param color  The color.
 */
+ (UIImage *)bf_imageWithColor:(UIColor *)color;

/**
 Create and return a pure color image with the given color and size.
 
 @param color  The color.
 @param size   New image's type.
 */
+ (UIImage *)bf_imageWithColor:(UIColor *)color size:(CGSize)size;

/**
 *  修改图片size
 *
 *  @param image      原图片
 *  @param targetSize 要修改的size
 *
 *  @return 修改后的图片
 */
+ (UIImage *)bf_image:(UIImage*)image byScalingToSize:(CGSize)targetSize;

/**
 设置图片圆角
 
 @param radius 圆角
 @param size 图片大小
 @return 修改后的图片
 */
- (UIImage*)bf_imageAddCornerWithRadius:(CGFloat)radius andSize:(CGSize)size;

/**
 根据指定高度比例缩放图片
 
 @param sourceImage 原图
 @param defineHeight 高度
 @return 修改后的图片
 */
- (UIImage *)bf_imageCompressForHeight:(UIImage *)sourceImage targetHeight:(CGFloat)defineHeight;
/**
 根据指定宽度比例缩放图片
 
 @param sourceImage 原图
 @param defineWidth 宽度
 @return 修改后的图片
 */
- (UIImage *)bf_imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth;

/**
 *  从图片中按指定的位置大小截取图片的一部分
 *
 *  @param image UIImage image 原始的图片
 *  @param rect  CGRect rect 要截取的区域
 *
 *  @return UIImage
 */
+ (UIImage *)bf_imageFromImage:(UIImage *)image inRect:(CGRect)rect;

//====================================================================================================================================
/**
 *  设置亮色遮罩
 */
- (UIImage *)bf_applyLightEffect;
/**
 *  设置特亮遮罩
 */
- (UIImage *)bf_applyExtraLightEffect;
/**
 *  设置暗色遮罩
 */
- (UIImage *)bf_applyDarkEffect;
/**
 *  设置颜色遮罩
 *
 *  @param tintColor 遮罩颜色
 */
- (UIImage *)bf_applyTintEffectWithColor:(UIColor *)tintColor;
/**
 *  设置自定义遮罩
 *
 *  @param blurRadius            高斯模糊数值
 *  @param tintColor             遮罩颜色
 *  @param saturationDeltaFactor 色相饱和度
 *  @param maskImage             遮罩照片
 */
- (UIImage *)bf_applyBlurWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;

// 截屏
+ (instancetype)bf_snapshotCurrentScreen;

// 图片模糊效果
- (UIImage *)bf_blur;

// 圆形图片
+ (UIImage *)bf_GetRoundImagewithImage:(UIImage *)image;

// 在图片上加居中的文字
- (UIImage *)bf_imageWithTitle:(NSString *)title fontSize:(CGFloat)fontSize titleColor:(UIColor *)titleColor;

/**
 取图片某一像素点的颜色
 
 @param point 图片上的某一点
 @return 图片上这一点的颜色
 */
- (UIColor *)bf_colorAtPixel:(CGPoint)point;

/**
 生成一个纯色的图片
 
 @param color 图片颜色
 @return 返回的纯色图片
 */
- (UIImage *)bf_imageWithColor:(UIColor *)color;

/** 获得灰度图 */
- (UIImage *)bf_convertToGrayImage;


+ (UIImage *)bf_animatedImageWithAnimatedGIFData:(NSData *)theData;
+ (UIImage *)bf_animatedImageWithAnimatedGIFURL:(NSURL *)theURL;


/** 合并两个图片为一个图片 */
+ (UIImage*)bf_mergeImage:(UIImage*)firstImage withImage:(UIImage*)secondImage;

/** 压缩图片 最大字节大小为maxLength */
- (NSData *)bf_compressWithMaxLength:(NSInteger)maxLength;

/** 纠正图片的方向 */
- (UIImage *)bf_fixOrientation;

/** 按给定的方向旋转图片 */
- (UIImage*)bf_rotate:(UIImageOrientation)orient;

/** 垂直翻转 */
- (UIImage *)bf_flipVertical;

/** 水平翻转 */
- (UIImage *)bf_flipHorizontal;

/** 将图片旋转degrees角度 */
- (UIImage *)bf_imageRotatedByDegrees:(CGFloat)degrees;

/** 将图片旋转radians弧度 */
- (UIImage *)bf_imageRotatedByRadians:(CGFloat)radians;


/** 截取当前image对象rect区域内的图像 */
- (UIImage *)bf_subImageWithRect:(CGRect)rect;

/** 压缩图片至指定尺寸 */
- (UIImage *)bf_rescaleImageToSize:(CGSize)size;

/** 压缩图片至指定像素 */
- (UIImage *)bf_rescaleImageToPX:(CGFloat )toPX;

/** 在指定的size里面生成一个平铺的图片 */
- (UIImage *)bf_getTiledImageWithSize:(CGSize)size;

/** UIView转化为UIImage */
+ (UIImage *)bf_imageFromView:(UIView *)view;

- (UIImage *)bf_imageCroppedToRect:(CGRect)rect;
- (UIImage *)bf_imageScaledToSize:(CGSize)size;
- (UIImage *)bf_imageScaledToFitSize:(CGSize)size;
- (UIImage *)bf_imageScaledToFillSize:(CGSize)size;
- (UIImage *)bf_imageCroppedAndScaledToSize:(CGSize)size
                             contentMode:(UIViewContentMode)contentMode
                                padToFit:(BOOL)padToFit;

- (UIImage *)bf_reflectedImageWithScale:(CGFloat)scale;
- (UIImage *)bf_imageWithReflectionWithScale:(CGFloat)scale gap:(CGFloat)gap alpha:(CGFloat)alpha;

//带有阴影效果的图片
- (UIImage *)bf_imageWithShadowColor:(UIColor *)color offset:(CGSize)offset blur:(CGFloat)blur;
- (UIImage *)bf_imageWithCornerRadius:(CGFloat)radius;
- (UIImage *)bf_imageWithAlpha:(CGFloat)alpha;
- (UIImage *)bf_imageWithMask:(UIImage *)maskImage;

- (UIImage *)bf_maskImageFromImageAlpha;

- (UIImage *)bf_imageByResizeToSize:(CGSize)size;

+ (UIImage *)bf_getThumbnailImage:(NSString *)videoPath;

+ (UIImage *)bf_acquireNameImageWithNameString:(NSString *)string imageSize:(CGSize)size;


+(UIImage *)bf_boxblurImage:(UIImage *)image;


+ (UIImage *)bf_createNikeNameImageName:(NSString *)name font:(UIFont *)font imageSize:(CGSize)size;
@end

NS_ASSUME_NONNULL_END
