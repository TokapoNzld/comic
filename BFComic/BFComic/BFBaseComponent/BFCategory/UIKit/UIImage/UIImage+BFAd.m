//
//  UIImage+BFAd.m
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "UIImage+BFAd.h"
#import <AVFoundation/AVFoundation.h>
#import <Accelerate/Accelerate.h>
#import <ImageIO/ImageIO.h>
#import <SDWebImage/UIImage+GIF.h>
#import <float.h>
#import <objc/runtime.h>
#if __has_feature(objc_arc)
#define toCF (__bridge CFTypeRef)
#define fromCF (__bridge id)
#else
#define toCF (CFTypeRef)
#define fromCF (id)
#endif

//由角度转换弧度
#define kDegreesToRadian(x) (M_PI * (x) / 180.0)
//由弧度转换角度
#define kRadianToDegrees(radian) (radian * 180.0) / (M_PI)

static const void *bf_externNamekey = &bf_externNamekey;
static const void *bf_imgDatakey = &bf_imgDatakey;

typedef NS_ENUM(NSInteger, ImageType) {
  ImageTypeJPEG,  // regular table view
  ImageTypePNG    // preferences style table view
};

@implementation UIImage (BFAd)

- (NSString *)bf_externName {
  return objc_getAssociatedObject(self, bf_externNamekey);
}

- (void)setBf_externName:(NSString *)bf_externName {
  objc_setAssociatedObject(self, bf_externNamekey, bf_externName, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSData *)bf_imgData {
  return objc_getAssociatedObject(self, bf_imgDatakey);
}

- (void)setBf_imgData:(NSData *)bf_imgData {
  objc_setAssociatedObject(self, bf_imgDatakey, bf_imgData, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+ (UIImage *)bf_imagePathName:(NSString *)name Class:(Class)classs bundleName:(NSString *)bundle {

  UIImage *image = [UIImage imageNamed:name];
  return image;
}

+ (UIImage *)bf_imageNamed:(NSString *)imageName targetClass:(Class)targetClass {
  NSBundle *currentBundle = [NSBundle bundleForClass:targetClass];
  NSString *curBundleName = currentBundle.infoDictionary[ @"CFBundleName" ];
  return [self bf_imageNamed:imageName targetClass:targetClass display:curBundleName];
}

+ (UIImage *)bf_imageNamed:(NSString *)imageName targetClass:(Class)targetClass display:(NSString *)display {
  NSInteger scale = [[UIScreen mainScreen] scale];
  NSBundle *currentBundle = [NSBundle bundleForClass:targetClass];
  NSString *name = [NSString stringWithFormat:@"%@@%zdx", imageName, scale];
  NSString *dir = [NSString stringWithFormat:@"%@.bundle", display];
  NSString *path = [currentBundle pathForResource:name ofType:@"png" inDirectory:dir];
  return path ? [UIImage imageWithContentsOfFile:path] : nil;
}

+ (UIImage *)bf_imageNamed:(NSString *)imageName display:(NSString *)display {
  return [self bf_imageNamed:imageName targetClass:NSClassFromString(@"AppDelegate") display:display];
}

+ (UIImage *)bf_imagePathWithName:(NSString *)imageName display:(NSString *)display {

  if ([display isEqualToString:@"SLMall"]) {
    // 商城的图片
    return [UIImage imageNamed:imageName];
  }

  NSInteger scale = [[UIScreen mainScreen] scale];
  NSBundle *currentBundle = [NSBundle bundleForClass:NSClassFromString(@"AppDelegate")];
  NSString *name = [NSString stringWithFormat:@"%@@%zdx", imageName, scale];
  NSString *dir = [NSString stringWithFormat:@"%@.bundle", display];
  NSString *path = [currentBundle pathForResource:name ofType:@"png" inDirectory:dir];
  return path ? [UIImage imageWithContentsOfFile:path] : nil;
}

- (NSData *)bf_adjustImageDataWithNewSize:(CGSize)newSize maxFileSize:(NSInteger)maxFileSize {
  // ImageSize
  UIGraphicsBeginImageContext(newSize);
  [self drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  // FileSize
  CGFloat compression = 1.0f;
  CGFloat minCompression = 0.1f;
  NSData *imageData = UIImageJPEGRepresentation(newImage, compression);
#warning 可算法优化(二分树+多线程)
  while ([imageData length] > maxFileSize && compression > minCompression) {
    compression -= 0.1;
    imageData = UIImageJPEGRepresentation(newImage, compression);
  }

  return imageData;
}

- (void)bf_adjustImageDataWithNewSize:(CGSize)newSize maxFileSize:(NSInteger)fileSize complected:(void (^)(NSString *encodedImageStr, NSData *imageData))complected {
  dispatch_async(dispatch_get_global_queue(0, DISPATCH_QUEUE_PRIORITY_DEFAULT), ^{
  // 压缩图片
#warning 调节ImageSize
    NSData *imageData = [self bf_adjustImageDataWithNewSize:newSize maxFileSize:fileSize];

    dispatch_async(dispatch_get_global_queue(0, DISPATCH_QUEUE_PRIORITY_DEFAULT), ^{
      // Data -> Base64
      NSString *encodedImageStr = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];

      dispatch_async(dispatch_get_main_queue(), ^{
        if (complected) {
          complected(encodedImageStr, imageData);
        }
      });
    });
  });
}

- (NSData *)bf_adjustImageDataWithDfaultWidth:(CGFloat)defaultWidth maxFileSize:(NSInteger)maxFileSize {
  // Covert ImageSize
  CGFloat originalWidth = self.size.width;
  CGFloat originalHeight = self.size.height;
  CGFloat targetWidth = defaultWidth;
  CGFloat targetHeight = (targetWidth / originalWidth) * originalHeight;
  // ImageSize
  UIGraphicsBeginImageContext(CGSizeMake(targetWidth, targetHeight));
  [self drawInRect:CGRectMake(0, 0, targetWidth, targetHeight)];
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  // FileSize
  CGFloat compression = 1.0f;
  CGFloat minCompression = 0.1f;
  NSData *imageData = UIImageJPEGRepresentation(newImage, compression);
#warning 可算法优化(二分树+多线程)
  while ([imageData length] > maxFileSize && compression > minCompression) {
    compression -= 0.1;
    imageData = UIImageJPEGRepresentation(newImage, compression);
  }

  return imageData;
}

- (void)bf_adjustImageDataWithithDfaultWidth:(CGFloat)defaultWidth maxFileSize:(NSInteger)fileSize complected:(void (^)(NSString *encodedImageStr, NSData *imageData))complected {
  dispatch_async(dispatch_get_global_queue(0, DISPATCH_QUEUE_PRIORITY_DEFAULT), ^{
    // 压缩图片
    NSData *imageData = [self bf_adjustImageDataWithDfaultWidth:defaultWidth maxFileSize:fileSize];

    dispatch_async(dispatch_get_global_queue(0, DISPATCH_QUEUE_PRIORITY_DEFAULT), ^{
      // Data -> Base64
      NSString *encodedImageStr = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];

      dispatch_async(dispatch_get_main_queue(), ^{
        if (complected) {
          complected(encodedImageStr, imageData);
        }
      });
    });
  });
}

- (UIImage *)bf_adjustShareImageWithithDfaultWidth:(CGFloat)defaultWidth maxFileSize:(NSInteger)fileSize {
  // Covert ImageSize
  CGFloat originalWidth = self.size.width;
  CGFloat originalHeight = self.size.height;
  CGFloat targetWidth = defaultWidth;
  CGFloat targetHeight = (targetWidth / originalWidth) * originalHeight;
  // ImageSize
  UIGraphicsBeginImageContext(CGSizeMake(targetWidth, targetHeight));
  [self drawInRect:CGRectMake(0, 0, targetWidth, targetHeight)];
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  NSData *imagePNGData = UIImagePNGRepresentation(newImage);
  if ([imagePNGData length] <= fileSize) {
    return newImage;
  }

  // FileSize
  CGFloat compression = 1.0f;
  CGFloat minCompression = 0.1f;
  NSData *imageJPEGData = UIImageJPEGRepresentation(newImage, compression);

  while ([imageJPEGData length] > fileSize && compression > minCompression && [imagePNGData length] > fileSize) {
    compression -= 0.1;
    imageJPEGData = UIImageJPEGRepresentation(newImage, compression);
    newImage = [UIImage imageWithData:imageJPEGData];
    imagePNGData = UIImagePNGRepresentation(newImage);
  }

  return [UIImage imageWithData:imagePNGData];
}

+ (UIImage *)bf_imageWithFileName:(NSString *)fileName {
  NSString *type = @"png";
  NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:type inDirectory:nil];

  return [UIImage imageWithContentsOfFile:path];
}

+ (UIImage *)bf_imageWithColor:(UIColor *)color {
  return [self bf_imageWithColor:color size:CGSizeMake(1, 1)];
}

+ (UIImage *)bf_fullScreenImageWithContentName:(NSString *)fileName {
  CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
  if (screenHeight > 737)  // x
  {
    fileName = [fileName stringByAppendingString:@"-812h"];
  }
  else if (screenHeight > 668.f)  // Plus
  {
    fileName = [fileName stringByAppendingString:@"-736h"];
  }
  else if (screenHeight > 569.f)  // 6
  {
    fileName = [fileName stringByAppendingString:@"-667h"];
  }
  else if (screenHeight > 481.f)  // 5
  {
    fileName = [fileName stringByAppendingString:@"-568h"];
  }
  else {
    fileName = [fileName stringByAppendingString:@"-768h"];
  }

  NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"jpg" inDirectory:nil];
  UIImage *image = [UIImage imageWithContentsOfFile:path];
  if (!image) {
    image = [[UIImage alloc] init];
  }
  return image;
}

+ (UIImage *)bf_imageWithColor:(UIColor *)color size:(CGSize)size {
  if (!color || size.width <= 0 || size.height <= 0)
    return nil;
  CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
  UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetFillColorWithColor(context, color.CGColor);
  CGContextFillRect(context, rect);
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

/**
 *  修改图片size
 *
 *  @param image      原图片
 *  @param targetSize 要修改的size
 *
 *  @return 修改后的图片
 */
+ (UIImage *)bf_image:(UIImage *)image byScalingToSize:(CGSize)targetSize {
  UIImage *sourceImage = image;
  UIImage *newImage = nil;

  UIGraphicsBeginImageContext(targetSize);

  CGRect thumbnailRect = CGRectZero;
  thumbnailRect.origin = CGPointZero;
  thumbnailRect.size.width = targetSize.width;
  thumbnailRect.size.height = targetSize.height;

  [sourceImage drawInRect:thumbnailRect];

  newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return newImage;
}

//指定高度度按比例缩放
- (UIImage *)bf_imageCompressForHeight:(UIImage *)sourceImage targetHeight:(CGFloat)defineHeight {

  UIImage *newImage = nil;
  CGSize imageSize = sourceImage.size;
  CGFloat width = imageSize.width;
  CGFloat height = imageSize.height;

  CGFloat targetHeight = defineHeight;
  CGFloat targetWidth = width / (height / defineHeight);

  CGSize size = CGSizeMake(targetWidth, targetHeight);
  CGFloat scaleFactor = 0.0;
  CGFloat scaledWidth = targetWidth;
  CGFloat scaledHeight = targetHeight;
  CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);

  if (CGSizeEqualToSize(imageSize, size) == NO) {
    CGFloat widthFactor = targetWidth / width;
    CGFloat heightFactor = targetHeight / height;

    if (widthFactor > heightFactor) {
      scaleFactor = widthFactor;
    }
    else {
      scaleFactor = heightFactor;
    }
    scaledWidth = width * scaleFactor;
    scaledHeight = height * scaleFactor;

    if (widthFactor > heightFactor) {
      thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
    }
    else if (widthFactor < heightFactor) {
      thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
    }
  }

  UIGraphicsBeginImageContext(size);

  CGRect thumbnailRect = CGRectZero;
  thumbnailRect.origin = thumbnailPoint;
  thumbnailRect.size.width = scaledWidth;
  thumbnailRect.size.height = scaledHeight;

  [sourceImage drawInRect:thumbnailRect];

  newImage = UIGraphicsGetImageFromCurrentImageContext();

  if (newImage == nil) {

    NSLog(@"scale image fail");
  }
  UIGraphicsEndImageContext();
  return newImage;
}
//指定宽度按比例缩放
- (UIImage *)bf_imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth {

  UIImage *newImage = nil;
  CGSize imageSize = sourceImage.size;
  CGFloat width = imageSize.width;
  CGFloat height = imageSize.height;
  CGFloat targetWidth = defineWidth;
  CGFloat targetHeight = height / (width / targetWidth);
  CGSize size = CGSizeMake(targetWidth, targetHeight);
  CGFloat scaleFactor = 0.0;
  CGFloat scaledWidth = targetWidth;
  CGFloat scaledHeight = targetHeight;
  CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);

  if (CGSizeEqualToSize(imageSize, size) == NO) {

    CGFloat widthFactor = targetWidth / width;
    CGFloat heightFactor = targetHeight / height;

    if (widthFactor > heightFactor) {
      scaleFactor = widthFactor;
    }
    else {
      scaleFactor = heightFactor;
    }
    scaledWidth = width * scaleFactor;
    scaledHeight = height * scaleFactor;

    if (widthFactor > heightFactor) {

      thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
    }
    else if (widthFactor < heightFactor) {

      thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
    }
  }

  UIGraphicsBeginImageContext(size);

  CGRect thumbnailRect = CGRectZero;
  thumbnailRect.origin = thumbnailPoint;
  thumbnailRect.size.width = scaledWidth;
  thumbnailRect.size.height = scaledHeight;

  [sourceImage drawInRect:thumbnailRect];

  newImage = UIGraphicsGetImageFromCurrentImageContext();

  if (newImage == nil) {

    NSLog(@"scale image fail");
  }
  UIGraphicsEndImageContext();
  return newImage;
}
+ (UIImage *)bf_imageFromImage:(UIImage *)image inRect:(CGRect)rect {

  //把像 素rect 转化为 点rect（如无转化则按原图像素取部分图片）
  CGFloat scale = [UIScreen mainScreen].scale;
  CGFloat x = rect.origin.x * scale, y = rect.origin.y * scale, w = rect.size.width * scale, h = rect.size.height * scale;
  CGRect dianRect = CGRectMake(x, y, w, h);

  //截取部分图片并生成新图片
  CGImageRef sourceImageRef = [image CGImage];
  CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, dianRect);
  UIImage *newImage = [UIImage imageWithCGImage:newImageRef scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
  return newImage;
}

- (UIImage *)bf_applyLightEffect {
  UIColor *tintColor = [UIColor colorWithWhite:1.0 alpha:0.3];
  return [self bf_applyBlurWithRadius:30 tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil];
}

- (UIImage *)bf_applyExtraLightEffect {
  UIColor *tintColor = [UIColor colorWithWhite:0.97 alpha:0.82];
  return [self bf_applyBlurWithRadius:20 tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil];
}

- (UIImage *)bf_applyDarkEffect {
  UIColor *tintColor = [UIColor colorWithWhite:0.11 alpha:0.73];
  return [self bf_applyBlurWithRadius:20 tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil];
}

- (UIImage *)bf_applyTintEffectWithColor:(UIColor *)tintColor {
  const CGFloat EffectColorAlpha = 0.6;
  UIColor *effectColor = tintColor;
  size_t componentCount = CGColorGetNumberOfComponents(tintColor.CGColor);
  if (componentCount == 2) {
    CGFloat b;
    if ([tintColor getWhite:&b alpha:NULL]) {
      effectColor = [UIColor colorWithWhite:b alpha:EffectColorAlpha];
    }
  }
  else {
    CGFloat r, g, b;
    if ([tintColor getRed:&r green:&g blue:&b alpha:NULL]) {
      effectColor = [UIColor colorWithRed:r green:g blue:b alpha:EffectColorAlpha];
    }
  }
  return [self bf_applyBlurWithRadius:10 tintColor:effectColor saturationDeltaFactor:-1.0 maskImage:nil];
}

- (UIImage *)bf_applyBlurWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(nonnull UIImage *)maskImage {
  // Check pre-conditions.
  if (self.size.width < 1 || self.size.height < 1) {
    NSLog(@"*** error: invalid size: (%.2f x %.2f). Both dimensions must be >= 1: %@", self.size.width, self.size.height, self);
    return nil;
  }
  if (!self.CGImage) {
    NSLog(@"*** error: image must be backed by a CGImage: %@", self);
    return nil;
  }
  if (maskImage && !maskImage.CGImage) {
    NSLog(@"*** error: maskImage must be backed by a CGImage: %@", maskImage);
    return nil;
  }

  CGRect imageRect = { CGPointZero, self.size };
  UIImage *effectImage = self;

  BOOL hasBlur = blurRadius > __FLT_EPSILON__;
  BOOL hasSaturationChange = fabs(saturationDeltaFactor - 1.) > __FLT_EPSILON__;
  if (hasBlur || hasSaturationChange) {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef effectInContext = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(effectInContext, 1.0, -1.0);
    CGContextTranslateCTM(effectInContext, 0, -self.size.height);
    CGContextDrawImage(effectInContext, imageRect, self.CGImage);

    vImage_Buffer effectInBuffer;
    effectInBuffer.data = CGBitmapContextGetData(effectInContext);
    effectInBuffer.width = CGBitmapContextGetWidth(effectInContext);
    effectInBuffer.height = CGBitmapContextGetHeight(effectInContext);
    effectInBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectInContext);

    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef effectOutContext = UIGraphicsGetCurrentContext();
    vImage_Buffer effectOutBuffer;
    effectOutBuffer.data = CGBitmapContextGetData(effectOutContext);
    effectOutBuffer.width = CGBitmapContextGetWidth(effectOutContext);
    effectOutBuffer.height = CGBitmapContextGetHeight(effectOutContext);
    effectOutBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectOutContext);

    if (hasBlur) {
      // A description of how to compute the box kernel width from the Gaussian
      // radius (aka standard deviation) appears in the SVG spec:
      // http://www.w3.org/TR/SVG/filters.html#feGaussianBlurElement
      //
      // For larger values of 's' (s >= 2.0), an approximation can be used: Three
      // successive box-blurs build a piece-wise quadratic convolution kernel, which
      // approximates the Gaussian kernel to within roughly 3%.
      //
      // let d = floor(s * 3*sqrt(2*pi)/4 + 0.5)
      //
      // ... if d is odd, use three box-blurs of size 'd', centered on the output pixel.
      //
      CGFloat inputRadius = blurRadius * [[UIScreen mainScreen] scale];
      NSUInteger radius = floor(inputRadius * 3. * sqrt(2 * M_PI) / 4 + 0.5);
      if (radius % 2 != 1) {
        radius += 1;  // force radius to be odd so that the three box-blur methodology works.
      }
      vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, (int)radius, (int)radius, 0, kvImageEdgeExtend);
      vImageBoxConvolve_ARGB8888(&effectOutBuffer, &effectInBuffer, NULL, 0, 0, (int)radius, (int)radius, 0, kvImageEdgeExtend);
      vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, (int)radius, (int)radius, 0, kvImageEdgeExtend);
    }
    BOOL effectImageBuffersAreSwapped = NO;
    if (hasSaturationChange) {
      CGFloat s = saturationDeltaFactor;
      CGFloat floatingPointSaturationMatrix[] = {
        0.0722 + 0.9278 * s,
        0.0722 - 0.0722 * s,
        0.0722 - 0.0722 * s,
        0,
        0.7152 - 0.7152 * s,
        0.7152 + 0.2848 * s,
        0.7152 - 0.7152 * s,
        0,
        0.2126 - 0.2126 * s,
        0.2126 - 0.2126 * s,
        0.2126 + 0.7873 * s,
        0,
        0,
        0,
        0,
        1,
      };
      const int32_t divisor = 256;
      NSUInteger matrixSize = sizeof(floatingPointSaturationMatrix) / sizeof(floatingPointSaturationMatrix[ 0 ]);
      int16_t saturationMatrix[ matrixSize ];
      for (NSUInteger i = 0; i < matrixSize; ++i) {
        saturationMatrix[ i ] = (int16_t)roundf(floatingPointSaturationMatrix[ i ] * divisor);
      }
      if (hasBlur) {
        vImageMatrixMultiply_ARGB8888(&effectOutBuffer, &effectInBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
        effectImageBuffersAreSwapped = YES;
      }
      else {
        vImageMatrixMultiply_ARGB8888(&effectInBuffer, &effectOutBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
      }
    }
    if (!effectImageBuffersAreSwapped)
      effectImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    if (effectImageBuffersAreSwapped)
      effectImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
  }

  // Set up output context.
  UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
  CGContextRef outputContext = UIGraphicsGetCurrentContext();
  CGContextScaleCTM(outputContext, 1.0, -1.0);
  CGContextTranslateCTM(outputContext, 0, -self.size.height);

  // Draw base image.
  CGContextDrawImage(outputContext, imageRect, self.CGImage);

  // Draw effect image.
  if (hasBlur) {
    CGContextSaveGState(outputContext);
    if (maskImage) {
      CGContextClipToMask(outputContext, imageRect, maskImage.CGImage);
    }
    CGContextDrawImage(outputContext, imageRect, effectImage.CGImage);
    CGContextRestoreGState(outputContext);
  }

  // Add in color tint.
  if (tintColor) {
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, tintColor.CGColor);
    CGContextFillRect(outputContext, imageRect);
    CGContextRestoreGState(outputContext);
  }

  // Output image is ready.
  UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return outputImage;
}

// 高效添加圆角图片
- (UIImage *)bf_imageAddCornerWithRadius:(CGFloat)radius andSize:(CGSize)size {
  CGRect rect = CGRectMake(0, 0, size.width, size.height);

  UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
  CGContextRef ctx = UIGraphicsGetCurrentContext();
  UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(radius, radius)];
  CGContextAddPath(ctx, path.CGPath);
  CGContextClip(ctx);
  [self drawInRect:rect];
  CGContextDrawPath(ctx, kCGPathFillStroke);
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return newImage;
}

//截屏
+ (instancetype)bf_snapshotCurrentScreen {
  UIWindow *window = [UIApplication sharedApplication].keyWindow;
  UIGraphicsBeginImageContextWithOptions(window.bounds.size, NO, 0);
  [window drawViewHierarchyInRect:window.frame afterScreenUpdates:YES];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

+ (UIImage *)bf_GetRoundImagewithImage:(UIImage *)image {
  CGFloat width = image.size.width;
  CGFloat height = image.size.height;
  CGFloat redius = ((width <= height) ? width : height) / 2;
  CGRect rect = CGRectMake(width / 2 - redius, height / 2 - redius, redius * 2, redius * 2);

  CGImageRef sourceImageRef = [image CGImage];
  CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, rect);
  UIImage *newImage = [UIImage imageWithCGImage:newImageRef];

  UIGraphicsBeginImageContextWithOptions(CGSizeMake(newImage.size.width, newImage.size.height), NO, 0);
  UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(newImage.size.width / 2, newImage.size.height / 2) radius:redius startAngle:0 endAngle:M_PI * 2 clockwise:0];
  [path addClip];
  [newImage drawAtPoint:CGPointZero];
  UIImage *imageCut = UIGraphicsGetImageFromCurrentImageContext();

  return imageCut;
}

//图片模糊效果
- (UIImage *)bf_blur {
  return [self bf_imgWithLightAlpha:0.1 radius:3 colorSaturationFactor:1];
}

- (UIImage *)bf_imgWithLightAlpha:(CGFloat)alpha radius:(CGFloat)radius colorSaturationFactor:(CGFloat)colorSaturationFactor {
  UIColor *tintColor = [UIColor colorWithWhite:1.0 alpha:alpha];
  return [self bf_imgBluredWithRadius:radius tintColor:tintColor saturationDeltaFactor:colorSaturationFactor maskImage:nil];
}

- (UIImage *)bf_imgBluredWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage {

  CGRect imageRect = { CGPointZero, self.size };
  UIImage *effectImage = self;

  BOOL hasBlur = blurRadius > __FLT_EPSILON__;
  BOOL hasSaturationChange = fabs(saturationDeltaFactor - 1.) > __FLT_EPSILON__;
  if (hasBlur || hasSaturationChange) {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef effectInContext = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(effectInContext, 1.0, -1.0);
    CGContextTranslateCTM(effectInContext, 0, -self.size.height);
    CGContextDrawImage(effectInContext, imageRect, self.CGImage);

    vImage_Buffer effectInBuffer;
    effectInBuffer.data = CGBitmapContextGetData(effectInContext);
    effectInBuffer.width = CGBitmapContextGetWidth(effectInContext);
    effectInBuffer.height = CGBitmapContextGetHeight(effectInContext);
    effectInBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectInContext);

    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef effectOutContext = UIGraphicsGetCurrentContext();
    vImage_Buffer effectOutBuffer;
    effectOutBuffer.data = CGBitmapContextGetData(effectOutContext);
    effectOutBuffer.width = CGBitmapContextGetWidth(effectOutContext);
    effectOutBuffer.height = CGBitmapContextGetHeight(effectOutContext);
    effectOutBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectOutContext);

    if (hasBlur) {
      CGFloat inputRadius = blurRadius * [[UIScreen mainScreen] scale];
      int radius = floor(inputRadius * 3. * sqrt(2 * M_PI) / 4 + 0.5);
      if (radius % 2 != 1) {
        radius += 1;  // force radius to be odd so that the three box-blur methodology works.
      }
      vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
      vImageBoxConvolve_ARGB8888(&effectOutBuffer, &effectInBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
      vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
    }
    BOOL effectImageBuffersAreSwapped = NO;
    if (hasSaturationChange) {
      CGFloat s = saturationDeltaFactor;
      CGFloat floatingPointSaturationMatrix[] = {
        0.0722 + 0.9278 * s,
        0.0722 - 0.0722 * s,
        0.0722 - 0.0722 * s,
        0,
        0.7152 - 0.7152 * s,
        0.7152 + 0.2848 * s,
        0.7152 - 0.7152 * s,
        0,
        0.2126 - 0.2126 * s,
        0.2126 - 0.2126 * s,
        0.2126 + 0.7873 * s,
        0,
        0,
        0,
        0,
        1,
      };
      const int32_t divisor = 256;
      NSUInteger matrixSize = sizeof(floatingPointSaturationMatrix) / sizeof(floatingPointSaturationMatrix[ 0 ]);
      int16_t saturationMatrix[ matrixSize ];
      for (NSUInteger i = 0; i < matrixSize; ++i) {
        saturationMatrix[ i ] = (int16_t)roundf(floatingPointSaturationMatrix[ i ] * divisor);
      }
      if (hasBlur) {
        vImageMatrixMultiply_ARGB8888(&effectOutBuffer, &effectInBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
        effectImageBuffersAreSwapped = YES;
      }
      else {
        vImageMatrixMultiply_ARGB8888(&effectInBuffer, &effectOutBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
      }
    }
    if (!effectImageBuffersAreSwapped)
      effectImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    if (effectImageBuffersAreSwapped)
      effectImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
  }

  // 开启上下文 用于输出图像
  UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
  CGContextRef outputContext = UIGraphicsGetCurrentContext();
  CGContextScaleCTM(outputContext, 1.0, -1.0);
  CGContextTranslateCTM(outputContext, 0, -self.size.height);

  // 开始画底图
  CGContextDrawImage(outputContext, imageRect, self.CGImage);

  // 开始画模糊效果
  if (hasBlur) {
    CGContextSaveGState(outputContext);
    if (maskImage) {
      CGContextClipToMask(outputContext, imageRect, maskImage.CGImage);
    }
    CGContextDrawImage(outputContext, imageRect, effectImage.CGImage);
    CGContextRestoreGState(outputContext);
  }

  // 添加颜色渲染
  if (tintColor) {
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, tintColor.CGColor);
    CGContextFillRect(outputContext, imageRect);
    CGContextRestoreGState(outputContext);
  }

  // 输出成品,并关闭上下文
  UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return outputImage;
}

- (UIColor *)bf_colorAtPixel:(CGPoint)point {
  if (!CGRectContainsPoint(CGRectMake(0.0f, 0.0f, self.size.width, self.size.height), point)) {
    return nil;
  }

  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  int bytesPerPixel = 4;
  int bytesPerRow = bytesPerPixel * 1;
  NSUInteger bitsPerComponent = 8;
  unsigned char pixelData[ 4 ] = { 0, 0, 0, 0 };

  CGContextRef context = CGBitmapContextCreate(pixelData, 1, 1, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
  CGColorSpaceRelease(colorSpace);
  CGContextSetBlendMode(context, kCGBlendModeCopy);

  CGContextTranslateCTM(context, -point.x, point.y - self.size.height);
  CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, self.size.width, self.size.height), self.CGImage);
  CGContextRelease(context);

  CGFloat red = (CGFloat)pixelData[ 0 ] / 255.0f;
  CGFloat green = (CGFloat)pixelData[ 1 ] / 255.0f;
  CGFloat blue = (CGFloat)pixelData[ 2 ] / 255.0f;
  CGFloat alpha = (CGFloat)pixelData[ 3 ] / 255.0f;

  return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

//生成一张纯色的图片
- (UIImage *)bf_imageWithColor:(UIColor *)color {
  CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
  UIGraphicsBeginImageContext(rect.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetFillColorWithColor(context, [color CGColor]);
  CGContextFillRect(context, rect);
  UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return theImage;
}

static int bf_delayCentisecondsForImageAtIndex(CGImageSourceRef const source, size_t const i) {
  int delayCentiseconds = 1;
  CFDictionaryRef const properties = CGImageSourceCopyPropertiesAtIndex(source, i, NULL);
  if (properties) {
    CFDictionaryRef const gifProperties = CFDictionaryGetValue(properties, kCGImagePropertyGIFDictionary);
    CFRelease(properties);
    if (gifProperties) {
      CFNumberRef const number = CFDictionaryGetValue(gifProperties, kCGImagePropertyGIFDelayTime);

      delayCentiseconds = (int)lrint([fromCF number doubleValue] * 100);
    }
  }
  return delayCentiseconds;
}

static void bf_createImagesAndDelays(CGImageSourceRef source, size_t count, CGImageRef imagesOut[ count ], int delayCentisecondsOut[ count ]) {
  for (size_t i = 0; i < count; ++i) {
    imagesOut[ i ] = CGImageSourceCreateImageAtIndex(source, i, NULL);
    delayCentisecondsOut[ i ] = bf_delayCentisecondsForImageAtIndex(source, i);
  }
}

static int bf_sum(size_t const count, int const *const values) {
  int theSum = 0;
  for (size_t i = 0; i < count; ++i) {
    theSum += values[ i ];
  }
  return theSum;
}

static int bf_pairGCD(int a, int b) {
  if (a < b)
    return bf_pairGCD(b, a);
  while (true) {
    int const r = a % b;
    if (r == 0)
      return b;
    a = b;
    b = r;
  }
}

static int bf_vectorGCD(size_t const count, int const *const values) {
  int gcd = values[ 0 ];
  for (size_t i = 1; i < count; ++i) {
    // Note that after I process the first few elements of the vector, `gcd` will probably be smaller than any remaining element.  By passing the smaller value as the second argument to `pairGCD`, I
    // avoid making it swap the arguments.
    gcd = bf_pairGCD(values[ i ], gcd);
  }
  return gcd;
}

static NSArray *bf_frameArray(size_t const count, CGImageRef const images[ count ], int const delayCentiseconds[ count ], int const totalDurationCentiseconds) {
  int const gcd = bf_vectorGCD(count, delayCentiseconds);
  size_t const frameCount = totalDurationCentiseconds / gcd;
  UIImage *frames[ frameCount ];
  for (size_t i = 0, f = 0; i < count; ++i) {
    UIImage *const frame = [UIImage imageWithCGImage:images[ i ]];
    for (size_t j = delayCentiseconds[ i ] / gcd; j > 0; --j) {
      frames[ f++ ] = frame;
    }
  }
  return [NSArray arrayWithObjects:frames count:frameCount];
}

static void bf_releaseImages(size_t const count, CGImageRef const images[ count ]) {
  for (size_t i = 0; i < count; ++i) {
    CGImageRelease(images[ i ]);
  }
}

static UIImage *bf_animatedImageWithAnimatedGIFImageSource(CGImageSourceRef const source) {
  size_t const count = CGImageSourceGetCount(source);
  CGImageRef images[ count ];
  int delayCentiseconds[ count ];  // in centiseconds
  bf_createImagesAndDelays(source, count, images, delayCentiseconds);
  int const totalDurationCentiseconds = bf_sum(count, delayCentiseconds);
  NSArray *const frames = bf_frameArray(count, images, delayCentiseconds, totalDurationCentiseconds);
  UIImage *const animation = [UIImage animatedImageWithImages:frames duration:(NSTimeInterval)totalDurationCentiseconds / 100.0];
  bf_releaseImages(count, images);
  return animation;
}

static UIImage *bf_animatedImageWithAnimatedGIFReleasingImageSource(CGImageSourceRef source) {
  if (source) {
    UIImage *const image = bf_animatedImageWithAnimatedGIFImageSource(source);
    CFRelease(source);
    return image;
  }
  else {
    return nil;
  }
}

+ (UIImage *)bf_animatedImageWithAnimatedGIFData:(NSData *)data {
  return bf_animatedImageWithAnimatedGIFReleasingImageSource(CGImageSourceCreateWithData(toCF data, NULL));
}

+ (UIImage *)bf_animatedImageWithAnimatedGIFURL:(NSURL *)url {
  return bf_animatedImageWithAnimatedGIFReleasingImageSource(CGImageSourceCreateWithURL(toCF url, NULL));
}

- (UIImage *)bf_imageCroppedToRect:(CGRect)rect {
  UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0f);

  [self drawAtPoint:CGPointMake(-rect.origin.x, -rect.origin.y)];

  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return image;
}

- (UIImage *)bf_imageScaledToSize:(CGSize)size {
  if (CGSizeEqualToSize(self.size, size)) {
    return self;
  }

  UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);

  [self drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];

  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return image;
}

- (UIImage *)bf_imageScaledToFitSize:(CGSize)size {
  CGFloat aspect = self.size.width / self.size.height;
  if (size.width / aspect <= size.height) {
    return [self bf_imageScaledToSize:CGSizeMake(size.width, size.width / aspect)];
  }
  else {
    return [self bf_imageScaledToSize:CGSizeMake(size.height * aspect, size.height)];
  }
}

- (UIImage *)bf_imageScaledToFillSize:(CGSize)size {
  if (CGSizeEqualToSize(self.size, size)) {
    return self;
  }

  CGFloat aspect = self.size.width / self.size.height;
  if (size.width / aspect >= size.height) {
    return [self bf_imageScaledToSize:CGSizeMake(size.width, size.width / aspect)];
  }
  else {
    return [self bf_imageScaledToSize:CGSizeMake(size.height * aspect, size.height)];
  }
}

- (UIImage *)bf_imageCroppedAndScaledToSize:(CGSize)size contentMode:(UIViewContentMode)contentMode padToFit:(BOOL)padToFit;
{
  CGRect rect = CGRectZero;
  switch (contentMode) {
    case UIViewContentModeScaleAspectFit: {
      CGFloat aspect = self.size.width / self.size.height;
      if (size.width / aspect <= size.height) {
        rect = CGRectMake(0.0f, (size.height - size.width / aspect) / 2.0f, size.width, size.width / aspect);
      }
      else {
        rect = CGRectMake((size.width - size.height * aspect) / 2.0f, 0.0f, size.height * aspect, size.height);
      }
      break;
    }
    case UIViewContentModeScaleAspectFill: {
      CGFloat aspect = self.size.width / self.size.height;
      if (size.width / aspect >= size.height) {
        rect = CGRectMake(0.0f, (size.height - size.width / aspect) / 2.0f, size.width, size.width / aspect);
      }
      else {
        rect = CGRectMake((size.width - size.height * aspect) / 2.0f, 0.0f, size.height * aspect, size.height);
      }
      break;
    }
    case UIViewContentModeCenter: {
      rect = CGRectMake((size.width - self.size.width) / 2.0f, (size.height - self.size.height) / 2.0f, self.size.width, self.size.height);
      break;
    }
    case UIViewContentModeTop: {
      rect = CGRectMake((size.width - self.size.width) / 2.0f, 0.0f, self.size.width, self.size.height);
      break;
    }
    case UIViewContentModeBottom: {
      rect = CGRectMake((size.width - self.size.width) / 2.0f, size.height - self.size.height, self.size.width, self.size.height);
      break;
    }
    case UIViewContentModeLeft: {
      rect = CGRectMake(0.0f, (size.height - self.size.height) / 2.0f, self.size.width, self.size.height);
      break;
    }
    case UIViewContentModeRight: {
      rect = CGRectMake(size.width - self.size.width, (size.height - self.size.height) / 2.0f, self.size.width, self.size.height);
      break;
    }
    case UIViewContentModeTopLeft: {
      rect = CGRectMake(0.0f, 0.0f, self.size.width, self.size.height);
      break;
    }
    case UIViewContentModeTopRight: {
      rect = CGRectMake(size.width - self.size.width, 0.0f, self.size.width, self.size.height);
      break;
    }
    case UIViewContentModeBottomLeft: {
      rect = CGRectMake(0.0f, size.height - self.size.height, self.size.width, self.size.height);
      break;
    }
    case UIViewContentModeBottomRight: {
      rect = CGRectMake(size.width - self.size.width, size.height - self.size.height, self.size.width, self.size.height);
      break;
    }
    default: {
      rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
      break;
    }
  }

  if (!padToFit) {
    if (rect.size.width < size.width) {
      size.width = rect.size.width;
      rect.origin.x = 0.0f;
    }
    if (rect.size.height < size.height) {
      size.height = rect.size.height;
      rect.origin.y = 0.0f;
    }
  }

  if (CGSizeEqualToSize(self.size, size)) {
    return self;
  }

  UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);

  [self drawInRect:rect];

  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return image;
}

+ (CGImageRef)bf_gradientMask {
  static CGImageRef sharedMask = NULL;
  if (sharedMask == NULL) {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(1, 256), YES, 0.0);
    CGContextRef gradientContext = UIGraphicsGetCurrentContext();
    CGFloat colors[] = { 0.0, 1.0, 1.0, 1.0 };
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, NULL, 2);
    CGPoint gradientStartPoint = CGPointMake(0, 0);
    CGPoint gradientEndPoint = CGPointMake(0, 256);
    CGContextDrawLinearGradient(gradientContext, gradient, gradientStartPoint, gradientEndPoint, kCGGradientDrawsAfterEndLocation);
    sharedMask = CGBitmapContextCreateImage(gradientContext);
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    UIGraphicsEndImageContext();
  }
  return sharedMask;
}

- (UIImage *)bf_reflectedImageWithScale:(CGFloat)scale {
  CGFloat height = ceil(self.size.height * scale);
  CGSize size = CGSizeMake(self.size.width, height);
  CGRect bounds = CGRectMake(0.0f, 0.0f, size.width, size.height);

  UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
  CGContextRef context = UIGraphicsGetCurrentContext();

  CGContextClipToMask(context, bounds, [[self class] bf_gradientMask]);

  CGContextScaleCTM(context, 1.0f, -1.0f);
  CGContextTranslateCTM(context, 0.0f, -self.size.height);
  [self drawInRect:CGRectMake(0.0f, 0.0f, self.size.width, self.size.height)];

  UIImage *reflection = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return reflection;
}

- (UIImage *)bf_imageWithReflectionWithScale:(CGFloat)scale gap:(CGFloat)gap alpha:(CGFloat)alpha {

  UIImage *reflection = [self bf_reflectedImageWithScale:scale];
  CGFloat reflectionOffset = reflection.size.height + gap;

  UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.size.width, self.size.height + reflectionOffset * 2.0f), NO, 0.0f);

  [reflection drawAtPoint:CGPointMake(0.0f, reflectionOffset + self.size.height + gap) blendMode:kCGBlendModeNormal alpha:alpha];

  [self drawAtPoint:CGPointMake(0.0f, reflectionOffset)];

  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return image;
}

- (UIImage *)bf_imageWithShadowColor:(UIColor *)color offset:(CGSize)offset blur:(CGFloat)blur {
  CGSize border = CGSizeMake(fabs(offset.width) + blur, fabs(offset.height) + blur);

  CGSize size = CGSizeMake(self.size.width + border.width * 2.0f, self.size.height + border.height * 2.0f);

  UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
  CGContextRef context = UIGraphicsGetCurrentContext();

  CGContextSetShadowWithColor(context, offset, blur, color.CGColor);

  [self drawAtPoint:CGPointMake(border.width, border.height)];

  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return image;
}

- (UIImage *)bf_imageWithCornerRadius:(CGFloat)radius {
  UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
  CGContextRef context = UIGraphicsGetCurrentContext();

  CGContextBeginPath(context);
  CGContextMoveToPoint(context, 0.0f, radius);
  CGContextAddLineToPoint(context, 0.0f, self.size.height - radius);
  CGContextAddArc(context, radius, self.size.height - radius, radius, M_PI, M_PI / 2.0f, 1);
  CGContextAddLineToPoint(context, self.size.width - radius, self.size.height);
  CGContextAddArc(context, self.size.width - radius, self.size.height - radius, radius, M_PI / 2.0f, 0.0f, 1);
  CGContextAddLineToPoint(context, self.size.width, radius);
  CGContextAddArc(context, self.size.width - radius, radius, radius, 0.0f, -M_PI / 2.0f, 1);
  CGContextAddLineToPoint(context, radius, 0.0f);
  CGContextAddArc(context, radius, radius, radius, -M_PI / 2.0f, M_PI, 1);
  CGContextClip(context);

  [self drawAtPoint:CGPointZero];

  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return image;
}

- (UIImage *)bf_imageWithAlpha:(CGFloat)alpha {
  UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);

  [self drawAtPoint:CGPointZero blendMode:kCGBlendModeNormal alpha:alpha];

  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return image;
}

- (UIImage *)bf_imageWithMask:(UIImage *)maskImage;
{
  UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
  CGContextRef context = UIGraphicsGetCurrentContext();

  CGContextClipToMask(context, CGRectMake(0.0f, 0.0f, self.size.width, self.size.height), maskImage.CGImage);

  [self drawAtPoint:CGPointZero];

  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return image;
}

- (UIImage *)bf_maskImageFromImageAlpha {
  NSInteger width = CGImageGetWidth(self.CGImage);
  NSInteger height = CGImageGetHeight(self.CGImage);

  NSInteger bytesPerRow = ((width + 3) / 4) * 4;
  void *data = calloc(bytesPerRow * height, sizeof(unsigned char *));
  CGContextRef context = CGBitmapContextCreate(data, width, height, 8, bytesPerRow, NULL, kCGImageAlphaOnly);
  CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), self.CGImage);

  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      NSInteger index = y * bytesPerRow + x;
      ((unsigned char *)data)[ index ] = 255 - ((unsigned char *)data)[ index ];
    }
  }

  CGImageRef maskRef = CGBitmapContextCreateImage(context);
  CGContextRelease(context);
  UIImage *mask = [UIImage imageWithCGImage:maskRef];
  CGImageRelease(maskRef);
  free(data);

  return mask;
}

+ (UIImage *)bf_mergeImage:(UIImage *)firstImage withImage:(UIImage *)secondImage {
  CGImageRef firstImageRef = firstImage.CGImage;
  CGFloat firstWidth = CGImageGetWidth(firstImageRef);
  CGFloat firstHeight = CGImageGetHeight(firstImageRef);
  CGImageRef secondImageRef = secondImage.CGImage;
  CGFloat secondWidth = CGImageGetWidth(secondImageRef);
  CGFloat secondHeight = CGImageGetHeight(secondImageRef);
  CGSize mergedSize = CGSizeMake(MAX(firstWidth, secondWidth), MAX(firstHeight, secondHeight));
  UIGraphicsBeginImageContext(mergedSize);
  [firstImage drawInRect:CGRectMake(0, 0, firstWidth, firstHeight)];
  [secondImage drawInRect:CGRectMake(0, 0, secondWidth, secondHeight)];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

- (NSData *)bf_compressWithMaxLength:(NSInteger)maxLength {
  CGFloat compress = 0.9f;
  NSData *data = UIImageJPEGRepresentation(self, compress);

  while (data.length > maxLength && compress > 0.01) {
    compress -= 0.02f;

    data = UIImageJPEGRepresentation(self, compress);
  }

  return data;
}

/** 获得灰度图 */
- (UIImage *)bf_convertToGrayImage {
  int width = self.size.width;
  int height = self.size.height;

  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
  CGContextRef context = CGBitmapContextCreate(nil, width, height, 8, 0, colorSpace, kCGImageAlphaNone);
  CGColorSpaceRelease(colorSpace);

  if (context == NULL) {
    return nil;
  }

  CGContextDrawImage(context, CGRectMake(0, 0, width, height), self.CGImage);
  CGImageRef contextRef = CGBitmapContextCreateImage(context);
  UIImage *grayImage = [UIImage imageWithCGImage:contextRef];
  CGContextRelease(context);
  CGImageRelease(contextRef);

  return grayImage;
}

/** 纠正图片的方向 */
- (UIImage *)bf_fixOrientation {
  if (self.imageOrientation == UIImageOrientationUp)
    return self;

  CGAffineTransform transform = CGAffineTransformIdentity;

  switch (self.imageOrientation) {
    case UIImageOrientationDown:
    case UIImageOrientationDownMirrored:
      transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
      transform = CGAffineTransformRotate(transform, M_PI);
      break;

    case UIImageOrientationLeft:
    case UIImageOrientationLeftMirrored:
      transform = CGAffineTransformTranslate(transform, self.size.width, 0);
      transform = CGAffineTransformRotate(transform, M_PI_2);
      break;

    case UIImageOrientationRight:
    case UIImageOrientationRightMirrored:
      transform = CGAffineTransformTranslate(transform, 0, self.size.height);
      transform = CGAffineTransformRotate(transform, -M_PI_2);
      break;
    case UIImageOrientationUp:
    case UIImageOrientationUpMirrored:
      break;
  }

  switch (self.imageOrientation) {
    case UIImageOrientationUpMirrored:
    case UIImageOrientationDownMirrored:
      transform = CGAffineTransformTranslate(transform, self.size.width, 0);
      transform = CGAffineTransformScale(transform, -1, 1);
      break;

    case UIImageOrientationLeftMirrored:
    case UIImageOrientationRightMirrored:
      transform = CGAffineTransformTranslate(transform, self.size.height, 0);
      transform = CGAffineTransformScale(transform, -1, 1);
      break;
    case UIImageOrientationUp:
    case UIImageOrientationDown:
    case UIImageOrientationLeft:
    case UIImageOrientationRight:
      break;
  }

  // Now we draw the underlying CGImage into a new context, applying the transform
  // calculated above.
  CGContextRef ctx =
    CGBitmapContextCreate(NULL, self.size.width, self.size.height, CGImageGetBitsPerComponent(self.CGImage), 0, CGImageGetColorSpace(self.CGImage), CGImageGetBitmapInfo(self.CGImage));
  CGContextConcatCTM(ctx, transform);

  switch (self.imageOrientation) {
    case UIImageOrientationLeft:
    case UIImageOrientationLeftMirrored:
    case UIImageOrientationRight:
    case UIImageOrientationRightMirrored:
      CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.height, self.size.width), self.CGImage);
      break;

    default:
      CGContextDrawImage(ctx, CGRectMake(0, 0, self.size.width, self.size.height), self.CGImage);
      break;
  }

  CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
  UIImage *img = [UIImage imageWithCGImage:cgimg];
  CGContextRelease(ctx);
  CGImageRelease(cgimg);

  return img;
}

/** 按给定的方向旋转图片 */
- (UIImage *)bf_rotate:(UIImageOrientation)orient {
  CGRect bnds = CGRectZero;
  UIImage *copy = nil;
  CGContextRef ctxt = nil;
  CGImageRef imag = self.CGImage;
  CGRect rect = CGRectZero;
  CGAffineTransform tran = CGAffineTransformIdentity;

  rect.size.width = CGImageGetWidth(imag);
  rect.size.height = CGImageGetHeight(imag);

  bnds = rect;

  switch (orient) {
    case UIImageOrientationUp:
      return self;

    case UIImageOrientationUpMirrored:
      tran = CGAffineTransformMakeTranslation(rect.size.width, 0.0);
      tran = CGAffineTransformScale(tran, -1.0, 1.0);
      break;

    case UIImageOrientationDown:
      tran = CGAffineTransformMakeTranslation(rect.size.width, rect.size.height);
      tran = CGAffineTransformRotate(tran, M_PI);
      break;

    case UIImageOrientationDownMirrored:
      tran = CGAffineTransformMakeTranslation(0.0, rect.size.height);
      tran = CGAffineTransformScale(tran, 1.0, -1.0);
      break;

    case UIImageOrientationLeft:
      bnds = bf_swapWidthAndHeight(bnds);
      tran = CGAffineTransformMakeTranslation(0.0, rect.size.width);
      tran = CGAffineTransformRotate(tran, 3.0 * M_PI / 2.0);
      break;

    case UIImageOrientationLeftMirrored:
      bnds = bf_swapWidthAndHeight(bnds);
      tran = CGAffineTransformMakeTranslation(rect.size.height, rect.size.width);
      tran = CGAffineTransformScale(tran, -1.0, 1.0);
      tran = CGAffineTransformRotate(tran, 3.0 * M_PI / 2.0);
      break;

    case UIImageOrientationRight:
      bnds = bf_swapWidthAndHeight(bnds);
      tran = CGAffineTransformMakeTranslation(rect.size.height, 0.0);
      tran = CGAffineTransformRotate(tran, M_PI / 2.0);
      break;

    case UIImageOrientationRightMirrored:
      bnds = bf_swapWidthAndHeight(bnds);
      tran = CGAffineTransformMakeScale(-1.0, 1.0);
      tran = CGAffineTransformRotate(tran, M_PI / 2.0);
      break;

    default:
      return self;
  }

  UIGraphicsBeginImageContext(bnds.size);
  ctxt = UIGraphicsGetCurrentContext();

  switch (orient) {
    case UIImageOrientationLeft:
    case UIImageOrientationLeftMirrored:
    case UIImageOrientationRight:
    case UIImageOrientationRightMirrored:
      CGContextScaleCTM(ctxt, -1.0, 1.0);
      CGContextTranslateCTM(ctxt, -rect.size.height, 0.0);
      break;

    default:
      CGContextScaleCTM(ctxt, 1.0, -1.0);
      CGContextTranslateCTM(ctxt, 0.0, -rect.size.height);
      break;
  }

  CGContextConcatCTM(ctxt, tran);
  CGContextDrawImage(UIGraphicsGetCurrentContext(), rect, imag);

  copy = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return copy;
}

/** 垂直翻转 */
- (UIImage *)bf_flipVertical {
  return [self bf_rotate:UIImageOrientationDownMirrored];
}

/** 水平翻转 */
- (UIImage *)bf_flipHorizontal {
  return [self bf_rotate:UIImageOrientationUpMirrored];
}

/** 将图片旋转弧度radians */
- (UIImage *)bf_imageRotatedByRadians:(CGFloat)radians {
  UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.size.width, self.size.height)];
  CGAffineTransform t = CGAffineTransformMakeRotation(radians);
  rotatedViewBox.transform = t;
  CGSize rotatedSize = rotatedViewBox.frame.size;

  UIGraphicsBeginImageContext(rotatedSize);
  CGContextRef bitmap = UIGraphicsGetCurrentContext();

  CGContextTranslateCTM(bitmap, rotatedSize.width / 2, rotatedSize.height / 2);

  CGContextRotateCTM(bitmap, radians);

  CGContextScaleCTM(bitmap, 1.0, -1.0);
  CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), [self CGImage]);

  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return newImage;
}

/** 将图片旋转角度degrees */
- (UIImage *)bf_imageRotatedByDegrees:(CGFloat)degrees {
  return [self bf_imageRotatedByRadians:kDegreesToRadian(degrees)];
}

/** 交换宽和高 */
static CGRect bf_swapWidthAndHeight(CGRect rect) {
  CGFloat swap = rect.size.width;

  rect.size.width = rect.size.height;
  rect.size.height = swap;

  return rect;
}

#pragma mark - 截取当前image对象rect区域内的图像
- (UIImage *)bf_subImageWithRect:(CGRect)rect {
  CGImageRef newImageRef = CGImageCreateWithImageInRect(self.CGImage, rect);

  UIImage *newImage = [UIImage imageWithCGImage:newImageRef];

  CGImageRelease(newImageRef);

  return newImage;
}

#pragma mark - 压缩图片至指定尺寸
- (UIImage *)bf_rescaleImageToSize:(CGSize)size {
  UIGraphicsBeginImageContext(size);

  [self drawInRect:CGRectMake(0, 0, size.width, size.height)];

  UIImage *resImage = UIGraphicsGetImageFromCurrentImageContext();

  UIGraphicsEndImageContext();

  return resImage;
}

#pragma mark - 压缩图片至指定像素
- (UIImage *)bf_rescaleImageToPX:(CGFloat)toPX {
  CGSize size = self.size;

  if (size.width <= toPX && size.height <= toPX) {
    return self;
  }

  CGFloat scale = size.width / size.height;

  if (size.width > size.height) {
    size.width = toPX;
    size.height = size.width / scale;
  }
  else {
    size.height = toPX;
    size.width = size.height * scale;
  }

  return [self bf_rescaleImageToSize:size];
}

#pragma mark - 指定大小生成一个平铺的图片
- (UIImage *)bf_getTiledImageWithSize:(CGSize)size {
  UIView *tempView = [[UIView alloc] init];
  tempView.bounds = (CGRect){ CGPointZero, size };
  tempView.backgroundColor = [UIColor colorWithPatternImage:self];

  UIGraphicsBeginImageContext(size);
  [tempView.layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *bgImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return bgImage;
}

#pragma mark - UIView转化为UIImage
+ (UIImage *)bf_imageFromView:(UIView *)view {
  CGFloat scale = [UIScreen mainScreen].scale;
  UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, scale);
  [view.layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();

  return image;
}

- (UIImage *)bf_imageWithTitle:(NSString *)title fontSize:(CGFloat)fontSize titleColor:(UIColor *)titleColor {
  //画布大小
  CGSize size = CGSizeMake(self.size.width, self.size.height);
  //创建一个基于位图的上下文
  UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);  // opaque:NO  scale:0.0

  [self drawAtPoint:CGPointMake(0.0, 0.0)];

  //文字居中显示在画布上
  NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
  paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
  paragraphStyle.alignment = NSTextAlignmentCenter;  //文字居中

  //计算文字所占的size,文字居中显示在画布上
  CGSize sizeText = [title boundingRectWithSize:self.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:fontSize] } context:nil].size;
  CGFloat width = self.size.width;
  CGFloat height = self.size.height;

  CGRect rect = CGRectMake((width - sizeText.width) / 2, (height - sizeText.height) / 2, sizeText.width, sizeText.height);
  //绘制文字
  [title drawInRect:rect withAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:fontSize], NSForegroundColorAttributeName : titleColor, NSParagraphStyleAttributeName : paragraphStyle }];

  //返回绘制的新图形
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return newImage;
}

- (UIImage *)bf_imageByResizeToSize:(CGSize)size {
  if (size.width <= 0 || size.height <= 0)
    return nil;
  UIGraphicsBeginImageContextWithOptions(size, NO, self.scale);
  [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

+ (UIImage *)bf_getThumbnailImage:(NSString *)videoPath {
  if (videoPath) {
    NSURL *url;
    if ([videoPath rangeOfString:@"file:///"].location != NSNotFound) {
      url = [NSURL URLWithString:videoPath];
    }
    else {
      url = [NSURL fileURLWithPath:videoPath];
    }
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    // 设定缩略图的方向，如果不设定，可能会在视频旋转90/180/270°时，获取到的缩略图是被旋转过的，而不是正向的。
    gen.appliesPreferredTrackTransform = YES;
    // 设置图片的最大size(分辨率)
    gen.maximumSize = CGSizeMake(300, 169);
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);  // 取封面图，一秒钟600帧
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    if (error) {
      UIImage *placeHoldImg = [[UIImage alloc] init];
      return placeHoldImg;
    }
    UIImage *thumb = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return thumb;
  }
  else {
    UIImage *placeHoldImg = [[UIImage alloc] init];
    return placeHoldImg;
  }
}





+ (UIImage *)bf_acquireNameImageWithNameString:(NSString *)string imageSize:(CGSize)size {
    return [UIImage bf_createNikeNameImageName:string imageSize:size];
}

// 按规则截取nikeName
+ (NSString *)bf_dealWithNikeName:(NSString *)nikeName {
    // 筛除部分特殊符号
    NSString *showName = @"";
    if (nikeName.length>2) {
        showName = [nikeName substringFromIndex:nikeName.length-2];
    }else {
        showName = nikeName;
    }
    return showName;
}
// 检查是否含有字母
+ (BOOL)bf_isStringContainLetterWith:(NSString *)str {
    if (!str) {
        return NO;
    }
    NSRegularExpression *numberRegular = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]" options:NSRegularExpressionCaseInsensitive error:nil];
    NSInteger count = [numberRegular numberOfMatchesInString:str options:NSMatchingReportProgress range:NSMakeRange(0, str.length)];
    //count是str中包含[A-Za-z]数字的个数，只要count>0，说明str中包含数字
    if (count > 0) {
        return YES;
    }
    return NO;
}
// 根据nikeName绘制图片
+ (UIImage *)bf_createNikeNameImageName:(NSString *)name imageSize:(CGSize)size{
    return [self bf_createNikeNameImageName:name font:[UIFont systemFontOfSize:14] imageSize:size];
}

+ (UIImage *)bf_createNikeNameImageName:(NSString *)name font:(UIFont *)font imageSize:(CGSize)size{
    name = [UIImage bf_dealWithNikeName:name];
    NSArray *colorArr = @[@"5384ed"];
    UIImage *image = [UIImage bf_imageColor:[UIImage bf_colorWithHexString:colorArr[ABS(name.hash % colorArr.count)] alpha:1.0] size:size cornerRadius:0];
    
    UIGraphicsBeginImageContextWithOptions (size, NO , 0.0 );
    
    [image drawAtPoint : CGPointMake ( 0 , 0 )];
    
    // 获得一个位图图形上下文
    
    CGContextRef context= UIGraphicsGetCurrentContext ();
    
    CGContextDrawPath (context, kCGPathStroke );
    
    // 画名字
    
    CGSize nameSize = [name sizeWithAttributes:@{NSFontAttributeName : font}];
    
    [name drawAtPoint : CGPointMake ( (size.width  - nameSize.width) / 2 , (size.height  - nameSize.height) / 2 ) withAttributes : @{ NSFontAttributeName :font, NSForegroundColorAttributeName :[UIColor whiteColor] } ];
    
    // 返回绘制的新图形
    
    UIImage *newImage= UIGraphicsGetImageFromCurrentImageContext ();
    
    UIGraphicsEndImageContext ();
    
    return newImage;
    
}


+ (UIImage *)bf_imageColor:(UIColor *)color size:(CGSize)size cornerRadius:(CGFloat)radius {
    if (CGSizeEqualToSize(size, CGSizeZero)) {
        size = CGSizeMake(1, 1);
    }
    
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, 0, [UIScreen mainScreen].scale);
    [color set];
    UIRectFill(rect);
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(size, 0, [UIScreen mainScreen].scale);
    
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    [colorImage drawInRect:rect];
    
    colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return colorImage;
}

+ (id)bf_colorWithHexString:(NSString*)hexColor alpha:(CGFloat)alpha {
    
    unsigned int red,green,blue;
    NSRange range;
    
    range.length = 2;
    range.location = 0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&red];
    
    range.location = 2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&green];
    
    range.location = 4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&blue];
    
    UIColor* retColor = [UIColor colorWithRed:(float)(red/255.0f)green:(float)(green / 255.0f) blue:(float)(blue / 255.0f)alpha:alpha];
    return retColor;
}

+(UIImage *)bf_boxblurImage:(UIImage *)image {
      NSInteger boxSize = (NSInteger)(10 * 5);
      boxSize = boxSize - (boxSize % 2) + 1;
      
      CGImageRef img = image.CGImage;
      
      vImage_Buffer inBuffer, outBuffer, rgbOutBuffer;
      vImage_Error error;
      
      void *pixelBuffer, *convertBuffer;
      
      CGDataProviderRef inProvider = CGImageGetDataProvider(img);
      CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
      
      convertBuffer = malloc( CGImageGetBytesPerRow(img) * CGImageGetHeight(img) );
      rgbOutBuffer.width = CGImageGetWidth(img);
      rgbOutBuffer.height = CGImageGetHeight(img);
      rgbOutBuffer.rowBytes = CGImageGetBytesPerRow(img);
      rgbOutBuffer.data = convertBuffer;
      
      inBuffer.width = CGImageGetWidth(img);
      inBuffer.height = CGImageGetHeight(img);
      inBuffer.rowBytes = CGImageGetBytesPerRow(img);
      inBuffer.data = (void *)CFDataGetBytePtr(inBitmapData);
      
      pixelBuffer = malloc( CGImageGetBytesPerRow(img) * CGImageGetHeight(img) );
      
      if (pixelBuffer == NULL) {
          NSLog(@"No pixelbuffer");
      }
      
      outBuffer.data = pixelBuffer;
      outBuffer.width = CGImageGetWidth(img);
      outBuffer.height = CGImageGetHeight(img);
      outBuffer.rowBytes = CGImageGetBytesPerRow(img);
      
      void *rgbConvertBuffer = malloc( CGImageGetBytesPerRow(img) * CGImageGetHeight(img) );
      vImage_Buffer outRGBBuffer;
      outRGBBuffer.width = CGImageGetWidth(img);
      outRGBBuffer.height = CGImageGetHeight(img);
      outRGBBuffer.rowBytes = 3;
      outRGBBuffer.data = rgbConvertBuffer;
      
      error = vImageBoxConvolve_ARGB8888(&inBuffer, &outBuffer, NULL, 0, 0, boxSize, boxSize, NULL, kvImageEdgeExtend);
      
      if (error) {
          NSLog(@"error from convolution %ld", error);
      }
      const uint8_t mask[] = {2, 1, 0, 3};
      
      vImagePermuteChannels_ARGB8888(&outBuffer, &rgbOutBuffer, mask, kvImageNoFlags);
      
      CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
      CGContextRef ctx = CGBitmapContextCreate(rgbOutBuffer.data,
                                               rgbOutBuffer.width,
                                               rgbOutBuffer.height,
                                               8,
                                               rgbOutBuffer.rowBytes,
                                               colorSpace,
                                               kCGImageAlphaNoneSkipLast);
      CGImageRef imageRef = CGBitmapContextCreateImage(ctx);
      UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
      
      //clean up
      CGContextRelease(ctx);
      
      free(pixelBuffer);
      free(convertBuffer);
      free(rgbConvertBuffer);
      CFRelease(inBitmapData);
      
      CGColorSpaceRelease(colorSpace);
      CGImageRelease(imageRef);
      
      return returnImage;
}
@end
