//
//  UIColor+BFAd.h
//  SmartVideo
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>
enum SLColorUIGradientStyle {
    /**
     *  Returns a gradual blend between colors originating at the leftmost point of an object's frame, and ending at the rightmost point of the object's frame.
     *
     *  @since 1.0
     */
    UIGradientStyleLeftToRight = 0,
    /**
     *  Returns a gradual blend between colors originating at the center of an object's frame, and ending at all edges of the object's frame. NOTE: Supports a Maximum of 2 Colors.
     *
     *  @since 1.0
     */
    UIGradientStyleRadial = 1,
    /**
     *  Returns a gradual blend between colors originating at the topmost point of an object's frame, and ending at the bottommost point of the object's frame.
     *
     *  @since 1.0
     */
    UIGradientStyleTopToBottom = 2,
    UIGradientStyleDiagonal = 3
};



@interface UIColor (BFAd)
/**
 *  Stores an object's UIColor image if the UIColor was created using colorWithPatternImage.
 *
 */
@property (nonatomic, strong) UIImage *gradientImage;

// 通过颜色返回一个1*1大小的纯色图片
- (UIImage *)bf_image;

+ (UIColor *)bf_transformWithHexString:(NSString *)hexString;


+ (UIColor *)bf_colorWithGradientStyle:(enum SLColorUIGradientStyle)gradientStyle withFrame:(CGRect)frame andColors:(NSArray<UIColor *> * _Nonnull)colors;

+ (UIColor *)bf_colorWithHex:(UInt32)hex;

+ (UIColor *)bf_colorWithHex:(UInt32)hex andAlpha:(CGFloat)alpha;

+ (UIColor *)bf_colorWithHexString:(NSString *)hexString;

- (NSString *)bf_HEXString;

+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
