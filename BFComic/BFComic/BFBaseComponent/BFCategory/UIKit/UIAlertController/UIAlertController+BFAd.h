//
//  UIAlertController+BFAd.h
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, OptionStyle) {
    OptionStyleStyleOK_Cancel = 0,
    OptionStyleStyleOnlyOK
};

typedef void(^actionHandler)(UIAlertAction *action, NSUInteger index);

@interface UIAlertController (BFAd)

/**
 自定义弹出框

 @param title 标题
 @param message 内容
 @param actionTitle 按钮数组
 @param actionStyle 按钮样式
 @param alertControllerStyle 弹出框样式（UIAlertControllerStyleActionSheet， UIAlertControllerStyleAlert）
 @param actionHandler 按钮响应事件
 */
+ (void)bf_actionSheettWithTitle:(NSString *)title
                      message:(NSString *)message
                 actionTitles:(NSArray<NSString *> *)actionTitle
             alertActionStyle:(UIAlertActionStyle)actionStyle
         alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle
                actionHandler:(actionHandler)actionHandler ;

/**
 自定义弹出框

 @param title 标题
 @param message 内容
 @param actionTitle 按钮数组
 @param actionStyle 按钮样式
 @param alertControllerStyle 弹出框样式
 @param vc 父VC
 @param actionHandler 按钮响应事件
 */
+ (void)bf_actionSheettWithTitle:(NSString *)title
                      message:(NSString *)message
                 actionTitles:(NSArray<NSString *> *)actionTitle
             alertActionStyle:(UIAlertActionStyle)actionStyle
         alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle
               viewController:(UIViewController *)vc
                actionHandler:(actionHandler)actionHandler;

/**
 自定义弹出框

 @param title 标题
 @param message 内容
 @param actionTitle 按钮数组
 @param style 弹出框样式
 @param actionHandler 按钮响应事件
 */
+ (void)bf_actionAlertWithTitle:(NSString *)title
                     message:(NSString *)message
                actionTitles:(NSArray<NSString *> *)actionTitle
            alertActionStyle:(UIAlertActionStyle)style
               actionHandler:(actionHandler)actionHandler;

/**
 自定义按钮颜色
 
 @param title 提示标题
 @param message 提示信息
 @param actionTitle 按钮标题，数组
 @param colors 按钮标题颜色，数组
 @param style 样式
 @param actionHandler 按钮响应事件
 */
+ (void)bf_actionAlertWithTitle:(NSString *)title
                  titleColor:(UIColor *)titleColor
                     message:(NSString *)message
                messageColor:(UIColor *)messageColor
                actionTitles:(NSArray<NSString *> *)actionTitle
            alertTitleColors:(NSArray<UIColor *> *)colors
            alertActionStyle:(UIAlertActionStyle)style
               actionHandler:(actionHandler)actionHandler;

/**
 中间弹窗
 
 @param title 标题
 @param message 消息
 @param optionStyle 弹窗选项样式，两个选择或只有一个
 @param okTitle 右边选项的文字
 @param cancelTitle 左边选项的文字
 @param okBlock 右边选择选中后执行的代码
 @param cancelBlock 左边选项选中后执行的代码
 @return UIAlertController
 */
+ (UIAlertController *)bf_alertControllerWithTitle:(NSString *)title message:(NSString *)message optionStyle:(OptionStyle)optionStyle OkTitle:(NSString *)okTitle cancelTitle:(NSString *)cancelTitle okBlock:(dispatch_block_t)okBlock cancelBlock:(dispatch_block_t)cancelBlock;


/**
 从下面出现的弹窗
 
 @param title 标题
 @param message 消息
 @param optionStyle 弹窗选项样式，两个选择或只有一个
 @param okTitle 上面选项的文字
 @param cancelTitle 下面选项的文字
 @param okBlock 上面选择选中后执行的代码
 @param cancelBlock 下面选项选中后执行的代码
 @return UIAlertController
 */
+ (UIAlertController *)bf_sheetAlertControllerWithTitle:(NSString *)title message:(NSString *)message optionStyle:(OptionStyle)optionStyle OkTitle:(NSString *)okTitle cancelTitle:(NSString *)cancelTitle okBlock:(dispatch_block_t)okBlock cancelBlock:(dispatch_block_t)cancelBlock;


+ (void)bf_showAlertViewInCenterWithTitle:(NSString *)title andShowView:(UIView *)view;


- (void)showWithAnimated:(BOOL)animated;
- (void)hideWithAnimated:(BOOL)animated;
@end

NS_ASSUME_NONNULL_END
