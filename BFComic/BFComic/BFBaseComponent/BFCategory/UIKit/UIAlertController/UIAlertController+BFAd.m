//
//  UIAlertController+BFAd.m
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "UIAlertController+BFAd.h"
#import "NSString+BFAd.h"

@implementation UIAlertController (BFAd)

+ (void)bf_actionSheettWithTitle:(NSString *)title message:(NSString *)message actionTitles:(NSArray<NSString *> *)actionTitle alertActionStyle:(UIAlertActionStyle)actionStyle alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle actionHandler:(actionHandler)actionHandler
{
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:alertControllerStyle];
    for (NSUInteger i = 0; i < actionTitle.count; i++) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)  {
            actionHandler(action, i+1);
        }];
        [alertC addAction:action];
    }
    if (actionStyle == UIAlertActionStyleCancel) {
        [alertC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    }
    
    NSLog(@"dddddddddddddd %@",[UIApplication sharedApplication].delegate.window.rootViewController);
    
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alertC animated:YES completion:nil];
    
}


+ (void)bf_actionSheettWithTitle:(NSString *)title message:(NSString *)message actionTitles:(NSArray<NSString *> *)actionTitle alertActionStyle:(UIAlertActionStyle)actionStyle alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle viewController:(UIViewController *)vc actionHandler:(actionHandler)actionHandler
{
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:alertControllerStyle];
    for (NSUInteger i = 0; i < actionTitle.count; i++) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)  {
            actionHandler(action, i+1);
        }];
        [alertC addAction:action];
    }
    if (actionStyle == UIAlertActionStyleCancel) {
        [alertC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    }
    [vc presentViewController:alertC animated:YES completion:nil];
    
}


+ (void)bf_actionAlertWithTitle:(NSString *)title message:(NSString *)message actionTitles:(NSArray<NSString *> *)actionTitle alertActionStyle:(UIAlertActionStyle)style actionHandler:(actionHandler)actionHandler
{
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    for (NSUInteger i = 0; i < actionTitle.count; i++) {
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:actionTitle[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)  {
            actionHandler(action, i);
        }];
        [alertC addAction:actionCancel];
    }
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alertC animated:YES completion:nil];
    
}


+ (void)bf_actionAlertWithTitle:(NSString *)title titleColor:(UIColor *)titleColor message:(NSString *)message messageColor:(UIColor *)messageColor actionTitles:(NSArray<NSString *> *)actionTitle alertTitleColors:(NSArray<UIColor *> *)colors alertActionStyle:(UIAlertActionStyle)style actionHandler:(actionHandler)actionHandler
{
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    NSMutableAttributedString *alertControllerStr = [[NSMutableAttributedString alloc] initWithString:title];
    [alertControllerStr addAttribute:NSForegroundColorAttributeName value:titleColor range:NSMakeRange(0, title.length)];
    [alertControllerStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Semibold" size:17.f] range:NSMakeRange(0, title.length)];
    [alertC setValue:alertControllerStr forKey:@"attributedTitle"];
    
    
    NSMutableAttributedString *alertControllerMessageStr = [[NSMutableAttributedString alloc] initWithString:message];
    [alertControllerMessageStr addAttribute:NSForegroundColorAttributeName value:messageColor range:NSMakeRange(0, message.length)];
    [alertControllerMessageStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"PingFangSC-Regular" size:15.f] range:NSMakeRange(0, message.length)];
    [alertC setValue:alertControllerMessageStr forKey:@"attributedMessage"];
    for (NSUInteger i = 0; i < actionTitle.count; i++) {
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:actionTitle[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)  {
            actionHandler(action, i);
        }];
        [actionCancel setValue:colors[i] forKey:@"titleTextColor"];
        
        [alertC addAction:actionCancel];
    }
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alertC animated:YES completion:nil];
    
}


+ (UIAlertController *)bf_alertControllerWithTitle:(NSString *)title message:(NSString *)message optionStyle:(OptionStyle)optionStyle OkTitle:(NSString *)okTitle cancelTitle:(NSString *)cancelTitle okBlock:(dispatch_block_t)okBlock cancelBlock:(dispatch_block_t)cancelBlock{
    
    UIAlertController* alert=[UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    if (optionStyle == OptionStyleStyleOnlyOK) {
        UIAlertAction* OK=[UIAlertAction actionWithTitle:okTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (okBlock) {
                okBlock();
            }
        }];
        [alert addAction:OK];
    } else {
        UIAlertAction* cancel=[UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            if (cancelBlock) {
                cancelBlock();
            }
        }];
        UIAlertAction* OK=[UIAlertAction actionWithTitle:okTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (okBlock) {
                okBlock();
            }
        }];
        [alert addAction:cancel];
        [alert addAction:OK];
    }
    return alert;
}


+ (UIAlertController *)bf_sheetAlertControllerWithTitle:(NSString *)title message:(NSString *)message optionStyle:(OptionStyle)optionStyle OkTitle:(NSString *)okTitle cancelTitle:(NSString *)cancelTitle okBlock:(dispatch_block_t)okBlock cancelBlock:(dispatch_block_t)cancelBlock {
    
    UIAlertController* alert=[UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    if (optionStyle == OptionStyleStyleOnlyOK) {
        UIAlertAction* OK=[UIAlertAction actionWithTitle:okTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (okBlock) {
                okBlock();
            }
        }];
        [alert addAction:OK];
    } else {
        UIAlertAction* cancel=[UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            if (cancelBlock) {
                cancelBlock();
            }
        }];
        UIAlertAction* OK=[UIAlertAction actionWithTitle:okTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (okBlock) {
                okBlock();
            }
        }];
        [alert addAction:cancel];
        [alert addAction:OK];
    }
    return alert;
}



+ (void)bf_showAlertViewInCenterWithTitle:(NSString *)title andShowView:(UIView *)view {

  UILabel *label = [view viewWithTag:10086];
  [label removeFromSuperview];
    
  CGSize titleSize = [title bf_sizeWithFont:[UIFont systemFontOfSize:15] maxW:30];

  UILabel *alertLabel =
    [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width / 2 - (titleSize.width + 30) / 2, [UIScreen mainScreen].bounds.size.height / 2 - 30, titleSize.width + 30, 30)];
  alertLabel.text = title;
  alertLabel.tag = 10086;
  alertLabel.textColor = [UIColor whiteColor];
  alertLabel.font = [UIFont systemFontOfSize:15];
  alertLabel.backgroundColor = [UIColor darkGrayColor];
  alertLabel.alpha = 0.7;
  alertLabel.layer.cornerRadius = 5;
  alertLabel.layer.masksToBounds = YES;
  alertLabel.textAlignment = NSTextAlignmentCenter;
  [view addSubview:alertLabel];

  [UIView animateWithDuration:2
    animations:^{
      alertLabel.alpha = 0;
    }
    completion:^(BOOL finished) {
      [alertLabel removeFromSuperview];
    }];
}

- (void)showWithAnimated:(BOOL)animated{
    
}

- (void)hideWithAnimated:(BOOL)animated{
    
}

@end
