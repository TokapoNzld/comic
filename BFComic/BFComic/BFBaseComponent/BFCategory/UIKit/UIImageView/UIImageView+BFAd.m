//
//  UIImageView+BFAd.m
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "UIImageView+BFAd.h"
#import <objc/runtime.h>
#import "UIView+WebCacheOperation.h"
#import "UIView+WebCache.h"

#import "UIImage+BFAd.h"

const char kSLProcessedImage;

@interface UIImageView ()

@property (assign, nonatomic) CGFloat bf_radius;
@property (assign, nonatomic) UIRectCorner bf_roundingCorners;
@property (assign, nonatomic) CGFloat bf_borderWidth;
@property (strong, nonatomic) UIColor *bf_borderColor;
@property (assign, nonatomic) BOOL bf_hadAddObserver;
@property (assign, nonatomic) BOOL bf_ssRounding;

@end


@implementation UIImageView (BFAd)

- (void)bf_setImageNoPlaceholderImage:(nullable NSString *)url{
    [self bf_setImageWithURL:[self bf_makeURLWithUrl:url] placeholderImage:nil options:0 progress:nil completed:nil];
}

- (void)bf_setImage:(nullable NSString *)url{
    NSString *placeholderImg = nil;
    #ifdef kPlaceholderImg
            placeholderImg = kPlaceholderImg;
    #endif
    [self bf_setImageWithURL:[self bf_makeURLWithUrl:url] placeholderImage:[UIImage imageNamed:placeholderImg] options:0 progress:nil completed:nil];
}

- (void)bf_setImage:(nullable NSString *)url placeholderImage:(nullable UIImage *)image {
    [self bf_setImageWithURL:[self bf_makeURLWithUrl:url] placeholderImage:image options:0 progress:nil completed:nil];
}

- (void)bf_setImage:(nullable NSString *)url
placeholderImageName:(nullable NSString *)placeholder{
    [self bf_setImageWithURL:[self bf_makeURLWithUrl:url] placeholderImage:[UIImage imageNamed:placeholder] options:0 progress:nil completed:nil];
}

- (NSURL *)bf_makeURLWithUrl:(NSString *)url{
    return [NSURL URLWithString:[self bf_makeUrlStr:url]];
}
- (NSString *)bf_makeUrlStr:(NSString *)url{
    url = [NSString stringWithFormat:@"%@",url];
    NSCharacterSet *encodeUrlSet = [NSCharacterSet URLQueryAllowedCharacterSet];
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:encodeUrlSet];
    if (![url hasPrefix:@"http://"] && ![url hasPrefix:@"https://"]) {
#ifdef kImgBaseUrl
        url = [NSString stringWithFormat:@"%@%@",kImgBaseUrl,url];
#endif
    }
    return url;
}
- (void)bf_setImageWithURL:(nullable NSURL *)url
          placeholderImage:(nullable UIImage *)placeholder
                   options:(SDWebImageOptions)options
                  progress:(nullable SDImageLoaderProgressBlock)progressBlock
                 completed:(nullable SDInternalCompletionBlock)completedBlock {
    [self sd_internalSetImageWithURL:url placeholderImage:placeholder options:options context:nil setImageBlock:nil progress:progressBlock completed:completedBlock];
}


/**
 * @brief attach border for UIImageView with width & color
 */
- (void)bf_attachBorderWidth:(CGFloat)width color:(UIColor *)color {
    self.bf_borderWidth = width;
    self.bf_borderColor = color;
}

#pragma mark - Kernel
/**
 * @brief clip the cornerRadius with image, UIImageView must be setFrame before, no off-screen-rendered
 */
- (void)bf_cornerRadiusWithImage:(UIImage *)image cornerRadius:(CGFloat)cornerRadius rectCornerType:(UIRectCorner)rectCornerType {
    CGSize size = self.bounds.size;
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize cornerRadii = CGSizeMake(cornerRadius, cornerRadius);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, scale);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    if (nil == currentContext) {
        return;
    }
    UIBezierPath *cornerPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:rectCornerType cornerRadii:cornerRadii];
    [cornerPath addClip];
    [self.layer renderInContext:currentContext];
    [self bf_drawBorder:cornerPath];
    UIImage *processedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if (processedImage) {
        objc_setAssociatedObject(processedImage, &kSLProcessedImage, @(1), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    self.image = processedImage;
}

/**
 * @brief clip the cornerRadius with image, draw the backgroundColor you want, UIImageView must be setFrame before, no off-screen-rendered, no Color Blended layers
 */
- (void)bf_cornerRadiusWithImage:(UIImage *)image cornerRadius:(CGFloat)cornerRadius rectCornerType:(UIRectCorner)rectCornerType backgroundColor:(UIColor *)backgroundColor {
    CGSize size = self.bounds.size;
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize cornerRadii = CGSizeMake(cornerRadius, cornerRadius);
    
    UIGraphicsBeginImageContextWithOptions(size, YES, scale);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    if (nil == currentContext) {
        return;
    }
    UIBezierPath *cornerPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:rectCornerType cornerRadii:cornerRadii];
    UIBezierPath *backgroundRect = [UIBezierPath bezierPathWithRect:self.bounds];
    [backgroundColor setFill];
    [backgroundRect fill];
    [cornerPath addClip];
    [self.layer renderInContext:currentContext];
    [self bf_drawBorder:cornerPath];
    UIImage *processedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if (processedImage) {
        objc_setAssociatedObject(processedImage, &kSLProcessedImage, @(1), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    self.image = processedImage;
}

/**
 * @brief set cornerRadius for UIImageView, no off-screen-rendered
 */
- (void)bf_cornerRadiusAdvance:(CGFloat)cornerRadius rectCornerType:(UIRectCorner)rectCornerType {
    self.bf_radius = cornerRadius;
    self.bf_roundingCorners = rectCornerType;
    self.bf_isRounding = NO;
    if (!self.bf_hadAddObserver) {
        [[self class] bf_swizzleDealloc];
        [self addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
        self.bf_hadAddObserver = YES;
    }
    //Xcode 8 xib 删除了控件的Frame信息，需要主动创造
    [self layoutIfNeeded];
}

/**
 * @brief become Rounding UIImageView, no off-screen-rendered
 */
- (void)bf_cornerRadiusRoundingRect {
    self.bf_isRounding = YES;
    if (!self.bf_hadAddObserver) {
        [[self class] bf_swizzleDealloc];
        [self addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
        self.bf_hadAddObserver = YES;
    }
    //Xcode 8 xib 删除了控件的Frame信息，需要主动创造
    [self layoutIfNeeded];
}

#pragma mark - Private
- (void)bf_drawBorder:(UIBezierPath *)path {
    if (0 != self.bf_borderWidth && nil != self.bf_borderColor) {
        [path setLineWidth:2 * self.bf_borderWidth];
        [self.bf_borderColor setStroke];
        [path stroke];
    }
}

- (void)bf_dealloc {
    if (self.bf_hadAddObserver) {
        [self removeObserver:self forKeyPath:@"image"];
    }
    [self bf_dealloc];
}

- (void)bf_validateFrame {
    if (self.frame.size.width == 0) {
        [self.class bf_swizzleLayoutSubviews];
    }
}

+ (void)bf_swizzleMethod:(SEL)oneSel anotherMethod:(SEL)anotherSel {
    Method oneMethod = class_getInstanceMethod(self, oneSel);
    Method anotherMethod = class_getInstanceMethod(self, anotherSel);
    method_exchangeImplementations(oneMethod, anotherMethod);
}

+ (void)bf_swizzleDealloc {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self bf_swizzleMethod:NSSelectorFromString(@"dealloc") anotherMethod:@selector(bf_dealloc)];
    });
}

+ (void)bf_swizzleLayoutSubviews {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self bf_swizzleMethod:@selector(layoutSubviews) anotherMethod:@selector(bf_LayoutSubviews)];
    });
}

- (void)bf_LayoutSubviews {
    [self bf_LayoutSubviews];
    if (self.bf_isRounding) {
        [self bf_cornerRadiusWithImage:self.image cornerRadius:self.frame.size.width/2 rectCornerType:UIRectCornerAllCorners];
    } else if (0 != self.bf_radius && 0 != self.bf_roundingCorners && nil != self.image) {
        [self bf_cornerRadiusWithImage:self.image cornerRadius:self.bf_radius rectCornerType:self.bf_roundingCorners];
    }
}

#pragma mark - KVO for .image
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"image"]) {
        UIImage *newImage = change[NSKeyValueChangeNewKey];
        if ([newImage isMemberOfClass:[NSNull class]]) {
            return;
        } else if ([objc_getAssociatedObject(newImage, &kSLProcessedImage) intValue] == 1) {
            return;
        }
        [self bf_validateFrame];
        if (self.bf_isRounding) {
            [self bf_cornerRadiusWithImage:newImage cornerRadius:self.frame.size.width/2 rectCornerType:UIRectCornerAllCorners];
        } else if (0 != self.bf_radius && 0 != self.bf_roundingCorners && nil != self.image) {
            [self bf_cornerRadiusWithImage:newImage cornerRadius:self.bf_radius rectCornerType:self.bf_roundingCorners];
        }
    }
}

#pragma mark property
- (CGFloat)bf_borderWidth {
    return [objc_getAssociatedObject(self, _cmd) floatValue];
}

- (void)setBf_borderWidth:(CGFloat)bf_borderWidth {
    objc_setAssociatedObject(self, @selector(bf_borderWidth), @(bf_borderWidth), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIColor *)bf_borderColor {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setBf_borderColor:(UIColor *)bf_borderColor {
    objc_setAssociatedObject(self, @selector(bf_borderColor), bf_borderColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)bf_hadAddObserver {
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setBf_hadAddObserver:(BOOL)bf_hadAddObserver {
    objc_setAssociatedObject(self, @selector(bf_hadAddObserver), @(bf_hadAddObserver), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)bf_isRounding {
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setBf_isRounding:(BOOL)bf_isRounding {
    objc_setAssociatedObject(self, @selector(bf_isRounding), @(bf_isRounding), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIRectCorner)bf_roundingCorners {
    return [objc_getAssociatedObject(self, _cmd) unsignedLongValue];
}

- (void)setBf_roundingCorners:(UIRectCorner)bf_roundingCorners {
    objc_setAssociatedObject(self, @selector(bf_roundingCorners), @(bf_roundingCorners), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CGFloat)bf_radius {
    return [objc_getAssociatedObject(self, _cmd) floatValue];
}

- (void)setBf_radius:(CGFloat)bf_radius {
    objc_setAssociatedObject(self, @selector(bf_radius), @(bf_radius), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+ (instancetype)bf_imageViewWithPNGImage:(NSString *)imageName frame:(CGRect)frame {
    UIImageView *imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    imageV.frame = frame;
    return imageV;
}



#pragma mark - LBBlurredImage Additions
CGFloat const kLBBlurredImageDefaultBlurRadius            = 20.0;
CGFloat const kLBBlurredImageDefaultSaturationDeltaFactor = 1.8;

- (void)setImageToBlur:(UIImage *)image
       completionBlock:(LBBlurredImageCompletionBlock)completion
{
    [self setImageToBlur:image
              blurRadius:kLBBlurredImageDefaultBlurRadius
         completionBlock:completion];
}

- (void)setImageToBlur:(UIImage *)image
            blurRadius:(CGFloat)blurRadius
       completionBlock:(LBBlurredImageCompletionBlock) completion
{
    NSParameterAssert(image);
    NSParameterAssert(blurRadius >= 0);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *blurredImage = [image bf_applyBlurWithRadius:blurRadius
                                                 tintColor:nil
                                     saturationDeltaFactor:kLBBlurredImageDefaultSaturationDeltaFactor
                                                 maskImage:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.image = blurredImage;
            if (completion) {
                completion();
            }
        });
    });
}

@end
