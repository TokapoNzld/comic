//
//  UIImageView+BFAd.h
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^LBBlurredImageCompletionBlock)(void);

extern CGFloat const kLBBlurredImageDefaultBlurRadius;


@interface UIImageView (BFAd)

/**
 * 不需要占位图的
 */
- (void)bf_setImageNoPlaceholderImage:(nullable NSString *)url;
/**
 * 默认有占位图的
 */
- (void)bf_setImage:(nullable NSString *)url;



- (void)bf_setImage:(nullable NSString *)url placeholderImage:(nullable UIImage *)image;

/**
 * 需要一个占位图片
 */
- (void)bf_setImage:(nullable NSString *)url placeholderImageName:(nullable NSString *)placeholder;

// 快速创建imageView
+ (instancetype)bf_imageViewWithPNGImage:(NSString *)imageName frame:(CGRect)frame;


- (void)bf_cornerRadiusAdvance:(CGFloat)cornerRadius rectCornerType:(UIRectCorner)rectCornerType;


- (void)bf_attachBorderWidth:(CGFloat)width color:(UIColor *)color;



- (void)setImageToBlur:(UIImage *)image
            blurRadius:(CGFloat)blurRadius
       completionBlock:(LBBlurredImageCompletionBlock)completion;

/**
 Set the blurred version of the provided image to the UIImageView
 with the default blur radius
 
 @param image the image to blur and set as UIImageView's image
 @param completion a completion block called after the image
 was blurred and set to the UIImageView (the block is dispatched on main thread)
 */
- (void)setImageToBlur:(UIImage *)image
       completionBlock:(LBBlurredImageCompletionBlock)completion;
@end

NS_ASSUME_NONNULL_END
