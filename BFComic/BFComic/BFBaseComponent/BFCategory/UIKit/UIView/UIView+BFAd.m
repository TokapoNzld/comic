//
//  UIView+BFAd.m
//  BBCCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "UIView+BFAd.h"
#import "UILabel+BFAd.h"
#import "NSString+BFAd.h"
#import "UIImageView+BFAd.h"

#import "objc/runtime.h"
#import "UIView+WebCacheOperation.h"
#import "UIView+WebCache.h"
//#import "UIImage+BFAd.h"

#import "UIColor+BFAd.h"
#import "UIAlertController+BFAd.h"
#import <Masonry/Masonry.h>


#import "NSDate+BFAd.h"
#import "UIView+Toast.h"
#import "UIImage+BFAd.h"

@implementation UIView (BBCExtend)
- (void)bf_showToastWithMessage:(NSString *)message {
    [self bf_showToastWithMessage:message Duration:2.0 Position:[CSToastManager defaultPosition]];
}

///在窗口谈
+ (void)bf_windowToastWithMessage:(NSString *)message
{
    [UIApplication.sharedApplication.keyWindow bf_showToastWithMessage:message];
}

- (void)bf_showToastWithDuration:(CGFloat)duration Message:(NSString *)message {
    [self bf_showToastWithMessage:message Duration:duration Position:[CSToastManager defaultPosition]];
}

- (void)bf_showToastWithMessage:(NSString *)message Duration:(CGFloat)duration Position:(id)position {
    [self hideAllToasts];
    [self makeToast:message duration:duration position:position];
}

-(void)removeAllSubviews{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

+ (void)bf_windowToastWithSussceMessage:(NSString *)message{
    UIView * tmp = UIApplication.sharedApplication.keyWindow;
    [tmp hideAllToasts];
    
    UIView * toastView = ({
        UIView * view = UIView.new;
        view.frame = CGRectMake(0, 0, 150, 110);
        view.layer.cornerRadius = 12;
        view.clipsToBounds = true;
        view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.8];
        
        UIImage *img = [UIImage bf_imageNamed:@"toast_success" display:@"SLBase"];
        UIImageView * imgView = [[UIImageView alloc] initWithImage:img];
        [view addSubview:imgView];
        
        UILabel * titLbl = UILabel.new;
        titLbl.textColor = UIColor.whiteColor;
        titLbl.textAlignment = NSTextAlignmentCenter;
        titLbl.font = [UIFont systemFontOfSize:14];
        titLbl.text = message;
        [view addSubview:titLbl];

        {
            [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(@20.);
                make.width.height.equalTo(@38.);
                make.centerX.equalTo(@0.);
            }];
            
            [titLbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(@0.).inset(10);
                make.top.equalTo(imgView.mas_bottom).offset(16);
            }];
        }
        view;
    });
        
    [tmp showToast:toastView duration:3.0 position:CSToastPositionCenter completion:nil];
}



/**
 *  当前视图在父视图中的位置
 *
 */
-(int)getSubviewIndex
{
    return (int)[self.superview.subviews indexOfObject:self];
}

/**
 *  将视图置于父视图最上面
 */
-(void)bringToFront
{
    [self.superview bringSubviewToFront:self];
}
/**
 *  将视图置于父视图最下面
 */
-(void)sendToBack
{
    [self.superview sendSubviewToBack:self];
}

/**
 *  视图层次上移一层
 */
-(void)bringOneLevelUp
{
    int currentIndex = [self getSubviewIndex];
    [self.superview exchangeSubviewAtIndex:currentIndex withSubviewAtIndex:currentIndex+1];
}

/**
 *  视图层次下移一层
 */
-(void)sendOneLevelDown
{
    int currentIndex = [self getSubviewIndex];
    [self.superview exchangeSubviewAtIndex:currentIndex withSubviewAtIndex:currentIndex-1];
}

/**
 *  是否在最上面
 *
 */
-(BOOL)isInFront
{
    return ([self.superview.subviews lastObject]==self);
}

/**
 *  是否在最下面
 *
 */
-(BOOL)isAtBack
{
    return ([self.superview.subviews objectAtIndex:0]==self);
}

/**
 *  交换层次
 *
 *  @param swapView 目标视图
 */
-(void)swapDepthsWithView:(UIView*)swapView
{
    [self.superview exchangeSubviewAtIndex:[self getSubviewIndex] withSubviewAtIndex:[swapView getSubviewIndex]];
}

/**
 指定角为圆角

 @param corners 需要处理为圆角的角
 @param cornerRadius 圆角半径
 */
- (void)roundedByRoundingCorners:(UIRectCorner)corners cornerRadius:(CGFloat)cornerRadius {
    if (@available(iOS 11.0, *)) {
        self.layer.cornerRadius = cornerRadius;
        self.layer.maskedCorners = (CACornerMask)corners;
    } else {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.CGPath;
        self.layer.mask = maskLayer;
    }

}

#pragma mark - Count Down
- (void)bf_changeWithCountDown:(NSInteger)seconds countDownHandler:(void (^)(id sender, NSInteger second, BOOL finished))countDownHandler {
    //倒计时时间
    __block NSInteger timeOut = seconds;
    __block BOOL finished = NO;
    __weak typeof(self) weakSelf = self;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    //每秒执行一次
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0 * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_timer, ^{
        NSInteger second = timeOut;
        //倒计时结束，关闭
        if (timeOut <= 0) {
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                finished = YES;
                !countDownHandler?:countDownHandler(weakSelf, second, finished);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                !countDownHandler?:countDownHandler(weakSelf, second, finished);
            });
            timeOut--;
        }
    });
    dispatch_resume(_timer);
}

#pragma mark - 生成二维码
+ (UIImage *)createQRCodeWithTargetString:(NSString *)targetString logoImage:(UIImage *_Nonnull)logoImage {
    // 1.创建一个二维码滤镜实例
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    
    // 2.给滤镜添加数据
    NSString *targetStr = targetString;
    NSData *targetData = [targetStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [filter setValue:targetData forKey:@"inputMessage"];
    
    // 3.生成二维码
    CIImage *image = [filter outputImage];
    
    // 4.高清处理: size 要大于等于视图显示的尺寸
    UIImage *img = [self createNonInterpolatedUIImageFromCIImage:image size:[UIScreen mainScreen].bounds.size.width];
    
    //5.嵌入LOGO
    //5.1开启图形上下文
    UIGraphicsBeginImageContext(img.size);
    //5.2将二维码的LOGO画入
    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
    
    UIImage *centerImg = logoImage;
    CGFloat centerW = img.size.width*0.25;
    CGFloat centerH = centerW;
    CGFloat centerX = (img.size.width-centerW)*0.5;
    CGFloat centerY = (img.size.height -centerH)*0.5;
    [centerImg drawInRect:CGRectMake(centerX, centerY, centerW, centerH)];
    //5.3获取绘制好的图片
    UIImage *finalImg = UIGraphicsGetImageFromCurrentImageContext();
    //5.4关闭图像上下文
    UIGraphicsEndImageContext();

    //6.生成最终二维码
    return finalImg;
}

///高清处理
+ (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image size:(CGFloat)size {
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    // 1.创建bitmap
    size_t width = CGRectGetWidth(extent)*scale;
    size_t height = CGRectGetHeight(extent)*scale;
    
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    //2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    
    return [UIImage imageWithCGImage:scaledImage];
}

@end
