//
//  UIView+BFAd.h
//  BBCCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (BFAd)
/// 展示toast，默认：2s，现在在中间
- (void)bf_showToastWithMessage:(NSString *)message;

- (void)bf_showToastWithDuration:(CGFloat)duration Message:(NSString *)message;

- (void)bf_showToastWithMessage:(NSString *)message Duration:(CGFloat)duration Position:(id)position;

///在窗口谈
+ (void)bf_windowToastWithMessage:(NSString *)message;

///在窗口谈成功
+ (void)bf_windowToastWithSussceMessage:(NSString *)message;

-(int)getSubviewIndex;
-(void)bringToFront;
-(void)sendToBack;
-(void)bringOneLevelUp;
-(void)sendOneLevelDown;
-(BOOL)isInFront;
-(BOOL)isAtBack;
-(void)swapDepthsWithView:(UIView*)swapView;

-(void)removeAllSubviews;

/**
 指定角为圆角

 @param corners 需要处理为圆角的角
 @param cornerRadius 圆角半径
 */
- (void)roundedByRoundingCorners:(UIRectCorner)corners cornerRadius:(CGFloat)cornerRadius;

#pragma mark - Count Down
/**
 根据倒计时间及倒计回调处理, 改变View
 
 @param seconds 倒计时间
 @param countDownHandler 倒计回调处理(每秒执行一次回调, 当且仅当倒计结束, finished为YES)
 */
- (void)bf_changeWithCountDown:(NSInteger)seconds countDownHandler:(void (^)(id sender, NSInteger second, BOOL finished))countDownHandler;

#pragma mark - 生成二维码

/**
生成二维码
@param targetString  二维码链接
@param logoImage  中心小logo
 */
+ (UIImage *)createQRCodeWithTargetString:(NSString *)targetString logoImage:(UIImage *_Nonnull )logoImage;

@end

NS_ASSUME_NONNULL_END
