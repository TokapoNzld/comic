//
//  UITextView+BFAd.h
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^textViewHeightDidChangedBlock)(CGFloat currentTextViewHeight);

@interface UITextView (BFAd)


/** 限制字数*/
@property (nonatomic, assign) NSInteger bf_limitCount;
/** lab的右边距(默认10)*/
@property (nonatomic, assign) CGFloat bf_labMargin;
/** lab的高度(默认20)*/
@property (nonatomic, assign) CGFloat bf_labHeight;
/** 统计限制字数Label*/
@property (nonatomic, readonly) UILabel *bf_inputLimitLabel;


/* 占位文字 */
@property (nonatomic, copy) NSString *bf_placeholder;


/* 占位文字颜色 */
@property (nonatomic, strong) UIColor *bf_placeholderColor;


/* 最大高度，如果需要随文字改变高度的时候使用 */
@property (nonatomic, assign) CGFloat bf_maxHeight;


/* 最小高度，如果需要随文字改变高度的时候使用 */
@property (nonatomic, assign) CGFloat bf_minHeight;

@property (nonatomic, copy) textViewHeightDidChangedBlock bf_textViewHeightDidChanged;


/* 获取图片数组 */
- (NSArray *)bf_getImages;


/* 自动高度的方法，maxHeight：最大高度 */
- (void)bf_autoHeightWithMaxHeight:(CGFloat)maxHeight;


/* 自动高度的方法，maxHeight：最大高度， textHeightDidChanged：高度改变的时候调用 */
- (void)bf_autoHeightWithMaxHeight:(CGFloat)maxHeight textViewHeightDidChanged:(textViewHeightDidChangedBlock)textViewHeightDidChanged;


/* 添加一张图片 image:要添加的图片 */
- (void)bf_addImage:(UIImage *)image;


/* 添加一张图片 image:要添加的图片 size:图片大小 */
- (void)bf_addImage:(UIImage *)image size:(CGSize)size;


/* 插入一张图片 image:要添加的图片 size:图片大小 index:插入的位置 */
- (void)bf_insertImage:(UIImage *)image size:(CGSize)size index:(NSInteger)index;


/* 添加一张图片 image:要添加的图片 multiple:放大／缩小的倍数 */
- (void)bf_addImage:(UIImage *)image multiple:(CGFloat)multiple;


/* 插入一张图片 image:要添加的图片 multiple:放大／缩小的倍数 index:插入的位置 */
- (void)bf_insertImage:(UIImage *)image multiple:(CGFloat)multiple index:(NSInteger)index;


@end

NS_ASSUME_NONNULL_END
