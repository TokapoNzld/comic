//
//  UITextView+BFAd.m
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "UITextView+BFAd.h"
#import <objc/runtime.h>


static char limitCountKey;
static char labMarginKey;
static char labHeightKey;


// 占位文字
static const void *SLPlaceholderViewKey = &SLPlaceholderViewKey;
// 占位文字颜色
static const void *SLPlaceholderColorKey = &SLPlaceholderColorKey;
// 最大高度
static const void *SLTextViewMaxHeightKey = &SLTextViewMaxHeightKey;
// 最小高度
static const void *SLTextViewMinHeightKey = &SLTextViewMinHeightKey;
// 高度变化的block
static const void *SLTextViewHeightDidChangedBlockKey = &SLTextViewHeightDidChangedBlockKey;
// 存储添加的图片
static const void *SLTextViewImageArrayKey = &SLTextViewImageArrayKey;
// 存储最后一次改变高度后的值
static const void *SLTextViewbf_lastHeightKey = &SLTextViewbf_lastHeightKey;

@interface UITextView ()

// 存储添加的图片
@property (nonatomic, strong) NSMutableArray *bf_imageArray;
// 存储最后一次改变高度后的值
@property (nonatomic, assign) CGFloat bf_lastHeight;

@end

@implementation UITextView (BFAd)

+ (void)load {
    // 交换dealoc
    Method dealoc = class_getInstanceMethod(self.class, NSSelectorFromString(@"dealloc"));
    Method myDealloc = class_getInstanceMethod(self.class, @selector(myDealloc));
    method_exchangeImplementations(dealoc, myDealloc);
    
    method_exchangeImplementations(class_getInstanceMethod(self.class, NSSelectorFromString(@"layoutSubviews")),
                                   class_getInstanceMethod(self.class, @selector(bf_limitCounter_swizzling_layoutSubviews)));
    
}

- (void)myDealloc {
//    // 移除监听
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//
//    UITextView *placeholderView = objc_getAssociatedObject(self, SLPlaceholderViewKey);
//
//    // 如果有值才去调用，这步很重要
//    if (placeholderView) {
//        NSArray *propertys = @[@"frame", @"bounds", @"font", @"text", @"textAlignment", @"textContainerInset"];
//        for (NSString *property in propertys) {
//            @try {
//                [self removeObserver:self forKeyPath:property];
//            } @catch (NSException *exception) {}
//        }
//    }
//
//    @try {
//        [self removeObserver:self forKeyPath:@"layer.borderWidth"];
//        [self removeObserver:self forKeyPath:@"text"];
//
//    } @catch (NSException *exception) {
//
//    } @finally {
//
//    }
//
//    [self myDealloc];
}

#pragma mark - swizzled
- (void)bf_limitCounter_swizzling_layoutSubviews {
    [self bf_limitCounter_swizzling_layoutSubviews];
    if (self.bf_limitCount) {
        UIEdgeInsets textContainerInset = self.textContainerInset;
        textContainerInset.bottom = self.bf_labHeight;
        self.contentInset = textContainerInset;
        CGFloat x = CGRectGetMinX(self.frame)+self.layer.borderWidth;
        CGFloat y = CGRectGetMaxY(self.frame)-self.contentInset.bottom-self.layer.borderWidth;
        CGFloat width = CGRectGetWidth(self.bounds)-self.layer.borderWidth*2;
        CGFloat height = self.bf_labHeight;
        self.bf_inputLimitLabel.frame = CGRectMake(x, y, width, height);
        if ([self.superview.subviews containsObject:self.bf_inputLimitLabel]) {
            return;
        }
        [self.superview insertSubview:self.bf_inputLimitLabel aboveSubview:self];
    }
}
#pragma mark - associated
-(NSInteger)bf_limitCount{
    return [objc_getAssociatedObject(self, &limitCountKey) integerValue];
}
- (void)setBf_limitCount:(NSInteger)bf_limitCount{
    objc_setAssociatedObject(self, &limitCountKey, @(bf_limitCount), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self updateLimitCount];
}
-(CGFloat)bf_labMargin{
    return [objc_getAssociatedObject(self, &labMarginKey) floatValue];
}
-(void)setBf_labMargin:(CGFloat)bf_labMargin{
    objc_setAssociatedObject(self, &labMarginKey, @(bf_labMargin), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self updateLimitCount];
}
-(CGFloat)bf_labHeight{
    return [objc_getAssociatedObject(self, &labHeightKey) floatValue];
}
-(void)setBf_labHeight:(CGFloat)bf_labHeight{
    objc_setAssociatedObject(self, &labHeightKey, @(bf_labHeight), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self updateLimitCount];
}
#pragma mark -config
- (void)bf_configTextView{
    self.bf_labHeight = 20;
    self.bf_labMargin = 10;
}
#pragma mark - update
- (void)updateLimitCount{
    if (self.text.length > self.bf_limitCount  && self.bf_limitCount) {
        UITextRange *markedRange = [self markedTextRange];
        if (markedRange) {
            return;
        }
        NSRange range = [self.text rangeOfComposedCharacterSequenceAtIndex:self.bf_limitCount];
        self.text = [self.text substringToIndex:range.location];
    }
    NSString *showText = [NSString stringWithFormat:@"%lu/%ld",(unsigned long)self.text.length,(long)self.bf_limitCount];
    self.bf_inputLimitLabel.text = showText;
    NSMutableAttributedString *attrString = [[NSMutableAttributedString
                                              alloc] initWithString:showText];
    NSUInteger length = [showText length];
    NSMutableParagraphStyle *
    style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.tailIndent = -self.bf_labMargin; //设置与尾部的距离
    style.alignment = NSTextAlignmentRight;//靠右显示
    [attrString addAttribute:NSParagraphStyleAttributeName value:style
                       range:NSMakeRange(0, length)];
    self.bf_inputLimitLabel.attributedText = attrString;
}

#pragma mark - lazzing
-(UILabel *)bf_inputLimitLabel{
    UILabel *label = objc_getAssociatedObject(self, @selector(bf_inputLimitLabel));
    if (!label) {
        label = [[UILabel alloc] init];
        label.backgroundColor = self.backgroundColor;
        label.textColor = [UIColor lightGrayColor];
        label.textAlignment = NSTextAlignmentRight;
        objc_setAssociatedObject(self, @selector(bf_inputLimitLabel), label, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateLimitCount)
                                                     name:UITextViewTextDidChangeNotification
                                                   object:self];
        [self addObserver:self forKeyPath:@"layer.borderWidth" options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:@"text" options:NSKeyValueObservingOptionNew context:nil];
        [self bf_configTextView];
    }
    return label;
}


#pragma mark - set && get
- (UITextView *)bf_placeholderView {
    
    // 为了让占位文字和textView的实际文字位置能够完全一致，这里用UITextView
    UITextView *placeholderView = objc_getAssociatedObject(self, SLPlaceholderViewKey);
    
    if (!placeholderView) {
        
        // 初始化数组
        self.bf_imageArray = [NSMutableArray array];
        
        placeholderView = [[UITextView alloc] init];
        // 动态添加属性的本质是: 让对象的某个属性与值产生关联
        objc_setAssociatedObject(self, SLPlaceholderViewKey, placeholderView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        placeholderView = placeholderView;
        
        // 设置基本属性
        placeholderView.scrollEnabled = placeholderView.userInteractionEnabled = NO;
        //        self.scrollEnabled = placeholderView.scrollEnabled = placeholderView.showsHorizontalScrollIndicator = placeholderView.showsVerticalScrollIndicator = placeholderView.userInteractionEnabled = NO;
        placeholderView.textColor = [UIColor lightGrayColor];
        placeholderView.backgroundColor = [UIColor clearColor];
        [self refreshPlaceholderView];
        [self addSubview:placeholderView];
        
        // 监听文字改变
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewTextChange) name:UITextViewTextDidChangeNotification object:self];
        
        // 这些属性改变时，都要作出一定的改变，尽管已经监听了TextDidChange的通知，也要监听text属性，因为通知监听不到setText：
        NSArray *propertys = @[@"frame", @"bounds", @"font", @"text", @"textAlignment", @"textContainerInset"];
        
        // 监听属性
        for (NSString *property in propertys) {
            [self addObserver:self forKeyPath:property options:NSKeyValueObservingOptionNew context:nil];
        }
        
    }
    return placeholderView;
}

- (void)setBf_placeholder:(NSString *)bf_placeholder {
    // 为placeholder赋值
    [self bf_placeholderView].text = bf_placeholder;
}

- (NSString *)bf_placeholder
{
    // 如果有placeholder值才去调用，这步很重要
    if (self.bf_placeholderExist) {
        return [self bf_placeholderView].text;
    }
    return nil;
}

- (void)setBf_placeholderColor:(UIColor *)bf_placeholderColor
{
    // 如果有placeholder值才去调用，这步很重要
    if (!self.bf_placeholderExist) {
        NSLog(@"请先设置placeholder值！");
    } else {
        self.bf_placeholderView.textColor = bf_placeholderColor;
        
        // 动态添加属性的本质是: 让对象的某个属性与值产生关联
        objc_setAssociatedObject(self, SLPlaceholderColorKey, bf_placeholderColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

- (UIColor *)bf_placeholderColor
{
    return objc_getAssociatedObject(self, SLPlaceholderColorKey);
}

- (void)setBf_maxHeight:(CGFloat)bf_maxHeight
{
    CGFloat max = bf_maxHeight;
    
    // 如果传入的最大高度小于textView本身的高度，则让最大高度等于本身高度
    if (bf_maxHeight < self.frame.size.height) {
        max = self.frame.size.height;
    }
    
    objc_setAssociatedObject(self, SLTextViewMaxHeightKey, [NSString stringWithFormat:@"%lf", max], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CGFloat)bf_maxHeight
{
    return [objc_getAssociatedObject(self, SLTextViewMaxHeightKey) doubleValue];
}

- (void)setBf_minHeight:(CGFloat)bf_minHeight
{
    objc_setAssociatedObject(self, SLTextViewMinHeightKey, [NSString stringWithFormat:@"%lf", bf_minHeight], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CGFloat)bf_minHeight
{
    return [objc_getAssociatedObject(self, SLTextViewMinHeightKey) doubleValue];
}

- (void)setBf_textViewHeightDidChanged:(textViewHeightDidChangedBlock)bf_textViewHeightDidChanged
{
    objc_setAssociatedObject(self, SLTextViewHeightDidChangedBlockKey, bf_textViewHeightDidChanged, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (textViewHeightDidChangedBlock)bf_textViewHeightDidChanged
{
    void(^textViewHeightDidChanged)(CGFloat currentHeight) = objc_getAssociatedObject(self, SLTextViewHeightDidChangedBlockKey);
    return textViewHeightDidChanged;
}

- (NSArray *)bf_getImages
{
    return self.bf_imageArray;
}

- (void)setBf_lastHeight:(CGFloat)bf_lastHeight {
    objc_setAssociatedObject(self, SLTextViewbf_lastHeightKey, [NSString stringWithFormat:@"%lf", bf_lastHeight], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CGFloat)bf_lastHeight {
    return [objc_getAssociatedObject(self, SLTextViewbf_lastHeightKey) doubleValue];
}

- (void)setBf_imageArray:(NSMutableArray *)bf_imageArray {
    objc_setAssociatedObject(self, SLTextViewImageArrayKey, bf_imageArray, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableArray *)bf_imageArray {
    return objc_getAssociatedObject(self, SLTextViewImageArrayKey);
}

- (void)bf_autoHeightWithMaxHeight:(CGFloat)maxHeight
{
    [self bf_autoHeightWithMaxHeight:maxHeight textViewHeightDidChanged:nil];
}
// 是否启用自动高度，默认为NO
static bool autoHeight = NO;
- (void)bf_autoHeightWithMaxHeight:(CGFloat)maxHeight textViewHeightDidChanged:(textViewHeightDidChangedBlock)textViewHeightDidChanged
{
    autoHeight = YES;
    [self bf_placeholderView];
    self.bf_maxHeight = maxHeight;
    if (textViewHeightDidChanged) self.bf_textViewHeightDidChanged = textViewHeightDidChanged;
}

#pragma mark - addImage
/* 添加一张图片 */
- (void)bf_addImage:(UIImage *)image
{
    [self bf_addImage:image size:CGSizeZero];
}

/* 添加一张图片 image:要添加的图片 size:图片大小 */
- (void)bf_addImage:(UIImage *)image size:(CGSize)size
{
    [self bf_insertImage:image size:size index:self.attributedText.length > 0 ? self.attributedText.length : 0];
}

/* 插入一张图片 image:要添加的图片 size:图片大小 index:插入的位置 */
- (void)bf_insertImage:(UIImage *)image size:(CGSize)size index:(NSInteger)index
{
    [self bf_addImage:image size:size index:index multiple:-1];
}

/* 添加一张图片 image:要添加的图片 multiple:放大／缩小的倍数 */
- (void)bf_addImage:(UIImage *)image multiple:(CGFloat)multiple
{
    [self bf_addImage:image size:CGSizeZero index:self.attributedText.length > 0 ? self.attributedText.length : 0 multiple:multiple];
}

/* 插入一张图片 image:要添加的图片 multiple:放大／缩小的倍数 index:插入的位置 */
- (void)bf_insertImage:(UIImage *)image multiple:(CGFloat)multiple index:(NSInteger)index
{
    [self bf_addImage:image size:CGSizeZero index:index multiple:multiple];
}

/* 插入一张图片 image:要添加的图片 size:图片大小 index:插入的位置 multiple:放大／缩小的倍数 */
- (void)bf_addImage:(UIImage *)image size:(CGSize)size index:(NSInteger)index multiple:(CGFloat)multiple {
    if (image) [self.bf_imageArray addObject:image];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = image;
    CGRect bounds = textAttachment.bounds;
    if (!CGSizeEqualToSize(size, CGSizeZero)) {
        bounds.size = size;
        textAttachment.bounds = bounds;
    } else if (multiple <= 0) {
        CGFloat oldWidth = textAttachment.image.size.width;
        CGFloat scaleFactor = oldWidth / (self.frame.size.width - 10);
        textAttachment.image = [UIImage imageWithCGImage:textAttachment.image.CGImage scale:scaleFactor orientation:UIImageOrientationUp];
    } else {
        bounds.size = image.size;
        textAttachment.bounds = bounds;
    }
    
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    [attributedString replaceCharactersInRange:NSMakeRange(index, 0) withAttributedString:attrStringWithImage];
    self.attributedText = attributedString;
    [self textViewTextChange];
    [self refreshPlaceholderView];
}


#pragma mark - KVO监听属性改变
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [self refreshPlaceholderView];
    if ([keyPath isEqualToString:@"text"]) [self textViewTextChange];
    
    if ([keyPath isEqualToString:@"layer.borderWidth"]||
        [keyPath isEqualToString:@"text"]) {
        [self updateLimitCount];
    }
    
}

// 刷新PlaceholderView
- (void)refreshPlaceholderView {
    
    UITextView *placeholderView = objc_getAssociatedObject(self, SLPlaceholderViewKey);
    
    // 如果有值才去调用，这步很重要
    if (placeholderView) {
        self.bf_placeholderView.frame = self.bounds;
        if (self.bf_maxHeight < self.bounds.size.height) self.bf_maxHeight = self.bounds.size.height;
        self.bf_placeholderView.font = self.font;
        self.bf_placeholderView.textAlignment = self.textAlignment;
        self.bf_placeholderView.textContainerInset = self.textContainerInset;
        self.bf_placeholderView.hidden = (self.text.length > 0 && self.text);
    }
}

// 处理文字改变
- (void)textViewTextChange {
    UITextView *placeholderView = objc_getAssociatedObject(self, SLPlaceholderViewKey);
    
    // 如果有值才去调用，这步很重要
    if (placeholderView) {
        self.bf_placeholderView.hidden = (self.text.length > 0 && self.text);
    }
    // 如果没有启用自动高度，不执行以下方法
    if (!autoHeight) return;
    if (self.bf_maxHeight >= self.bounds.size.height) {
        
        // 计算高度
        NSInteger currentHeight = ceil([self sizeThatFits:CGSizeMake(self.bounds.size.width, MAXFLOAT)].height);
        
        // 如果高度有变化，调用block
        if (currentHeight != self.bf_lastHeight) {
            // 是否可以滚动
            self.scrollEnabled = currentHeight >= self.bf_maxHeight;
            CGFloat currentTextViewHeight = currentHeight >= self.bf_maxHeight ? self.bf_maxHeight : currentHeight;
            // 改变textView的高度
            if (currentTextViewHeight >= self.bf_minHeight) {
                CGRect frame = self.frame;
                frame.size.height = currentTextViewHeight;
                self.frame = frame;
                // 调用block
                if (self.bf_textViewHeightDidChanged) self.bf_textViewHeightDidChanged(currentTextViewHeight);
                // 记录当前高度
                self.bf_lastHeight = currentTextViewHeight;
            }
        }
    }
    
    if (!self.isFirstResponder) [self becomeFirstResponder];
}

// 判断是否有placeholder值，这步很重要
- (BOOL)bf_placeholderExist {
    
    // 获取对应属性的值
    UITextView *placeholderView = objc_getAssociatedObject(self, SLPlaceholderViewKey);
    
    // 如果有placeholder值
    if (placeholderView) return YES;
    
    return NO;
}
@end
