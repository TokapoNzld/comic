//
//  NSDate+BFAd.h
//  yios
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//
#import "NSDate+BFAd.h"

@implementation NSDate (BFAd)

#pragma mark - Data Properties
- (NSInteger)bf_year
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:self] year];
}

- (NSInteger)bf_month
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:self] month];
}

- (NSInteger)bf_day
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:self] day];
}

- (NSInteger)bf_hour
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:self] hour];
}

- (NSInteger)bf_minute
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitMinute fromDate:self] minute];
}

- (NSInteger)bf_second
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitSecond fromDate:self] second];
}

- (NSInteger)bf_nanosecond
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitSecond fromDate:self] nanosecond];
}

- (NSInteger)bf_weekday
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:self] weekday];
}

- (NSInteger)bf_weekdayOrdinal
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekdayOrdinal fromDate:self] weekdayOrdinal];
}

- (NSInteger)bf_weekOfMonth
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekOfMonth fromDate:self] weekOfMonth];
}

- (NSInteger)bf_weekOfYear
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekOfYear fromDate:self] weekOfYear];
}

- (NSInteger)bf_yearForWeekOfYear
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitYearForWeekOfYear fromDate:self] yearForWeekOfYear];
}

- (NSInteger)bf_quarter
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitQuarter fromDate:self] quarter];
}

#pragma mark - Comparison

- (BOOL)bf_isLeapMonth
{
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitQuarter fromDate:self] isLeapMonth];
}

- (BOOL)bf_isLeapYear
{
    NSUInteger year = self.bf_year;
    return ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0)));
}

- (BOOL)bf_isToday
{
    if (fabs(self.timeIntervalSinceNow) >= 60 * 60 * 24) return NO; // 日期与当前日期的时间差距
    return [NSDate new].bf_day == self.bf_day;
}

- (BOOL)bf_isYesterday
{
    NSDate *added = [self bf_dateByAddingDays:1];
    return [added bf_isToday]; // 昨天 + 1 = 今天
}

- (BOOL)bf_isTomorrow
{
    NSDate *added = [self bf_dateByAddingDays:-1];
    return [added bf_isToday]; // 明天 - 1 = 今天
}

- (BOOL)bf_isTheDayBeforeYesterday
{
    NSDate *added = [self bf_dateByAddingDays:2];
    return [added bf_isToday]; // 前天 + 2 = 今天
}

- (BOOL)bf_isTheDayAfterTomorrow
{
    NSDate *added = [self bf_dateByAddingDays:-2];
    return [added bf_isToday]; // 后天 - 2 = 今天
}

- (BOOL)bf_isThisYear {
    return [self bf_isYearEqualToDate:[NSDate date]];
}

- (BOOL)bf_isThisMonth {
    if (!self.bf_isThisYear) return  NO;
    
    return [self bf_isMonthEqualToDate:[NSDate date]];
}

- (BOOL)bf_isThisDay {
    if (fabs(self.timeIntervalSinceNow) >= 60 * 60 * 24) return NO; // 日期与当前日期的时间差距
    return [self bf_isDayEqualToDate:[NSDate new]];
}

- (BOOL)bf_isThisHour {
    if (fabs(self.timeIntervalSinceNow) >= 60 * 60) return NO;
    
    return [self bf_isHourEqualToDate:[NSDate new]];
}

- (BOOL)bf_isThisMinute {
    if (fabs(self.timeIntervalSinceNow) >= 60) return NO;
    
    return [self bf_isMinuteEqualToDate:[NSDate new]];
}

- (BOOL)bf_isThisSecond {
    if (fabs(self.timeIntervalSinceNow) >= 1) return NO;
    
    return [self bf_isSecondEqualToDate:[NSDate new]];
}

#pragma mark - Date Modify
- (NSDate *)bf_dateByAddingYears:(NSInteger)years
{
    NSCalendar *calendar =  [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:years];
    return [calendar dateByAddingComponents:components toDate:self options:0];
}

- (NSDate *)bf_dateByAddingMonths:(NSInteger)months
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setMonth:months];
    return [calendar dateByAddingComponents:components toDate:self options:0];
}

- (NSDate *)bf_dateByAddingWeeks:(NSInteger)weeks
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setWeekOfYear:weeks];
    return [calendar dateByAddingComponents:components toDate:self options:0];
}

- (NSDate *)bf_dateByAddingDays:(NSInteger)days
{
    NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + 86400 * days; // 86400 = 60 * 60 * 24, timeIntervalSinceReferenceDate -> 以2001/01/01 00:00:00 UTC为基准时间, 返回实例保存的时间与2001/01/01 00:00:00 UTC的时间间隔
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}

- (NSDate *)bf_dateByAddingHours:(NSInteger)hours
{
    NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + 3600 * hours;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}

- (NSDate *)bf_dateByAddingMinutes:(NSInteger)minutes
{
    NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + 60 * minutes;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}

- (NSDate *)bf_dateByAddingSeconds:(NSInteger)seconds
{
    NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + seconds;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}

#pragma mark - Date Format
- (NSString *)bf_stringWithFormat:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    [formatter setLocale:[NSLocale currentLocale]];
    return [formatter stringFromDate:self];
}

- (NSString *)bf_stringWithFormat:(NSString *)format timeZone:(NSTimeZone *)timeZone locale:(NSLocale *)locale
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    if (timeZone) [formatter setTimeZone:timeZone];
    if (locale) [formatter setLocale:locale];
    return [formatter stringFromDate:self];
}

- (NSString *)bf_stringWithISOFormat
{
    static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]; // Apple suggest
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    });
    return [formatter stringFromDate:self];
}

+ (NSDate *)bf_dateWithString:(NSString *)dateString format:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter dateFromString:dateString];
}

+ (NSDate *)bf_dateWithString:(NSString *)dateString format:(NSString *)format timeZone:(NSTimeZone *)timeZone locale:(NSLocale *)locale
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    if (timeZone) [formatter setTimeZone:timeZone];
    if (locale) [formatter setLocale:locale];
    return [formatter dateFromString:dateString];
}

+ (NSDate *)bf_dateWithISOFormatString:(NSString *)dateString
{
    static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    });
    return [formatter dateFromString:dateString];
}

#pragma mark - Equal
- (BOOL)bf_isEqualToDate:(NSDate *)other toUnitGranularity:(NSCalendarUnit)unit {
    if (!other) return NO;
    if (![other isKindOfClass:[NSDate class]]) return NO;
    
    if (unit & NSCalendarUnitYear) {
        if (self.bf_year != other.bf_year) return NO;
    }
    
    if (unit & NSCalendarUnitMonth) {
        if (self.bf_month != other.bf_month) return NO;
    }
    
    if (unit & NSCalendarUnitDay) {
        if (self.bf_day != other.bf_day) return NO;
    }
    
    if (unit & NSCalendarUnitHour) {
        if (self.bf_hour != other.bf_hour) return NO;
    }
    
    if (unit & NSCalendarUnitMinute) {
        if (self.bf_minute != other.bf_minute) return NO;
    }
    
    if (unit & NSCalendarUnitSecond) {
        if (self.bf_second != other.bf_second) return NO;
    }
    
    return YES;
}
- (BOOL)bf_isYearEqualToDate:(NSDate *)other {
    return [self bf_isEqualToDate:other toUnitGranularity:NSCalendarUnitYear];
}

- (BOOL)bf_isMonthEqualToDate:(NSDate *)other {
    return [self bf_isEqualToDate:other toUnitGranularity:NSCalendarUnitMonth];
}

- (BOOL)bf_isDayEqualToDate:(NSDate *)other {
    return [self bf_isEqualToDate:other toUnitGranularity:NSCalendarUnitDay];
}

- (BOOL)bf_isHourEqualToDate:(NSDate *)other {
    return [self bf_isEqualToDate:other toUnitGranularity:NSCalendarUnitHour];
}

- (BOOL)bf_isMinuteEqualToDate:(NSDate *)other {
    return [self bf_isEqualToDate:other toUnitGranularity:NSCalendarUnitMinute];
}

- (BOOL)bf_isSecondEqualToDate:(NSDate *)other {
    return [self bf_isEqualToDate:other toUnitGranularity:NSCalendarUnitSecond];
}

#pragma mark - Other
+ (void)bf_saveCurrentDate
{
    NSDate *nowDate = [NSDate date];
    NSUserDefaults *dataUser = [NSUserDefaults standardUserDefaults];
    [dataUser setObject:nowDate forKey:@"nowDate"];
    [dataUser synchronize];
}

+ (BOOL)bf_isSameDayWithNowDate
{
    NSDate *now = [NSDate date];
    NSDate *agoDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"nowDate"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *agoDateString = [dateFormatter stringFromDate:agoDate];
    NSString *nowDateString = [dateFormatter stringFromDate:now];
    return [agoDateString isEqualToString:nowDateString];
}


@end

