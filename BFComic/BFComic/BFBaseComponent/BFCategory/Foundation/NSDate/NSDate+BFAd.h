//
//  NSDate+BFAd.m
//  yios
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSDate (BFAd)

#pragma mark - Data Properties
@property (nonatomic, readonly) NSInteger bf_year;              ///< 年
@property (nonatomic, readonly) NSInteger bf_month;             ///< 月(1~12)
@property (nonatomic, readonly) NSInteger bf_day;               ///< 日(1~31)
@property (nonatomic, readonly) NSInteger bf_hour;              ///< 时(0~23)
@property (nonatomic, readonly) NSInteger bf_minute;            ///< 分(0~59)
@property (nonatomic, readonly) NSInteger bf_second;            ///< 秒(0~59)
@property (nonatomic, readonly) NSInteger bf_nanosecond;        ///< 毫微秒
@property (nonatomic, readonly) NSInteger bf_weekday;           ///< 星期(1~7, 星期首日由用户设置决定)
@property (nonatomic, readonly) NSInteger bf_weekdayOrdinal;    ///< 日期是本月的第几个星期几
@property (nonatomic, readonly) NSInteger bf_weekOfMonth;       ///< 日期是本月的第几周(一般周日为星期首日)
@property (nonatomic, readonly) NSInteger bf_weekOfYear;        ///< 日期是本年的第几周(1~53)
@property (nonatomic, readonly) NSInteger bf_yearForWeekOfYear; ///< 日期的weekOfYear中所属的年(ISO Week)
@property (nonatomic, readonly) NSInteger bf_quarter;           ///< 季度


#pragma mark - Comparison
@property (nonatomic, readonly) BOOL bf_isLeapMonth;                ///< 日期是否为闰月
@property (nonatomic, readonly) BOOL bf_isLeapYear;                 ///< 日期是否为闰年
@property (nonatomic, readonly) BOOL bf_isToday;                    ///< 日期是否今天(由日期使用locale决定)
@property (nonatomic, readonly) BOOL bf_isYesterday;                ///< 日期是否昨天(由日期使用locale决定)
@property (nonatomic, readonly) BOOL bf_isTomorrow;                 ///< 日期是否明天(由日期使用locale决定)
@property (nonatomic, readonly) BOOL bf_isTheDayBeforeYesterday;    ///< 日期是否前天(由日期使用locale决定)
@property (nonatomic, readonly) BOOL bf_isTheDayAfterTomorrow;      ///< 日期是否后天(由日期使用locale决定)

@property (nonatomic, readonly) BOOL bf_isThisYear;             ///< 日期是否是今年
@property (nonatomic, readonly) BOOL bf_isThisMonth;            ///< 日期是否是本月
@property (nonatomic, readonly) BOOL bf_isThisDay;              ///< 日期是否是今天(由日期使用locale决定)
@property (nonatomic, readonly) BOOL bf_isThisHour;             ///< 日期是否是当前小时内
@property (nonatomic, readonly) BOOL bf_isThisMinute;           ///< 日期是否是当前分钟内
@property (nonatomic, readonly) BOOL bf_isThisSecond;           ///< 日期是否是当前秒内


#pragma mark - Date Modify
/**
 *  获取日期增加指定年数后的新日期
 *
 *  @param years 指定年数
 */
- (NSDate *)bf_dateByAddingYears:(NSInteger)years;

/**
 *  获取日期增加指定月数后的新日期
 *
 *  @param months 指定月数
 */
- (NSDate *)bf_dateByAddingMonths:(NSInteger)months;

/**
 *  获取日期增加指定周数后的新日期
 *
 *  @param weeks 指定周数
 */
- (NSDate *)bf_dateByAddingWeeks:(NSInteger)weeks;

/**
 *  获取日期增加指定天数后的新日期
 *
 *  @param days 指定天数
 */
- (NSDate *)bf_dateByAddingDays:(NSInteger)days;

/**
 *  获取日期增加指定小时数后的新日期
 *
 *  @param hours 指定小时数
 */
- (NSDate *)bf_dateByAddingHours:(NSInteger)hours;

/**
 *  获取日期增加指定分钟数后的新日期
 *
 *  @param minutes 指定分钟数
 */
- (NSDate *)bf_dateByAddingMinutes:(NSInteger)minutes;

/**
 *  获取日期增加指定秒数后的新日期
 *
 *  @param seconds 指定秒数
 */
- (NSDate *)bf_dateByAddingSeconds:(NSInteger)seconds;

#pragma mark - Date Format
/**
 *  根据日期格式字符串, 获取日期字符串
 *
 *  @param format 日期格式字符串(@"yyyy-MM-dd HH:mm:ss"...)
 */
- (NSString *)bf_stringWithFormat:(NSString *)format;

/**
 *  根据日期格式字符串、目标时区和目标local, 获取日期字符串
 *
 *  @param format   日期格式字符串
 *  @param timeZone 目标时区
 *  @param locale   目标local
 */
- (NSString *)bf_stringWithFormat:(NSString *)format timeZone:(NSTimeZone *)timeZone locale:(NSLocale *)locale;

/**
 *  根据ISO8601日期格式("2010-07-09T16:13:30+12:00"), 获取日期字符串
 */
- (NSString *)bf_stringWithISOFormat;

/**
 *  根据日期字符串和日期格式字符串, 获取日期
 *
 *  @param dateString 日期字符串
 *  @param format     日期格式字符串
 */
+ (NSDate *)bf_dateWithString:(NSString *)dateString format:(NSString *)format;

/**
 *  根据日期字符串、日期格式字符串、目标时区和目标local, 获取日期
 *
 *  @param dateString 日期字符串
 *  @param format     日期格式字符串
 *  @param timeZone   目标时区(可为nil)
 *  @param locale     目标local(可为nil)
 */
+ (NSDate *)bf_dateWithString:(NSString *)dateString format:(NSString *)format timeZone:(NSTimeZone *)timeZone locale:(NSLocale *)locale;

/**
 *  根据ISO8601日期格式的日期字符串("2010-07-09T16:13:30+08:00"), 获取日期
 */
+ (NSDate *)bf_dateWithISOFormatString:(NSString *)dateString;


#pragma mark - Equal
/**
 根据日期匹配项, 判断日期是否与指定日期相等(简单匹配)

 @param other 指定日期
 @param unit  日期匹配项
 @return 相等返回YES, 否则返回NO
 */
- (BOOL)bf_isEqualToDate:(NSDate *)other toUnitGranularity:(NSCalendarUnit)unit;

/**
 日期的年是否与指定日期相等

 @param other 指定日期
 @return 相等返回YES, 否则返回NO
 */
- (BOOL)bf_isYearEqualToDate:(NSDate *)other;

/**
 日期的月是否与指定日期相等

 @param other 指定日期
 @return 相等返回YES, 否则返回NO
 */
- (BOOL)bf_isMonthEqualToDate:(NSDate *)other;

/**
 日期的日是否与指定日期相等

 @param other 指定日期
 @return 相等返回YES, 否则返回NO
 */
- (BOOL)bf_isDayEqualToDate:(NSDate *)other;

/**
 日期的时是否与指定日期相等

 @param other 指定日期
 @return 相等返回YES, 否则返回NO
 */
- (BOOL)bf_isHourEqualToDate:(NSDate *)other;

/**
 日期的分是否与指定日期相等

 @param other 指定日期
 @return 相等返回YES, 否则返回NO
 */
- (BOOL)bf_isMinuteEqualToDate:(NSDate *)other;

/**
 日期的秒是否与指定日期相等

 @param other 指定日期
 @return 相等返回YES, 否则返回NO
 */
- (BOOL)bf_isSecondEqualToDate:(NSDate *)other;


#pragma mark - Ohter
/**
 *  保存当前日期
 */
+ (void)bf_saveCurrentDate;

/**
 *  判断易记录的日期是否与当前日期相同
 */
+ (BOOL)bf_isSameDayWithNowDate;


@end
