//
//  NSArray+BFAd.m
//  
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "NSArray+BFAd.h"

@implementation NSArray (BFAd)
- (id)bf_safeObjectAtIndex:(NSUInteger)index{
    if(self.count == 0) {
        NSLog(@"--- mutableArray have no objects ---");
        return (nil);
    }
    if(index > MAX(self.count - 1, 0)) {
        NSLog(@"--- index:%li out of mutableArray range ---", (long)index);
        return (nil);
    }
    return ([self objectAtIndex:index]);
}
@end
