//
//  NSArray+BFAd.h
//  
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (BFAd)
- (id)bf_safeObjectAtIndex:(NSUInteger)index;

@end
