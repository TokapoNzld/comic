//
//  NSURL+BFAd.m
//  NSURL
//
//  Created by majunwen on 2022/1/17.
//

#import "NSURL+BFAd.h"
#import <objc/runtime.h>

@implementation NSURL (BFAd)

+ (void)load
{
    // 交换方法(URLWithString:)
    method_exchangeImplementations(class_getInstanceMethod(object_getClass((id)self), @selector(URLWithString:)),class_getInstanceMethod(object_getClass((id)self), @selector(bf_URLWithString:)));
}

+ (nullable instancetype)bf_URLWithString:(NSString *)string{
    if ([self isContainChinese:string]) {
        NSCharacterSet *characterSet = [NSCharacterSet URLFragmentAllowedCharacterSet];
        string = [string stringByAddingPercentEncodingWithAllowedCharacters:characterSet];
    }
    return [self bf_URLWithString:string];
}


+ (BOOL)isContainChinese:(NSString *)str
{
    for(int i = 0; i < [str length];i++)
    {
        int a = [str characterAtIndex:i];
        if( a > 0x4E00 && a < 0x9FFF)
        {
            return YES;
        }
    }
    return NO;
}

@end
