//
//  NSMutableAttributedString+BFAd.m
//  Pods
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "NSMutableAttributedString+BFAd.h"

@implementation NSMutableAttributedString (BFAd)

#pragma mark - 设置字符串字体 颜色

- (void)bf_attributedStringSetFont:(UIFont *)font range:(NSRange)range {
    NSInteger location = range.location;
    NSInteger length = range.length;
    if (location > self.string.length || self.length - location < length) {
        return;
    }
    [self addAttributes:@{NSFontAttributeName: font} range:range];
}
- (void)bf_attributedStringSetColor:(UIColor *)color range:(NSRange)range {
    NSInteger location = range.location;
    NSInteger length = range.length;
    if (location > self.string.length || self.length - location < length) {
        return;
    }
    [self addAttributes:@{NSForegroundColorAttributeName : color} range:range];
}

@end
