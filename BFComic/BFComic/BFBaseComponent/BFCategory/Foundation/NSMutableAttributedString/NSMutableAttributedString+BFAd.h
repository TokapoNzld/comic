//
//  NSMutableAttributedString+BFAd.h
//  Pods
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableAttributedString (BFAd)

/// 设置字体
- (void)bf_attributedStringSetFont:(UIFont *)font range:(NSRange)range;

/// 设置颜色
- (void)bf_attributedStringSetColor:(UIColor *)color range:(NSRange)range;

@end

NS_ASSUME_NONNULL_END
