//
//  NSString+BFAd.h
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface NSString (BFAd)

/**
 日期格式

 - dateFormatterTypeToyyyyMMddSymbol: 2018-02-08
 - dateFormatterTypeToyyyyMMddChinese: 2018年02月08日
 - dateFormatterTypeTohhmmssSymbol: 16:03:02
 - dateFormatterTypeTohhmmssChinese: 16时03分02秒
 - dateFormatterTypeToyyyyMMddhhmmssSymbol: 2018-02-08 16:03:02
 - dateFormatterTypeToyyyyMMddhhmmssChinese: 2018年02月08日 16时03分02秒
 - dateFormatterTypeToCustom: 自定义
 */
typedef NS_ENUM(NSInteger, dateFormatterType) {

  dateFormatterTypeToyyyyMMddSymbol,

  dateFormatterTypeToyyyyMMddChinese,

  dateFormatterTypeTohhmmssSymbol,

  dateFormatterTypeTohhmmssChinese,

  dateFormatterTypeToyyyyMMddhhmmssSymbol,

  dateFormatterTypeToyyyyMMddhhmmssChinese,

  dateFormatterTypeToCustom,
};

//================================== 字符串大小、长度 ==============================================================
- (CGSize)bf_sizeWithFont:(UIFont *)font maxW:(CGFloat)maxW;
- (CGSize)bf_sizeWithFont:(UIFont *)font;

- (CGSize)bf_sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;
- (CGSize)bf_sizeWithLabelWidth:(CGFloat)width font:(UIFont *)font;

- (CGSize)bf_sizeSpacingWithFont:(UIFont *)font maxW:(CGFloat)maxW;

+ (NSString *)bf_makeUrlStr:(NSString *)url;

//================================ ================================ ================================
- (CGFloat)bf_heightWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width withLineSpace:(CGFloat)lineSpace;
/**
 *  @brief 计算文字的高度
 *
 *  @param font  字体(默认为系统字体)
 *  @param width 约束宽度
 */
- (CGFloat)bf_heightWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width;
/**
 *  @brief 计算文字的宽度
 *
 *  @param font   字体(默认为系统字体)
 *  @param height 约束高度
 */
- (CGFloat)bf_widthWithFont:(UIFont *)font constrainedToHeight:(CGFloat)height;

/**
 *  @brief 计算文字的大小
 *
 *  @param font  字体(默认为系统字体)
 *  @param width 约束宽度
 */
- (CGSize)bf_sizeWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width;
/**
 *  @brief 计算文字的大小
 *
 *  @param font   字体(默认为系统字体)
 *  @param height 约束高度
 */
- (CGSize)bf_sizeWithFont:(UIFont *)font constrainedToHeight:(CGFloat)height;

+ (NSMutableAttributedString *)bf_getAttributedWithString:(NSString *)string WithLineSpace:(CGFloat)lineSpace font:(UIFont *)font;

- (NSMutableAttributedString *)bf_colorStr:(NSString *)colorStr withColor:(UIColor *)color font:(NSInteger)font line:(CGFloat)lineSpace;

/**  将一个字符串中的特定字符添加颜色 */
- (NSMutableAttributedString *)bf_colorStr:(NSString *)colorStr withColor:(UIColor *)color font:(NSInteger)font;

- (NSMutableAttributedString *)bf_colorStrArr:(NSArray *)colorStrArr withColor:(UIColor *)color font:(NSInteger)font;

- (NSMutableAttributedString *)bf_colorStr:(NSString *)colorStr withColor:(UIColor *)color font:(NSInteger)font otherColor:(UIColor *)otherColor;
/**  将一个字符串中的特定字符改变大小 */
- (NSMutableAttributedString *)bf_fontStr:(NSString *)colorStr font:(NSInteger)font;

- (NSString *)bf_getDateFromFormat:(NSString *)fromFormat toFormat:(NSString *)toFormat;

- (NSMutableAttributedString *)bf_colorStr:(NSString *)colorStr withColor:(UIColor *)color customFont:(NSInteger)font;

/**
 *  @brief 利用正则给字符串中的数字加颜色
 *
 *  @param origiColor   原始颜色
 *  @param purposeColor 目标颜色
 */
//- (NSMutableAttributedString *)bf_getTheAttriFromOriginalFontColor:(UIColor *)origiColor purposeColor:(UIColor *)purposeColor withFont:(NSInteger)fontSie;

+ (NSString *)bf_stringJsonValue:(id)JsonValue;

/** 求行数*/
+ (NSUInteger)bf_lineCountWithAtttributeString:(NSAttributedString *)attrStr size:(CGSize)size;

/** 根据要求的位数展示千分分隔符*/
- (NSString *)bf_seperatorTheStringByDemandLength:(NSInteger)demandLength;

/** 移除字符串尾部的叹号*/
- (NSString *)bf_removeTheStringInnerLastGanTanHao;

///整数不展示小数点
- (NSString *)bf_handleTheMoneyLastZeroString;

///处理两个数据中的小数据
- (NSString *)bf_copareTheMinInTheSecond:(NSString *)secnondStr;

- (NSAttributedString *)bf_setTheKernSpaceAndColor:(UIColor *)foreColor;

///根据经纬度求出城市名字
//+ (void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude;
///根据城市名字求出城市ID
//+ (NSString *)bf_getThtCityIDByTheCityName:(NSString *)cityName;

/** 格式化银行卡号  4113 2331 1991 0112 */
- (NSString *)bf_bankFormat;

/** 处理后台返回的不规则时间 */
- (NSString *)bf_mydataFormat;

//检查字符串中包含有多少个字符
+ (NSInteger)bf_checkTheCharCountOfString:(NSString *)str;

/// 价格字符串格式化
- (NSString *)bf_reviseMoneyStringToDecNumberString;
//================================ ================================ ================================

+ (NSString *)bf_magenageAbsoluteTime:(NSString *)timeStamp isDate:(BOOL)isDate;
- (NSString *)bf_IDcard_no:(NSString *)card_no;
+ (NSString *)bf_IDcard_no:(NSString *)card_no frontNo:(NSInteger)f lastNo:(NSInteger)l;
- (NSString *)bf_htmlImageAdaptWith:(long)width;
- (NSString *)bf_htmlImageAdaptWith:(long)width withHeight:(long)height;
//================================ ================================ ================================
- (BOOL)bf_containsString:(NSString *)string;
- (NSString *)bf_reformatPhoneNumber;
- (BOOL)bf_validateEmail;
- (BOOL)bf_validatePhone;
- (NSString *)bf_trim;

//================================ ================================ ================================
/**
 秒转换时间

 @param num 秒数
 @param dataFormatter 枚举的日期格式
 @param CustomDataFormatter 自定义日期格式  没有传@""
 @return 字符的时间
 */
+ (NSString *)bf_convertTime:(NSInteger)num withDataFormat:(dateFormatterType)dataFormatter withCustomDataFormatter:(NSString *)CustomDataFormatter;

//====================================== 字符串判断 ==========================================================
/// 添加划线(价格)
- (NSMutableAttributedString *)bf_PCAddUnderLine;

/**
 是否为手机号
 */
+ (BOOL)bf_isPhoneNumber:(NSString *)str;
/**
 是否为邮箱
 */
+ (BOOL)bf_isEmail:(NSString *)str;
/**
 正则匹配用户密码6-20位数字、字母和下划线三选二组合
 */
+ (BOOL)bf_isPassword:(NSString *)str;
/**
 银行卡正则
 */
+ (BOOL)bf_IsBankCard:(NSString *)cardNumber;
/**
 是否全是空格
 */
+ (BOOL)bf_isEmpty:(NSString *)str;
/**
 是否有效数字
 */
+ (BOOL)bf_validateNumber:(NSString *)number;
///  追加文档目录
- (NSString *)bf_appendDocumentPath;
///  追加缓存目录
- (NSString *)bf_appendCachePath;
///  追加临时目录
- (NSString *)bf_appendTempPath;
/**
 根据左边和右边的字符串,获得中间特定字符串
 @param strLeft 左边匹配字符串
 @param strRight 右边匹配的字符串
 */
- (NSString *)bf_substringWithinBoundsLeft:(NSString *)strLeft right:(NSString *)strRight;

/**
 字符串反转

 @param str 要反转的字符串
 @return 反转之后的字符串
 */
- (NSString *)bf_reverseWordsInString:(NSString *)str;

/** 判断URL中是否包含中文 */
- (BOOL)bf_isContainChinese;

/** 获取字符数量 */
- (int)bf_wordsCount;

/** JSON字符串转成NSDictionary */
- (NSDictionary *)bf_dictionaryValue;

/**
 *  手机号码的有效性:分电信、联通、移动和小灵通
 */
- (BOOL)bf_isMobileNumberClassification;
/**
 *  手机号有效性
 */
- (BOOL)bf_isMobileNumber;

/**
 *  邮箱的有效性
 */
- (BOOL)bf_isEmailAddress;

/**
 *  精确的身份证号码有效性检测
 *
 *  @param value 身份证号
 */
+ (BOOL)bf_accurateVerifyIDCardNumber:(NSString *)value;

/**
 *  车牌号的有效性
 */
- (BOOL)bf_isCarNumber;

/**
 *  银行卡的有效性
 */
- (BOOL)bf_bankCardluhmCheck;

/**
 *  IP地址有效性
 */
- (BOOL)bf_isIPAddress;

/**
 *  Mac地址有效性
 */
- (BOOL)bf_isMacAddress;

/**
 *  网址有效性
 */
- (BOOL)bf_isValidUrl;

/**
 *  纯汉字
 */
- (BOOL)bf_isValidChinese;

/**
 *  邮政编码
 */
- (BOOL)bf_isValidPostalcode;

/**
 *  工商税号
 */
- (BOOL)bf_isValidTaxNo;

/** 清除html标签 */
- (NSString *)bf_stringByStrippingHTML;

/** 清除js脚本 */
- (NSString *)bf_stringByRemovingScriptsAndStrippingHTML;

/** 去除空格 */
- (NSString *)bf_trimmingWhitespace;

/** 去除空格与空行 */
//- (NSString *)bf_trimmingitespaceAndNewlines;

/** 加密 */
- (NSString *)bf_toMD5;
- (NSString *)bf_to16MD5;
- (NSString *)bf_sha1;
- (NSString *)bf_sha256;
- (NSString *)bf_sha512;

#pragma mark - Data convert to string or string to data.
/**
 *    string与Data转化
 */
- (NSData *)bf_toData;
+ (NSString *)bf_toStringWithData:(NSData *)data;

#pragma mark - Date convert to string
///date转string
+ (NSString *)bf_toStringWithDate:(NSDate *)date dateFormat:(NSString *)dateFormat;

/// 秒字符串转时分秒：hh:mm:ss
- (NSString *)bf_stringConvertToTime;

///获取当前时间
- (NSString *)bf_dateFormatToNowTime;

/**

 @return 大于等于1km显示xxkm  小于xxm
 */
- (NSString *)bf_distanceValue;

/**
 @return 保持2位有效小数
 */
- (NSString *)bf_priceValue;

/**
 @return 大于等于1万显示xx万  小于xx
 */
- (NSString *)bf_commentsValue;

/** nil则返回空字符串：@"" */
extern NSString *safeString(NSString *str);

NSString *safeZeroString(NSString *str);

extern NSString *safeZero(NSString *str);

NSString *safeToDayString(NSString *str);

NSString *safeHideZeroString(NSString *str);

-(CGFloat)widthForFont:(UIFont *)font;

@end

NS_ASSUME_NONNULL_END
