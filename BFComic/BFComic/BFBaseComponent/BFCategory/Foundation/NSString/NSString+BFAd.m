//
//  NSString+BFAd.m
//  SLCategory
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "NSString+BFAd.h"

#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreText/CoreText.h>

//#import "NSData+YYAdd.h"

// key跟后台协商一个即可，保持一致
static NSString *const PSW_AES_KEY = @"0123456789abcSL";
// 这里的偏移量也需要跟后台一致，一般跟key一样就行
static NSString *const AES_IV_PARAMETER = @"0123456789abcSL";

@implementation NSString (BFAd)

- (NSMutableAttributedString *)bf_PCAddUnderLine {

  NSString *string = self;
  if (string.length == 0) {
    return nil;
  }

  NSDictionary *attribtDic = @{ NSStrikethroughStyleAttributeName : [NSNumber numberWithInteger:NSUnderlineStyleSingle] };
  NSMutableAttributedString *attribtString = [[NSMutableAttributedString alloc] initWithString:string attributes:attribtDic];
  return attribtString;
}

- (CGSize)bf_sizeWithFont:(UIFont *)font maxW:(CGFloat)maxW {
  return [self bf_sizeWithFont:font maxSize:CGSizeMake(maxW, MAXFLOAT)];
}

//返回字符串所占用的尺寸.
- (CGSize)bf_sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize {
  NSDictionary *attrs = @{ NSFontAttributeName : font };
  return [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (CGSize)bf_sizeWithFont:(UIFont *)font {
  return [self bf_sizeWithFont:font maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
}

- (CGSize)bf_sizeSpacingWithFont:(UIFont *)font maxW:(CGFloat)maxW {
  NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
  paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
  paraStyle.alignment = NSTextAlignmentLeft;
  paraStyle.lineSpacing = 4;  //设置行间距
  paraStyle.hyphenationFactor = 1.0;
  paraStyle.firstLineHeadIndent = 0.0;
  paraStyle.paragraphSpacingBefore = 0.0;
  paraStyle.headIndent = 0;
  paraStyle.tailIndent = 0;
  //设置字间距 NSKernAttributeName:@1.5f
  NSDictionary *dic = @{ NSFontAttributeName : font, NSParagraphStyleAttributeName : paraStyle, NSKernAttributeName : @1.5f };

  CGSize maxSize = CGSizeMake(maxW, MAXFLOAT);
  return [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
}

+ (NSString *)bf_StringWithUUID {
  CFUUIDRef uuidObj = CFUUIDCreate(nil);
  NSString *uuidString = (NSString *)CFBridgingRelease(CFUUIDCreateString(nil, uuidObj));
  CFRelease(uuidObj);
  //    NSLog(@"原始 GUID:%@",uuidString);
  uuidString = [uuidString stringByReplacingOccurrencesOfString:@"-" withString:@""];
  //    NSLog(@"最终 GUID:%@",uuidString);
  return uuidString;
}

- (CGSize)bf_sizeWithLabelWidth:(CGFloat)width font:(UIFont *)font {
  NSDictionary *dict = @{ NSFontAttributeName : font };
  CGRect rect = [self boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:(NSStringDrawingUsesLineFragmentOrigin)attributes:dict context:nil];
  CGFloat sizeWidth = ceilf(CGRectGetWidth(rect));
  CGFloat sizeHieght = ceilf(CGRectGetHeight(rect));
  return CGSizeMake(sizeWidth, sizeHieght);
}

+ (NSString *)bf_makeUrlStr:(NSString *)url {
  url = [NSString stringWithFormat:@"%@", url];
  NSCharacterSet *encodeUrlSet = [NSCharacterSet URLQueryAllowedCharacterSet];
  url = [url stringByAddingPercentEncodingWithAllowedCharacters:encodeUrlSet];

  if (![url hasPrefix:@"http://"] && ![url hasPrefix:@"https://"]) {
#ifndef kImgUrl
#define kImgUrl @""
    url = [NSString stringWithFormat:@"%@%@", kImgUrl, url];
#endif
  }
  return url;
}

- (CGFloat)bf_heightWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width withLineSpace:(CGFloat)lineSpace {
  UIFont *textFont = font ? font : [UIFont systemFontOfSize:[UIFont systemFontSize]];

  CGSize textSize;

#if __IPHONE_OS_VERSION_MIN_REQUIRED < 70000
  if ([self respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;

    if (lineSpace) {
      paragraph.lineSpacing = lineSpace;
    }

    NSDictionary *attributes = @{ NSFontAttributeName : textFont, NSParagraphStyleAttributeName : paragraph };
    textSize =
      [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine) attributes:attributes context:nil].size;
  }
  else {
    textSize = [self sizeWithFont:textFont constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
  }
#else
  NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
  paragraph.lineBreakMode = NSLineBreakByWordWrapping;
  if (lineSpace) {
    paragraph.lineSpacing = lineSpace;
  }
  NSDictionary *attributes = @{ NSFontAttributeName : textFont, NSParagraphStyleAttributeName : paragraph };
  textSize =
    [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine) attributes:attributes context:nil].size;
#endif

  return ceil(textSize.height);
}

/**
 *  @brief 计算文字的高度
 *
 *  @param font  字体(默认为系统字体)
 *  @param width 约束宽度
 */
- (CGFloat)bf_heightWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width {
  return [self bf_sizeWithFont:font constrainedToWidth:width].height;
}

/**
 *  @brief 计算文字的宽度
 *
 *  @param font   字体(默认为系统字体)
 *  @param height 约束高度
 */
- (CGFloat)bf_widthWithFont:(UIFont *)font constrainedToHeight:(CGFloat)height {
  return [self bf_sizeWithFont:font constrainedToHeight:height].width;
}

/**
 *  @brief 计算文字的大小
 *
 *  @param font  字体(默认为系统字体)
 *  @param width 约束宽度
 */
- (CGSize)bf_sizeWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width {
  UIFont *textFont = font ? font : [UIFont systemFontOfSize:[UIFont systemFontSize]];

  CGSize textSize;

#if __IPHONE_OS_VERSION_MIN_REQUIRED < 70000
  if ([self respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSFontAttributeName : textFont, NSParagraphStyleAttributeName : paragraph };
    textSize =
      [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine) attributes:attributes context:nil].size;
  }
  else {
    textSize = [self sizeWithFont:textFont constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
  }
#else
  NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
  paragraph.lineBreakMode = NSLineBreakByWordWrapping;
  NSDictionary *attributes = @{ NSFontAttributeName : textFont, NSParagraphStyleAttributeName : paragraph };
  textSize =
    [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine) attributes:attributes context:nil].size;
#endif

  return CGSizeMake(ceil(textSize.width), ceil(textSize.height));
}

/**
 *  @brief 计算文字的大小
 *
 *  @param font   字体(默认为系统字体)
 *  @param height 约束高度
 */
- (CGSize)bf_sizeWithFont:(UIFont *)font constrainedToHeight:(CGFloat)height {
  UIFont *textFont = font ? font : [UIFont systemFontOfSize:[UIFont systemFontSize]];

  CGSize textSize;

#if __IPHONE_OS_VERSION_MIN_REQUIRED < 70000
  if ([self respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSFontAttributeName : textFont, NSParagraphStyleAttributeName : paragraph };
    textSize =
      [self boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine) attributes:attributes context:nil].size;
  }
  else {
    textSize = [self sizeWithFont:textFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, height) lineBreakMode:NSLineBreakByWordWrapping];
  }
#else
  NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
  paragraph.lineBreakMode = NSLineBreakByWordWrapping;
  NSDictionary *attributes = @{ NSFontAttributeName : textFont, NSParagraphStyleAttributeName : paragraph };
  textSize =
    [self boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine) attributes:attributes context:nil].size;
#endif

  return CGSizeMake(ceil(textSize.width), ceil(textSize.height));
}

+ (NSMutableAttributedString *)bf_getAttributedWithString:(NSString *)string WithLineSpace:(CGFloat)lineSpace font:(UIFont *)font {

  NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
  //调整行间距
  paragraphStyle.lineSpacing = lineSpace;
  NSDictionary *attriDict = @{ NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : font };
  NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string attributes:attriDict];
  return attributedString;
}

- (NSMutableAttributedString *)bf_colorStr:(NSString *)colorStr withColor:(UIColor *)color font:(NSInteger)font line:(CGFloat)lineSpace {

  NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:self];

  if (lineSpace > 0) {
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    //调整行间距
    paragraphStyle.lineSpacing = lineSpace;
    NSDictionary *attriDict = @{ NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : [UIFont systemFontOfSize:font] };

    attribute = [[NSMutableAttributedString alloc] initWithString:self attributes:attriDict];
  }

  NSRange range = [self rangeOfString:colorStr];
  NSMutableDictionary *dic = [NSMutableDictionary dictionary];
  dic[ NSForegroundColorAttributeName ] = color;

  if (font > 0) {
    dic[ NSFontAttributeName ] = [UIFont systemFontOfSize:font];
  }

  [attribute addAttributes:dic range:NSMakeRange(range.location, range.length)];

  return attribute;
}

/**  将一个字符串中的特定字符添加颜色 */
- (NSMutableAttributedString *)bf_colorStr:(NSString *)colorStr withColor:(UIColor *)color font:(NSInteger)font {
  return [self bf_colorStr:colorStr withColor:color font:font line:0];
}

- (NSMutableAttributedString *)bf_colorStr:(NSString *)colorStr withColor:(UIColor *)color font:(NSInteger)font otherColor:(UIColor *)otherColor {

  NSRange range = [self rangeOfString:colorStr];
  NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:self];

  NSMutableDictionary *dic = [NSMutableDictionary dictionary];

  if (otherColor) {
    dic[ NSForegroundColorAttributeName ] = otherColor;
    [attribute addAttributes:dic range:NSMakeRange(0, self.length)];
  }

  dic[ NSForegroundColorAttributeName ] = color;

  if (font > 0) {
    dic[ NSFontAttributeName ] = [UIFont systemFontOfSize:font];
  }
  [attribute addAttributes:dic range:NSMakeRange(range.location, range.length)];

  return attribute;
}

- (NSMutableAttributedString *)bf_colorStr:(NSString *)colorStr withColor:(UIColor *)color customFont:(NSInteger)font {
  return [self bf_colorStr:colorStr withColor:color font:font otherColor:nil];
}

- (NSMutableAttributedString *)bf_colorStrArr:(NSArray *)colorStrArr withColor:(UIColor *)color font:(NSInteger)font {

  NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:self];
  NSMutableDictionary *dic = [NSMutableDictionary dictionary];
  dic[ NSForegroundColorAttributeName ] = color;

  if (font > 0) {
    dic[ NSFontAttributeName ] = [UIFont systemFontOfSize:font];
  }

  for (int i = 0; i < colorStrArr.count; i++) {

    NSRange range = [self rangeOfString:colorStrArr[ i ]];
    [attribute addAttributes:dic range:NSMakeRange(range.location, range.length)];
  }

  return attribute;
}

- (NSMutableAttributedString *)bf_fontStr:(NSString *)colorStr font:(NSInteger)font {
  NSRange range = [self rangeOfString:colorStr];
  NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:self];
  NSMutableDictionary *dic = [NSMutableDictionary dictionary];

  if (font > 0) {
    dic[ NSFontAttributeName ] = [UIFont systemFontOfSize:font];
  }
  [attribute addAttributes:dic range:NSMakeRange(range.location, range.length)];

  return attribute;
}

- (NSString *)bf_getDateFromFormat:(NSString *)fromFormat toFormat:(NSString *)toFormat {

  NSDateFormatter *fromFormat1 = [[NSDateFormatter alloc] init];
  [fromFormat1 setDateFormat:fromFormat];  //原格式

  NSDate *date = [fromFormat1 dateFromString:self];
  NSLog(@"----%@", date);

  [fromFormat1 setDateFormat:toFormat];  //新格式

  return [fromFormat1 stringFromDate:date];
}

+ (NSString *)bf_stringJsonValue:(id)JsonValue {
  NSString *string = nil;
  if ([JsonValue isKindOfClass:[NSNull class]]) {
    string = @"";
  }
  else {
    if ([JsonValue isKindOfClass:[NSString class]]) {
      string = JsonValue;
    }
    else if ([JsonValue isKindOfClass:[NSNumber class]]) {
      string = [JsonValue stringValue];
    }
  }
  return string;
}

+ (NSUInteger)bf_lineCountWithAtttributeString:(NSAttributedString *)attrStr size:(CGSize)size {
  if (!attrStr) {
    return 0;
  }

  UIFont *font = [attrStr attribute:NSFontAttributeName atIndex:0 effectiveRange:NULL];
  if (!font) {
    font = [UIFont systemFontOfSize:17];
  }
  NSDictionary *attrs = @{ NSFontAttributeName : font };
  if (attrStr.string.length <= 0) {
    return 0;
  }
  attrStr = [[NSAttributedString alloc] initWithString:attrStr.string attributes:attrs];
  CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attrStr);
  if (!framesetter)
    return 0;
  CGAffineTransform affine = CGAffineTransformIdentity;
  CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, attrStr.length), CGPathCreateWithRect(CGRectMake(0, 0, size.width, FLT_MAX), &affine), (__bridge CFDictionaryRef)attrs);
  CFRelease(framesetter);
  if (!frame) {
    return 0;
  }
  NSUInteger lineCount = CFArrayGetCount(CTFrameGetLines(frame));
  CFRelease(frame);
  return lineCount;
}

/** 根据要求的位数展示千分分隔符*/
- (NSString *)bf_seperatorTheStringByDemandLength:(NSInteger)demandLength {

  NSString *seperatorStr = [NSString string];
  NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];

  formatter.numberStyle = NSNumberFormatterDecimalStyle;

  if (self.length >= demandLength) {

    seperatorStr = [formatter stringFromNumber:@([self integerValue])];
  }
  else {
    seperatorStr = self;
  }

  return seperatorStr;
}

/** 移除字符串尾部的叹号*/
- (NSString *)bf_removeTheStringInnerLastGanTanHao {

  NSString *str = [NSString string];
  if ([self hasSuffix:@"!"]) {

    str = [self substringToIndex:(self.length - 1)];
  }
  else {
    str = self;
  }

  return str;
}

///整数不展示小数点
- (NSString *)bf_handleTheMoneyLastZeroString {

  NSArray *arr = [self componentsSeparatedByString:@"."];
  NSString *originalStr = [arr objectAtIndex:0];

  if (arr.count >= 2) {
    NSString *lastStr = [arr objectAtIndex:1];

    if ([lastStr intValue] <= 0) {
      return originalStr;
    }
    else {

      if ([lastStr integerValue] % 10 == 0) {
        originalStr = [originalStr stringByAppendingFormat:@".%@", [lastStr substringToIndex:1]];
      }
      else {

        if (lastStr.length >= 2) {
          originalStr = [originalStr stringByAppendingFormat:@".%@", [lastStr substringToIndex:2]];
        }
        else {
          originalStr = [originalStr stringByAppendingFormat:@".%@", [lastStr substringToIndex:1]];
        }
      }
      return originalStr;
    }
  }
  else {
    return originalStr;
  }
}

///处理两个数据中的小数据
- (NSString *)bf_copareTheMinInTheSecond:(NSString *)secnondStr {

  CGFloat firstF = [self doubleValue];
  CGFloat secondF = [secnondStr doubleValue];
  if (secondF > 0.0 && firstF > 0.0) {
    return (firstF > secondF) ? self : secnondStr;
  }
  else if (firstF <= 0.0 && secondF > 0.0) {
    return secnondStr;
  }
  else if (firstF > 0.0 && secondF <= 0.0) {
    return self;
  }
  else {
    return @"0";
  }
}

- (NSAttributedString *)bf_setTheKernSpaceAndColor:(UIColor *)foreColor {

  NSMutableDictionary *dict = [NSMutableDictionary dictionary];
  dict[ NSKernAttributeName ] = [NSNumber numberWithFloat:3.0f];  //这里修改字符间距
  if (foreColor) {
    dict[ NSForegroundColorAttributeName ] = foreColor;
  }

  NSAttributedString *attribute1 = [[NSAttributedString alloc] initWithString:self attributes:dict];
  return attribute1;
}

/** 格式化银行卡号  4113 2331 1991 0112 */
- (NSString *)bf_bankFormat {

  NSString *newString = @"";
  NSString *text = self;

  while (text.length > 0) {
    NSString *subString = [text substringToIndex:MIN(text.length, 4)];
    newString = [newString stringByAppendingString:subString];
    if (subString.length == 4) {
      newString = [newString stringByAppendingString:@" "];
    }
    text = [text substringFromIndex:MIN(text.length, 4)];
  }
  return newString;
}

/** 处理后台返回的不规则时间 */
- (NSString *)bf_mydataFormat {

  NSString *timestr = self;
  timestr = [timestr stringByReplacingOccurrencesOfString:@" " withString:@""];
  timestr = [timestr stringByReplacingOccurrencesOfString:@"-" withString:@""];
  timestr = [timestr stringByReplacingOccurrencesOfString:@"年" withString:@""];
  timestr = [timestr stringByReplacingOccurrencesOfString:@"月" withString:@""];
  timestr = [timestr stringByReplacingOccurrencesOfString:@"日" withString:@""];
  timestr = [timestr stringByReplacingOccurrencesOfString:@"/" withString:@""];
  timestr = [timestr stringByReplacingOccurrencesOfString:@"." withString:@""];

  return timestr;
}

//检查字符串中包含有多少个字符
+ (NSInteger)bf_checkTheCharCountOfString:(NSString *)str {

  NSInteger count = 0;
  for (NSInteger i = 0; i < [str length]; i++) {
    UniChar a = [str characterAtIndex:i];
    if (a > 0x4e00 && a < 0x9fff) {
      count += 2;
    }
    else {
      count++;
    }
  }
  NSLog(@" %s  -%ld", __FUNCTION__, (long)count);
  return count;
}

//判断是否有中文
+ (BOOL)bf_IsChinese:(NSString *)str {
  for (int i = 0; i < [str length]; i++) {
    int a = [str characterAtIndex:i];
    if (a > 0x4e00 && a < 0x9fff) {
      return YES;
    }
  }
  return NO;
}

- (NSString *)bf_reviseMoneyStringToDecNumberString {
  //直接传入精度丢失有问题的Double类型
  double conversionValue = [self doubleValue];
  NSString *doubleString = [NSString stringWithFormat:@"%lf", conversionValue];
  NSDecimalNumber *decNumber = [NSDecimalNumber decimalNumberWithString:doubleString];
  return [decNumber stringValue];
}

//  日期转换
+ (NSString *)bf_magenageAbsoluteTime:(NSString *)timeStamp isDate:(BOOL)isDate {

  NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[timeStamp longLongValue] / 1000];
  NSDateFormatter *format = [[NSDateFormatter alloc] init];
  if (isDate)
    [format setDateFormat:@"yyyy.MM.dd"];
  else
    [format setDateFormat:@"yyyy.MM.dd HH:mm"];

  NSString *dateString = [format stringFromDate:confromTimesp];

  return dateString;
}
//显示前四位 后四位
- (NSString *)bf_IDcard_no:(NSString *)card_no {
  if (card_no.length <= 8) {
    return card_no;
  }
  NSString *fisrt = [card_no substringToIndex:4];
  NSString *end = [card_no substringFromIndex:card_no.length - 4];
  NSMutableString *tmp = [NSMutableString string];
  for (int i = 0; i < card_no.length - 8; i++) {
    [tmp appendString:@"*"];
  }
  return [NSString stringWithFormat:@"%@%@%@", fisrt, tmp, end];
}

//截取前f个  后l个 其他 ＊＊＊＊显示 用于证件 比如身份证 或 银行卡
+ (NSString *)bf_IDcard_no:(NSString *)card_no frontNo:(NSInteger)f lastNo:(NSInteger)l {
  if (card_no.length <= f + l) {
    return card_no;
  }
  NSString *fisrt = [card_no substringToIndex:f];
  NSString *end = [card_no substringFromIndex:card_no.length - l];
  NSMutableString *tmp = [NSMutableString string];
  for (int i = 0; i < card_no.length - f + l; i++) {
    [tmp appendString:@"*"];
  }
  return [NSString stringWithFormat:@"%@%@%@", fisrt, tmp, end];
}

- (NSString *)bf_htmlImageAdaptWith:(long)width {
  NSString *newString = [NSString stringWithFormat:@".jpeg\" width=\"%ldpx\"", width];
  return [self stringByReplacingOccurrencesOfString:@".jpeg\"" withString:newString];
}

- (NSString *)bf_htmlImageAdaptWith:(long)width withHeight:(long)height {
  // .jpg\" width=\"550\" height=\"342\"
  NSString *newString = [NSString stringWithFormat:@".jpg\" width=\"%ldpx\" height=\"%ldpx\"", width, height];
  return [self stringByReplacingOccurrencesOfString:@".jpg\"" withString:newString];
}

- (BOOL)bf_containsString:(NSString *)string {
  NSRange range = [[self lowercaseString] rangeOfString:[string lowercaseString]];
  return range.location != NSNotFound;
}

- (NSString *)bf_reformatPhoneNumber {
  NSString *string = self;
  if ([string bf_containsString:@"-"]) {
    string = [string stringByReplacingOccurrencesOfString:@"-" withString:@""];
  }

  if ([string bf_containsString:@"("]) {
    string = [string stringByReplacingOccurrencesOfString:@"(" withString:@""];
  }

  if ([string bf_containsString:@")"]) {
    string = [string stringByReplacingOccurrencesOfString:@")" withString:@""];
  }

  if (([string bf_containsString:@"+86"])) {
    string = [string stringByReplacingOccurrencesOfString:@"+86" withString:@""];
  }

  if ([string bf_containsString:@" "]) {
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
  }

  return string;
}

- (BOOL)bf_validateEmail {
  NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
  NSPredicate *email = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
  return [email evaluateWithObject:self];
}

- (BOOL)bf_validatePhone {
  //    NSString *phoneRegex = @"^13[0-9]{9}$|^14[0-9]{9}$|^15[0-9]{9}$|^18[0-9]{9}$|^17[0-9]{9}$|^400[0-9]{7}-?([1-9]{1}[0-9]{0,4})?$";
  NSString *phoneRegex = @"^1\\d{10}$";

  NSPredicate *phone = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
  return [phone evaluateWithObject:self];
}

- (NSString *)bf_trim {
  return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSString *)bf_convertTime:(NSInteger)num withDataFormat:(dateFormatterType)dataFormatter withCustomDataFormatter:(NSString *)CustomDataFormatter {

  NSString *dataFormatterStr;

  switch (dataFormatter) {
    case dateFormatterTypeToyyyyMMddSymbol:
      dataFormatterStr = @"yyyy-MM-dd";
      break;

    case dateFormatterTypeToyyyyMMddChinese:
      dataFormatterStr = @"yyyy年MM月dd日";
      break;

    case dateFormatterTypeTohhmmssSymbol:
      dataFormatterStr = @"hh:mm:ss";
      break;

    case dateFormatterTypeTohhmmssChinese:
      dataFormatterStr = @"hh时mm分ss秒";
      break;

    case dateFormatterTypeToyyyyMMddhhmmssSymbol:
      dataFormatterStr = @"yyyy-MM-dd HH:mm:ss";
      break;

    case dateFormatterTypeToyyyyMMddhhmmssChinese:
      dataFormatterStr = @"yyyy年MM月dd日 hh时mm分ss秒";
      break;

    case dateFormatterTypeToCustom:
      dataFormatterStr = CustomDataFormatter;
      break;

    default:
      break;
  }

  NSDate *date = [NSDate dateWithTimeIntervalSince1970:num];
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:dataFormatterStr];
  return [dateFormatter stringFromDate:date];
}

- (NSString *)bf_appendDocumentPath {
  return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject stringByAppendingPathComponent:self.lastPathComponent];
}

- (NSString *)bf_appendCachePath {
  return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject stringByAppendingPathComponent:self.lastPathComponent];
}

- (NSString *)bf_appendTempPath {
  return [NSTemporaryDirectory() stringByAppendingPathComponent:self.lastPathComponent];
}

- (NSString *)bf_substringWithinBoundsLeft:(NSString *)strLeft right:(NSString *)strRight {
  NSRange rangeSub;
  NSString *strSub;

  NSRange range;
  range = [self rangeOfString:strLeft options:0];

  if (range.location == NSNotFound) {
    return nil;
  }

  rangeSub.location = range.location + range.length;

  range.location = rangeSub.location;
  range.length = [self length] - range.location;
  range = [self rangeOfString:strRight options:0 range:range];

  if (range.location == NSNotFound) {
    return nil;
  }

  rangeSub.length = range.location - rangeSub.location;
  strSub = [[self substringWithRange:rangeSub] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

  return strSub;
}

/**
 阿拉伯数字转成中文

 @param arebic 阿拉伯数字
 @return 返回的中文数字
 */
+ (NSString *)bf__translation:(NSString *)arebic {
  NSString *str = arebic;
  NSArray *arabic_numerals = @[ @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"0" ];
  NSArray *chinese_numerals = @[ @"一", @"二", @"三", @"四", @"五", @"六", @"七", @"八", @"九", @"零" ];
  NSArray *digits = @[ @"个", @"十", @"百", @"千", @"万", @"十", @"百", @"千", @"亿", @"十", @"百", @"千", @"兆" ];
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:chinese_numerals forKeys:arabic_numerals];

  NSMutableArray *sums = [NSMutableArray array];
  for (int i = 0; i < str.length; i++) {
    NSString *substr = [str substringWithRange:NSMakeRange(i, 1)];
    NSString *a = [dictionary objectForKey:substr];
    NSString *b = digits[ str.length - i - 1 ];
    NSString *sum = [a stringByAppendingString:b];
    if ([a isEqualToString:chinese_numerals[ 9 ]]) {
      if ([b isEqualToString:digits[ 4 ]] || [b isEqualToString:digits[ 8 ]]) {
        sum = b;
        if ([[sums lastObject] isEqualToString:chinese_numerals[ 9 ]]) {
          [sums removeLastObject];
        }
      }
      else {
        sum = chinese_numerals[ 9 ];
      }

      if ([[sums lastObject] isEqualToString:sum]) {
        continue;
      }
    }

    [sums addObject:sum];
  }

  NSString *sumStr = [sums componentsJoinedByString:@""];
  NSString *chinese = [sumStr substringToIndex:sumStr.length - 1];
  NSLog(@"%@", str);
  NSLog(@"%@", chinese);
  return chinese;
}

- (NSString *)bf_reverseWordsInString:(NSString *)str {
  NSMutableString *reverString = [NSMutableString stringWithCapacity:str.length];
  [str enumerateSubstringsInRange:NSMakeRange(0, str.length)
                          options:NSStringEnumerationReverse | NSStringEnumerationByComposedCharacterSequences
                       usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                         [reverString appendString:substring];
                       }];
  return reverString;
}

+ (NSString *)bf__transform:(NSString *)chinese {
  //将NSString装换成NSMutableString
  NSMutableString *pinyin = [chinese mutableCopy];
  //将汉字转换为拼音(带音标)
  CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformMandarinLatin, NO);
  NSLog(@"%@", pinyin);
  //去掉拼音的音标
  CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformStripCombiningMarks, NO);
  NSLog(@"%@", pinyin);
  //返回最近结果
  return pinyin;
}

- (BOOL)bf_isContainChinese {
  NSUInteger length = [self length];
  for (NSUInteger i = 0; i < length; i++) {
    NSRange range = NSMakeRange(i, 1);
    NSString *subString = [self substringWithRange:range];
    const char *cString = [subString UTF8String];
    if (strlen(cString) == 3) {
      return YES;
    }
  }
  return NO;
}

/** 获取字符数量 */
- (int)bf_wordsCount {
  NSInteger n = self.length;
  int i;
  int l = 0, a = 0, b = 0;
  unichar c;

  for (i = 0; i < n; i++) {
    c = [self characterAtIndex:i];
    if (isblank(c)) {
      b++;
    }
    else if (isascii(c)) {
      a++;
    }
    else {
      l++;
    }
  }

  if (a == 0 && l == 0) {
    return 0;
  }
  return l + (int)ceilf((float)(a + b) / 2.0);
}

- (NSDictionary *)bf_dictionaryValue {
  NSError *errorJson;
  NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[self dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&errorJson];
  if (errorJson != nil) {
#ifdef DEBUG
    NSLog(@"fail to get dictioanry from JSON: %@, error: %@", self, errorJson);
#endif
  }
  return jsonDict;
}

/*
 是否为手机号
 */
+ (BOOL)bf_isPhoneNumber:(NSString *)str {
  NSString *pattern = @"^1[0-9]{10}$";
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
  return [pred evaluateWithObject:str];
}
/**
 正则匹配用户密码6-20位数字、字母和下划线三选二组合
 /Users/meiSL/Desktop/BonadeBaseComponent/SLCategory/Classes/UIButton+ZXImageTitlePosition.h */
+ (BOOL)bf_isPassword:(NSString *)str {
  NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)(?!_+$)[a-zA-Z0-9_]{6,20}";
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
  return [pred evaluateWithObject:str];
}
/*
 是否为邮箱
 */
+ (BOOL)bf_isEmail:(NSString *)str {
  NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
  NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
  return [emailTest evaluateWithObject:str];
}
/*
 银行卡正则
 */
+ (BOOL)bf_IsBankCard:(NSString *)cardNumber {
  return [cardNumber bf_bankCardluhmCheck];
}
/*
 是否为空字符串
 */
+ (BOOL)bf_isEmpty:(NSString *)str {

  if (!str) {
    return YES;
  }
  if ([str isKindOfClass:[NSNull class]]) {
    return YES;
  }
  NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
  NSString *trimmedStr = [str stringByTrimmingCharactersInSet:set];
  if (!trimmedStr.length) {
    return YES;
  }
  return NO;
}

/*
 是否有效数字
 */
+ (BOOL)bf_validateNumber:(NSString *)number {
  BOOL res = YES;
  NSCharacterSet *tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
  int i = 0;
  while (i < number.length) {
    NSString *string = [number substringWithRange:NSMakeRange(i, 1)];
    NSRange range = [string rangeOfCharacterFromSet:tmpSet];
    if (range.length == 0) {
      res = NO;
      break;
    }
    i++;
  }
  return res;
}

#pragma mark - 正则相关
- (BOOL)bf_isValidateByRegex:(NSString *)regex {
  NSPredicate *pre = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
  return [pre evaluateWithObject:self];
}

#pragma mark -

//手机号分服务商
- (BOOL)bf_isMobileNumberClassification {
  /**
   * 手机号码
   * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188,1705
   * 联通：130,131,132,152,155,156,185,186,1709
   * 电信：133,1349,153,180,189,1700,173
   */
  //    NSString * MOBILE = @"^1((3//d|5[0-35-9]|8[025-9])//d|70[059])\\d{7}$";//总况

  /**
   * 中国移动：China Mobile
   * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188，1705
   */
  NSString *CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d|705)\\d{7}$";
  /**
   * 中国联通：China Unicom
   * 130,131,132,152,155,156,185,186,1709
   */
  NSString *CU = @"^1((3[0-2]|5[256]|8[56])\\d|709)\\d{7}$";
  /**
   * 中国电信：China Telecom
   * 133,1349,153,180,189,1700,173
   */
  NSString *CT = @"^1((33|53|73|8[09])\\d|349|700)\\d{7}$";

  /**
   * 大陆地区固话及小灵通
   * 区号：010,020,021,022,023,024,025,027,028,029
   * 号码：七位或八位
   */
  NSString *PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";

  //    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];

  if (([self bf_isValidateByRegex:CM]) || ([self bf_isValidateByRegex:CU]) || ([self bf_isValidateByRegex:CT]) || ([self bf_isValidateByRegex:PHS])) {
    return YES;
  }
  else {
    return NO;
  }
}

//手机号有效性
- (BOOL)bf_isMobileNumber {
  NSString *mobileRegex = @"^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$";
  BOOL ret1 = [self bf_isValidateByRegex:mobileRegex];
  return ret1;
}

//邮箱
- (BOOL)bf_isEmailAddress {
  NSString *emailRegex = @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
  return [self bf_isValidateByRegex:emailRegex];
}

//车牌
- (BOOL)bf_isCarNumber {
  //车牌号:湘K-DE829 香港车牌号码:粤Z-J499港
  NSString *carRegex = @"^[\u4e00-\u9fff]{1}[a-zA-Z]{1}[-][a-zA-Z_0-9]{4}[a-zA-Z_0-9_\u4e00-\u9fff]$";  //其中\u4e00-\u9fa5表示unicode编码中汉字已编码部分，\u9fa5-\u9fff是保留部分，将来可能会添加
  return [self bf_isValidateByRegex:carRegex];
}

- (BOOL)bf_isMacAddress {
  NSString *macAddRegex = @"([A-Fa-f\\d]{2}:){5}[A-Fa-f\\d]{2}";
  return [self bf_isValidateByRegex:macAddRegex];
}

- (BOOL)bf_isValidUrl {
  NSString *regex = @"^((http)|(https))+:[^\\s]+\\.[^\\s]*$";
  return [self bf_isValidateByRegex:regex];
}

- (BOOL)bf_isValidChinese;
{
  NSString *chineseRegex = @"^[\u4e00-\u9fa5]+$";
  return [self bf_isValidateByRegex:chineseRegex];
}

- (BOOL)bf_isValidPostalcode {
  NSString *postalRegex = @"^[0-8]\\d{5}(?!\\d)$";
  return [self bf_isValidateByRegex:postalRegex];
}

- (BOOL)bf_isValidTaxNo {
  NSString *taxNoRegex = @"[0-9]\\d{13}([0-9]|X)$";
  return [self bf_isValidateByRegex:taxNoRegex];
}

#pragma mark - 算法相关
//精确的身份证号码有效性检测
+ (BOOL)bf_accurateVerifyIDCardNumber:(NSString *)value {
  value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

  int length = 0;
  if (!value) {
    return NO;
  }
  else {
    length = (int)value.length;

    if (length != 15 && length != 18) {
      return NO;
    }
  }
  // 省份代码
  NSArray *areasArray = @[
    @"11", @"12", @"13", @"14", @"15", @"21", @"22", @"23", @"31", @"32", @"33", @"34", @"35", @"36", @"37", @"41", @"42", @"43",
    @"44", @"45", @"46", @"50", @"51", @"52", @"53", @"54", @"61", @"62", @"63", @"64", @"65", @"71", @"81", @"82", @"91"
  ];

  NSString *valueStart2 = [value substringToIndex:2];
  BOOL areaFlag = NO;
  for (NSString *areaCode in areasArray) {
    if ([areaCode isEqualToString:valueStart2]) {
      areaFlag = YES;
      break;
    }
  }

  if (!areaFlag) {
    return false;
  }

  NSRegularExpression *regularExpression;
  NSUInteger numberofMatch;

  int year = 0;
  switch (length) {
    case 15:
      year = [value substringWithRange:NSMakeRange(6, 2)].intValue + 1900;

      if (year % 4 == 0 || (year % 100 == 0 && year % 4 == 0)) {

        regularExpression =
          [[NSRegularExpression alloc] initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$"
                                               options:NSRegularExpressionCaseInsensitive
                                                 error:nil];  //测试出生日期的合法性
      }
      else {
        regularExpression =
          [[NSRegularExpression alloc] initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$"
                                               options:NSRegularExpressionCaseInsensitive
                                                 error:nil];  //测试出生日期的合法性
      }
      numberofMatch = [regularExpression numberOfMatchesInString:value options:NSMatchingReportProgress range:NSMakeRange(0, value.length)];

      if (numberofMatch > 0) {
        return YES;
      }
      else {
        return NO;
      }
    case 18:
      year = [value substringWithRange:NSMakeRange(6, 4)].intValue;
      if (year % 4 == 0 || (year % 100 == 0 && year % 4 == 0)) {

        regularExpression = [[NSRegularExpression alloc]
          initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$"
                  options:NSRegularExpressionCaseInsensitive
                    error:nil];  //测试出生日期的合法性
      }
      else {
        regularExpression = [[NSRegularExpression alloc]
          initWithPattern:@"^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$"
                  options:NSRegularExpressionCaseInsensitive
                    error:nil];  //测试出生日期的合法性
      }
      numberofMatch = [regularExpression numberOfMatchesInString:value options:NSMatchingReportProgress range:NSMakeRange(0, value.length)];

      if (numberofMatch > 0) {
        int S = ([value substringWithRange:NSMakeRange(0, 1)].intValue + [value substringWithRange:NSMakeRange(10, 1)].intValue) * 7 +
                ([value substringWithRange:NSMakeRange(1, 1)].intValue + [value substringWithRange:NSMakeRange(11, 1)].intValue) * 9 +
                ([value substringWithRange:NSMakeRange(2, 1)].intValue + [value substringWithRange:NSMakeRange(12, 1)].intValue) * 10 +
                ([value substringWithRange:NSMakeRange(3, 1)].intValue + [value substringWithRange:NSMakeRange(13, 1)].intValue) * 5 +
                ([value substringWithRange:NSMakeRange(4, 1)].intValue + [value substringWithRange:NSMakeRange(14, 1)].intValue) * 8 +
                ([value substringWithRange:NSMakeRange(5, 1)].intValue + [value substringWithRange:NSMakeRange(15, 1)].intValue) * 4 +
                ([value substringWithRange:NSMakeRange(6, 1)].intValue + [value substringWithRange:NSMakeRange(16, 1)].intValue) * 2 + [value substringWithRange:NSMakeRange(7, 1)].intValue * 1 +
                [value substringWithRange:NSMakeRange(8, 1)].intValue * 6 + [value substringWithRange:NSMakeRange(9, 1)].intValue * 3;
        int Y = S % 11;
        NSString *M = @"F";
        NSString *JYM = @"10X98765432";
        M = [JYM substringWithRange:NSMakeRange(Y, 1)];  // 判断校验位
        NSString *test = [value substringWithRange:NSMakeRange(17, 1)];
        if ([[M lowercaseString] isEqualToString:[test lowercaseString]]) {
          return YES;  // 检测ID的校验位
        }
        else {
          return NO;
        }
      }
      else {
        return NO;
      }
    default:
      return NO;
  }
}

/** 银行卡号有效性问题Luhn算法
 *  现行 16 位银联卡现行卡号开头 6 位是 622126～622925 之间的，7 到 15 位是银行自定义的，
 *  可能是发卡分行，发卡网点，发卡序号，第 16 位是校验码。
 *  16 位卡号校验位采用 Luhm 校验方法计算：
 *  1，将未带校验位的 15 位卡号从右依次编号 1 到 15，位于奇数位号上的数字乘以 2
 *  2，将奇位乘积的个十位全部相加，再加上所有偶数位上的数字
 *  3，将加法和加上校验位能被 10 整除。
 */
- (BOOL)bf_bankCardluhmCheck {
  NSString *lastNum = [[self substringFromIndex:(self.length - 1)] copy];   //取出最后一位
  NSString *forwardNum = [[self substringToIndex:(self.length - 1)] copy];  //前15或18位

  NSMutableArray *forwardArr = [[NSMutableArray alloc] initWithCapacity:0];
  for (int i = 0; i < forwardNum.length; i++) {
    NSString *subStr = [forwardNum substringWithRange:NSMakeRange(i, 1)];
    [forwardArr addObject:subStr];
  }

  NSMutableArray *forwardDescArr = [[NSMutableArray alloc] initWithCapacity:0];
  for (int i = (int)(forwardArr.count - 1); i > -1; i--) {  //前15位或者前18位倒序存进数组
    [forwardDescArr addObject:forwardArr[ i ]];
  }

  NSMutableArray *arrOddNum = [[NSMutableArray alloc] initWithCapacity:0];   //奇数位*2的积 < 9
  NSMutableArray *arrOddNum2 = [[NSMutableArray alloc] initWithCapacity:0];  //奇数位*2的积 > 9
  NSMutableArray *arrEvenNum = [[NSMutableArray alloc] initWithCapacity:0];  //偶数位数组

  for (int i = 0; i < forwardDescArr.count; i++) {
    NSInteger num = [forwardDescArr[ i ] intValue];
    if (i % 2) {  //偶数位
      [arrEvenNum addObject:[NSNumber numberWithInteger:num]];
    }
    else {  //奇数位
      if (num * 2 < 9) {
        [arrOddNum addObject:[NSNumber numberWithInteger:num * 2]];
      }
      else {
        NSInteger decadeNum = (num * 2) / 10;
        NSInteger unitNum = (num * 2) % 10;
        [arrOddNum2 addObject:[NSNumber numberWithInteger:unitNum]];
        [arrOddNum2 addObject:[NSNumber numberWithInteger:decadeNum]];
      }
    }
  }

  __block NSInteger sumOddNumTotal = 0;
  [arrOddNum enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
    sumOddNumTotal += [obj integerValue];
  }];

  __block NSInteger sumOddNum2Total = 0;
  [arrOddNum2 enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
    sumOddNum2Total += [obj integerValue];
  }];

  __block NSInteger sumEvenNumTotal = 0;
  [arrEvenNum enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
    sumEvenNumTotal += [obj integerValue];
  }];

  NSInteger lastNumber = [lastNum integerValue];

  NSInteger luhmTotal = lastNumber + sumEvenNumTotal + sumOddNum2Total + sumOddNumTotal;

  return (luhmTotal % 10 == 0) ? YES : NO;
}

- (BOOL)bf_isIPAddress {
  NSString *regex = [NSString stringWithFormat:@"^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$"];
  NSPredicate *pre = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
  BOOL rc = [pre evaluateWithObject:self];

  if (rc) {
    NSArray *componds = [self componentsSeparatedByString:@","];

    BOOL v = YES;
    for (NSString *s in componds) {
      if (s.integerValue > 255) {
        v = NO;
        break;
      }
    }

    return v;
  }

  return NO;
}

- (NSString *)bf_stringByStrippingHTML {
  return [self stringByReplacingOccurrencesOfString:@"<[^>]+>" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, self.length)];
}

- (NSString *)bf_stringByRemovingScriptsAndStrippingHTML {
  NSMutableString *mString = [self mutableCopy];
  NSError *error;
  NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<script[^>]*>[\\w\\W]*</script>" options:NSRegularExpressionCaseInsensitive error:&error];
  NSArray *matches = [regex matchesInString:mString options:NSMatchingReportProgress range:NSMakeRange(0, [mString length])];
  for (NSTextCheckingResult *match in [matches reverseObjectEnumerator]) {
    [mString replaceCharactersInRange:match.range withString:@""];
  }
  return [mString bf_stringByStrippingHTML];
}

- (NSString *)bf_trimmingWhitespace {
  return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (NSString *)bf_trimmingWhitespaceAndNewlines {
  return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)bf_toMD5 {
  if (self == nil || [self length] == 0) {
    return nil;
  }

  unsigned char digest[ CC_MD5_DIGEST_LENGTH ], i;
  CC_MD5([self UTF8String], (int)[self lengthOfBytesUsingEncoding:NSUTF8StringEncoding], digest);
  NSMutableString *ms = [NSMutableString string];

  for (i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
    [ms appendFormat:@"%02x", (int)(digest[ i ])];
  }

  return [ms copy];
}

- (NSString *)bf_to16MD5 {
  if (self == nil || [self length] == 0) {
    return nil;
  }

  return [[self bf_toMD5] substringWithRange:NSMakeRange(8, 16)];
}

- (NSString *)bf_sha1 {
  if (self == nil || [self length] == 0) {
    return nil;
  }

  unsigned char digest[ CC_SHA1_DIGEST_LENGTH ], i;
  CC_SHA1([self UTF8String], (int)[self lengthOfBytesUsingEncoding:NSUTF8StringEncoding], digest);
  NSMutableString *ms = [NSMutableString string];

  for (i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
    [ms appendFormat:@"%02x", (int)(digest[ i ])];
  }

  return [ms copy];
}

- (NSString *)bf_sha256 {
  if (self == nil || [self length] == 0) {
    return nil;
  }

  unsigned char digest[ CC_SHA256_DIGEST_LENGTH ], i;
  CC_SHA256([self UTF8String], (int)[self lengthOfBytesUsingEncoding:NSUTF8StringEncoding], digest);
  NSMutableString *ms = [NSMutableString string];

  for (i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
    [ms appendFormat:@"%02x", (int)(digest[ i ])];
  }

  return [ms copy];
}

- (NSString *)bf_sha512 {
  if (self == nil || [self length] == 0) {
    return nil;
  }

  unsigned char digest[ CC_SHA512_DIGEST_LENGTH ], i;
  CC_SHA512([self UTF8String], (int)[self lengthOfBytesUsingEncoding:NSUTF8StringEncoding], digest);
  NSMutableString *ms = [NSMutableString string];

  for (i = 0; i < CC_SHA512_DIGEST_LENGTH; i++) {
    [ms appendFormat:@"%02x", (int)(digest[ i ])];
  }

  return [ms copy];
}

- (NSData *)bf_toData {
  return [self dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *)bf_toStringWithData:(NSData *)data {
  if (data && [data isKindOfClass:[NSData class]]) {
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
  }
  return nil;
}

+ (NSString *)bf_toStringWithDate:(NSDate *)date dateFormat:(NSString *)dateFormat{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    dateFormatter.dateFormat= dateFormat ;
    return [dateFormatter stringFromDate:date];
}

- (NSString *)bf_stringConvertToTime {
  if (self.length == 0) {
    return @"00:00:00";
  }
  NSInteger timeInt = [self longLongValue];
  NSInteger hourInt = timeInt / 3600;
  NSInteger minuteInt = (timeInt - hourInt * 3600) / 60;
  NSInteger secondInt = (timeInt - hourInt * 3600 - minuteInt * 60) % 60;
  return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hourInt, (long)minuteInt, (long)secondInt];
}

-(NSString *)bf_dateFormatToNowTime{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *str = self;
    if ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1) {
        str = @"YYYY-MM-dd HH:mm:ss";
    }
    [formatter setDateFormat:str];
    NSDate *datenow = [NSDate date];
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return currentTimeString;
}

- (NSString *)bf_distanceValue {
  if (self.doubleValue >= 1000) {
    return [NSString stringWithFormat:@"%.1fkm", self.doubleValue / 1000.f];
  }
  return [NSString stringWithFormat:@"%dm", self.intValue];
}

- (NSString *)bf_priceValue {
  return [self bf_reviseMoneyStringToDecNumberString];
}

- (NSString *)bf_commentsValue {
  if (self.integerValue >= 10000) {
    return [NSString stringWithFormat:@"%.1f万", self.integerValue / 10000.f];
  }
  return self;
}

/**
 *  AES加解密算法
 *
 *  @param operation kCCEncrypt（加密）kCCDecrypt（解密）
 *  @param data      待操作Data数据
 *  @param key       key
 *  @param iv        向量
 *
 *
 */
- (NSData *)bf_AES128operation:(CCOperation)operation data:(NSData *)data key:(NSString *)key iv:(NSString *)iv {

  char keyPtr[ kCCKeySizeAES128 + 1 ];  // kCCKeySizeAES128是加密位数 可以替换成256位的
  bzero(keyPtr, sizeof(keyPtr));
  [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
  // IV
  char ivPtr[ kCCBlockSizeAES128 + 1 ];
  bzero(ivPtr, sizeof(ivPtr));
  [iv getCString:ivPtr maxLength:sizeof(ivPtr) encoding:NSUTF8StringEncoding];

  size_t bufferSize = [data length] + kCCBlockSizeAES128;
  void *buffer = malloc(bufferSize);
  size_t numBytesEncrypted = 0;

  // 设置加密参数
  /**
   这里设置的参数ios默认为CBC加密方式，如果需要其他加密方式如ECB，在kCCOptionPKCS7Padding这个参数后边加上kCCOptionECBMode，即kCCOptionPKCS7Padding |
   kCCOptionECBMode，但是记得修改上边的偏移量，因为只有CBC模式有偏移量之说

   */
  CCCryptorStatus cryptorStatus = CCCrypt(operation, kCCAlgorithmAES128, kCCOptionPKCS7Padding, keyPtr, kCCKeySizeAES128, ivPtr, [data bytes], [data length], buffer, bufferSize, &numBytesEncrypted);

  if (cryptorStatus == kCCSuccess) {
    NSLog(@"Success");
    return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
  }
  else {
    NSLog(@"Error");
  }

  free(buffer);
  return nil;
}

NSString *safeString(NSString *str)
{
    if ([str isKindOfClass:NSNumber.class]) {
        return [NSString stringWithFormat:@"%@", str];
    }
    if ([str isKindOfClass:NSNull.class]) {
        return @"";
    }
    if (str == nil) {
        return @"";
    }
    return str;
}

NSString *safeZeroString(NSString *str)
{
    if ([str isKindOfClass:NSNumber.class]) {
        return [NSString stringWithFormat:@"%@", str];
    }
    if ([str isKindOfClass:NSNull.class]) {
        return @" ";
    }
    if (str == nil) {
        return @" ";
    }
    return str;
}

NSString *safeZero(NSString *str)
{
    if (safeString(str).length == 0) {
        return @"0";
    }
    return str;
}

NSString *safeToDayString(NSString *str)
{
    if (safeString(str).length == 0) {
        return @"";
    }else if (str.length > 10){
        str = [str substringToIndex:11];
    }
    return str;
}

NSString *safeHideZeroString(NSString *str)
{
    if (safeString(str).length == 0) {
        return @"";
    }else if ([str hasSuffix:@".00"]){
        str = [str stringByReplacingOccurrencesOfString:@".00" withString:@""];
    }else if ([str containsString:@"."]){
        if ([str hasSuffix:@"0"]) {
            str = [str substringToIndex:str.length-1];
        }
    }
    return str;
}

-(CGFloat)widthForFont:(UIFont *)font{
    return [self bf_sizeWithFont:font].width;
}

@end
