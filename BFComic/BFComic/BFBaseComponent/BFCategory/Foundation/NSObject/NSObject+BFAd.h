//
//  NSObject+BFAd.h
//  testModel
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//


//#import "NSObject+YYModel.h"

@interface NSObject (BFAd)
//+ (id)bf_parse:(id)json;


#pragma mark - Base

/**
 获取类名

 @return 类名字符串
 */
+ (NSString *)bf_className;


/**
 model 转 jsonString

 @return jsonString
 */
-(NSString *)modelToJSONString;


/**
 获取类名

 @return 类名字符串
 */
- (NSString *)bf_className;

/**
 根据延迟时间, 执行指定方法
 
 @param selector 指定方法
 @param delay 延迟时间
 */
- (void)bf_performSelector:(SEL)selector afterDelay:(NSTimeInterval)delay;


#pragma mark - Associated Value
/**
 根据Key和Value, 关联对象(retain, strong, nonatomic)
 
 @param value Value
 @param key   Key
 */
- (void)bf_setAssociatedValue:(id)value withKey:(const void *)key;

/**
 根据Key和Value, 关联对象(weak)
 
 @param value Value
 @param key   Key
 */
- (void)bf_setAssociatedWeakValue:(id)value withKey:(const void *)key;

/**
 根据Key和Value, 关联对象(copy, nonatomic)
 
 @param value Value
 @param key   Key
 */
- (void)bf_setAssociatedCopyValue:(id)value withKey:(const void *)key;

/**
 根据Key, 获取关联对象的Value
 
 @param key Key
 @return Key对应关联对象的Value
 */
- (id)bf_getAssociatedValueForKey:(const void *)key;

/**
 移除所有关联对象
 */
- (void)bf_removeAssociatedValues;

@end
