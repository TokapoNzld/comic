//
//  NSObject+BFAd.m
//  testModel
//
//  Created by xiaoxin on 01/10/2020.
//  Copyright (c) 2020 xiaoxin. All rights reserved.
//

#import "NSObject+BFAd.h"
#import <objc/runtime.h>
#import "MJExtension.h"

@implementation NSObject (BFAd)

//+ (id)bf_parse:(id)json{
//    if ([json isKindOfClass:[NSArray class]]) {
//        return [NSArray modelArrayWithClass:[self class] json:json];
//    }else if ([json isKindOfClass:[NSDictionary class]] ||
//              [json isKindOfClass:[NSData class]]) {
//        return [self modelWithJSON:json];
//    }
//    return json;
//}


#pragma mark - Base
+ (NSString *)bf_className {
    return NSStringFromClass(self);
}

-(NSString *)modelToJSONString{
    
        
    return [self mj_JSONString];
}

- (NSString *)bf_className {
    return [NSString stringWithUTF8String:class_getName([self class])];
}

- (void)bf_performSelector:(SEL)selector afterDelay:(NSTimeInterval)delay {
    [self performSelector:selector withObject:nil afterDelay:delay];
}

#pragma mark - Associated Value
- (void)bf_setAssociatedValue:(id)value withKey:(const void *)key {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)bf_setAssociatedWeakValue:(id)value withKey:(const void *)key {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_ASSIGN);
}

- (void)bf_setAssociatedCopyValue:(id)value withKey:(const void *)key {
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (id)bf_getAssociatedValueForKey:(const void *)key {
    return objc_getAssociatedObject(self, key);
}

- (void)bf_removeAssociatedValues {
    objc_removeAssociatedObjects(self);
}

@end
