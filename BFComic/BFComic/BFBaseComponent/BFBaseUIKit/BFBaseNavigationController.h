//
//  BFBaseNavigationController.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BFBaseNavigationController : UINavigationController

/// 配置导航栏
/// @param bgColor 背景
/// @param titleColor 标题颜色
/// @param titleFontSize 标题字体
+ (void)configNavWithBackgroundColor:(UIColor *)bgColor titleColor:(UIColor *)titleColor titleFontSize:(CGFloat)titleFontSize;

@end

NS_ASSUME_NONNULL_END
