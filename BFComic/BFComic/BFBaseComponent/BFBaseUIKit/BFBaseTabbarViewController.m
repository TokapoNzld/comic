//
//  BFBaseTabbarViewController.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import "BFBaseTabbarViewController.h"
#import "BF_HomeViewController.h"
#import "BF_DiscoveryViewController.h"
#import "BF_BookshelfViewController.h"
#import "BF_MineViewController.h"
#import "BFBaseNavigationController.h"

@interface BFBaseTabbarViewController () <UITabBarControllerDelegate>

@property (nonatomic, strong) NSMutableArray *base_viewControllers;

@end

@implementation BFBaseTabbarViewController

- (instancetype)init {
    if ([super init]) {
        self.delegate = self;
        [self initViewControllers];
    }
    return self;
}

- (void)initViewControllers {
    BF_HomeViewController *homeVC = [[BF_HomeViewController alloc] init];
    [self registerViewController:homeVC imageName:@"home" title:@"首页"];
    
    BF_DiscoveryViewController *discoveryVC = [[BF_DiscoveryViewController alloc] init];
    [self registerViewController:discoveryVC imageName:@"discovery" title:@"发现"];
    
    BF_BookshelfViewController *bookshelfVC = [[BF_BookshelfViewController alloc] init];
    [self registerViewController:bookshelfVC imageName:@"bookshelf" title:@"书架"];
    
    BF_MineViewController *mineVC = [[BF_MineViewController alloc] init];
    [self registerViewController:mineVC imageName:@"mine" title:@"我的"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBar.tintColor = AppTabbarSelectedColor;
    self.tabBar.backgroundColor = AppMainThemeColor;
    [self configureTabbar];
}

-(void)configureTabbar{
    CGRect rect = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
     UIGraphicsBeginImageContext(rect.size);
     CGContextRef context = UIGraphicsGetCurrentContext();
     CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
     CGContextFillRect(context, rect);
     UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
      if (@available(iOS 13.0, *)) {
          UITabBarAppearance *tabBarAppearance = [self.tabBar.standardAppearance copy];
          [tabBarAppearance setBackgroundEffect:nil];
//          [tabBarAppearance setBackgroundImage: [[UIImage imageNamed:@"bg_tab"] stretchableImageWithLeftCapWidth:180 topCapHeight:60]];
//          [tabBarAppearance setShadowColor:[UIColor clearColor]];
          [self.tabBar setStandardAppearance:tabBarAppearance];
      } else {
//          [self.tabBar setBackgroundImage: [[UIImage imageNamed:@"bg_tab"] stretchableImageWithLeftCapWidth:180 topCapHeight:60]];
//          [self.tabBar setShadowImage:img];
    }
    self.tabBar.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.tabBar.layer.shadowOffset = CGSizeMake(0, -3);
    self.tabBar.layer.shadowOpacity = 0.1;
}


#pragma mark - 注册VC
- (void)registerViewController:(UIViewController *)vc imageName:(NSString *)imageName title:(NSString *)title {
    [self registerViewController:vc normalImageName:[NSString stringWithFormat:@"%@",imageName] selectedImageName:[NSString stringWithFormat:@"%@_select",imageName] title:title normalTitleColor: AppTabbarNormalColor selectedTitleColor:AppTabbarSelectedColor];
}

/// 注册VC的接口协议
/// @param vc 需要加载到TabBar的VC
/// @param normalImageName 未选中的icon
/// @param selectedImageName 选中的icon
/// @param title 标题
/// @param normalTitleColor 未选中的标题色
/// @param selectedTitleColor 选中的标题色
- (void)registerViewController:(UIViewController *)vc normalImageName:(NSString *)normalImageName selectedImageName:(NSString *)selectedImageName title:(NSString *)title normalTitleColor:(UIColor *)normalTitleColor selectedTitleColor:(UIColor *)selectedTitleColor {
    
    vc.tabBarItem.image = [[UIImage imageNamed:normalImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.tabBarItem.title = title;
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:normalTitleColor} forState:UIControlStateNormal];
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:selectedTitleColor} forState:UIControlStateSelected];
    
    [BFBaseNavigationController configNavWithBackgroundColor:UIColorFromHex(0xF6F6F8) titleColor:UIColorFromHex(0x353739) titleFontSize:18];
    
    UINavigationController *nav = [[BFBaseNavigationController alloc] initWithRootViewController:vc];
    nav.title = title;
    [self.base_viewControllers addObject:nav];
    
    
    self.viewControllers = self.base_viewControllers;
}

#pragma mark - Getter && Setter
- (NSMutableArray *)base_viewControllers {
    if (!_base_viewControllers) {
        _base_viewControllers = [[NSMutableArray alloc] init];
    }
    return _base_viewControllers;
}

@end
