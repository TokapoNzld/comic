//
//  BFBaseViewController.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BFBaseViewController : UIViewController

/// 状态栏样式
@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;

/// 是否隐藏导航栏
@property (nonatomic, assign) BOOL isHidenNavBar;

#pragma mark - Setup
- (void)base_setupViews;

- (void)base_setupLayout;

- (void)base_setupTempData;


@end

NS_ASSUME_NONNULL_END
