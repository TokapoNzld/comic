//
//  BFBaseViewController.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import "BFBaseViewController.h"

@interface BFBaseViewController ()<UIGestureRecognizerDelegate>

@end

@implementation BFBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self == self.navigationController.viewControllers.firstObject) {
        self.navigationItem.leftBarButtonItems = nil;
    }
    _isHidenNavBar = NO;
//    if (@available(iOS 13.0, *)) {
//        _statusBarStyle = UIStatusBarStyleDarkContent;
//    } else {
//        // Fallback on earlier versions
//        _statusBarStyle = UIStatusBarStyleLightContent;
//    }
    
//    self.statusBarStyle = UIStatusBarStyleLightContent;
    self.view.backgroundColor = AppMainThemeColor;
    [self base_setupViews];
    [self base_setupLayout];
    [self base_setupTempData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

//    self.navigationController.navigationBar.hidden = _isHidenNavBar;
    [self.navigationController setNavigationBarHidden:_isHidenNavBar animated:NO];
    self.tabBarController.navigationItem.title = self.title;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    printf("\n------>进入 %s %s\n",[self.title UTF8String],[NSStringFromClass(self.class) UTF8String]);

}

#pragma mark - Setup
- (void)base_setupViews {
    
}

- (void)base_setupLayout {
    
}

- (void)base_setupTempData{
    
}

#pragma mark - Gesture delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    return self.navigationController.childViewControllers.count > 1;
}

//-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
//    return YES;
//}.

@end
