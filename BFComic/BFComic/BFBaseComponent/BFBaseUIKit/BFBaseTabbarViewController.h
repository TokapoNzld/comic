//
//  BFBaseTabbarViewController.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BFBaseTabbarViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END
