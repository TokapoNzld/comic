//
//  BFBaseNavigationController.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import "BFBaseNavigationController.h"

@interface BFBaseNavigationController ()<UIGestureRecognizerDelegate,UINavigationControllerDelegate>

@property (class, nonatomic, strong) UIColor *bgColor;
@property (class, nonatomic, strong) UIColor *titleColor;
@property (class, nonatomic, assign) CGFloat titleFontSize;

@end

@implementation BFBaseNavigationController
static UIColor *_bgColor = nil;
static UIColor *_titleColor = nil;
static CGFloat _titleFontSize = 0;

+ (void)configNavWithBackgroundColor:(UIColor *)bgColor titleColor:(UIColor *)titleColor titleFontSize:(CGFloat)titleFontSize {
    self.bgColor = bgColor;
    self.titleColor = titleColor;
    self.titleFontSize = titleFontSize;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    
    NSDictionary *textAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont systemFontOfSize:18 weight:UIFontWeightBold]};
    
    if (@available(iOS 13.0, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        appearance.backgroundColor = AppNavigationBarBackgroundColor;
        appearance.shadowColor = AppNavigationBarBackgroundColor;
        appearance.titleTextAttributes = textAttributes;
        self.navigationBar.scrollEdgeAppearance = appearance;
        self.navigationBar.standardAppearance = appearance;

    } else {
        UINavigationBar *navBar = self.navigationBar;
        [navBar setBarTintColor: AppNavigationBarBackgroundColor];
        [navBar setTitleTextAttributes: textAttributes];
    }
    self.navigationController.navigationBar.translucent = NO;
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.viewControllers.count){
        /// 统一隐藏底部tabbar
        viewController.hidesBottomBarWhenPushed = YES;
        
        [self base_navTypeDefaultWithController:viewController];
    }
    [self.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18]}];
    [super pushViewController:viewController animated:animated];
}

-(void)base_backAction{
    [self popViewControllerAnimated:YES];
}

#pragma mark - Accessors
//- (UIStatusBarStyle)preferredStatusBarStyle{
//
//    return self.topViewController.preferredStatusBarStyle;
//}
//
//- (UIViewController *)childViewControllerForStatusBarStyle {
//    return self.topViewController;
//}
//- (BOOL)prefersStatusBarHidden {
//    return self.topViewController.prefersStatusBarHidden;
//}

+ (void)setBgColor:(UIColor *)bgColor {
    _bgColor = bgColor;
}

+ (UIColor *)bgColor {
    return _bgColor;
}

+ (void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
}

+ (UIColor *)titleColor {
    return _titleColor;
}

+ (void)setTitleFontSize:(CGFloat)titleFontSize {
    _titleFontSize = titleFontSize;
}

+ (CGFloat)titleFontSize {
    return _titleFontSize;
}

#pragma mark - NavBar
- (void)base_navTypeDefaultWithController:(UIViewController *)vc {
    if (!self.viewControllers.count) {
        return;
    }
    vc.navigationItem.leftBarButtonItem = [self base_customItemWithTitle:@"" titleColor:nil imageName:@"arrow_black" target:self selector:@selector(base_backAction) contentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
}

- (UIBarButtonItem *)base_customItemWithTitle:(nullable NSString *)title
                              titleColor:(nullable UIColor *)titleColor
                               imageName:(nullable NSString *)imageName
                                  target:(id)target
                                selector:(SEL)selector
              contentHorizontalAlignment:(UIControlContentHorizontalAlignment)contentHorizontalAlignment{
    UIButton *item = [[UIButton alloc] init];
    titleColor = titleColor?titleColor:[UIColor whiteColor];
    CGFloat itemWidth = 10;
    if ((title.length)) {
        [item setTitle:title forState:UIControlStateNormal];
        [item.titleLabel setFont:[UIFont systemFontOfSize:25 weight:UIFontWeightSemibold]];
        [item setTitleColor:titleColor forState:UIControlStateNormal];
        [item setTitleColor:[titleColor colorWithAlphaComponent:.5f] forState:UIControlStateHighlighted];
        [item setTitleColor:[titleColor colorWithAlphaComponent:.5f] forState:UIControlStateDisabled];
    }
    
    if (imageName.length) {
        [item setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    
    [item sizeToFit];
    item.bounds = CGRectMake(0, 0, itemWidth, 14);
    item.contentHorizontalAlignment = contentHorizontalAlignment;
    [item addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:item];
}

@end
