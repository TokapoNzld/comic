//
//  AppDelegate.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/7.
//

#import "AppDelegate.h"
#import "BFBaseTabbarViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = [[BFBaseTabbarViewController alloc] init];
    [self.window makeKeyAndVisible];
    
    return YES;
}


@end
