//
//  BFBaseRequest.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import "BFBaseRequest.h"
#import <AFNetworking/AFNetworking.h>
#import "BFToastManager.h"
@interface BFBaseRequest ()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation BFBaseRequest

+ (BFBaseRequest *)manager {
    static BFBaseRequest *request = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        request = [[self alloc] init];
    });
    return request;
}

- (instancetype)init {
    if ([super init]) {
        _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://120.77.29.103:8706/"]];
        _sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        _sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"application/x-www-form-urlencoded",@"image/jpeg", nil];
    }
    return self;
}

- (void)GET:(NSString *)URLString parameters:(id)parameters downloadProgress:(void (^)(NSProgress * _Nonnull))downloadProgress success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [self setHeaderDate];
    [self.sessionManager GET:URLString parameters:parameters headers:nil progress:downloadProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
        [BFToastManager bf_showErrorWithStatus:error.localizedDescription duration:1.0];
    }];
}

- (void)POST:(NSString *)URLString parameters:(id)parameters downloadProgress:(void (^)(NSProgress * _Nonnull))downloadProgress success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [self setHeaderDate];
    [self.sessionManager POST:URLString parameters:parameters headers:nil progress:downloadProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(task, error);
        [BFToastManager bf_showErrorWithStatus:error.localizedDescription duration:1.0];
    }];
}


- (void)setHeaderDate {
    [self.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
}

@end
