//
//  BF_ApiConfig.h
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#ifndef BF_ApiConfig_h
#define BF_ApiConfig_h

//获取热门漫画
#define BF_API_HOME_INDEXHOT        @"cartoon/index/getIndexInfo"
//漫画详情+目录
#define BF_API_CARTOONBYID     @"cartoon/cartoon/getCartoonById"
//漫画详情页
#define BF_API_CARTOONDETAILBYID     @"cartoon/cartoon/getCartoonDetailById"

//手机号码注册
#define BF_API_REGISTER_MOBILE         @"cartoon/user/registerByMobile"

//获取当前登录用户信息
#define BF_API_USER_INFO            @"cartoon/user/getUserInfo"

//手机号密码登录
#define BF_API_LOGIN_PASSWORD       @"cartoon/login/loginByMobile"

//退出登录
#define BF_API_LOGIN_LOGOUT         @"cartoon/login/logout"

//获取短信验证码(1-用户注册,2-找回密码,3-绑定手机,4-手机登录)
#define BF_API_LOGIN_SMSCODE        @"cartoon/sms/sendCode"

//发现
#define BF_API_DISCOVERY_FIND       @"cartoon/find/getFindList"

//获取书架列表
#define BF_API_USER_BOOKSELF_LIST   @"cartoon/userBookSelf/getUserBookSelfList"
//加入/删除书架
#define BF_API_UPDATE_COMIC_BOOKSELF   @"cartoon/userBookSelf/operationUserBookSelf"

#endif /* BF_ApiConfig_h */
