//
//  BFBaseRequest.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

///请求成功回调
typedef void (^BFNetworkSessionSuccess)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject);
///请求失败回调
typedef void (^BFNetworkSessionFailure)(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error);

@interface BFBaseRequest : NSObject

+ (BFBaseRequest *)manager;

- (void)GET:(NSString *)URLString parameters:(nullable id)parameters downloadProgress:(nullable void (^)(NSProgress *progress))downloadProgress success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;


- (void)POST:(NSString *)URLString parameters:(nullable id)parameters downloadProgress:(nullable void (^)(NSProgress *progress))downloadProgress
 success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

@end

NS_ASSUME_NONNULL_END
