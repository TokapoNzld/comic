//
//  BFToastManager.h
//  BFComic
//
//  Created by 俊文 on 2021/6/16.
//

#import <SVProgressHUD/SVProgressHUD.h>

NS_ASSUME_NONNULL_BEGIN


/**
 RXToastManager是对SVProgressHUD的二次封装
 
 @discussion 包含常规的分状态的（包括成功、失败、错误等）文字提醒，进度显示。
 */
@interface BFToastManager : SVProgressHUD

/// 设置前景色
/// @param color 前景色
+ (void)bf_setPanelForegroundColor:(UIColor *)color;

/// 设置背景色
/// @param color 背景色
+ (void)bf_setPanelBackgroundColor:(UIColor *)color;

/// 显示转圈圈
+ (void)bf_show;

/// 显示状态文字
/// @param status 状态文字
+ (void)bf_showWithStatus:(nullable NSString*)status;

/// 显示纯文字
/// @param status 文字
/// @param duration 时间
+ (void)bf_showWithText:(NSString *)status duration:(NSTimeInterval)duration;
/// 成功弹窗提示
/// @param status 成功的文本
/// @param duration 显示时间
+ (void)bf_showSuccessWithStatus:(nullable NSString*)status duration:(NSTimeInterval)duration;

/// 错误弹窗提示
/// @param status 显示错误的提示文本
/// @param duration 时长
+ (void)bf_showErrorWithStatus:(nullable NSString*)status duration:(NSTimeInterval)duration;


/// 警告弹窗提示
/// @param status 显示文本
/// @param duration 时长
+ (void)bf_showWarningWithStatus:(NSString *)status duration:(NSTimeInterval)duration;
/// 显示环状进度
/// @param progress 进度
+ (void)bf_showProgress:(float)progress;

/// 含描述文字的进度圈
/// @param progress 进度
/// @param status 描述文本
+ (void)bf_showProgress:(float)progress status:(nullable NSString*)status;

/// 收起弹窗
+ (void)bf_dismiss;

/// 延迟收起弹窗
/// @param duration 延迟时间
+ (void)bf_dismissWithDelay:(NSTimeInterval)duration;

///设置成功图片logo
+ (void)bf_setSuccessImage:(UIImage *)successImage;

///设置失败图片logo
+ (void)bf_setFailImage:(UIImage *)failImage;

///设置警告图片logo
+ (void)bf_setWarningImage:(UIImage *)warningImage;

@end

NS_ASSUME_NONNULL_END
