//
//  BFToastManager.m
//  BFComic
//
//  Created by 俊文 on 2021/6/16.
//

#import "BFToastManager.h"
#define BFToastSuccessImageName @"toast_icon_success"
#define BFToastFailImageName @"toast_icon_fail"
#define BFToastWarningImageName @"toast_icon_warning"
//字符串是否为空
#define KStringIsEmpty(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )
@implementation BFToastManager

static UIImage *successImage = nil;
static UIImage *failImage = nil;
static UIImage *warningImage = nil;
static CGSize BFToastMinimumSize;

+ (void)bf_setPanelForegroundColor:(UIColor *)color{
    [SVProgressHUD setForegroundColor:color];
}

+ (void)bf_setPanelBackgroundColor:(UIColor *)color{
    [SVProgressHUD setBackgroundColor:color];
}

+ (void)bf_show{
    [SVProgressHUD show];
}

+ (void)bf_showWithStatus:(NSString *)status{
    if (KStringIsEmpty(status)) {
        return;
    }
    [self configToast];
    [SVProgressHUD showWithStatus:status];
}
+ (void)bf_showWarningWithStatus:(NSString *)status duration:(NSTimeInterval)duration {
    if (KStringIsEmpty(status)) {
        return;
    }
    [self configToast];
    UIImage *image = [UIImage imageNamed:BFToastWarningImageName];
    if (warningImage) {
        image = warningImage;
    }
    [self setInfoImage:image];
    [self showInfoWithStatus:status];
    [self dismissWithDelay:duration];
}

+ (void)bf_showWithText:(NSString *)status duration:(NSTimeInterval)duration {
    if (KStringIsEmpty(status)) {
        return;
    }
    [self configToast];
    [self showImage:[UIImage imageNamed:@"这里不要改成new,new会有空白"] status:status];
    [self dismissWithDelay:duration];
}

+ (void)bf_showSuccessWithStatus:(NSString *)status duration:(NSTimeInterval)duration {
    if (KStringIsEmpty(status)) {
        return;
    }
    [self configToast];
    UIImage *image = [UIImage imageNamed:BFToastSuccessImageName];
    if (successImage) {
        image = successImage;
    }
    [SVProgressHUD setSuccessImage:image];
    [SVProgressHUD showSuccessWithStatus:status];
    [SVProgressHUD dismissWithDelay:duration];
}

+ (void)bf_showErrorWithStatus:(NSString *)status duration:(NSTimeInterval)duration{
    if (KStringIsEmpty(status)) {
        return;
    }
    [self configToast];
    UIImage *image = [UIImage imageNamed:BFToastFailImageName];
    if (failImage) {
        image = failImage;
    }
    [SVProgressHUD setErrorImage:image];
    [SVProgressHUD showErrorWithStatus:status];
    [SVProgressHUD dismissWithDelay:duration];
}

+ (void)bf_showProgress:(float)progress{
    [self configToast];
    [SVProgressHUD showProgress:progress];
}

+ (void)bf_showProgress:(float)progress status:(NSString *)status{
    [self configToast];
    [SVProgressHUD showProgress:progress status:status];
}

+ (void)bf_dismiss{
    
    [SVProgressHUD dismiss];
}

+ (void)bf_dismissWithDelay:(NSTimeInterval)duration {
    
    [SVProgressHUD dismissWithDelay:duration];
}

+ (void)configToast {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    if (BFToastMinimumSize.height == 0 && BFToastMinimumSize.width == 0) {
        BFToastMinimumSize = CGSizeMake(150, 70);
    }
    [SVProgressHUD setMinimumSize:BFToastMinimumSize];
}

+(void)bf_setSuccessImage:(UIImage *)successImage{
    successImage = successImage;
}

+(void)bf_setFailImage:(UIImage *)failImage{
    failImage = failImage;
}

+(void)bf_setWarningImage:(UIImage *)warningImage{
    warningImage = warningImage;
}


@end
