//
//  AppDelegate.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/7.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

