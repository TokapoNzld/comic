//
//  BF_BookIntroductionItem.h
//  BFComic
//
//  Created by majunwen on 2022/3/9.
//

#import <UIKit/UIKit.h>
#import "BookDetail_InfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BookDetail_IntroductionItem : UICollectionViewCell

@property (nonatomic,strong) BookDetail_InfoModel *bookdetail_introductionModel;
@property (nonatomic,copy) void(^bookdetail_addToBookshelfBlock)(NSString *comicId);

@end

NS_ASSUME_NONNULL_END
