//
//  BF_BookDirectoryItem.h
//  BFComic
//
//  Created by 马俊文 on 2022/3/9.
//

#import <UIKit/UIKit.h>
#import "BookDetail_InfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BookDetail_DirectoryItem : UICollectionViewCell

@property (nonatomic,strong) BookDetail_ChapterModel *bookdetail_directoryModel;
@property (nonatomic,assign) NSInteger bookdetail_directoryIndex;

@end

NS_ASSUME_NONNULL_END
