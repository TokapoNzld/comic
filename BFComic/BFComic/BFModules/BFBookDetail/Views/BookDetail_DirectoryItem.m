//
//  BF_BookDirectoryItem.m
//  BFComic
//
//  Created by 马俊文 on 2022/3/9.
//

#import "BookDetail_DirectoryItem.h"

@interface BookDetail_DirectoryItem()

@property (nonatomic,strong) UILabel *bookdetail_titleLabel;

@end

@implementation BookDetail_DirectoryItem

- (void)setBookdetail_directoryIndex:(NSInteger)bookdetail_directoryIndex{
    _bookdetail_directoryIndex = bookdetail_directoryIndex;
    self.bookdetail_titleLabel.text = [NSString stringWithFormat:@"第%ld话",bookdetail_directoryIndex+1];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self bookdetail_BookDirectoryItem_setupUI];
        [self bookdetail_BookDirectoryItem_setupLayout];
    }
    return self;
}

- (void)bookdetail_BookDirectoryItem_setupUI{
    self.layer.cornerRadius = 5;
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self.contentView addSubview:self.bookdetail_titleLabel];
}

- (void)bookdetail_BookDirectoryItem_setupLayout{
    [self.bookdetail_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
    }];
}

#pragma mark - lazy loading
-(UILabel *)bookdetail_titleLabel{
    if (!_bookdetail_titleLabel) {
        _bookdetail_titleLabel = [[UILabel alloc] init];
        _bookdetail_titleLabel.text = @"第1话";
        _bookdetail_titleLabel.font = [UIFont systemFontOfSize:15];
        _bookdetail_titleLabel.textColor = UIColorFromRGB(240, 240, 240);
    }
    return _bookdetail_titleLabel;
}


@end
