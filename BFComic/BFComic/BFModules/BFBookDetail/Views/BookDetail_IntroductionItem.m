//
//  BF_BookIntroductionItem.m
//  BFComic
//
//  Created by majunwen on 2022/3/9.
//

#import "BookDetail_IntroductionItem.h"
#import "SDWebImage.h"

#define kBookDetailIntroductionItemTextColor UIColorFromRGB(240, 240, 240)

@interface BookDetail_IntroductionItem()

@property (nonatomic,strong) UIImageView *bookdetail_bookImageView;
@property (nonatomic,strong) UILabel *bookdetail_nameLabel;
@property (nonatomic,strong) UILabel *bookdetail_authorLabel;
@property (nonatomic,strong) UILabel *bookdetail_totalChapterLabel;
@property (nonatomic,strong) UIButton *bookdetail_addBookcaseButton;
@property (nonatomic,strong) UILabel *bookdetail_contentLabel;
@property (nonatomic,strong) UILabel *bookdetail_chapterTitleLabel;

@end

@implementation BookDetail_IntroductionItem

-(void)setBookdetail_introductionModel:(BookDetail_InfoModel *)bookdetail_introductionModel{
    _bookdetail_introductionModel = bookdetail_introductionModel;
    [self.bookdetail_bookImageView sd_setImageWithURL:[NSURL URLWithString:bookdetail_introductionModel.imgUrl] placeholderImage:kDefaultImage];
    self.bookdetail_nameLabel.text = bookdetail_introductionModel.cartoonTitle;
    self.bookdetail_authorLabel.text = bookdetail_introductionModel.author;
    self.bookdetail_totalChapterLabel.text = [NSString stringWithFormat:@"共%ld话",bookdetail_introductionModel.list.count];
    self.bookdetail_contentLabel.text = [NSString stringWithFormat:@"简介: %@", bookdetail_introductionModel.describe];
    
    NSString *result = @"";
    if ([self.bookdetail_introductionModel.bookSelfFlag intValue] == 0) {
        result = @"加入";
    }else if ([self.bookdetail_introductionModel.bookSelfFlag intValue] == 1) {
        result = @"移出";
    }
    [self.bookdetail_addBookcaseButton setTitle:[NSString stringWithFormat:@"%@书架",result] forState:UIControlStateNormal];
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self bookdetail_IntroductionItem_setupUI];
        [self bookdetail_IntroductionItem_setupLayout];
    }
    return self;
}

- (void)bookdetail_IntroductionItem_addToBookshelf {
    self.bookdetail_addToBookshelfBlock ? self.bookdetail_addToBookshelfBlock(self.bookdetail_introductionModel.comicId) : nil;
}

- (void)bookdetail_IntroductionItem_setupUI{
    [self.contentView addSubview:self.bookdetail_bookImageView];
    [self.contentView addSubview:self.bookdetail_nameLabel];
    [self.contentView addSubview:self.bookdetail_authorLabel];
    [self.contentView addSubview:self.bookdetail_totalChapterLabel];
    [self.contentView addSubview:self.bookdetail_addBookcaseButton];
    [self.contentView addSubview:self.bookdetail_contentLabel];
    [self.contentView addSubview:self.bookdetail_chapterTitleLabel];
}

- (void)bookdetail_IntroductionItem_setupLayout{
    [self.bookdetail_bookImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(120);
    }];
    
    [self.bookdetail_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bookdetail_bookImageView.mas_right).offset(6);
        make.top.equalTo(self.bookdetail_bookImageView.mas_top).offset(3);
        make.right.equalTo(self.contentView.mas_right).offset(-12);
    }];
    
    [self.bookdetail_authorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bookdetail_nameLabel.mas_left);
        make.top.equalTo(self.bookdetail_nameLabel.mas_bottom).offset(10);
    }];
    
    [self.bookdetail_totalChapterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bookdetail_nameLabel.mas_left);
        make.top.equalTo(self.bookdetail_authorLabel.mas_bottom).offset(10);
    }];
    
    [self.bookdetail_addBookcaseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bookdetail_nameLabel.mas_left);
        make.top.equalTo(self.bookdetail_totalChapterLabel.mas_bottom).offset(8);
        make.width.mas_equalTo(70);
    }];
    
    [self.bookdetail_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bookdetail_bookImageView.mas_left);
        make.top.equalTo(self.bookdetail_bookImageView.mas_bottom).offset(20);
        make.right.mas_equalTo(0);
    }];
    
    [self.bookdetail_chapterTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bookdetail_contentLabel.mas_bottom).offset(40);
        make.left.equalTo(self.bookdetail_bookImageView.mas_left);
    }];
}

#pragma mark - lazy loading
-(UIImageView *)bookdetail_bookImageView{
    if (!_bookdetail_bookImageView) {
        _bookdetail_bookImageView = [[UIImageView alloc] init];
        _bookdetail_bookImageView.image = [UIImage imageNamed:@"comic_placeholder"];
        _bookdetail_bookImageView.clipsToBounds = YES;
        _bookdetail_bookImageView.layer.cornerRadius = 5;
        _bookdetail_bookImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _bookdetail_bookImageView;
}

-(UILabel *)bookdetail_nameLabel{
    if (!_bookdetail_nameLabel) {
        _bookdetail_nameLabel = [[UILabel alloc] init];
        _bookdetail_nameLabel.text = @"海贼王";
        _bookdetail_nameLabel.textColor = kBookDetailIntroductionItemTextColor;
        _bookdetail_nameLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
        _bookdetail_nameLabel.numberOfLines = 0;
    }
    return _bookdetail_nameLabel;
}

-(UILabel *)bookdetail_authorLabel{
    if (!_bookdetail_authorLabel) {
        _bookdetail_authorLabel = [[UILabel alloc] init];
        _bookdetail_authorLabel.text = @"作者：尾田荣一郎";
        _bookdetail_authorLabel.textColor = kBookDetailIntroductionItemTextColor;
        _bookdetail_authorLabel.font = [UIFont systemFontOfSize:14];
    }
    return _bookdetail_authorLabel;
}

-(UILabel *)bookdetail_totalChapterLabel{
    if (!_bookdetail_totalChapterLabel) {
        _bookdetail_totalChapterLabel = [[UILabel alloc] init];
        _bookdetail_totalChapterLabel.text = @"共200话";
        _bookdetail_totalChapterLabel.textColor = kBookDetailIntroductionItemTextColor;
        _bookdetail_totalChapterLabel.font = [UIFont systemFontOfSize:14];
    }
    return _bookdetail_totalChapterLabel;
}

-(UIButton *)bookdetail_addBookcaseButton{
    if (!_bookdetail_addBookcaseButton) {
        _bookdetail_addBookcaseButton = [[UIButton alloc] init];
        [_bookdetail_addBookcaseButton setTitle:@"加入书架" forState:UIControlStateNormal];
        [_bookdetail_addBookcaseButton setTitleColor:kBookDetailIntroductionItemTextColor forState:UIControlStateNormal];
        _bookdetail_addBookcaseButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _bookdetail_addBookcaseButton.layer.cornerRadius = 5;
        _bookdetail_addBookcaseButton.layer.borderWidth = 1.0f;
        _bookdetail_addBookcaseButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [_bookdetail_addBookcaseButton addTarget:self action:@selector(bookdetail_IntroductionItem_addToBookshelf) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bookdetail_addBookcaseButton;
}

-(UILabel *)bookdetail_contentLabel{
    if (!_bookdetail_contentLabel) {
        _bookdetail_contentLabel = [[UILabel alloc] init];
//        _bookdetail_contentLabel.text = @"时值“大海贼时代”，为了寻找传说中海贼王罗杰所留下的大秘宝“ONE PIECE”，无数海贼扬起旗帜，互相争斗。有一个梦想成为海贼王的少年叫路飞，他因误食“恶魔果实”而成为了橡皮人，在获得超人能力的同时付出了一辈子无法游泳的代价。十年后，路飞为实现与因救他而断臂的香克斯的约定而出海，他在旅途中不断寻找志同道合的伙伴，开始了以成为海贼王为目标的冒险旅程 [11]  。";
        _bookdetail_contentLabel.textColor = kBookDetailIntroductionItemTextColor;
        _bookdetail_contentLabel.font = [UIFont systemFontOfSize:15];
        _bookdetail_contentLabel.numberOfLines = 0;
    }
    return _bookdetail_contentLabel;
}

-(UILabel *)bookdetail_chapterTitleLabel{
    if (!_bookdetail_chapterTitleLabel) {
        _bookdetail_chapterTitleLabel = [[UILabel alloc] init];
        _bookdetail_chapterTitleLabel.text = @"章节列表";
        _bookdetail_chapterTitleLabel.textColor = kBookDetailIntroductionItemTextColor;
        _bookdetail_chapterTitleLabel.font = [UIFont systemFontOfSize:18];
    }
    return _bookdetail_chapterTitleLabel;
}

@end
