//
//  BookDetail_ViewModel.m
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import "BookDetail_ViewModel.h"
#import "BF_ApiConfig.h"
#import "YYModel.h"

@implementation BookDetail_ViewModel

- (void)bookDetail_ViewModel_requestCartoonDetailById:(NSString *)comicId success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] GET:BF_API_CARTOONBYID parameters:@{@"id":comicId} downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        if ([responseObject[@"code"] integerValue] == 200) {
            self.bookdetail_model = [BookDetail_InfoModel yy_modelWithDictionary:responseObject[@"data"]];
        }
        success ? success(task,responseObject) : nil;
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}

- (void)bookDetail_ViewModel_requestAddComicBookshelfById:(NSString *)comicId success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] GET:BF_API_UPDATE_COMIC_BOOKSELF parameters:@{@"cartoonId":comicId} downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        success ? success(task,responseObject) : nil;
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}

@end
