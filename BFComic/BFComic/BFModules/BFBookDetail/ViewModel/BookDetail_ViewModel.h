//
//  BookDetail_ViewModel.h
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import <Foundation/Foundation.h>
#import "BFBaseRequest.h"
#import "BookDetail_InfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BookDetail_ViewModel : NSObject

@property (nonatomic,strong) BookDetail_InfoModel *bookdetail_model;

- (void)bookDetail_ViewModel_requestCartoonDetailById:(NSString *)comicId success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

- (void)bookDetail_ViewModel_requestAddComicBookshelfById:(NSString *)comicId success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

@end

NS_ASSUME_NONNULL_END
