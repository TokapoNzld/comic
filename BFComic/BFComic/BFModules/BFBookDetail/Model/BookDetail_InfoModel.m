//
//  BookDetail_InfoModel.m
//  BFComic
//
//  Created by majunwen on 2022/4/29.
//

#import "BookDetail_InfoModel.h"

@implementation BookDetail_ChapterModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
        @"chapterId" : @"id"
    };
}

@end

@implementation BookDetail_InfoModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
        @"comicId" : @"id"
    };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
        @"list" : [BookDetail_ChapterModel class]
    };
}

@end
