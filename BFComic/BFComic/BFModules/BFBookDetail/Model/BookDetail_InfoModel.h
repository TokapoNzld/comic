//
//  BookDetail_InfoModel.h
//  BFComic
//
//  Created by majunwen on 2022/4/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookDetail_ChapterModel : NSObject

@property (nonatomic,copy) NSString *chapterId;
@property (nonatomic,copy) NSString *isHistory;
@property (nonatomic,copy) NSString *msg;
@property (nonatomic,copy) NSString *rank;

@end

@interface BookDetail_InfoModel : NSObject

@property (nonatomic,copy) NSString *author;
@property (nonatomic,copy) NSString *bookSelfFlag;
@property (nonatomic,copy) NSString *cartoonCatalogueNumber;
@property (nonatomic,copy) NSString *cartoonTitle;
@property (nonatomic,copy) NSString *classification;
@property (nonatomic,copy) NSString *describe;
@property (nonatomic,copy) NSString *hot;
@property (nonatomic,copy) NSString *comicId;
@property (nonatomic,copy) NSString *imgUrl;
@property (nonatomic,copy) NSArray<BookDetail_ChapterModel *> *list;
@property (nonatomic,copy) NSString *region;
@property (nonatomic,copy) NSString *state;
@property (nonatomic,copy) NSString *theme;
@property (nonatomic,copy) NSString *updateTime;

@end

NS_ASSUME_NONNULL_END
