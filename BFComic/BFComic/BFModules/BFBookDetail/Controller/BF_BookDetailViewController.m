//
//  BF_BookDetailViewController.m
//  BFComic
//
//  Created by majunwen on 2022/3/9.
//

#import "BF_BookDetailViewController.h"
#import "BookDetail_IntroductionItem.h"
#import "BookDetail_DirectoryItem.h"
#import "BF_ReaderViewController.h"
#import "BookDetail_ViewModel.h"
#import "NSString+BFAd.h"
#import "BFToastManager.h"

@interface BF_BookDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) UICollectionView *bookdetail_collectionView;
@property (nonatomic,strong) BookDetail_ViewModel *bookdetail_ViewModel;

@end

@implementation BF_BookDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = AppMainThemeColor;
    [self bookdetail_BookDetailViewController_requestDetail];
}

-(void)base_setupViews{
    [self.view addSubview:self.bookdetail_collectionView];
}

-(void)base_setupLayout{
    [self.bookdetail_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

-(void)bookdetail_BookDetailViewController_requestDetail {
    __weak typeof(self) weakSelf = self;
    [self.bookdetail_ViewModel bookDetail_ViewModel_requestCartoonDetailById:self.bookdetail_comicId success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [weakSelf.bookdetail_collectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

-(void)bookdetail_BookDetailViewController_addToBookshelfWithComicId:(NSString *)comicId {
    NSString *result = @"";
    if ([self.bookdetail_ViewModel.bookdetail_model.bookSelfFlag intValue] == 0) {
        result = @"加入";
    }else if ([self.bookdetail_ViewModel.bookdetail_model.bookSelfFlag intValue] == 1) {
        result = @"移出";
    }
    __weak typeof(self) weakSelf = self;
    [self.bookdetail_ViewModel bookDetail_ViewModel_requestAddComicBookshelfById:comicId success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == 200) {
            [BFToastManager bf_showSuccessWithStatus:[NSString stringWithFormat:@"%@书架成功！",result] duration:1.0];
            [[NSNotificationCenter defaultCenter] postNotificationName:kBookshelfAddToComicNotificationName object:nil userInfo:nil];
            [weakSelf bookdetail_BookDetailViewController_requestDetail];
        }else {
            [BFToastManager bf_showSuccessWithStatus:[NSString stringWithFormat:@"%@书架失败！",result] duration:1.0];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        [BFToastManager bf_showSuccessWithStatus:[NSString stringWithFormat:@"%@书架失败！",result] duration:1.0];
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

#pragma mark - UICollectionView
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 1) {
        return self.bookdetail_ViewModel.bookdetail_model.list.count;
    }
    return 1;
}

-(__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        BookDetail_IntroductionItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BF_BookIntroductionItem" forIndexPath:indexPath];
        cell.bookdetail_introductionModel = self.bookdetail_ViewModel.bookdetail_model;
        cell.bookdetail_addToBookshelfBlock = ^(NSString * _Nonnull comicId) {
            [self bookdetail_BookDetailViewController_addToBookshelfWithComicId:comicId];
        };
        return cell;
    }else{
        BookDetail_DirectoryItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BF_BookDirectoryItem" forIndexPath:indexPath];
        cell.bookdetail_directoryIndex = indexPath.row;
        cell.bookdetail_directoryModel = self.bookdetail_ViewModel.bookdetail_model.list[indexPath.row];
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) return;
    BF_ReaderViewController *vc = [[BF_ReaderViewController alloc] init];
    vc.reader_chapterId = self.bookdetail_ViewModel.bookdetail_model.list[indexPath.row].chapterId;
    vc.reader_comicId = self.bookdetail_ViewModel.bookdetail_model.comicId;
    [self.navigationController pushViewController:vc animated:YES];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        CGFloat contentHeight = [self.bookdetail_ViewModel.bookdetail_model.describe bf_heightWithFont:[UIFont systemFontOfSize:15] constrainedToWidth:kScreenWidth-30];
        return CGSizeMake(kScreenWidth-30, 215 + contentHeight);
    }else{
        return CGSizeMake((kScreenWidth - 30 - 30)/4, 30);
    }
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 16;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}

#pragma mark - lazy loading
-(UICollectionView *)bookdetail_collectionView{
    if (!_bookdetail_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _bookdetail_collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _bookdetail_collectionView.delegate = self;
        _bookdetail_collectionView.dataSource = self;
        _bookdetail_collectionView.contentInset = UIEdgeInsetsMake(15, 15, 0, 15);
        _bookdetail_collectionView.showsVerticalScrollIndicator = NO;
        [_bookdetail_collectionView registerClass:[BookDetail_IntroductionItem class] forCellWithReuseIdentifier:@"BF_BookIntroductionItem"];
        [_bookdetail_collectionView registerClass:[BookDetail_DirectoryItem class] forCellWithReuseIdentifier:@"BF_BookDirectoryItem"];
        _bookdetail_collectionView.backgroundColor = AppMainThemeColor;
    }
    return _bookdetail_collectionView;
}

-(BookDetail_ViewModel *)bookdetail_ViewModel{
    if (!_bookdetail_ViewModel) {
        _bookdetail_ViewModel = [[BookDetail_ViewModel alloc] init];
    }
    return _bookdetail_ViewModel;
}

@end
