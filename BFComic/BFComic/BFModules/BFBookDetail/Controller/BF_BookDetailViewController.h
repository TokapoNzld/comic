//
//  BF_BookDetailViewController.h
//  BFComic
//
//  Created by majunwen on 2022/3/9.
//

#import "BFBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BF_BookDetailViewController : BFBaseViewController

@property (nonatomic,copy) NSString *bookdetail_comicId;

@end

NS_ASSUME_NONNULL_END
