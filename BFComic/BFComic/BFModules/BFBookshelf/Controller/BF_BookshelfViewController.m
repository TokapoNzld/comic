//
//  BookshelfViewController.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import "BF_BookshelfViewController.h"
#import "Bookshelf_ContentCollectionViewCell.h"
#import "Bookshelf_ViewModel.h"
#import "BF_BookDetailViewController.h"

@interface BF_BookshelfViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *bookshelf_collectionView;
@property (nonatomic, strong) Bookshelf_ViewModel *bookshelf_comicViewModel;

@end

@implementation BF_BookshelfViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = AppMainThemeColor;
    self.navigationItem.title = @"书架";
    [self bookshelf_BookshelfViewController_requestBookSelfComicList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookshelf_BookshelfViewController_requestBookSelfComicList) name:kLoginViewModelLoginNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookshelf_BookshelfViewController_requestBookSelfComicList) name:kLoginViewModelLogoutNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookshelf_BookshelfViewController_requestBookSelfComicList) name:kBookshelfAddToComicNotificationName object:nil];
}

- (void)base_setupViews {
    [self.view addSubview:self.bookshelf_collectionView];
}

- (void)base_setupLayout {
    [self.bookshelf_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.view);
        make.left.equalTo(self.view.mas_left).offset(12);
        make.right.equalTo(self.view.mas_right).offset(-12);
    }];
}

-(void)bookshelf_BookshelfViewController_requestBookSelfComicList{
    __weak typeof(self) weakSelf = self;
    [self.bookshelf_comicViewModel bookshelf_ViewModel_requestFindComicWithSuccess:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [weakSelf.bookshelf_collectionView reloadData];
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

#pragma mark - UICollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.bookshelf_comicViewModel.bookshelf_comicList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Bookshelf_ContentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Bookshelf_ContentCollectionViewCell" forIndexPath:indexPath];
    cell.bookshelf_cellComicModel = self.bookshelf_comicViewModel.bookshelf_comicList[indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = collectionView.bounds.size.width/3;
    return CGSizeMake(width, 200);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    BF_BookDetailViewController *vc = [[BF_BookDetailViewController alloc] init];
    vc.bookdetail_comicId = self.bookshelf_comicViewModel.bookshelf_comicList[indexPath.row].comicId;
    vc.navigationItem.title = self.bookshelf_comicViewModel.bookshelf_comicList[indexPath.row].cartoonTitle;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Getter && Setter
- (UICollectionView *)bookshelf_collectionView {
    if (!_bookshelf_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        _bookshelf_collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _bookshelf_collectionView.delegate = self;
        _bookshelf_collectionView.dataSource = self;
        _bookshelf_collectionView.showsVerticalScrollIndicator = NO;
        _bookshelf_collectionView.backgroundColor = AppMainThemeColor;
        _bookshelf_collectionView.showsVerticalScrollIndicator = false;
        _bookshelf_collectionView.contentInset = UIEdgeInsetsMake(10, 0, 10, 0);
        [_bookshelf_collectionView registerClass:[Bookshelf_ContentCollectionViewCell class] forCellWithReuseIdentifier:@"Bookshelf_ContentCollectionViewCell"];
    }
    return _bookshelf_collectionView;
}

-(Bookshelf_ViewModel *)bookshelf_comicViewModel{
    if (!_bookshelf_comicViewModel) {
        _bookshelf_comicViewModel = [[Bookshelf_ViewModel alloc] init];
    }
    return _bookshelf_comicViewModel;
}

@end
