//
//  Bookshelf_ContentCollectionViewCell.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import <UIKit/UIKit.h>
#import "Bookshelf_ComicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Bookshelf_ContentCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) Bookshelf_ComicModel *bookshelf_cellComicModel;

@end

NS_ASSUME_NONNULL_END
