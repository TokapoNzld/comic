//
//  Bookshelf_ContentCollectionViewCell.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import "Bookshelf_ContentCollectionViewCell.h"
#import "SDWebImage.h""

@interface Bookshelf_ContentCollectionViewCell ()

@property (nonatomic, strong) UIImageView *bookshelf_imageView;
@property (nonatomic, strong) UILabel *bookshelf_titleLabel;
@property (nonatomic, strong) UILabel *bookshelf_recordLabel;

@end

@implementation Bookshelf_ContentCollectionViewCell

-(void)setBookshelf_cellComicModel:(Bookshelf_ComicModel *)bookshelf_cellComicModel{
    _bookshelf_cellComicModel = bookshelf_cellComicModel;
    
    [self.bookshelf_imageView sd_setImageWithURL:[NSURL URLWithString:bookshelf_cellComicModel.imgUrl] placeholderImage:kDefaultImage];
    self.bookshelf_titleLabel.text = bookshelf_cellComicModel.cartoonTitle;
    self.bookshelf_recordLabel.text = bookshelf_cellComicModel.cartoonCatalogueMsg;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if ([super initWithFrame:frame]) {
        self.contentView.backgroundColor = AppMainThemeColor;
        [self bookshelf_ContentCollectionViewCell_setupUI];
        [self bookshelf_ContentCollectionViewCell_setupLayout];
    }
    return self;
}

- (void)bookshelf_ContentCollectionViewCell_setupUI {
    [self.contentView addSubview:self.bookshelf_imageView];
    [self.contentView addSubview:self.bookshelf_titleLabel];
    [self.contentView addSubview:self.bookshelf_recordLabel];
}

- (void)bookshelf_ContentCollectionViewCell_setupLayout {
    [self.bookshelf_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(5);
        make.left.equalTo(self.contentView.mas_left).offset(8);
        make.right.equalTo(self.contentView.mas_right).offset(-8);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-50);
    }];
    
    [self.bookshelf_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bookshelf_imageView.mas_bottom).offset(5);
        make.left.right.equalTo(self.bookshelf_imageView);
    }];
    
    [self.bookshelf_recordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bookshelf_titleLabel.mas_bottom).offset(5);
        make.left.right.equalTo(self.bookshelf_imageView);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-5);
    }];
}

#pragma mark - Getter && Setter
- (UIImageView *)bookshelf_imageView {
    if (!_bookshelf_imageView) {
        _bookshelf_imageView = [[UIImageView alloc] init];
//        _bookshelf_imageView.image = [UIImage imageNamed:@"comic_placeholder2"];
        _bookshelf_imageView.layer.masksToBounds = YES;
        _bookshelf_imageView.layer.cornerRadius = 8;
    }
    return _bookshelf_imageView;
}

- (UILabel *)bookshelf_titleLabel {
    if (!_bookshelf_titleLabel) {
        _bookshelf_titleLabel = [[UILabel alloc] init];
//        _bookshelf_titleLabel.text = @"鬼灭之刃";
        _bookshelf_titleLabel.textAlignment = NSTextAlignmentCenter;
        _bookshelf_titleLabel.font = [UIFont systemFontOfSize:14];
        _bookshelf_titleLabel.textColor = [UIColor whiteColor];
    }
    return _bookshelf_titleLabel;
}

- (UILabel *)bookshelf_recordLabel {
    if (!_bookshelf_recordLabel) {
        _bookshelf_recordLabel = [[UILabel alloc] init];
        _bookshelf_recordLabel.text = @"234/999话";
        _bookshelf_recordLabel.textAlignment = NSTextAlignmentCenter;
        _bookshelf_recordLabel.font = [UIFont systemFontOfSize:14];
        _bookshelf_recordLabel.textColor = [UIColor whiteColor];
    }
    return _bookshelf_recordLabel;
}

@end
