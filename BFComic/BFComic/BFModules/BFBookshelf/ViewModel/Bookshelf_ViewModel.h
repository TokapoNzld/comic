//
//  Bookshelf_ViewModel.h
//  BFComic
//
//  Created by majunwen on 2022/5/24.
//

#import <Foundation/Foundation.h>
#import "BFBaseRequest.h"
#import "Bookshelf_ComicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Bookshelf_ViewModel : NSObject

@property (nonatomic,strong) NSArray <Bookshelf_ComicModel *> *bookshelf_comicList;

- (void)bookshelf_ViewModel_requestFindComicWithSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

@end

NS_ASSUME_NONNULL_END
