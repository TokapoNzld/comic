//
//  Bookshelf_ComicModel.m
//  BFComic
//
//  Created by majunwen on 2022/5/24.
//

#import "Bookshelf_ComicModel.h"

@implementation Bookshelf_ComicModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
        @"comicId" : @"id"
    };
}

@end
