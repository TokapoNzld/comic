//
//  Bookshelf_ViewModel.m
//  BFComic
//
//  Created by majunwen on 2022/5/24.
//

#import "Bookshelf_ViewModel.h"
#import "BF_ApiConfig.h"
#import "MJExtension.h"

@implementation Bookshelf_ViewModel

- (void)bookshelf_ViewModel_requestFindComicWithSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] GET:BF_API_USER_BOOKSELF_LIST parameters:nil downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        self.bookshelf_comicList = [Bookshelf_ComicModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}

@end
