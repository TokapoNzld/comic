//
//  Bookshelf_ComicModel.h
//  BFComic
//
//  Created by majunwen on 2022/5/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Bookshelf_ComicModel : NSObject

@property (nonatomic,copy) NSString *author;
@property (nonatomic,copy) NSString *cartoonTitle;
@property (nonatomic,copy) NSString *comicId;
@property (nonatomic,copy) NSString *hot;
@property (nonatomic,copy) NSString *imgUrl;
@property (nonatomic,copy) NSString *cartoonCatalogueMsg;

@end

NS_ASSUME_NONNULL_END
