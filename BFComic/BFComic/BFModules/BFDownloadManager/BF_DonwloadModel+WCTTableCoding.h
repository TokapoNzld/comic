//
//  BF_DonwloadModel+WCTTableCoding.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/22.
//

#import "BF_DonwloadModel.h"
#import <WCDB/WCDB.h>

@interface BF_DonwloadModel (WCTTableCoding) <WCTTableCoding>

WCDB_PROPERTY(comic_id)
WCDB_PROPERTY(pageNum)
WCDB_PROPERTY(filePaths)

@end
