//
//  BF_DonwloadModel.mm
//  BFComic
//
//  Created by TokapoNzld on 2022/3/22.
//

#import "BF_DonwloadModel+WCTTableCoding.h"
#import "BF_DonwloadModel.h"
#import <WCDB/WCDB.h>

@implementation BF_DonwloadModel

WCDB_IMPLEMENTATION(BF_DonwloadModel)
WCDB_SYNTHESIZE(BF_DonwloadModel, comic_id)
WCDB_SYNTHESIZE(BF_DonwloadModel, pageNum)
WCDB_SYNTHESIZE(BF_DonwloadModel, filePaths)

WCDB_PRIMARY_ASC_AUTO_INCREMENT(BF_DonwloadModel, comic_id)
  
@end
