//
//  BF_DonwloadModel.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/22.
//

#import <Foundation/Foundation.h>

/*
 {
    漫画id: 123456,
    总话数: 1002,
    文件路径:[
        {
            第1话: [路径数组],
            第2话: [路径数组]
        }
    ]
 }
 
 e.g:
 {
    "comic_id": 123456,
    "pageNum": 1002
    "filePages": [
        {
            "1":
                    [
                        ../Document/Donwloads/123456/123456_1_1.png, // 文件名 = 漫画id + 话数 + 第几张图片
                        ../Document/Donwloads/123456/123456_1_2.png,
                        ../Document/Donwloads/123456/123456_1_3.png,
                        ... ... ...
                    ]
        },
        {
             "2":
                     [
                         ../Document/Donwloads/123456/123456_2_1.png, // 文件名 = 漫画id + 话数 + 第几张图片
                         ../Document/Donwloads/123456/123456_2_2.png,
                         ../Document/Donwloads/123456/123456_2_3.png,
                         ... ... ...
                     ]
        }
    ]
 }
 **/

@interface BF_DonwloadModel : NSObject

@property (nonatomic, assign) NSInteger comic_id;  // 漫画id
@property (nonatomic, assign) NSInteger pageNum;   // 漫画总话数
@property (nonatomic, strong) NSArray<NSDictionary *> *filePaths;   // 文件路径数组

@end
