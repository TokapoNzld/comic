//
//  BF_ReaderBatteryTimeView.m
//  BFComic
//
//  Created by majunwen on 2022/3/25.
//

#import "Reader_BatteryTimeView.h"
#import "Reader_BatteryView.h"

@interface Reader_BatteryTimeView()

@property (nonatomic,strong) Reader_BatteryView *reader_batteryView;
@property (nonatomic,strong) UILabel *reader_locationTimeLabel;

@end

@implementation Reader_BatteryTimeView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self reader_BatteryTimeView_setupUI];
        [self reader_BatteryTimeView_setupLayout];
        [self reader_BatteryTimeView_setBlock];
    }
    return self;
}

-(void)reader_BatteryTimeView_setupUI{
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    
    [self addSubview:self.reader_batteryView];
    [self addSubview:self.reader_locationTimeLabel];
}

-(void)reader_BatteryTimeView_setupLayout{
    [self.reader_batteryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(12);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [self.reader_locationTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.reader_batteryView.mas_left).offset(-5);
    }];
}

-(void)reader_BatteryTimeView_setBlock{
    __weak typeof(self) weakSelf = self;
    self.reader_batteryView.reader_batteryChangeBlock = ^(float batteryLevel) {
//        weakSelf.reader_locationTimeLabel.text = []
    };
}

#pragma mark - lazy loading
-(Reader_BatteryView *)reader_batteryView{
    if (!_reader_batteryView) {
        _reader_batteryView = [[Reader_BatteryView alloc] init];
    }
    return _reader_batteryView;
}

-(UILabel *)reader_locationTimeLabel{
    if (!_reader_locationTimeLabel) {
        _reader_locationTimeLabel = [[UILabel alloc] init];
        _reader_locationTimeLabel.text = @"2话 5/100 17:38 WIFI 80%";
        _reader_locationTimeLabel.font =  [UIFont systemFontOfSize:11];
        _reader_locationTimeLabel.textColor = [UIColor whiteColor];
    }
    return _reader_locationTimeLabel;
}

@end
