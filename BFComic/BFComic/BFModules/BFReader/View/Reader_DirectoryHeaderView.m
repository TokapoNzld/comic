//
//  BF_ReaderDirectoryHeaderView.m
//  BFComic
//
//  Created by 马俊文 on 2022/3/10.
//

#import "Reader_DirectoryHeaderView.h"
#import "SDWebImage.h"

@interface Reader_DirectoryHeaderView()

@property (nonatomic,strong) UIImageView *reader_directoryImageView;
@property (nonatomic,strong) UILabel *reader_directoryNameLabel;
@property (nonatomic,strong) UILabel *reader_directoryTypeLabel;
@property (nonatomic,strong) UILabel *reader_directoryTotalChapterLabel;
@property (nonatomic,strong) UIButton *reader_directorySortButton;

@end

@implementation Reader_DirectoryHeaderView

-(void)setReader_directoryHeaderModel:(BookDetail_InfoModel *)reader_directoryHeaderModel{
    _reader_directoryHeaderModel = reader_directoryHeaderModel;
    [self.reader_directoryImageView sd_setImageWithURL:[NSURL URLWithString:reader_directoryHeaderModel.imgUrl] placeholderImage:kDefaultImage];
    self.reader_directoryNameLabel.text = reader_directoryHeaderModel.cartoonTitle;
    self.reader_directoryTypeLabel.text = reader_directoryHeaderModel.theme;
    self.reader_directoryTotalChapterLabel.text = [NSString stringWithFormat:@"%@·共%ld章",reader_directoryHeaderModel.state,reader_directoryHeaderModel.list.count];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self reader_DirectoryHeaderView_setupUI];
        [self reader_DirectoryHeaderView_setupLayout];
    }
    return self;
}

-(void)reader_DirectoryHeaderView_setupUI{
    self.backgroundColor = UIColorFromHex(0xECECEC);
    [self addSubview:self.reader_directoryImageView];
    [self addSubview:self.reader_directoryNameLabel];
    [self addSubview:self.reader_directoryTypeLabel];
    [self addSubview:self.reader_directoryTotalChapterLabel];
    [self addSubview:self.reader_directorySortButton];
}

-(void)reader_DirectoryHeaderView_setupLayout{
    [self.reader_directoryImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(20);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(80);
    }];
    
    [self.reader_directoryNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.reader_directoryImageView.mas_right).offset(8);
        make.top.equalTo(self.reader_directoryImageView.mas_top).offset(8);
    }];
    
    [self.reader_directoryTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.reader_directoryNameLabel.mas_left);
        make.top.equalTo(self.reader_directoryNameLabel.mas_bottom).offset(15);
    }];
    
    [self.reader_directoryTotalChapterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.reader_directoryImageView.mas_left);
        make.top.equalTo(self.reader_directoryImageView.mas_bottom).offset(15);
    }];
    
    [self.reader_directorySortButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.equalTo(self.reader_directoryTotalChapterLabel.mas_centerY);
    }];
}

#pragma mark - lazy loading
-(UIImageView *)reader_directoryImageView{
    if (!_reader_directoryImageView) {
        _reader_directoryImageView = [[UIImageView alloc] init];
        _reader_directoryImageView.layer.cornerRadius = 5;
        _reader_directoryImageView.clipsToBounds = YES;
        _reader_directoryImageView.contentMode = UIViewContentModeScaleAspectFill;
        _reader_directoryImageView.image = [UIImage imageNamed:@"comic_placeholder2"];
    }
    return _reader_directoryImageView;
}

-(UILabel *)reader_directoryNameLabel{
    if (!_reader_directoryNameLabel) {
        _reader_directoryNameLabel = [[UILabel alloc] init];
        _reader_directoryNameLabel.text = @"鬼灭之刃";
        _reader_directoryNameLabel.textColor = UIColorFromHex(0x333333);
        _reader_directoryNameLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
    }
    return _reader_directoryNameLabel;
}

-(UILabel *)reader_directoryTypeLabel{
    if (!_reader_directoryTypeLabel) {
        _reader_directoryTypeLabel = [[UILabel alloc] init];
        _reader_directoryTypeLabel.text = @"热血漫画";
        _reader_directoryTypeLabel.textColor = UIColorFromHex(0x666666);
        _reader_directoryTypeLabel.font = [UIFont systemFontOfSize:14];
    }
    return _reader_directoryTypeLabel;
}

-(UILabel *)reader_directoryTotalChapterLabel{
    if (!_reader_directoryTotalChapterLabel) {
        _reader_directoryTotalChapterLabel = [[UILabel alloc] init];
        _reader_directoryTotalChapterLabel.text = @"连载中·共0章";
        _reader_directoryTotalChapterLabel.textColor = UIColorFromHex(0x666666);
        _reader_directoryTotalChapterLabel.font = [UIFont systemFontOfSize:14];
    }
    return _reader_directoryTotalChapterLabel;
}

-(UIButton *)reader_directorySortButton{
    if (!_reader_directorySortButton) {
        _reader_directorySortButton = [[UIButton alloc] init];
        [_reader_directorySortButton setTitle:@"倒序" forState:UIControlStateNormal];
        [_reader_directorySortButton setTitleColor:UIColorFromHex(0x333333) forState:UIControlStateNormal];
        _reader_directorySortButton.titleLabel.font = [UIFont systemFontOfSize:15];
    }
    return _reader_directorySortButton;
}

@end
