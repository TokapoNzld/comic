//
//  BF_ReaderCoverView.m
//  BFComic
//
//  Created by majunwen on 2022/3/10.
//

#import "Reader_CoverView.h"
#import "UIButton+BFAd.h"

@interface Reader_CoverView()

@property (nonatomic,strong) UIView *reader_naviView;
@property (nonatomic,strong) UIButton *reader_backButton;
@property (nonatomic,strong) UILabel *reader_titleLabel;
@property (nonatomic,strong) UIButton *reader_dismissButton;
@property (nonatomic,strong) UIView *reader_naviLineView;
@property (nonatomic,strong) UIView *reader_bottomView;
@property (nonatomic,strong) UIButton *reader_directoryButton;
@property (nonatomic,strong) UIButton *reader_settingButton;
@property (nonatomic,strong) UIView *reader_bottomLineView;
@property (nonatomic,assign) CGFloat statusHeight;

@end

@implementation Reader_CoverView

#pragma mark - public method
+(instancetype)reader_CoverView_init{
    Reader_CoverView *view = [[Reader_CoverView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:view];
    view.hidden = YES;
    return view;
}

-(void)reader_CoverView_smartClick{
    self.isHidden ? [self reader_CoverView_show] : [self reader_CoverView_dismiss];
}


#pragma mark - private method
-(void)reader_CoverView_dismiss{
    [UIView animateWithDuration:0.5 animations:^{
        [self.reader_naviView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(-44-self.statusHeight);
        }];
        
        [self.reader_bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(60+kBottomSafeHeight);
        }];
        
        
        [self setNeedsLayout];
        [self layoutIfNeeded];
    }completion:^(BOOL finished) {
        self.hidden = YES;
    }];
    
}

-(void)reader_CoverView_show{
    
    self.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        [self.reader_naviView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
        }];
        
        [self.reader_bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
        }];
        
        
        [self setNeedsLayout];
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - action
-(void)reader_CoverView_showDirectoryAction{
    [self reader_CoverView_dismiss];
    self.reader_coverPushDirectoryBlock ? self.reader_coverPushDirectoryBlock() : nil;
}

-(void)reader_CoverView_showSettingAction{
    [self reader_CoverView_dismiss];
    self.reader_coverPushSettingBlock ? self.reader_coverPushSettingBlock() : nil;
}

- (void)reader_CoverView_clickBackButtonAction {
    if (self.backCallback) {
        self.backCallback();
        [self removeFromSuperview];
    }
}

#pragma mark - init
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.statusHeight = ISBangsPhone ? 44.0 : 20.0;
        [self reader_CoverView_setupUI];
        [self reader_CoverView_setupLayout];
    }
    return self;
}

-(void)layoutSubviews{
    [self.reader_directoryButton bf_setImagePosition:ButtonEdgeInsetsStyleTop spacing:5];
    [self.reader_settingButton bf_setImagePosition:ButtonEdgeInsetsStyleTop spacing:5];
}

#pragma mark - set up UI
-(void)reader_CoverView_setupUI{
    [self addSubview:self.reader_naviView];
    [self.reader_naviView addSubview:self.reader_backButton];
    [self.reader_naviView addSubview:self.reader_titleLabel];
    [self.reader_naviView addSubview:self.reader_naviLineView];
    [self addSubview:self.reader_dismissButton];
    [self addSubview:self.reader_bottomView];
    [self.reader_bottomView addSubview:self.reader_directoryButton];
    [self.reader_bottomView addSubview:self.reader_settingButton];
    [self.reader_bottomView addSubview:self.reader_bottomLineView];
}

-(void)reader_CoverView_setupLayout{
    [self.reader_naviView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(44+self.statusHeight);
        make.top.mas_equalTo(-44-self.statusHeight);
    }];
    
    [self.reader_backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(10 + self.statusHeight);
    }];
    
    [self.reader_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.reader_backButton.mas_right).offset(20);
        make.centerY.equalTo(self.reader_backButton.mas_centerY);
    }];
    
    [self.reader_dismissButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.reader_naviView.mas_bottom);
        make.bottom.equalTo(self.reader_bottomView.mas_top);
    }];
    
    [self.reader_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(60+kBottomSafeHeight);
        make.bottom.equalTo(self.mas_bottom).offset(60+kBottomSafeHeight);
    }];
    
    [self.reader_directoryButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(100);
        make.top.mas_equalTo(25);
    }];
    
    [self.reader_settingButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-100);
        make.centerY.equalTo(self.reader_directoryButton.mas_centerY);
    }];
    
    [self.reader_naviLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.reader_naviView);
        make.height.mas_equalTo(1);
    }];
    
    [self.reader_bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.reader_bottomView);
        make.height.mas_equalTo(1);
    }];
}

#pragma mark - lazy loading
-(UIView *)reader_naviView{
    if (!_reader_naviView) {
        _reader_naviView = [[UIView alloc] init];
        _reader_naviView.backgroundColor = AppMainThemeColor;
    }
    return _reader_naviView;
}

-(UIButton *)reader_backButton{
    if (!_reader_backButton) {
        _reader_backButton = [[UIButton alloc] init];
        [_reader_backButton setImage:[UIImage imageNamed:@"arrow_black"] forState:UIControlStateNormal];
        [_reader_backButton addTarget:self action:@selector(reader_CoverView_clickBackButtonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reader_backButton;
}

-(UILabel *)reader_titleLabel{
    if (!_reader_titleLabel) {
        _reader_titleLabel = [[UILabel alloc] init];
        _reader_titleLabel.text = @"第1话";
        _reader_titleLabel.textColor = AppMainTextLabelColor;
        _reader_titleLabel.font = [UIFont systemFontOfSize:14];
    }
    return _reader_titleLabel;
}

-(UIButton *)reader_dismissButton{
    if (!_reader_dismissButton) {
        _reader_dismissButton = [[UIButton alloc] init];
        [_reader_dismissButton addTarget:self action:@selector(reader_CoverView_dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reader_dismissButton;
}

-(UIView *)reader_bottomView{
    if (!_reader_bottomView) {
        _reader_bottomView = [[UIView alloc] init];
        _reader_bottomView.backgroundColor = AppMainThemeColor;
    }
    return _reader_bottomView;
}

-(UIButton *)reader_directoryButton{
    if (!_reader_directoryButton) {
        _reader_directoryButton = [[UIButton alloc] init];
        [_reader_directoryButton setTitle:@"目录" forState:UIControlStateNormal];
        [_reader_directoryButton setTitleColor:AppMainTextLabelColor forState:UIControlStateNormal];
        [_reader_directoryButton setImage:[UIImage imageNamed:@"icon_reader_directory"] forState:UIControlStateNormal];
        _reader_directoryButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_reader_directoryButton addTarget:self action:@selector(reader_CoverView_showDirectoryAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reader_directoryButton;
}

-(UIButton *)reader_settingButton{
    if (!_reader_settingButton) {
        _reader_settingButton = [[UIButton alloc] init];
        [_reader_settingButton setTitle:@"设置" forState:UIControlStateNormal];
        [_reader_settingButton setTitleColor:AppMainTextLabelColor forState:UIControlStateNormal];
        [_reader_settingButton setImage:[UIImage imageNamed:@"icon_reader_setting"] forState:UIControlStateNormal];
        _reader_settingButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_reader_settingButton addTarget:self action:@selector(reader_CoverView_showSettingAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reader_settingButton;
}

-(UIView *)reader_naviLineView{
    if (!_reader_naviLineView) {
        _reader_naviLineView = [[UIView alloc] init];
        _reader_naviLineView.backgroundColor = UIColorFromHex(0xEEEEEE);
    }
    return _reader_naviLineView;
}

-(UIView *)reader_bottomLineView{
    if (!_reader_bottomLineView) {
        _reader_bottomLineView = [[UIView alloc] init];
        _reader_bottomLineView.backgroundColor = UIColorFromHex(0xEEEEEE);
    }
    return _reader_bottomLineView;
}

@end
