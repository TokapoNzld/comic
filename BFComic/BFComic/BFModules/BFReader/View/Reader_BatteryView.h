//
//  BF_ReaderBatteryView.h
//  BFComic
//
//  Created by majunwen on 2022/3/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Reader_BatteryView : UIView

@property (nonatomic,copy) void(^reader_batteryChangeBlock)(float batteryLevel);

@end

NS_ASSUME_NONNULL_END
