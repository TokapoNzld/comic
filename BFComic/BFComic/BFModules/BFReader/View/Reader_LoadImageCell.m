//
//  Reader_LoadImageCell.m
//  BFComic
//
//  Created by majunwen on 2022/3/10.
//
/**
 *  前期先用SDWebImage
 *  暂时可先不用CATiledLayer，不到4-5M以上
 *  因为CATiledLayer每次都会去进行绘制，每次加载的空白页可能会体验稍差一点点
 *  如果后期图片太大可以使用其加载
 */

#import "Reader_LoadImageCell.h"
#import "SDWebImage.h"

@interface Reader_LoadImageCell()

@property (nonatomic,strong) UILabel *reader_defaultLabel;
@property (nonatomic,strong) UIImageView *reader_comicImageView;

@end

@implementation Reader_LoadImageCell

-(void)setReader_imageModel:(Reader_ComicImageModel *)reader_imageModel{
    _reader_imageModel = reader_imageModel;
    [self.reader_comicImageView sd_setImageWithURL:[NSURL URLWithString:reader_imageModel.pageUrl]];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self reader_LoadImageCell_setupUI];
        [self reader_LoadImageCell_setupLayout];
    }
    return self;
}

-(void)setReader_comicIndex:(NSInteger)reader_comicIndex{
    _reader_comicIndex = reader_comicIndex;
    self.reader_defaultLabel.text = [NSString stringWithFormat:@"%ld",reader_comicIndex];
}

- (void)reader_LoadImageCell_setupUI{
    [self.contentView addSubview:self.reader_defaultLabel];
    [self.contentView addSubview:self.reader_comicImageView];
}


- (void)reader_LoadImageCell_setupLayout{
    [self.reader_defaultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.reader_comicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}

#pragma mark - lazy loading
-(UILabel *)reader_defaultLabel{
    if (!_reader_defaultLabel) {
        _reader_defaultLabel = [[UILabel alloc] init];
        _reader_defaultLabel.textColor = [UIColor whiteColor];
        _reader_defaultLabel.backgroundColor = [UIColor blackColor];
        _reader_defaultLabel.font = [UIFont systemFontOfSize:100 weight:UIFontWeightSemibold];
        _reader_defaultLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _reader_defaultLabel;
}


-(UIImageView *)reader_comicImageView{
    if (!_reader_comicImageView) {
        _reader_comicImageView = [[UIImageView alloc] init];
        _reader_comicImageView.contentMode = UIViewContentModeScaleAspectFill;
        _reader_comicImageView.clipsToBounds = YES;
//        _reader_comicImageView.image = [UIImage imageNamed:@"comic_placeholder1"];
    }
    return _reader_comicImageView;
}


@end
