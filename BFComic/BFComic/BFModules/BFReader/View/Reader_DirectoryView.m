//
//  BF_ReaderDirectoryView.m
//  BFComic
//
//  Created by majunwen on 2022/3/10.
//

#import "Reader_DirectoryView.h"
#import "Reader_DirectoryHeaderView.h"
#import "UILabel+BFAd.h"
#import "BookDetail_ViewModel.h"

@interface Reader_DirectoryView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *reader_directoryTableView;
@property (nonatomic,strong) UIButton *reader_directoryDismissButton;
@property (nonatomic,strong) BookDetail_ViewModel *reader_directoryViewModel;

@end

@implementation Reader_DirectoryView

#pragma mark - public method
+(instancetype)reader_DirectoryView_init{
    Reader_DirectoryView *view = [[Reader_DirectoryView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:view];
    view.hidden = YES;
    return view;
}

-(void)reader_DirectoryView_smartShow{
    self.isHidden ? [self reader_DirectoryView_show] : [self reader_DirectoryView_dismiss];
}

#pragma mark - private method
-(void)reader_DirectoryView_dismiss{
    [UIView animateWithDuration:0.5 animations:^{
        [self.reader_directoryTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(-kScreenWidth + 80);
        }];
        
        [self layoutIfNeeded];
        [self setNeedsLayout];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

-(void)reader_DirectoryView_show{
    self.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        [self.reader_directoryTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
        }];
        
        [self layoutIfNeeded];
        [self setNeedsLayout];
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - request
-(void)reader_DirectoryView_requestInfo {
    __weak typeof(self) weakSelf = self;
    [self.reader_directoryViewModel bookDetail_ViewModel_requestCartoonDetailById:self.reader_directoryComicId success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [weakSelf.reader_directoryTableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

#pragma mark - init UI
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self reader_DirectoryView_setupUI];
        [self reader_DirectoryView_setupLayout];
    }
    return self;
}

-(void)reader_DirectoryView_setupUI{
    [self addSubview:self.reader_directoryDismissButton];
    [self addSubview:self.reader_directoryTableView];
}

-(void)reader_DirectoryView_setupLayout{
    [self.reader_directoryDismissButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.reader_directoryTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.mas_equalTo(-kScreenWidth + 80);
        make.width.mas_equalTo(kScreenWidth - 80);
    }];
}

#pragma mark - UITableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.reader_directoryViewModel.bookdetail_model.list.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BookDetail_ChapterModel *model = self.reader_directoryViewModel.bookdetail_model.list[indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@",model.rank,model.msg];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = UIColorFromHex(0x333333);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.edgeInset = UIEdgeInsetsMake(0, 10, 0, 0);
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    Reader_DirectoryHeaderView *headerView = [[Reader_DirectoryHeaderView alloc] init];
    headerView.reader_directoryHeaderModel = self.reader_directoryViewModel.bookdetail_model;
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 150;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] init];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .1f;
}

#pragma mark - lazy loading
-(UITableView *)reader_directoryTableView{
    if (!_reader_directoryTableView) {
        _reader_directoryTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _reader_directoryTableView.delegate = self;
        _reader_directoryTableView.dataSource = self;
        _reader_directoryTableView.rowHeight = 50;
        [_reader_directoryTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        _reader_directoryTableView.backgroundColor = UIColorFromHex(0xECECEC);
        _reader_directoryTableView.showsVerticalScrollIndicator = NO;
    }
    return _reader_directoryTableView;
}

-(UIButton *)reader_directoryDismissButton{
    if (!_reader_directoryDismissButton) {
        _reader_directoryDismissButton = [[UIButton alloc] init];
        _reader_directoryDismissButton.backgroundColor = [UIColorFromHex(0x000000) colorWithAlphaComponent:0.3];
        [_reader_directoryDismissButton addTarget:self action:@selector(reader_DirectoryView_dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reader_directoryDismissButton;
}

-(BookDetail_ViewModel *)reader_directoryViewModel{
    if (!_reader_directoryViewModel) {
        _reader_directoryViewModel = [[BookDetail_ViewModel alloc] init];
    }
    return _reader_directoryViewModel;
}

@end
