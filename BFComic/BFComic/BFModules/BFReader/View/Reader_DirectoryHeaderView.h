//
//  BF_ReaderDirectoryHeaderView.h
//  BFComic
//
//  Created by 马俊文 on 2022/3/10.
//

#import <UIKit/UIKit.h>
#import "BookDetail_InfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Reader_DirectoryHeaderView : UIView

@property (nonatomic,strong) BookDetail_InfoModel *reader_directoryHeaderModel;

@end

NS_ASSUME_NONNULL_END
