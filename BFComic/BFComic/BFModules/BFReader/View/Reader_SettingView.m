//
//  BF_ReaderSettingView.m
//  BFComic
//
//  Created by 马俊文 on 2022/3/10.
//

#import "Reader_SettingView.h"

@interface Reader_SettingView()

@property (nonatomic,strong) UIButton *reader_settingClickButton;
@property (nonatomic,strong) UIView *reader_settingWhiteView;
@property (nonatomic,strong) UILabel *reader_brightnessLabel;
@property (nonatomic,strong) UISlider *reader_brightnessSlider;
@property (nonatomic,strong) UILabel *reader_modeLabel;
@property (nonatomic,strong) UIButton *reader_horizontalModeButton;
@property (nonatomic,strong) UIButton *reader_verticalModeButton;
@property (nonatomic,strong) UILabel *reader_autoReadLabel;
@property (nonatomic,strong) UIButton *reader_autoReadButton;
@property (nonatomic,strong) UILabel *reader_tipsLabel;
@property (nonatomic,strong) UISwitch *reader_tipsSwitch;

@end

@implementation Reader_SettingView

#pragma mark - public method
+(instancetype)reader_SettingView_init{
    Reader_SettingView *view = [[Reader_SettingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:view];
    view.hidden = YES;
    return view;
}

-(void)reader_SettingView_smartShow{
    self.isHidden ? [self reader_SettingView_show] : [self reader_SettingView_dismiss];
}

-(void)reader_BatteryView_stopAutoRead{
    self.reader_autoReadButton.selected = YES;
    [self reader_SettingView_autoRead:self.reader_autoReadButton];
}

#pragma mark - private method
-(void)reader_SettingView_dismiss{
    [UIView animateWithDuration:0.5 animations:^{
        [self.reader_settingWhiteView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self.mas_bottom);
        }];
        
        [self layoutIfNeeded];
        [self setNeedsLayout];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

-(void)reader_SettingView_show{
    self.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        [self.reader_settingWhiteView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
        }];
        
        [self layoutIfNeeded];
        [self setNeedsLayout];
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - action
-(void)reader_SettingView_brightnessChange:(UISlider *)slider{
    self.reader_settingBrightnessBlock ? self.reader_settingBrightnessBlock(slider) : nil;
}


-(void)reader_SettingView_readMode:(UIButton *)button{
    if (button.isSelected) {
        return;
    }
    if (button == self.reader_horizontalModeButton) {
        self.reader_verticalModeButton.selected = NO;
    }else if(button == self.reader_verticalModeButton) {
        self.reader_horizontalModeButton.selected = NO;
    }
    button.selected = !button.isSelected;
    self.reader_settingReadModeBlock ? self.reader_settingReadModeBlock(self.reader_verticalModeButton == button) : nil;
    [self reader_SettingView_dismiss];
}

-(void)reader_SettingView_autoRead:(UIButton *)button{
    button.selected = !button.isSelected;
    self.reader_settingAutoReadBlock ? self.reader_settingAutoReadBlock(button.isSelected) : nil;
    [self reader_SettingView_dismiss];
}

-(void)reader_SettingView_showTips:(UISwitch *)switchButton{
    self.reader_settingShowTipsBlock ? self.reader_settingShowTipsBlock(switchButton.isOn) : nil;
}

#pragma mark - init UI
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self reader_SettingView_setupUI];
        [self reader_SettingView_setupLayout];
    }
    return self;
}


-(void)reader_SettingView_setupUI{
    [self addSubview:self.reader_settingClickButton];
    [self addSubview:self.reader_settingWhiteView];
    [self.reader_settingWhiteView addSubview:self.reader_brightnessLabel];
    [self.reader_settingWhiteView addSubview:self.reader_brightnessSlider];
    [self.reader_settingWhiteView addSubview:self.reader_modeLabel];
    [self.reader_settingWhiteView addSubview:self.reader_horizontalModeButton];
    [self.reader_settingWhiteView addSubview:self.reader_verticalModeButton];
    [self.reader_settingWhiteView addSubview:self.reader_autoReadLabel];
    [self.reader_settingWhiteView addSubview:self.reader_autoReadButton];
    [self.reader_settingWhiteView addSubview:self.reader_tipsLabel];
    [self.reader_settingWhiteView addSubview:self.reader_tipsSwitch];
}

-(void)reader_SettingView_setupLayout{
    CGFloat buttonWidth = 60;
    CGFloat buttonHeight = 48;
    
    [self.reader_settingClickButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.reader_settingWhiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.mas_bottom);
    }];
    
    [self.reader_brightnessLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(30);
    }];
    
    [self.reader_brightnessSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.reader_brightnessLabel.mas_right).offset(50);
        make.right.mas_equalTo(-15);
        make.centerY.equalTo(self.reader_brightnessLabel.mas_centerY);
    }];
    
    [self.reader_modeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.reader_brightnessLabel.mas_left);
        make.top.equalTo(self.reader_brightnessLabel.mas_bottom).offset(40);
    }];

    [self.reader_verticalModeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.equalTo(self.reader_modeLabel.mas_centerY);
        make.width.mas_equalTo(buttonWidth);
        make.height.mas_equalTo(buttonHeight);
    }];
    
    [self.reader_horizontalModeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.reader_verticalModeButton.mas_left).offset(-20);
        make.centerY.equalTo(self.reader_verticalModeButton.mas_centerY);
        make.width.mas_equalTo(buttonWidth);
        make.height.mas_equalTo(buttonHeight);
    }];
    
//    [self.reader_autoReadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.reader_modeLabel.mas_bottom).offset(40);
//        make.left.equalTo(self.reader_brightnessLabel.mas_left);
//    }];
//
//    [self.reader_autoReadButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-20);
//        make.centerY.equalTo(self.reader_autoReadLabel.mas_centerY);
//        make.width.mas_equalTo(buttonWidth);
//        make.height.mas_equalTo(buttonHeight);
//    }];
    
    [self.reader_tipsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.reader_modeLabel.mas_bottom).offset(40);
        make.left.equalTo(self.reader_brightnessLabel.mas_left);
        make.bottom.mas_equalTo(-20-kBottomSafeHeight);
    }];
    
    [self.reader_tipsSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.equalTo(self.reader_tipsLabel.mas_centerY);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(25);
    }];
}


#pragma mark - lazy loading
-(UIButton *)reader_settingClickButton{
    if (!_reader_settingClickButton) {
        _reader_settingClickButton = [[UIButton alloc] init];
        _reader_settingClickButton.backgroundColor = [UIColorFromHex(0x333333) colorWithAlphaComponent:0.3f];
        [_reader_settingClickButton addTarget:self action:@selector(reader_SettingView_smartShow) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reader_settingClickButton;
}

-(UIView *)reader_settingWhiteView{
    if (!_reader_settingWhiteView) {
        _reader_settingWhiteView = [[UIView alloc] init];
        _reader_settingWhiteView.backgroundColor = AppMainThemeColor;
    }
    return _reader_settingWhiteView;
}

-(UILabel *)reader_brightnessLabel{
    if (!_reader_brightnessLabel) {
        _reader_brightnessLabel = [[UILabel alloc] init];
        _reader_brightnessLabel.text = @"亮度设置";
        _reader_brightnessLabel.font = [UIFont systemFontOfSize:16];
        _reader_brightnessLabel.textColor = AppMainTextLabelColor;
    }
    return _reader_brightnessLabel;
}

-(UISlider *)reader_brightnessSlider{
    if (!_reader_brightnessSlider) {
        _reader_brightnessSlider = [[UISlider alloc] init];
        _reader_brightnessSlider.minimumTrackTintColor = UIColorFromHex(0xDE5330);
        _reader_brightnessSlider.maximumTrackTintColor = UIColorFromHex(0xECECEC);
        _reader_brightnessSlider.value = [UIScreen mainScreen].brightness;
        [_reader_brightnessSlider addTarget:self action:@selector(reader_SettingView_brightnessChange:) forControlEvents:UIControlEventValueChanged];
    }
    return _reader_brightnessSlider;
}

-(UILabel *)reader_modeLabel{
    if (!_reader_modeLabel) {
        _reader_modeLabel = [[UILabel alloc] init];
        _reader_modeLabel.text = @"阅读模式";
        _reader_modeLabel.font = [UIFont systemFontOfSize:16];
        _reader_modeLabel.textColor = AppMainTextLabelColor;
    }
    return _reader_modeLabel;
}

-(UIButton *)reader_horizontalModeButton{
    if (!_reader_horizontalModeButton) {
        _reader_horizontalModeButton = [[UIButton alloc] init];
        [_reader_horizontalModeButton setImage:[UIImage imageNamed:@"icon_reader_hor"] forState:UIControlStateNormal];
        [_reader_horizontalModeButton setImage:[UIImage imageNamed:@"icon_reader_hor_select"] forState:UIControlStateSelected];
        [_reader_horizontalModeButton addTarget:self action:@selector(reader_SettingView_readMode:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reader_horizontalModeButton;
}

-(UIButton *)reader_verticalModeButton{
    if (!_reader_verticalModeButton) {
        _reader_verticalModeButton = [[UIButton alloc] init];
        [_reader_verticalModeButton setImage:[UIImage imageNamed:@"icon_reader_ver"] forState:UIControlStateNormal];
        [_reader_verticalModeButton setImage:[UIImage imageNamed:@"icon_reader_ver_select"] forState:UIControlStateSelected];
        _reader_verticalModeButton.selected = YES;
        [_reader_verticalModeButton addTarget:self action:@selector(reader_SettingView_readMode:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reader_verticalModeButton;
}

-(UILabel *)reader_autoReadLabel{
    if (!_reader_autoReadLabel) {
        _reader_autoReadLabel = [[UILabel alloc] init];
        _reader_autoReadLabel.text = @"自动阅读";
        _reader_autoReadLabel.font = [UIFont systemFontOfSize:16];
        _reader_autoReadLabel.textColor = UIColorFromHex(0x333333);
    }
    return _reader_autoReadLabel;
}

-(UIButton *)reader_autoReadButton{
    if (!_reader_autoReadButton) {
        _reader_autoReadButton = [[UIButton alloc] init];
        [_reader_autoReadButton setImage:[UIImage imageNamed:@"icon_reader_auto"] forState:UIControlStateNormal];
        [_reader_autoReadButton setImage:[UIImage imageNamed:@"icon_reader_auto_select"] forState:UIControlStateSelected];
        [_reader_autoReadButton addTarget:self action:@selector(reader_SettingView_autoRead:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _reader_autoReadButton;
}

-(UILabel *)reader_tipsLabel{
    if (!_reader_tipsLabel) {
        _reader_tipsLabel = [[UILabel alloc] init];
        _reader_tipsLabel.text = @"电池时间显示";
        _reader_tipsLabel.font = [UIFont systemFontOfSize:16];
        _reader_tipsLabel.textColor = AppMainTextLabelColor;
    }
    return _reader_tipsLabel;
}

-(UISwitch *)reader_tipsSwitch{
    if (!_reader_tipsSwitch) {
        _reader_tipsSwitch = [[UISwitch alloc] init];
        _reader_tipsSwitch.onTintColor = UIColorFromHex(0xDE5330);
        [_reader_tipsSwitch addTarget:self action:@selector(reader_SettingView_showTips:) forControlEvents:UIControlEventValueChanged];
        _reader_tipsSwitch.on = YES;
    }
    return _reader_tipsSwitch;
}


@end
