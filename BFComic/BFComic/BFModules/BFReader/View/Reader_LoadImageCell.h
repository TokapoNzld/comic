//
//  Reader_LoadImageCell.h
//  BFComic
//
//  Created by majunwen on 2022/3/10.
//

#import <UIKit/UIKit.h>
#import "Reader_ComicImageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Reader_LoadImageCell : UICollectionViewCell

@property (nonatomic,strong) Reader_ComicImageModel *reader_imageModel;
@property (nonatomic,assign) NSInteger reader_comicIndex;

@end

NS_ASSUME_NONNULL_END
