//
//  BF_ReaderCoverView.h
//  BFComic
//
//  Created by majunwen on 2022/3/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ReaderCoverView_Back_Callback)(void);

@interface Reader_CoverView : UIView

@property (nonatomic,copy) void(^reader_coverPushDirectoryBlock)(void);
@property (nonatomic,copy) void(^reader_coverPushSettingBlock)(void);

+(instancetype)reader_CoverView_init;

-(void)reader_CoverView_smartClick;

@property (nonatomic, strong) ReaderCoverView_Back_Callback backCallback;

@end

NS_ASSUME_NONNULL_END
