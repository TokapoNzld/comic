//
//  BF_ReaderSettingView.h
//  BFComic
//
//  Created by 马俊文 on 2022/3/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Reader_SettingView : UIView

@property (nonatomic,copy) void(^reader_settingBrightnessBlock)(UISlider *slider);
//isHorizontal是横向
@property (nonatomic,copy) void(^reader_settingReadModeBlock)(BOOL isHorizontal);
@property (nonatomic,copy) void(^reader_settingAutoReadBlock)(BOOL isAuto);
@property (nonatomic,copy) void(^reader_settingShowTipsBlock)(BOOL isShow);

+(instancetype)reader_SettingView_init;

-(void)reader_SettingView_smartShow;

- (void)reader_BatteryView_stopAutoRead;

@end

NS_ASSUME_NONNULL_END
