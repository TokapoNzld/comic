//
//  BF_ReaderDirectoryView.h
//  BFComic
//
//  Created by majunwen on 2022/3/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Reader_DirectoryView : UIView

@property (nonatomic,copy) NSString *reader_directoryComicId;

+(instancetype)reader_DirectoryView_init;

-(void)reader_DirectoryView_smartShow;

-(void)reader_DirectoryView_requestInfo;

@end

NS_ASSUME_NONNULL_END
