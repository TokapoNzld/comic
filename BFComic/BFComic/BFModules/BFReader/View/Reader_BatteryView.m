//
//  BF_ReaderBatteryView.m
//  BFComic
//
//  Created by majunwen on 2022/3/25.
//

#import "Reader_BatteryView.h"

@interface Reader_BatteryView()

@property (nonatomic,strong) UIView *reader_batteryHeadView;
@property (nonatomic,assign) float reader_batteryLevel;

@end

@implementation Reader_BatteryView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        [self reader_BatteryView_setupUI];
        [self reader_BatteryView_setupLayout];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reader_BatteryView_listenBattery:) name:UIDeviceBatteryStateDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reader_BatteryView_listenBattery:) name:UIDeviceBatteryLevelDidChangeNotification object:nil];
    }
    return self;
}

- (void)reader_BatteryView_setupUI{
    self.layer.cornerRadius = 2.0;
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [UIDevice currentDevice].batteryMonitoringEnabled = YES;
    self.reader_batteryLevel = [UIDevice currentDevice].batteryLevel;
    
    
    [self addSubview:self.reader_batteryHeadView];
}

- (void)reader_BatteryView_setupLayout{
    [self.reader_batteryHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_right);
        make.centerY.equalTo(self.mas_centerY);
        make.width.mas_equalTo(2);
        make.height.mas_equalTo(5);
    }];
}

-(void)reader_BatteryView_listenBattery:(id)sender{
    self.reader_batteryLevel = [UIDevice currentDevice].batteryLevel;
    self.reader_batteryChangeBlock ? self.reader_batteryChangeBlock(self.reader_batteryLevel) : nil;
    
    [self layoutIfNeeded];
    [self setNeedsLayout];
}


- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rectBounds = CGRectMake(2, 2, (self.bounds.size.width - 4) * self.reader_batteryLevel, self.bounds.size.height - 4);
    CGContextSetLineWidth(context, 1.0);
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextAddRect(context, rectBounds);
    CGContextFillPath(context);
}

#pragma mark - lazy loading
-(UIView *)reader_batteryHeadView{
    if (!_reader_batteryHeadView) {
        _reader_batteryHeadView = [[UIView alloc] init];
        _reader_batteryHeadView.backgroundColor = [UIColor whiteColor];
    }
    return _reader_batteryHeadView;
}


@end
