//
//  Reader_ComicImageModel.m
//  BFComic
//
//  Created by majunwen on 2022/4/29.
//

#import "Reader_ComicImageModel.h"

@implementation Reader_ComicImageModel

@end

@implementation Reader_ComicImageTotalModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : [Reader_ComicImageModel class]
    };
}

@end
