//
//  Reader_ComicImageModel.h
//  BFComic
//
//  Created by majunwen on 2022/4/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Reader_ComicImageModel : NSObject

@property (nonatomic,assign) NSInteger page;
@property (nonatomic,copy) NSString *pageUrl;

@end

@interface Reader_ComicImageTotalModel : NSObject

@property (nonatomic,strong) NSArray <Reader_ComicImageModel *> *list;
@property (nonatomic,copy) NSString *nextCartoonCatalogueId;
@property (nonatomic,copy) NSString *preCartoonCatalogueId;
@property (nonatomic,assign) NSInteger page;
@property (nonatomic,assign) NSInteger totalNumber;
@property (nonatomic,assign) NSInteger totalPage;
@property (nonatomic,copy) NSString *msg;
@property (nonatomic,assign) NSInteger rank;

@end

NS_ASSUME_NONNULL_END
