//
//  BF_ReaderViewController.h
//  BFComic
//
//  Created by majunwen on 2022/3/10.
//

#import "BFBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BF_ReaderViewController : BFBaseViewController

@property (nonatomic,copy) NSString *reader_chapterId;
@property (nonatomic,copy) NSString *reader_comicId;

@end

NS_ASSUME_NONNULL_END
