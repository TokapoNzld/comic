//
//  BF_ReaderViewController.m
//  BFComic
//
//  Created by majunwen on 2022/3/10.
//

#import "BF_ReaderViewController.h"
#import "Reader_LoadImageCell.h"
#import "Reader_CoverView.h"
#import "Reader_DirectoryView.h"
#import "Reader_SettingView.h"
#import "Reader_BatteryTimeView.h"
#import "Reader_ComicInfoViewModel.h"
#import "MJRefresh.h"

@interface BF_ReaderViewController ()<CALayerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) UICollectionView *reader_collectioinView;
@property (nonatomic,strong) Reader_CoverView *reader_coverView;
@property (nonatomic,strong) Reader_DirectoryView *reader_directoryView;
@property (nonatomic,strong) Reader_SettingView *reader_settingView;
@property (nonatomic,strong) Reader_BatteryTimeView *reader_batteryTimeView;
@property (nonatomic,assign) NSInteger reader_currentRow;
@property (nonatomic,strong) dispatch_source_t reader_gcdTimer;
@property (nonatomic,assign) BOOL reader_isAutoRead;
@property (nonatomic,strong) Reader_ComicInfoViewModel *reader_viewModel;

@property (nonatomic,assign) NSInteger reader_prePage;
@property (nonatomic,assign) NSInteger reader_nextPage;
@property (nonatomic,strong) Reader_ComicImageModel *reader_preComicModel;

@property (nonatomic,assign) CGFloat scrollOffsetY;

@end

@implementation BF_ReaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = AppMainThemeColor;
    
    self.reader_prePage = 0;
    self.reader_nextPage = 1;
    [self reader_ReaderViewController_setupBlock];
    
    __weak typeof(self) weakSelf = self;
    self.reader_collectioinView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf reader_ReaderViewController_requestPreData];
    }];

    self.reader_collectioinView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf reader_ReaderViewController_requestNextData];
    }];
    
    [self reader_ReaderViewController_requestImageByPage:self.reader_prePage isTop:YES chapterId:self.reader_chapterId];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)reader_ReaderViewController_requestPreData {
    self.reader_prePage -= 1;
    if (self.reader_prePage == 0) {
        self.reader_chapterId = self.reader_viewModel.totalComicModel.preCartoonCatalogueId;
        self.reader_prePage = -1;
    }
    self.reader_preComicModel = self.reader_viewModel.totalComicModel.list.firstObject;
    [self reader_ReaderViewController_requestImageByPage:self.reader_prePage isTop:YES chapterId:self.reader_chapterId];
}

-(void)reader_ReaderViewController_requestNextData {
    self.reader_nextPage += 1;
    if (self.reader_nextPage > self.reader_viewModel.totalComicModel.totalPage) {
        self.reader_chapterId = self.reader_viewModel.totalComicModel.nextCartoonCatalogueId;
        self.reader_nextPage = -1;
    }
//    self.reader_nextComicModel = self.reader_viewModel.totalComicModel.list[self.reader_viewModel.totalComicModel.list.count -3];
    [self reader_ReaderViewController_requestImageByPage:self.reader_nextPage isTop:NO chapterId:self.reader_chapterId];
}


-(void)reader_ReaderViewController_requestImageByPage:(NSInteger)page isTop:(BOOL)isTop chapterId:(NSString *)chapterId{
    __weak typeof(self) weakSelf = self;
    if (chapterId == nil) {
        [self.reader_collectioinView.mj_footer endRefreshing];
        [self.reader_collectioinView.mj_header endRefreshing];
        return;
    }
    [self.reader_viewModel reader_ComicInfoViewModel_requestComicImageById:chapterId page:page isTop:isTop success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [weakSelf.reader_collectioinView reloadData];
        //跳到指定位置
        NSInteger index = 0;
        if (isTop) {
            weakSelf.reader_prePage = weakSelf.reader_viewModel.totalComicModel.page;
            index = [weakSelf.reader_viewModel.totalComicModel.list indexOfObject:weakSelf.reader_preComicModel];
            if (index >=0 & index < weakSelf.reader_viewModel.totalComicModel.list.count) {
                NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForItem:(index - 1) inSection:0];
                [weakSelf.reader_collectioinView scrollToItemAtIndexPath:scrollIndexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
            }
        }else {
            [weakSelf.reader_collectioinView setContentOffset:CGPointMake(0, weakSelf.scrollOffsetY) animated:NO];
        }
        if (weakSelf.reader_prePage < 0 && weakSelf.reader_nextPage < 0) {
            weakSelf.reader_prePage = weakSelf.reader_viewModel.totalComicModel.page;
            weakSelf.reader_nextPage = weakSelf.reader_viewModel.totalComicModel.page;
        }
        [weakSelf.reader_collectioinView.mj_footer endRefreshing];
        [weakSelf.reader_collectioinView.mj_header endRefreshing];
        if (weakSelf.reader_viewModel.totalComicModel.preCartoonCatalogueId == nil) {
            weakSelf.reader_collectioinView.mj_header.hidden = YES;
        }
        if (weakSelf.reader_viewModel.totalComicModel.nextCartoonCatalogueId == nil) {
            [weakSelf.reader_collectioinView.mj_footer endRefreshingWithNoMoreData];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        NSLog(@"error = %@",error.localizedDescription);
        if (isTop) {
            weakSelf.reader_prePage = weakSelf.reader_viewModel.totalComicModel.page;
        }else {
            weakSelf.reader_nextPage = weakSelf.reader_viewModel.totalComicModel.page;
        }
        [weakSelf.reader_collectioinView.mj_footer endRefreshing];
        [weakSelf.reader_collectioinView.mj_header endRefreshing];
    }];
}

-(void)base_setupViews{
    [self.view addSubview:self.reader_collectioinView];
    [self.view addSubview:self.reader_batteryTimeView];
    
    self.reader_coverView = [Reader_CoverView reader_CoverView_init];
    self.reader_directoryView = [Reader_DirectoryView reader_DirectoryView_init];
    self.reader_directoryView.reader_directoryComicId = self.reader_comicId;
    [self.reader_directoryView reader_DirectoryView_requestInfo];
    self.reader_settingView = [Reader_SettingView reader_SettingView_init];
    [self reader_ReaderViewController_addTagGesture];
}

-(void)base_setupLayout{
    [self.reader_collectioinView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.reader_batteryTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(-kBottomSafeHeight);
        make.height.mas_equalTo(20);
    }];
}

- (void)base_setupTempData {
    __weak typeof(self) weakSelf = self;
    self.reader_coverView.backCallback = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
}

-(void)reader_ReaderViewController_addTagGesture{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reader_ReaderViewController_tagAction)];
    [self.view addGestureRecognizer:tap];
}

-(void)reader_ReaderViewController_setupBlock{
    __weak typeof(self) weakSelf = self;
    self.reader_coverView.reader_coverPushDirectoryBlock = ^{
        [weakSelf.reader_directoryView reader_DirectoryView_smartShow];
    };
    
    self.reader_coverView.reader_coverPushSettingBlock = ^{
        [weakSelf.reader_settingView reader_SettingView_smartShow];
    };
    
    self.reader_settingView.reader_settingBrightnessBlock = ^(UISlider *slider){
        [UIScreen mainScreen].brightness = slider.value;
    };
    
    self.reader_settingView.reader_settingReadModeBlock = ^(BOOL isHorizontal) {
        
            UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        if (isHorizontal) {
            layout.scrollDirection = UICollectionViewScrollDirectionVertical;
            layout.itemSize = CGSizeMake(CGRectGetWidth(weakSelf.view.frame), CGRectGetWidth(weakSelf.view.frame)*270.0/360);
            weakSelf.reader_collectioinView.pagingEnabled = NO;
            [weakSelf.reader_collectioinView setTransform:CGAffineTransformMakeScale(1, 1)];
            weakSelf.view.backgroundColor = AppMainThemeColor;
        }else{
            CGFloat height = CGRectGetHeight(weakSelf.view.frame) - kNaviHeight - kBottomSafeHeight - 200;
            layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
            layout.itemSize = CGSizeMake(CGRectGetWidth(weakSelf.view.frame), height);
            weakSelf.reader_collectioinView.pagingEnabled = YES;
            [weakSelf.reader_collectioinView setTransform:CGAffineTransformMakeScale(-1, 1)];
            weakSelf.view.backgroundColor = AppMainThemeColor;
        }
        weakSelf.reader_collectioinView.collectionViewLayout = layout;
        [weakSelf.reader_collectioinView reloadData];
    };
    
    self.reader_settingView.reader_settingAutoReadBlock = ^(BOOL isAuto) {
        weakSelf.reader_currentRow = [weakSelf reader_ReaderViewController_getTopCellRow];
        //开启定时器滚动
        isAuto ? dispatch_resume(weakSelf.reader_gcdTimer) : dispatch_suspend(weakSelf.reader_gcdTimer);
        weakSelf.reader_isAutoRead = isAuto;
    };
    
    self.reader_settingView.reader_settingShowTipsBlock = ^(BOOL isShow) {
        weakSelf.reader_batteryTimeView.hidden = !isShow;
    };
}

-(NSInteger)reader_ReaderViewController_getTopCellRow{
    NSArray <NSIndexPath *>*visibleIndexPaths = [self.reader_collectioinView indexPathsForVisibleItems];
    return visibleIndexPaths.firstObject.row;
}

#pragma mark - Action
-(void)reader_ReaderViewController_tagAction{
    [self.reader_coverView reader_CoverView_smartClick];
}

-(void)reader_ReaderViewController_autoScroll{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.reader_collectioinView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.reader_currentRow inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    });
    self.reader_currentRow += 1;
    if (self.reader_currentRow >= 10 - 1) {
        dispatch_suspend(self.reader_gcdTimer);
    }
}


#pragma mark - CollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.reader_viewModel.totalComicModel.list.count;
}

-(__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Reader_LoadImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BF_ReaderLoadImageCell" forIndexPath:indexPath];
    cell.reader_comicIndex = indexPath.row;
    cell.reader_imageModel = self.reader_viewModel.totalComicModel.list[indexPath.row];
    return cell;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return .1f;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return .1f;
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"==================");
//    if (indexPath.row == 1) {
//        [self reader_ReaderViewController_requestPreData];
//    }
    Reader_LoadImageCell *cell = (Reader_LoadImageCell *)[self.reader_collectioinView visibleCells].lastObject;
//    if (cell.reader_comicIndex > self.reader_viewModel.totalComicModel.list.count - 10) {
//        self.scrollOffsetY = scrollView.contentOffset.y;
//        [self reader_ReaderViewController_requestNextData];
//    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.scrollOffsetY = scrollView.contentOffset.y;
}

//隐藏状态栏
- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - ScrollView

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (self.reader_isAutoRead){
        [self.reader_settingView reader_BatteryView_stopAutoRead];
    }
}


#pragma mark - lazy loading
-(UICollectionView *)reader_collectioinView{
    if (!_reader_collectioinView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.itemSize = CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame)*270.0/360);
        _reader_collectioinView = [[UICollectionView alloc] initWithFrame:[UIScreen mainScreen].bounds collectionViewLayout:layout];
        _reader_collectioinView.delegate = self;
        _reader_collectioinView.dataSource = self;
        [_reader_collectioinView registerClass:[Reader_LoadImageCell class] forCellWithReuseIdentifier:@"BF_ReaderLoadImageCell"];
        _reader_collectioinView.showsVerticalScrollIndicator = NO;
        _reader_collectioinView.backgroundColor = AppMainThemeColor;
    }
    return _reader_collectioinView;
}

-(Reader_BatteryTimeView *)reader_batteryTimeView{
    if (!_reader_batteryTimeView) {
        _reader_batteryTimeView = [[Reader_BatteryTimeView alloc] init];
    }
    return _reader_batteryTimeView;
}

-(dispatch_source_t)reader_gcdTimer{
    if (!_reader_gcdTimer) {
        _reader_gcdTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(0, 0));
        dispatch_source_set_timer(_reader_gcdTimer, DISPATCH_TIME_NOW, 3.0 * NSEC_PER_SEC, 0.0 * NSEC_PER_SEC);
        __weak typeof(self) weakSelf = self;
        dispatch_source_set_event_handler(_reader_gcdTimer, ^{
            [weakSelf reader_ReaderViewController_autoScroll];
        });
    }
    return _reader_gcdTimer;
}

-(Reader_ComicInfoViewModel *)reader_viewModel{
    if (!_reader_viewModel) {
        _reader_viewModel = [[Reader_ComicInfoViewModel alloc] init];
    }
    return _reader_viewModel;
}

@end
