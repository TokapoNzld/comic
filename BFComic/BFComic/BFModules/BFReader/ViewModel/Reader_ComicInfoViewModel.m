//
//  Reader_ComicInfoViewModel.m
//  BFComic
//
//  Created by majunwen on 2022/4/29.
//

#import "Reader_ComicInfoViewModel.h"
#import "BF_ApiConfig.h"
#import "MJExtension.h"

@implementation Reader_ComicInfoViewModel

- (void)reader_ComicInfoViewModel_requestComicImageById:(NSString *)comicId page:(NSInteger)page isTop:(BOOL)isTop success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"id":comicId,@"page":@(page)}];
    if (page < 0) {
        [params removeObjectForKey:@"page"];
    }
    [[BFBaseRequest manager] GET:BF_API_CARTOONDETAILBYID parameters:params downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        if ([responseObject[@"code"] integerValue] == 200) {
            Reader_ComicImageTotalModel *newTotalModel = [Reader_ComicImageTotalModel mj_objectWithKeyValues:responseObject[@"data"]];
            NSMutableArray <Reader_ComicImageModel *>*comics = [NSMutableArray array];
            if (isTop) {
                [comics addObjectsFromArray:newTotalModel.list];
                [comics addObjectsFromArray:self.totalComicModel.list];
            }else{
                [comics addObjectsFromArray:self.totalComicModel.list];
                [comics addObjectsFromArray:newTotalModel.list];
            }
            newTotalModel.list = comics;
            self.totalComicModel = newTotalModel;
        }
        success ? success(task,responseObject) : nil;
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}

@end
