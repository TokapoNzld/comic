//
//  Reader_ComicInfoViewModel.h
//  BFComic
//
//  Created by majunwen on 2022/4/29.
//

#import <Foundation/Foundation.h>
#import "BFBaseRequest.h"
#import "Reader_ComicImageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Reader_ComicInfoViewModel : NSObject

@property (nonatomic,strong) Reader_ComicImageTotalModel *totalComicModel;

- (void)reader_ComicInfoViewModel_requestComicImageById:(NSString *)comicId page:(NSInteger)page isTop:(BOOL)isTop success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

@end

NS_ASSUME_NONNULL_END
