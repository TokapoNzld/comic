//
//  MineViewController.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import "BF_MineViewController.h"
#import "Mine_UserInfoHeaderView.h"
#import "Mine_ContentTableViewCell.h"
#import "BF_LoginViewController.h"
#import "User_InfoManager.h"
#import "Login_ViewModel.h"
#import "BFToastManager.h"

@interface BF_MineViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *mine_tableView;
@property (nonatomic, strong) NSArray *mine_dataSource;

@end

@implementation BF_MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的";
    self.view.backgroundColor = AppMainThemeColor;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.mine_dataSource = nil;
    [self.mine_tableView reloadData];
}

- (void)base_setupViews {
    [self.view addSubview:self.mine_tableView];
}

- (void)base_setupLayout {
    [self.mine_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)mine_MineViewController_requestLogout{
    [BFToastManager bf_show];
    [Login_ViewModel login_ViewModel_requestLoginoutSuccess:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [BFToastManager bf_dismiss];
        if ([responseObject[@"code"] integerValue] == 200) {
            [BFToastManager bf_showSuccessWithStatus:@"退出登录成功" duration:1.0];
            self.mine_dataSource = nil;
            [self.mine_tableView reloadData];
        }else{
            [BFToastManager bf_showSuccessWithStatus:@"退出登录失败" duration:1.0];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        [BFToastManager bf_dismiss];
        [BFToastManager bf_showSuccessWithStatus:error.localizedDescription duration:1.0];
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mine_dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *modelDict = self.mine_dataSource[indexPath.row];
    Mine_ContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Mine_ContentTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.mine_iconImageView.image = [UIImage imageNamed:modelDict[@"icon"]];
    cell.mine_titleLabel.text = modelDict[@"title"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    Mine_UserInfoHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Mine_UserInfoHeaderView"];
    [headerView mine_UserInfoHeaderView_reloadData];
    headerView.mine_userInfoClickBlock = ^{
        if ([User_InfoManager sharedInstance].user_Model.userId.length != 0) {
            return;
        }
        BF_LoginViewController *vc = [[BF_LoginViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    };
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 150;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *modelDict = self.mine_dataSource[indexPath.row];
    if ([modelDict[@"title"] isEqualToString:@"退出登录"]) {
        [self mine_MineViewController_requestLogout];
    }
}

#pragma mark - Getter && Setter
- (UITableView *)mine_tableView {
    if (!_mine_tableView) {
        _mine_tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _mine_tableView.delegate = self;
        _mine_tableView.dataSource = self;
        _mine_tableView.backgroundColor = AppMainThemeColor;
        [_mine_tableView registerClass:[Mine_UserInfoHeaderView class] forHeaderFooterViewReuseIdentifier:@"Mine_UserInfoHeaderView"];
        [_mine_tableView registerClass:[Mine_ContentTableViewCell class] forCellReuseIdentifier:@"Mine_ContentTableViewCell"];
    }
    return _mine_tableView;
}

-(NSArray *)mine_dataSource{
    if (!_mine_dataSource) {
        if ([User_InfoManager sharedInstance].user_Model.userId.length != 0) {
            _mine_dataSource = @[@{@"icon":@"mine_delete_icon",@"title":@"清理缓存"},
                                 @{@"icon":@"mine_about_icon",@"title":@"关于我们"},
                                 @{@"icon":@"mine_logout_icon",@"title":@"退出登录"},];
        }else {
            _mine_dataSource = @[@{@"icon":@"mine_delete_icon",@"title":@"清理缓存"},
                                 @{@"icon":@"mine_about_icon",@"title":@"关于我们"},];
        }
    }
    return _mine_dataSource;
}

@end
