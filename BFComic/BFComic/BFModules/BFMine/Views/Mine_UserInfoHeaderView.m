//
//  Mine_UserInfoHeaderView.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import "Mine_UserInfoHeaderView.h"
#import "User_InfoManager.h"
#import "SDWebImage.h"

@interface Mine_UserInfoHeaderView ()

@property (nonatomic, strong) UIImageView *mine_headerImageView;
@property (nonatomic, strong) UILabel *mine_usernameLabel;

@end

@implementation Mine_UserInfoHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithReuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = AppMainThemeColor;
        [self mine_UserInfoHeaderView_setupUI];
        [self mine_UserInfoHeaderView_setupLayout];
        [self mine_UserInfoHeaderView_reloadData];
    }
    return self;
}

- (void)mine_UserInfoHeaderView_reloadData {
    User_InfoModel *model = [User_InfoManager sharedInstance].user_Model;
    if (model.userId.length != 0) {
        [self.mine_headerImageView sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:kDefaultImage];
        self.mine_usernameLabel.text = model.nickname;
    }else {
        self.mine_headerImageView.image = kDefaultImage;
        self.mine_usernameLabel.text = @"请登录";
    }
}

- (void)mine_UserInfoHeaderView_setupUI {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mine_UserInfoHeaderView_click)];
    [self addGestureRecognizer:tap];
    
    [self.contentView addSubview:self.mine_headerImageView];
    [self.contentView addSubview:self.mine_usernameLabel];
}

- (void)mine_UserInfoHeaderView_setupLayout {
    [self.mine_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.width.equalTo(@120);
        make.height.equalTo(@120);
    }];
    
    [self.mine_usernameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mine_headerImageView);
        make.left.equalTo(self.mine_headerImageView.mas_right).offset(20);
        make.right.equalTo(self.contentView.mas_right).offset(-20);
    }];
}

- (void)mine_UserInfoHeaderView_click {
    self.mine_userInfoClickBlock ? self.mine_userInfoClickBlock() : nil;
}

#pragma mark - Getter && Setter
- (UIImageView *)mine_headerImageView {
    if (!_mine_headerImageView) {
        _mine_headerImageView = [[UIImageView alloc] init];
//        _mine_headerImageView.image = [UIImage imageNamed:@"header_placeholder"];
        _mine_headerImageView.layer.masksToBounds = YES;
        _mine_headerImageView.layer.cornerRadius = 60;
    }
    return _mine_headerImageView;
}

- (UILabel *)mine_usernameLabel {
    if (!_mine_usernameLabel) {
        _mine_usernameLabel = [[UILabel alloc] init];
//        _mine_usernameLabel.text = @"用户234567623";
        _mine_usernameLabel.font = [UIFont systemFontOfSize:16];
        _mine_usernameLabel.textColor = [UIColor whiteColor];
    }
    return _mine_usernameLabel;
}

@end
