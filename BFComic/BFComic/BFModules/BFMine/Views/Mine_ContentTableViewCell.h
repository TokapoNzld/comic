//
//  Mine_ContentTableViewCell.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Mine_ContentTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *mine_iconImageView;
@property (nonatomic, strong) UILabel *mine_titleLabel;

@end

NS_ASSUME_NONNULL_END
