//
//  Mine_UserInfoHeaderView.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Mine_UserInfoHeaderView : UITableViewHeaderFooterView

- (void)mine_UserInfoHeaderView_reloadData;

@property (nonatomic,copy) void(^mine_userInfoClickBlock)(void);

@end

NS_ASSUME_NONNULL_END
