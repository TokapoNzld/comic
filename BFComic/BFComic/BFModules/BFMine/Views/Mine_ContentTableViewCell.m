//
//  Mine_ContentTableViewCell.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import "Mine_ContentTableViewCell.h"

@interface Mine_ContentTableViewCell ()

@end

@implementation Mine_ContentTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = AppMainThemeColor;
        [self mine_ContentTableViewCell_setupUI];
        [self mine_ContentTableViewCell_setupLayout];
    }
    return self;
}

- (void)mine_ContentTableViewCell_setupUI {
    [self.contentView addSubview:self.mine_iconImageView];
    [self.contentView addSubview:self.mine_titleLabel];
}

- (void)mine_ContentTableViewCell_setupLayout {
    [self.mine_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.width.height.equalTo(@24);
    }];
    
    [self.mine_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mine_iconImageView);
        make.left.equalTo(self.mine_iconImageView.mas_right).offset(12);
        make.right.equalTo(self.contentView.mas_right).offset(-12);
    }];
}

#pragma mark - Getter && Setter
- (UIImageView *)mine_iconImageView {
    if (!_mine_iconImageView) {
        _mine_iconImageView = [[UIImageView alloc] init];
    }
    return _mine_iconImageView;
}

- (UILabel *)mine_titleLabel {
    if (!_mine_titleLabel) {
        _mine_titleLabel = [[UILabel alloc] init];
        _mine_titleLabel.text = @"清理缓存";
        _mine_titleLabel.font = [UIFont systemFontOfSize:16];
        _mine_titleLabel.textColor = [UIColor whiteColor];
    }
    return _mine_titleLabel;
}

@end
