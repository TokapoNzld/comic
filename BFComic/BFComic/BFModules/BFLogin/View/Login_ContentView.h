//
//  Login_ContentView.h
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import <UIKit/UIKit.h>
#import "Login_InfoUnitView.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    Login_LoginType,
    Login_RegisterType
} Login_Type;

@interface Login_ContentView : UIView

@property (nonatomic,copy) void(^login_backBlock)(void);
@property (nonatomic,copy) void(^login_getCodeBlock)(void);
@property (nonatomic,copy) void(^login_commitBlock)(void);
@property (nonatomic,copy) void(^login_registerBlock)(void);

@property (nonatomic,assign) Login_Type login_viewType;
@property (nonatomic,strong) Login_InfoUnitView *login_phoneInfoView;
@property (nonatomic,strong) Login_InfoUnitView *login_pwdInfoView;
@property (nonatomic,strong) Login_InfoUnitView *login_againPwdInfoView;
@property (nonatomic,strong) Login_InfoUnitView *login_getCodeInfoView;
@property (nonatomic,strong,readonly) UIButton *login_checkButton;

@end

NS_ASSUME_NONNULL_END
