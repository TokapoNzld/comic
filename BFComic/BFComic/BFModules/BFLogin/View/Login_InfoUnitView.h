//
//  Login_infoUnitView.h
//  BFComic
//
//  Created by majunwen on 2022/4/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    Login_infoUnitTypePhone,
    Login_infoUnitTypeGetCode,
    Login_infoUnitTypePassword,
} Login_infoUnitType;

@interface Login_InfoUnitView : UIView

@property (nonatomic,strong) UILabel *login_infoTitleLabel;
@property (nonatomic,strong) UITextField *login_infoTextField;
@property (nonatomic,strong) UIButton *login_infoCodeButton;

@property (nonatomic,assign) Login_infoUnitType login_unitType;

@end

NS_ASSUME_NONNULL_END
