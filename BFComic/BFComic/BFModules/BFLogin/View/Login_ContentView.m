//
//  Login_ContentView.m
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import "Login_ContentView.h"
#import "UIColor+BFAd.h"

@interface Login_ContentView()<UITextFieldDelegate,UITextViewDelegate>

@property (nonatomic,strong) UIButton *login_backButton;
@property (nonatomic,strong) UILabel *login_titleLabel;
@property (nonatomic,strong) UIButton *login_checkButton;
@property (nonatomic,strong) UITextView *login_agreeTextView;
@property (nonatomic,strong) UIButton *login_commitButton;
@property (nonatomic,strong) UIButton *login_registerButton;

@property (nonatomic,strong) dispatch_source_t login_gcdTimer;
@property (nonatomic,assign) NSInteger login_time;

@end

@implementation Login_ContentView

-(void)setLogin_viewType:(Login_Type)login_viewType{
    _login_viewType = login_viewType;
    
    switch (login_viewType) {
        case Login_RegisterType:
        {
            self.login_titleLabel.text = @"注册";
            self.login_registerButton.hidden = YES;
            [self.login_commitButton setTitle:@"注册" forState:UIControlStateNormal];
            [self.login_checkButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.login_pwdInfoView.mas_bottom).offset(135);
            }];
        }
            break;
            
        case Login_LoginType:
        {
            self.login_againPwdInfoView.hidden = YES;
            self.login_getCodeInfoView.hidden = YES;
        }
            break;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        [self login_ContentView_setupViews];
        [self login_ContentView_setupAutoLayout];
    }
    return self;
}

#pragma mark - init view
-(void)login_ContentView_setupViews{
    [self addSubview:self.login_backButton];
    [self addSubview:self.login_titleLabel];
    [self addSubview:self.login_phoneInfoView];
    [self addSubview:self.login_pwdInfoView];
    [self addSubview:self.login_againPwdInfoView];
    [self addSubview:self.login_getCodeInfoView];
    [self addSubview:self.login_checkButton];
    [self addSubview:self.login_agreeTextView];
    [self addSubview:self.login_commitButton];
    [self addSubview:self.login_registerButton];
    
    self.login_time = 60;
}


-(void)login_ContentView_setupAutoLayout{
    [self.login_backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(9);
        make.height.mas_equalTo(16);
        make.left.mas_equalTo(26);
        make.top.mas_equalTo(24);
    }];
    
    [self.login_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.login_backButton.mas_left);
        make.top.equalTo(self.login_backButton.mas_bottom).offset(19);
    }];
    
    [self.login_phoneInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.equalTo(self.login_titleLabel.mas_bottom).offset(40);
        make.right.mas_equalTo(-12);
    }];
    
    [self.login_pwdInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.login_phoneInfoView);
        make.top.equalTo(self.login_phoneInfoView.mas_bottom);
    }];
    
    [self.login_againPwdInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.login_phoneInfoView);
        make.top.equalTo(self.login_pwdInfoView.mas_bottom);
    }];
    
    [self.login_getCodeInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.login_phoneInfoView);
        make.top.equalTo(self.login_againPwdInfoView.mas_bottom);
    }];
    
    [self.login_checkButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.login_pwdInfoView.mas_bottom).offset(15);
        make.left.equalTo(self.login_phoneInfoView.login_infoTitleLabel.mas_left);
        make.height.width.mas_equalTo(18);
    }];
    
    [self.login_agreeTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.login_checkButton.mas_right).offset(4);
//        make.centerY.equalTo(self.login_checkButton.mas_centerY).offset(-1);
        make.top.equalTo(self.login_checkButton.mas_top).offset(1);
        make.right.mas_equalTo(-22);
        make.height.mas_equalTo(28);
    }];
    
    [self.login_commitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.login_checkButton.mas_bottom).offset(38);
        make.centerX.equalTo(self.mas_centerX);
        make.width.mas_equalTo(265);
        make.height.mas_equalTo(43);
    }];
    
    [self.login_registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-30);
        make.centerX.equalTo(self.mas_centerX);
    }];
}

#pragma mark - button action

-(void)login_ContentView_checkAction:(UIButton *)button{
    button.selected = !button.isSelected;
}

-(void)login_ContentView_commitAction:(UIButton *)button{
    self.login_commitBlock ? self.login_commitBlock() : nil;
}

-(void)login_ContentView_getCodeAction{
    if (self.login_phoneInfoView.login_infoTextField.text.length == 11) {
        dispatch_resume(self.login_gcdTimer);
    }
    self.login_getCodeBlock ? self.login_getCodeBlock() : nil;
}

-(void)login_ContentView_backAction{
    self.login_backBlock ? self.login_backBlock() : nil;
}

-(void)login_ContentView_registerAction{
    self.login_registerBlock ? self.login_registerBlock() : nil;
}


#pragma mark - UITextViewDelegate
-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    NSLog(@"link");
    
    return NO;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *allStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.login_pwdInfoView.login_infoTextField) {
        if (self.login_phoneInfoView.login_infoTextField.text.length == 11 && allStr.length >= 6 && self.login_getCodeInfoView.login_infoTextField.text.length >= 4 && self.login_againPwdInfoView.login_infoTextField.text.length >= 6) {
            self.login_commitButton.backgroundColor = UIColorFromHex(0xDE5330);
            self.login_commitButton.userInteractionEnabled = YES;
        }else if(self.login_viewType == Login_LoginType && allStr.length >= 6 && self.login_phoneInfoView.login_infoTextField.text.length == 11) {
            self.login_commitButton.backgroundColor = UIColorFromHex(0xDE5330);
            self.login_commitButton.userInteractionEnabled = YES;
        }else{
            self.login_commitButton.backgroundColor = UIColorFromHex(0xD6D6D6);
            self.login_commitButton.userInteractionEnabled = NO;
        }
        
        return YES;
    }
    
    if (textField == self.login_getCodeInfoView.login_infoTextField) {
        if (self.login_phoneInfoView.login_infoTextField.text.length == 11 && allStr.length >= 4 && self.login_pwdInfoView.login_infoTextField.text.length >= 6 && self.login_againPwdInfoView.login_infoTextField.text.length >= 6 && self.login_viewType == Login_RegisterType) {
            self.login_commitButton.backgroundColor = UIColorFromHex(0xDE5330);
            self.login_commitButton.userInteractionEnabled = YES;
        }else{
            self.login_commitButton.backgroundColor = UIColorFromHex(0xD6D6D6);
            self.login_commitButton.userInteractionEnabled = NO;
        }
        
        return YES;
    }
    
    if (textField == self.login_againPwdInfoView.login_infoTextField) {
        if (self.login_phoneInfoView.login_infoTextField.text.length == 11 && self.login_getCodeInfoView.login_infoTextField.text.length >= 4 && self.login_pwdInfoView.login_infoTextField.text.length >= 6 && allStr.length >= 6) {
            self.login_commitButton.backgroundColor = UIColorFromHex(0xDE5330);
            self.login_commitButton.userInteractionEnabled = YES;
        }else{
            self.login_commitButton.backgroundColor = UIColorFromHex(0xD6D6D6);
            self.login_commitButton.userInteractionEnabled = NO;
        }
        
        return YES;
    }
    
    if (textField == self.login_phoneInfoView.login_infoTextField) {
        NSString *phoneNum = @"";
        if (allStr.length >= 11) {
            phoneNum = [allStr substringToIndex:11];
        }
        
        if (phoneNum.length == 11 && self.login_pwdInfoView.login_infoTextField.text.length >= 6 && self.login_getCodeInfoView.login_infoTextField.text.length >= 4 && self.login_againPwdInfoView.login_infoTextField.text.length >= 6) {
            self.login_commitButton.backgroundColor = UIColorFromHex(0xDE5330);
            self.login_commitButton.userInteractionEnabled = YES;
        }else if(self.login_viewType == Login_LoginType && phoneNum.length == 11 && self.login_pwdInfoView.login_infoTextField.text.length >= 6) {
            self.login_commitButton.backgroundColor = UIColorFromHex(0xDE5330);
            self.login_commitButton.userInteractionEnabled = YES;
        }else{
            self.login_commitButton.backgroundColor = UIColorFromHex(0xD6D6D6);
            self.login_commitButton.userInteractionEnabled = NO;
        }
        
        return allStr.length <= 11;
    }
    
    return YES;
}


#pragma mark - lazy loading
-(UIButton *)login_backButton{
    if (!_login_backButton) {
        _login_backButton = [[UIButton alloc] init];
        [_login_backButton setImage:[UIImage imageNamed:@"ic_login_return"] forState:UIControlStateNormal];
        [_login_backButton addTarget:self action:@selector(login_ContentView_backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _login_backButton;
}

-(UILabel *)login_titleLabel{
    if (!_login_titleLabel) {
        _login_titleLabel = [[UILabel alloc] init];
        _login_titleLabel.text = @"登录";
        _login_titleLabel.font = [UIFont systemFontOfSize:24 weight:UIFontWeightSemibold];
        _login_titleLabel.textColor = UIColorFromHex(0x222222);
    }
    return _login_titleLabel;
}


-(Login_InfoUnitView *)login_phoneInfoView{
    if (!_login_phoneInfoView) {
        _login_phoneInfoView = [[Login_InfoUnitView alloc] init];
        _login_phoneInfoView.login_infoTextField.delegate = self;
        _login_phoneInfoView.login_infoTitleLabel.text = @"手机号";
        _login_phoneInfoView.login_infoTextField.placeholder = @"请输入手机号";
        _login_phoneInfoView.login_infoTextField.keyboardType = UIKeyboardTypeNumberPad;
        _login_phoneInfoView.login_unitType = Login_infoUnitTypePhone;
    }
    return _login_phoneInfoView;
}

-(Login_InfoUnitView *)login_pwdInfoView {
    if (!_login_pwdInfoView) {
        _login_pwdInfoView = [[Login_InfoUnitView alloc] init];
        _login_pwdInfoView.login_infoTextField.delegate = self;
        _login_pwdInfoView.login_infoTitleLabel.text = @"密码";
        _login_pwdInfoView.login_infoTextField.placeholder = @"请输入密码";
        _login_pwdInfoView.login_infoTextField.secureTextEntry = YES;
    }
    return _login_pwdInfoView;
}

-(Login_InfoUnitView *)login_againPwdInfoView {
    if (!_login_againPwdInfoView) {
        _login_againPwdInfoView = [[Login_InfoUnitView alloc] init];
        _login_againPwdInfoView.login_infoTextField.delegate = self;
        _login_againPwdInfoView.login_infoTitleLabel.text = @"确认密码";
        _login_againPwdInfoView.login_infoTextField.placeholder = @"再次输入密码";
        _login_againPwdInfoView.login_infoTextField.secureTextEntry = YES;
    }
    return _login_againPwdInfoView;
}

-(Login_InfoUnitView *)login_getCodeInfoView {
    if (!_login_getCodeInfoView) {
        _login_getCodeInfoView = [[Login_InfoUnitView alloc] init];
        _login_getCodeInfoView.login_infoTextField.delegate = self;
        _login_getCodeInfoView.login_infoTitleLabel.text = @"验证码";
        _login_getCodeInfoView.login_infoTextField.placeholder = @"请输入验证码";
        _login_getCodeInfoView.login_infoTextField.keyboardType = UIKeyboardTypeNumberPad;
        _login_getCodeInfoView.login_unitType = Login_infoUnitTypeGetCode;
        [_login_getCodeInfoView.login_infoCodeButton addTarget:self action:@selector(login_ContentView_getCodeAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _login_getCodeInfoView;
}


-(UIButton *)login_checkButton{
    if (!_login_checkButton) {
        _login_checkButton = [[UIButton alloc] init];
        [_login_checkButton setImage:[UIImage imageNamed:@"ic_login_non_choose"] forState:UIControlStateNormal];
        [_login_checkButton setImage:[UIImage imageNamed:@"ic_login_choose"] forState:UIControlStateSelected];
        [_login_checkButton addTarget:self action:@selector(login_ContentView_checkAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _login_checkButton;
}

-(UITextView *)login_agreeTextView{
    if (!_login_agreeTextView) {
        _login_agreeTextView = [[UITextView alloc] init];
        _login_agreeTextView.textContainerInset = UIEdgeInsetsZero;
        _login_agreeTextView.delegate = self;
        _login_agreeTextView.editable = NO;
        
        NSString *contentString = @"继续即代表您同意漫画《用户使用协议》和《隐私保护政策》";
        NSRange agreeRange = [contentString rangeOfString:@"《用户使用协议》"];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSDictionary *linkDic = @{ NSLinkAttributeName : [NSURL URLWithString:@"/register"] };
        [attr addAttributes:linkDic range:agreeRange];
        
        NSRange agreeRangeS = [contentString rangeOfString:@"《隐私保护政策》"];
        NSDictionary *linkDicS = @{ NSLinkAttributeName : [NSURL URLWithString:@"/secret"] };
        [attr addAttributes:linkDicS range:agreeRangeS];
        
        [attr addAttributes:@{ NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:UIColorFromHex(0x999999) } range:NSMakeRange(0, contentString.length)];
        
        _login_agreeTextView.linkTextAttributes = @{NSForegroundColorAttributeName:UIColorFromHex(0xDE5330),NSFontAttributeName:[UIFont systemFontOfSize:12]};
        
        _login_agreeTextView.attributedText = attr;
        
    }
    return _login_agreeTextView;
}

-(UIButton *)login_commitButton{
    if (!_login_commitButton) {
        _login_commitButton = [[UIButton alloc] init];
        [_login_commitButton setTitle:@"登录" forState:UIControlStateNormal];
        [_login_commitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _login_commitButton.backgroundColor = UIColorFromHex(0xD6D6D6);
        _login_commitButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _login_commitButton.layer.cornerRadius = 21;
        [_login_commitButton addTarget:self action:@selector(login_ContentView_commitAction:) forControlEvents:UIControlEventTouchUpInside];
        _login_commitButton.userInteractionEnabled = NO;
    }
    return _login_commitButton;
}

-(UIButton *)login_registerButton{
    if (!_login_registerButton) {
        _login_registerButton = [[UIButton alloc] init];
        [_login_registerButton setTitle:@"注册账号" forState:UIControlStateNormal];
        [_login_registerButton setTitleColor:UIColorFromHex(0xDE5330) forState:UIControlStateNormal];
        _login_registerButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_login_registerButton addTarget:self action:@selector(login_ContentView_registerAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _login_registerButton;
}


-(dispatch_source_t)login_gcdTimer{
    if (!_login_gcdTimer) {
        _login_gcdTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(0, 0));
        dispatch_source_set_timer(_login_gcdTimer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0.0 * NSEC_PER_SEC);
        dispatch_source_set_event_handler(_login_gcdTimer, ^{
            self.login_time --;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.login_getCodeInfoView.login_infoCodeButton setTitle:[NSString stringWithFormat:@"%ld s",self.login_time] forState:UIControlStateNormal];
                self.login_getCodeInfoView.login_infoCodeButton.userInteractionEnabled = NO;
            });
            
            
            if(self.login_time < 0) {
                self.login_time = 60;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.login_getCodeInfoView.login_infoCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
                    self.login_getCodeInfoView.login_infoCodeButton.userInteractionEnabled = YES;
                });
                // 终止定时器
                dispatch_suspend(self->_login_gcdTimer);
            }
        });
    }
    return _login_gcdTimer;
}

@end
