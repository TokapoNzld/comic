//
//  Login_infoUnitView.m
//  BFComic
//
//  Created by majunwen on 2022/4/27.
//

#import "Login_InfoUnitView.h"

@interface Login_InfoUnitView()

@property (nonatomic,strong) UIButton *login_infoClearButton;
@property (nonatomic,strong) UIView *login_infoLineView;

@end

@implementation Login_InfoUnitView

-(void)setLogin_unitType:(Login_infoUnitType)login_unitType{
    _login_unitType = login_unitType;
    
    switch (login_unitType) {
        case Login_infoUnitTypePhone:
        {
            [self.login_infoTextField mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.login_infoTitleLabel.mas_right).offset(30);
                make.centerY.equalTo(self.login_infoTitleLabel.mas_centerY);
                make.right.equalTo(self.login_infoClearButton.mas_left).offset(-10);
            }];
            
            self.login_infoClearButton.hidden = NO;
        }
            break;
        case Login_infoUnitTypePassword:
            break;
        case Login_infoUnitTypeGetCode:
        {
            [self.login_infoTextField mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.login_infoTitleLabel.mas_right).offset(30);
                make.centerY.equalTo(self.login_infoTitleLabel.mas_centerY);
                make.right.equalTo(self.login_infoCodeButton.mas_left).offset(-10);
            }];
            
            self.login_infoCodeButton.hidden = NO;
        }
            break;
    }
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self login_infoUnitView_setupView];
        [self login_infoUnitView_setupLayout];
    }
    return self;
}

-(void)login_infoUnitView_setupView{
    [self addSubview:self.login_infoTitleLabel];
    [self addSubview:self.login_infoTextField];
    [self addSubview:self.login_infoClearButton];
    [self addSubview:self.login_infoLineView];
    [self addSubview:self.login_infoCodeButton];
}

-(void)login_infoUnitView_setupLayout{
    [self.login_infoTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(30);
        make.bottom.mas_equalTo(-15);
        make.width.mas_equalTo(60);
    }];
    
    [self.login_infoTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.login_infoTitleLabel.mas_right).offset(30);
        make.centerY.equalTo(self.login_infoTitleLabel.mas_centerY);
        make.right.mas_equalTo(-12);
    }];
    
    [self.login_infoClearButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.width.height.mas_equalTo(14);
        make.centerY.equalTo(self.login_infoTitleLabel.mas_centerY);
    }];
    
    [self.login_infoCodeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.width.mas_equalTo(75);
        make.centerY.equalTo(self.login_infoTitleLabel.mas_centerY);
    }];
    
    [self.login_infoLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

-(void)login_InfoUnitView_clearPhoneAction{
    self.login_infoTextField.text = @"";
}

#pragma mark - lazy loading
-(UILabel *)login_infoTitleLabel{
    if (!_login_infoTitleLabel) {
        _login_infoTitleLabel = [[UILabel alloc] init];
        _login_infoTitleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
        _login_infoTitleLabel.textColor = UIColorFromHex(0x222222);
    }
    return _login_infoTitleLabel;
}

-(UITextField *)login_infoTextField{
    if (!_login_infoTextField) {
        _login_infoTextField = [[UITextField alloc] init];
        _login_infoTextField.textColor = UIColorFromHex(0x222222);
        _login_infoTextField.font = [UIFont systemFontOfSize:14];
    }
    return _login_infoTextField;
}

-(UIButton *)login_infoClearButton{
    if (!_login_infoClearButton) {
        _login_infoClearButton = [[UIButton alloc] init];
        [_login_infoClearButton setImage:[UIImage imageNamed:@"ic_login_close"] forState:UIControlStateNormal];
        _login_infoClearButton.hidden = YES;
        [_login_infoClearButton addTarget:self action:@selector(login_InfoUnitView_clearPhoneAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _login_infoClearButton;
}

-(UIView *)login_infoLineView{
    if (!_login_infoLineView) {
        _login_infoLineView = [[UIView alloc] init];
        _login_infoLineView.backgroundColor = UIColorFromHex(0xEEEEEE);
    }
    return _login_infoLineView;
}

-(UIButton *)login_infoCodeButton{
    if (!_login_infoCodeButton) {
        _login_infoCodeButton = [[UIButton alloc] init];
        [_login_infoCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_login_infoCodeButton setTitleColor:UIColorFromHex(0xDE5330) forState:UIControlStateNormal];
        _login_infoCodeButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _login_infoCodeButton.hidden = YES;
    }
    return _login_infoCodeButton;
}


@end
