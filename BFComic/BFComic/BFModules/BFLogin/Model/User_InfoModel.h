//
//  User_InfoModel.h
//  BFComic
//
//  Created by majunwen on 2022/4/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface User_InfoModel : NSObject

@property (nonatomic,copy) NSString *account;
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *birthday;
@property (nonatomic,copy) NSString *email;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSString *telephone;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *wxOpenid;

@end

NS_ASSUME_NONNULL_END
