//
//  User_InfoManager.h
//  BFComic
//
//  Created by majunwen on 2022/4/27.
//

#import <Foundation/Foundation.h>
#import "User_InfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface User_InfoManager : NSObject

@property (nonatomic,strong) User_InfoModel *user_Model;

//清空usermodel
-(void)user_clearUserModel;

//单例方法
+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
