//
//  User_InfoManager.m
//  BFComic
//
//  Created by majunwen on 2022/4/27.
//

#import "User_InfoManager.h"
#import "MJExtension.h"

@interface User_InfoManager()

@end

@implementation User_InfoManager

NSString *kUserInfoModelKey = @"kUserInfoModelKey";

-(void)setUser_Model:(User_InfoModel *)user_Model{
    NSString *jsonString = [user_Model mj_JSONString];
    [[NSUserDefaults standardUserDefaults] setValue:jsonString forKey:kUserInfoModelKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(User_InfoModel *)user_Model{
    NSString *modelJsonString = [[NSUserDefaults standardUserDefaults] valueForKey:kUserInfoModelKey];
    return [User_InfoModel mj_objectWithKeyValues:modelJsonString];
}

-(void)user_clearUserModel {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserInfoModelKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

static User_InfoManager *manager;
//初始化方法
- (instancetype)init{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [super init];
    });
    return manager;
}

//单例方法
+ (instancetype)sharedInstance{
    return [[self alloc] init];
}

//alloc会调用allocWithZone，确保使用同一块内存地址
+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [super allocWithZone:zone];
    });
    return manager;
}

//copy的时候会调用copyWithZone
- (id)copyWithZone:(NSZone *)zone{
    return manager;
}

- (id)mutableCopyWithZone:(NSZone *)zone{
    return manager;
}

@end
