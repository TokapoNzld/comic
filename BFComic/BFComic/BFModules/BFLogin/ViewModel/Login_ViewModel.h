//
//  Login_ViewModel.h
//  BFComic
//
//  Created by majunwen on 2022/4/26.
//

#import <Foundation/Foundation.h>
#import "BFBaseRequest.h"
#import "User_InfoManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface Login_ViewModel : NSObject

///发送验证码
- (void)login_ViewModel_requestSendCodeWithPhone:(NSString *)phone success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

///注册
- (void)login_ViewModel_requestRegisterWithPhone:(NSString *)phone code:(NSString *)code password:(NSString *)password confirmPassword:(NSString *)confirmPassword success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

///登录
- (void)login_ViewModel_requestLoginWithPhone:(NSString *)phone password:(NSString *)password success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

///退出登录
+ (void)login_ViewModel_requestLoginoutSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

///获取用户信息
- (void)login_ViewModel_requestUserInfoSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

@end

NS_ASSUME_NONNULL_END
