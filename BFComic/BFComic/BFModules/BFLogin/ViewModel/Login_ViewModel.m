//
//  Login_ViewModel.m
//  BFComic
//
//  Created by majunwen on 2022/4/26.
//

#import "Login_ViewModel.h"
#import "BF_ApiConfig.h"
#import "MJExtension.h"

@implementation Login_ViewModel

- (void)login_ViewModel_requestSendCodeWithPhone:(NSString *)phone success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] GET:BF_API_LOGIN_SMSCODE parameters:@{@"telephone":phone,@"type":@"1"} downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}

- (void)login_ViewModel_requestRegisterWithPhone:(NSString *)phone code:(NSString *)code password:(NSString *)password confirmPassword:(NSString *)confirmPassword success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] POST:BF_API_REGISTER_MOBILE parameters:@{@"account":phone,@"code":code,@"confirmPassword":confirmPassword,@"password":password} downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        if ([responseObject[@"code"] integerValue] == 200) {
            User_InfoModel *model = [User_InfoModel mj_objectWithKeyValues:responseObject[@"data"]];
            [User_InfoManager sharedInstance].user_Model = model;
        }
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}

- (void)login_ViewModel_requestLoginWithPhone:(NSString *)phone password:(NSString *)password success:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] POST:BF_API_LOGIN_PASSWORD parameters:@{@"account":phone,@"password":password} downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        if ([responseObject[@"code"] integerValue] == 200) {
            User_InfoModel *model = [User_InfoModel mj_objectWithKeyValues:responseObject[@"data"]];
            [User_InfoManager sharedInstance].user_Model = model;
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoginViewModelLoginNotificationName object:nil userInfo:nil];
        }
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}

+ (void)login_ViewModel_requestLoginoutSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] POST:BF_API_LOGIN_LOGOUT parameters:nil downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        if ([responseObject[@"code"] integerValue] == 200) {
            [[User_InfoManager sharedInstance] user_clearUserModel];
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoginViewModelLogoutNotificationName object:nil userInfo:nil];
        }
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}


- (void)login_ViewModel_requestUserInfoSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] GET:BF_API_USER_INFO parameters:nil downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        if ([responseObject[@"code"] integerValue] == 200) {
            User_InfoModel *model = [User_InfoModel mj_objectWithKeyValues:responseObject[@"data"]];
            [User_InfoManager sharedInstance].user_Model = model;
        }
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}

@end
