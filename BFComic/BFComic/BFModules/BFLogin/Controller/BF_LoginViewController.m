//
//  BF_LoginViewController.m
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import "BF_LoginViewController.h"
#import "Login_ViewModel.h"
#import "BFToastManager.h"

@interface BF_LoginViewController ()

@property (nonatomic,strong) Login_ContentView *login_infoView;
@property (nonatomic,strong) Login_ViewModel *login_mainViewModel;

@end

@implementation BF_LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.isHidenNavBar = YES;
    [self base_setupBlock];
}

- (void)base_setupViews{
    [self.view addSubview:self.login_infoView];
}

-(void)base_setupLayout{
    [self.login_infoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kStatusHeight);
        make.left.right.bottom.equalTo(self.view);
    }];
}

-(void)base_setupBlock {
    __weak typeof(self) weakSelf = self;
    self.login_infoView.login_backBlock = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    self.login_infoView.login_getCodeBlock = ^{
        [weakSelf login_LoginViewController_getCode];
    };
    
    self.login_infoView.login_commitBlock = ^{
        if (weakSelf.login_controllerType == Login_RegisterType) {
            [weakSelf login_LoginViewController_register];
        }else if(weakSelf.login_controllerType == Login_LoginType) {
            [weakSelf login_LoginViewController_login];
        }
    };
    
    self.login_infoView.login_registerBlock = ^{
        BF_LoginViewController *vc = [[BF_LoginViewController alloc] init];
        vc.login_controllerType = Login_RegisterType;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
}

-(void)login_LoginViewController_register {
    if (!self.login_infoView.login_checkButton.isSelected) {
        [BFToastManager bf_showErrorWithStatus:@"请先同意协议" duration:1.2];
        return;
    }
    NSString *phone = self.login_infoView.login_phoneInfoView.login_infoTextField.text;
    NSString *password = self.login_infoView.login_pwdInfoView.login_infoTextField.text;
    NSString *confirmPassword = self.login_infoView.login_againPwdInfoView.login_infoTextField.text;
    NSString *code = self.login_infoView.login_getCodeInfoView.login_infoTextField.text;
    [BFToastManager bf_show];
    [self.login_mainViewModel login_ViewModel_requestRegisterWithPhone:phone code:code password:password confirmPassword:confirmPassword success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [BFToastManager bf_dismiss];
        if ([responseObject[@"code"] integerValue] == 200) {
            [self.navigationController popViewControllerAnimated:YES];
            [BFToastManager bf_showSuccessWithStatus:@"注册成功" duration:1.0];
        }else {
            [BFToastManager bf_showSuccessWithStatus:responseObject[@"msg"] duration:1.0];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        [BFToastManager bf_dismiss];
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

-(void)login_LoginViewController_getCode {
    NSString *phone = self.login_infoView.login_phoneInfoView.login_infoTextField.text;
    if (phone.length == 11) {
        [self.login_mainViewModel login_ViewModel_requestSendCodeWithPhone:phone success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 200) {
                NSString *msg = responseObject[@"msg"];
                [BFToastManager bf_showSuccessWithStatus:msg duration:1.0];
            }else {
                [BFToastManager bf_showSuccessWithStatus:responseObject[@"msg"] duration:1.0];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
            NSLog(@"error = %@",error.localizedDescription);
        }];
    }
}

-(void)login_LoginViewController_login {
    if (!self.login_infoView.login_checkButton.isSelected) {
        [BFToastManager bf_showErrorWithStatus:@"请先同意协议" duration:1.2];
        return;
    }
    NSString *phone = self.login_infoView.login_phoneInfoView.login_infoTextField.text;
    NSString *password = self.login_infoView.login_pwdInfoView.login_infoTextField.text;
    [BFToastManager bf_show];
    [self.login_mainViewModel login_ViewModel_requestLoginWithPhone:phone password:password success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [BFToastManager bf_dismiss];
        if ([responseObject[@"code"] integerValue] == 200) {
            [self.navigationController popViewControllerAnimated:YES];
            [BFToastManager bf_showSuccessWithStatus:@"登录成功" duration:1.0];
            
            [self login_LoginViewController_getUserInfo];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        [BFToastManager bf_dismiss];
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

-(void)login_LoginViewController_getUserInfo {
    [self.login_mainViewModel login_ViewModel_requestUserInfoSuccess:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
            
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

#pragma mark - lazy loading
-(Login_ContentView *)login_infoView{
    if (!_login_infoView) {
        _login_infoView = [[Login_ContentView alloc] init];
        _login_infoView.login_viewType = _login_controllerType;
    }
    return _login_infoView;
}

-(Login_ViewModel *)login_mainViewModel{
    if (!_login_mainViewModel) {
        _login_mainViewModel = [[Login_ViewModel alloc] init];
    }
    return _login_mainViewModel;
}

@end
