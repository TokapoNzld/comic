//
//  BF_LoginViewController.h
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import "BFBaseViewController.h"
#import "Login_ContentView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BF_LoginViewController : BFBaseViewController

@property (nonatomic,assign) Login_Type login_controllerType;

@end

NS_ASSUME_NONNULL_END
