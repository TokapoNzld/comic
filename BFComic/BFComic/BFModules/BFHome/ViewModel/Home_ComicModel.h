//
//  Home_ComicModel.h
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface Home_ComicUnitModel : NSObject

@property (nonatomic,copy) NSString *cartoonTitle;
@property (nonatomic,copy) NSString *comicId;
@property (nonatomic,copy) NSString *imgUrl;

@end

@interface Home_ComicModel : NSObject

@property (nonatomic,strong) NSArray<Home_ComicUnitModel *> *list;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *type;

@end


NS_ASSUME_NONNULL_END
