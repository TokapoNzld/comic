//
//  BF_HomeViewModel.m
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import "Home_ViewModel.h"
#import "BF_ApiConfig.h"
#import "YYModel.h"
#import "MJExtension.h"

@implementation Home_ViewModel

- (void)home_ViewModel_requestHomeHotComicWithSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] GET:BF_API_HOME_INDEXHOT parameters:nil downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        self.home_comics = [Home_ComicModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}


@end
