//
//  BF_HomeViewModel.h
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import <Foundation/Foundation.h>
#import "BFBaseRequest.h"
#import "Home_ComicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Home_ViewModel : NSObject

@property (nonatomic,strong) NSArray<Home_ComicModel *> *home_comics;

- (void)home_ViewModel_requestHomeHotComicWithSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

@end

NS_ASSUME_NONNULL_END
