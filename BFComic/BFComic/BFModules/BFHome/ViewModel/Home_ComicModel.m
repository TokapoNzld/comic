//
//  Home_ComicModel.m
//  BFComic
//
//  Created by majunwen on 2022/4/25.
//

#import "Home_ComicModel.h"

@implementation Home_ComicUnitModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
        @"comicId" : @"id"
    };
}

@end

@implementation Home_ComicModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
        @"list" : [Home_ComicUnitModel class]
    };
}

@end
