//
//  BF_ContentSelectCollectionViewCell.h
//  BFComic
//
//  Created by majunwen on 2022/4/26.
//

#import <UIKit/UIKit.h>
#import "Home_ComicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Home_ContentSelectCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) Home_ComicUnitModel *home_selectModel;

@end

NS_ASSUME_NONNULL_END
