//
//  Home_CycleScrollHeaderView.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import <UIKit/UIKit.h>
#import "Home_ComicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Home_CycleScrollHeaderView : UICollectionViewCell

@property (nonatomic,strong) Home_ComicModel *home_bannerModel;

@end

NS_ASSUME_NONNULL_END
