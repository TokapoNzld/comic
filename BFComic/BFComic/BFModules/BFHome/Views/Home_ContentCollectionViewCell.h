//
//  Home_ContentCollectionViewCell.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import <UIKit/UIKit.h>
#import "Home_ComicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Home_ContentCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) Home_ComicUnitModel *home_contentModel;

@end

NS_ASSUME_NONNULL_END
