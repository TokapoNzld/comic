//
//  Home_HeaderTitleReusableView.h
//  BFComic
//
//  Created by majunwen on 2022/4/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Home_HeaderTitleReusableView : UICollectionReusableView

- (void)updateContent:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
