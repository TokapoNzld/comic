//
//  Home_HeaderTitleReusableView.m
//  BFComic
//
//  Created by majunwen on 2022/4/26.
//

#import "Home_HeaderTitleReusableView.h"

@interface Home_HeaderTitleReusableView()

@property (nonatomic,strong) UILabel *titleLabel;

@end

@implementation Home_HeaderTitleReusableView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self home_HeaderTitleReusableView_setupView];
        [self home_HeaderTitleReusableView_setupLayout];
    }
    return self;
}

-(void)home_HeaderTitleReusableView_setupView {
    [self addSubview:self.titleLabel];
}

-(void)home_HeaderTitleReusableView_setupLayout {
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(-10);
    }];
}

- (void)updateContent:(NSString *)title {
    self.titleLabel.text = title;
}

#pragma mark - lazy loading
-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"热门动漫";
        _titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
        _titleLabel.textColor = [UIColor whiteColor];
    }
    return _titleLabel;
}


@end
