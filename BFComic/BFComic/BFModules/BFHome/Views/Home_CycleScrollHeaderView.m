//
//  Home_CycleScrollHeaderView.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import "Home_CycleScrollHeaderView.h"
#import <SDCycleScrollView/SDCycleScrollView.h>

@interface Home_CycleScrollHeaderView ()

@property (nonatomic, strong) SDCycleScrollView *home_cycleScrollView;

@end

@implementation Home_CycleScrollHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self.contentView addSubview:self.home_cycleScrollView];
        [self.home_cycleScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        self.layer.cornerRadius = 10;
        self.layer.masksToBounds = YES;
    }
    return self;
}

- (void)setHome_bannerModel:(Home_ComicModel *)home_bannerModel {
    NSMutableArray *imageArray = [[NSMutableArray alloc] init];
    NSMutableArray *titleArray = [[NSMutableArray alloc] init];
    for (Home_ComicUnitModel *model in home_bannerModel.list) {
        [imageArray addObject:model.imgUrl];
//        [titleArray addObject:model.cartoonTitle];
    }
    self.home_cycleScrollView.imageURLStringsGroup = imageArray.copy;
    self.home_cycleScrollView.titlesGroup = titleArray.copy;
}

- (SDCycleScrollView *)home_cycleScrollView {
    if (!_home_cycleScrollView) {
        _home_cycleScrollView = [[SDCycleScrollView alloc] init];
        _home_cycleScrollView.localizationImageNamesGroup = @[@"banner1", @"banner2", @"banner3"];
        _home_cycleScrollView.titlesGroup = @[@"banenr1", @"banner2", @"banner3"];
    }
    return _home_cycleScrollView;
}

@end
