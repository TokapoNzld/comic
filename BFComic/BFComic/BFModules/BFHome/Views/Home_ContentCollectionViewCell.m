//
//  Home_ContentCollectionViewCell.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import "Home_ContentCollectionViewCell.h"
#import "SDWebImage.h"

@interface Home_ContentCollectionViewCell ()

@property (nonatomic, strong) UIImageView *home_contentImageView;
@property (nonatomic, strong) UILabel *home_contentTitleLabel;

@end

@implementation Home_ContentCollectionViewCell

-(void)setHome_contentModel:(Home_ComicUnitModel *)home_contentModel{
    _home_contentModel = home_contentModel;
    [self.home_contentImageView sd_setImageWithURL:[NSURL URLWithString:home_contentModel.imgUrl] placeholderImage:kDefaultImage];
    self.home_contentTitleLabel.text = home_contentModel.cartoonTitle;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if ([super initWithFrame:frame]) {
        [self home_ContentCollectionCell_setupUI];
        [self home_ContentCollectionCell_setupLayout];
    }
    return self;
}

- (void)home_ContentCollectionCell_setupUI {
    [self.contentView addSubview:self.home_contentImageView];
    [self.contentView addSubview:self.home_contentTitleLabel];
}

- (void)home_ContentCollectionCell_setupLayout {
    [self.home_contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-20);
    }];
    
    [self.home_contentTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.home_contentImageView.mas_bottom).offset(5);
        make.left.equalTo(self.contentView.mas_left).offset(5);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
    }];
}

- (UIImageView *)home_contentImageView {
    if (!_home_contentImageView) {
        _home_contentImageView = [[UIImageView alloc] init];
        _home_contentImageView.image = [UIImage imageNamed:@"comic_placeholder"];
        _home_contentImageView.clipsToBounds = true;
        _home_contentImageView.contentMode = UIViewContentModeScaleAspectFill;
        _home_contentImageView.layer.cornerRadius = 5;
        _home_contentImageView.layer.masksToBounds = true;
    }
    return _home_contentImageView;
}

- (UILabel *)home_contentTitleLabel {
    if (!_home_contentTitleLabel) {
        _home_contentTitleLabel = [[UILabel alloc] init];
        _home_contentTitleLabel.text = @"海贼王";
        _home_contentTitleLabel.textAlignment = NSTextAlignmentCenter;
        _home_contentTitleLabel.font = [UIFont systemFontOfSize:14];
        _home_contentTitleLabel.textColor = [UIColor whiteColor];
    }
    return _home_contentTitleLabel;
}

@end
