//
//  BF_ContentSelectCollectionViewCell.m
//  BFComic
//
//  Created by majunwen on 2022/4/26.
//

#import "Home_ContentSelectCollectionViewCell.h"
#import "SDWebImage.h"

@interface Home_ContentSelectCollectionViewCell()

@property (nonatomic,strong) UIImageView *home_selectImageView;
@property (nonatomic,strong) UILabel *home_comicTitleLabel;
@property (nonatomic,strong) UILabel *home_comicChapterLabel;

@end

@implementation Home_ContentSelectCollectionViewCell

-(void)setHome_selectModel:(Home_ComicUnitModel *)home_selectModel{
    _home_selectModel = home_selectModel;
    [self.home_selectImageView sd_setImageWithURL:[NSURL URLWithString:home_selectModel.imgUrl] placeholderImage:kDefaultImage];
    self.home_comicTitleLabel.text = home_selectModel.cartoonTitle;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self home_ContentSelectCollectionViewCell_setupView];
        [self home_ContentSelectCollectionViewCell_setupLayout];
    }
    return self;
}

-(void)home_ContentSelectCollectionViewCell_setupView{
    [self.contentView addSubview:self.home_selectImageView];
    [self.contentView addSubview:self.home_comicTitleLabel];
    [self.contentView addSubview:self.home_comicChapterLabel];
}

-(void)home_ContentSelectCollectionViewCell_setupLayout{
    [self.home_selectImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(self.contentView);
        make.width.mas_equalTo(90);
    }];
    
    [self.home_comicTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.home_selectImageView.mas_top).offset(2);
        make.right.equalTo(self.contentView.mas_right);
        make.left.equalTo(self.home_selectImageView.mas_right).offset(6);
    }];
    
    [self.home_comicChapterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.home_comicTitleLabel.mas_bottom).offset(12);
        make.left.right.equalTo(self.home_comicTitleLabel);
    }];
}

#pragma mark - lazy loading
-(UIImageView *)home_selectImageView{
    if (!_home_selectImageView) {
        _home_selectImageView = [[UIImageView alloc] init];
        _home_selectImageView.image = [UIImage imageNamed:@"comic_placeholder"];
        _home_selectImageView.clipsToBounds = true;
        _home_selectImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _home_selectImageView;
}

-(UILabel *)home_comicTitleLabel {
    if (!_home_comicTitleLabel) {
        _home_comicTitleLabel = [[UILabel alloc] init];
        _home_comicTitleLabel.text = @"海贼王";
        _home_comicTitleLabel.font = [UIFont systemFontOfSize:14];
    }
    return _home_comicTitleLabel;
}

-(UILabel *)home_comicChapterLabel {
    if (!_home_comicChapterLabel) {
        _home_comicChapterLabel = [[UILabel alloc] init];
        _home_comicChapterLabel.text = @"共116章";
        _home_comicChapterLabel.font = [UIFont systemFontOfSize:14];
    }
    return _home_comicChapterLabel;
}

@end
