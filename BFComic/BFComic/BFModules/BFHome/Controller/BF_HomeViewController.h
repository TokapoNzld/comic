//
//  HomeViewController.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import <UIKit/UIKit.h>
#import "BFBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BF_HomeViewController : BFBaseViewController

@end

NS_ASSUME_NONNULL_END
