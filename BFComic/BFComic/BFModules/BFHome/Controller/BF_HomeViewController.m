//
//  HomeViewController.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import "BF_HomeViewController.h"
#import "Home_CycleScrollHeaderView.h"
#import "BF_BookDetailViewController.h"
#import "Home_ViewModel.h"
#import "Home_ContentCollectionViewCell.h"
#import "Home_HeaderTitleReusableView.h"
#import "Home_ContentSelectCollectionViewCell.h"
#import "MJRefresh.h"
#import "BF_SearchViewController.h"

@interface BF_HomeViewController () <UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *home_ContentCollectionView;
@property (nonatomic, strong) Home_ViewModel *home_mainViewModel;

@end

@implementation BF_HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"首页";
    [self home_HomeViewController_requestIndexHot];
    
    __weak typeof(self) weakSelf = self;
    self.home_ContentCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf home_HomeViewController_requestIndexHot];
    }];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"nav_search"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(clickSearchAction)];
}

- (void)clickSearchAction {
    BF_SearchViewController *searchVC = [[BF_SearchViewController alloc] init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)base_setupViews {
    [self.view addSubview:self.home_ContentCollectionView];
}

- (void)base_setupLayout {
    [self.home_ContentCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

-(void)home_HomeViewController_requestIndexHot {
    __weak typeof(self) weakSelf = self;
    [self.home_mainViewModel home_ViewModel_requestHomeHotComicWithSuccess:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [weakSelf.home_ContentCollectionView reloadData];
        [weakSelf.home_ContentCollectionView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        [weakSelf.home_ContentCollectionView.mj_header endRefreshing];
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.home_mainViewModel.home_comics.count;;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    Home_ComicModel *model = self.home_mainViewModel.home_comics[section];
    if ([model.type isEqualToString: @"banner"]) {
        return 1;
    }
    return model.list.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [[UICollectionViewCell alloc] init];
    Home_ComicModel *model = self.home_mainViewModel.home_comics[indexPath.section];
    if ([model.type isEqualToString:@"banner"]) {
        Home_CycleScrollHeaderView *bannerCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Home_CycleScrollHeaderView" forIndexPath:indexPath];
        bannerCell.home_bannerModel = model;
        cell = bannerCell;
    }else if ([model.type isEqualToString:@"list"]) {
        Home_ContentCollectionViewCell *selectCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Home_ContentCollectionViewCell" forIndexPath:indexPath];
        selectCell.home_contentModel = model.list[indexPath.row];
        cell = selectCell;
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = 0;
    CGFloat height = 0;
    Home_ComicModel *model = self.home_mainViewModel.home_comics[indexPath.section];
    if ([model.type isEqualToString:@"banner"]) {
        width = kScreenWidth - 24;
        height = width * 9 / 16;
    }else if ([model.type isEqualToString:@"list"]) {
        width = (kScreenWidth - 40)/2;
        height = width * 9 / 16;
    }
    return CGSizeMake(width, height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (section == 2) {
        return 30;
    }
    return 20;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    BF_BookDetailViewController *vc = [[BF_BookDetailViewController alloc] init];
    vc.bookdetail_comicId = self.home_mainViewModel.home_comics[indexPath.section].list[indexPath.row].comicId;
    vc.navigationItem.title = self.home_mainViewModel.home_comics[indexPath.section].list[indexPath.row].cartoonTitle;
    [self.navigationController pushViewController:vc animated:YES];
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    // 视图添加到 UICollectionReusableView 创建的对象中
    if (kind == UICollectionElementKindSectionHeader) {
        Home_HeaderTitleReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Home_HeaderTitleReusableView" forIndexPath:indexPath];
        Home_ComicModel *model = self.home_mainViewModel.home_comics[indexPath.section];
        [headerView updateContent:model.title];
        return headerView;
    }
    return nil;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeZero;
    }
    return CGSizeMake(kScreenWidth, 70);
}

#pragma mark - Getter && Setter
- (UICollectionView *)home_ContentCollectionView {
    if (!_home_ContentCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _home_ContentCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _home_ContentCollectionView.delegate = self;
        _home_ContentCollectionView.dataSource = self;
        _home_ContentCollectionView.showsVerticalScrollIndicator = NO;
        _home_ContentCollectionView.backgroundColor = [UIColor clearColor];
        _home_ContentCollectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
        [_home_ContentCollectionView registerClass:[Home_ContentCollectionViewCell class] forCellWithReuseIdentifier:@"Home_ContentCollectionViewCell"];
        [_home_ContentCollectionView registerClass:[Home_CycleScrollHeaderView class] forCellWithReuseIdentifier:@"Home_CycleScrollHeaderView"];
        [_home_ContentCollectionView registerClass:[Home_ContentSelectCollectionViewCell class] forCellWithReuseIdentifier:@"Home_ContentSelectCollectionViewCell"];
        [_home_ContentCollectionView registerClass:[Home_HeaderTitleReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Home_HeaderTitleReusableView"];
    }
    return _home_ContentCollectionView;
}

-(Home_ViewModel *)home_mainViewModel{
    if (!_home_mainViewModel) {
        _home_mainViewModel = [[Home_ViewModel alloc] init];
    }
    return _home_mainViewModel;
}

@end
