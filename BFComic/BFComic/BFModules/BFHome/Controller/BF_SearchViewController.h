//
//  BF_SearchViewController.h
//  BFComic
//
//  Created by TokapoNzld on 2022/10/23.
//

#import <UIKit/UIKit.h>
#import "BFBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BF_SearchViewController : BFBaseViewController

@end

NS_ASSUME_NONNULL_END
