//
//  DiscoveryViewController.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/8.
//

#import "BF_DiscoveryViewController.h"
#import "Discovery_ContentTableViewCell.h"
#import "BF_BookDetailViewController.h"
#import "Discovery_ViewModel.h"
#import "MJRefresh.h"

@interface BF_DiscoveryViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *discovery_tableView;
@property (nonatomic, strong) Discovery_ViewModel *discovery_mainViewModel;

@end

@implementation BF_DiscoveryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"发现";
    [self discovery_DiscoveryViewController_requestFindList];
    
    __weak typeof(self) weakSelf = self;
    self.discovery_tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf discovery_DiscoveryViewController_requestFindList];
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)base_setupViews {
    [self.view addSubview:self.discovery_tableView];
}

- (void)base_setupLayout {
    [self.discovery_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

-(void)discovery_DiscoveryViewController_requestFindList{
    __weak typeof(self) weakSelf = self;
    [self.discovery_mainViewModel discovery_ViewModel_requestFindComicWithSuccess:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        [weakSelf.discovery_tableView reloadData];
        [weakSelf.discovery_tableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        [weakSelf.discovery_tableView.mj_header endRefreshing];
        NSLog(@"error = %@",error.localizedDescription);
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.discovery_mainViewModel.discovery_finds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Discovery_ContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Discovery_ContentTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.discovery_cellComicModel = self.discovery_mainViewModel.discovery_finds[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BF_BookDetailViewController *vc = [[BF_BookDetailViewController alloc] init];
    vc.bookdetail_comicId = self.discovery_mainViewModel.discovery_finds[indexPath.row].comicId;
    vc.navigationItem.title = self.discovery_mainViewModel.discovery_finds[indexPath.row].cartoonTitle;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Getter setter
- (UITableView *)discovery_tableView {
    if (!_discovery_tableView) {
        _discovery_tableView = [[UITableView alloc] init];
        _discovery_tableView.delegate = self;
        _discovery_tableView.dataSource = self;
        _discovery_tableView.showsVerticalScrollIndicator = false;
        _discovery_tableView.backgroundColor = AppMainThemeColor;
        [_discovery_tableView registerClass:[Discovery_ContentTableViewCell class] forCellReuseIdentifier:@"Discovery_ContentTableViewCell"];
    }
    return _discovery_tableView;
}

-(Discovery_ViewModel *)discovery_mainViewModel{
    if (!_discovery_mainViewModel) {
        _discovery_mainViewModel = [[Discovery_ViewModel alloc] init];
    }
    return _discovery_mainViewModel;
}

@end
