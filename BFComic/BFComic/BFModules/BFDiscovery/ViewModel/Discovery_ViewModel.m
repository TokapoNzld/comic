//
//  Discovery_ViewModel.m
//  BFComic
//
//  Created by majunwen on 2022/5/23.
//

#import "Discovery_ViewModel.h"
#import "BF_ApiConfig.h"
#import "MJExtension.h"

@implementation Discovery_ViewModel

- (void)discovery_ViewModel_requestFindComicWithSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure {
    [[BFBaseRequest manager] GET:BF_API_DISCOVERY_FIND parameters:nil downloadProgress:nil success:^(NSURLSessionDataTask * _Nullable task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        self.discovery_finds = [Discovery_ComicModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
        success(task,responseObject);
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nullable error) {
        failure ? failure(task,error) : nil;
    }];
}

@end
