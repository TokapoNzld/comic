//
//  Discovery_ComicModel.m
//  BFComic
//
//  Created by majunwen on 2022/5/23.
//

#import "Discovery_ComicModel.h"

@implementation Discovery_ComicModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
        @"comicId" : @"id"
    };
}

@end
