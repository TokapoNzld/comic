//
//  Discovery_ViewModel.h
//  BFComic
//
//  Created by majunwen on 2022/5/23.
//

#import <Foundation/Foundation.h>
#import "BFBaseRequest.h"
#import "Discovery_ComicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Discovery_ViewModel : NSObject

@property (nonatomic,strong) NSArray<Discovery_ComicModel *> *discovery_finds;

- (void)discovery_ViewModel_requestFindComicWithSuccess:(BFNetworkSessionSuccess)success failure:(BFNetworkSessionFailure)failure;

@end

NS_ASSUME_NONNULL_END
