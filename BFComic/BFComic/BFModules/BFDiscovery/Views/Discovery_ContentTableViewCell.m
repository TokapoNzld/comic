//
//  Discovery_ContentTableViewCell.m
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import "Discovery_ContentTableViewCell.h"
#import "SDWebImage/SDWebImage.h"

@interface Discovery_ContentTableViewCell ()

@property (nonatomic, strong) UIImageView *discovery_imageView;
@property (nonatomic, strong) UILabel *discovery_titleLabel;
@property (nonatomic, strong) UILabel *discovery_authorLabel;
@property (nonatomic, strong) UILabel *discovery_hotLabel;
@property (nonatomic, strong) UIView  *lineView;

@end

@implementation Discovery_ContentTableViewCell

- (void)setDiscovery_cellComicModel:(Discovery_ComicModel *)discovery_cellComicModel{
    _discovery_cellComicModel = discovery_cellComicModel;
    [self.discovery_imageView sd_setImageWithURL:[NSURL URLWithString:discovery_cellComicModel.imgUrl] placeholderImage:kDefaultImage];
    self.discovery_titleLabel.text = discovery_cellComicModel.cartoonTitle;
    self.discovery_authorLabel.text = [NSString stringWithFormat:@"作者: %@",discovery_cellComicModel.author];
    if (discovery_cellComicModel.hot.length != 0) {
        self.discovery_hotLabel.text = [NSString stringWithFormat:@"热度: %@",discovery_cellComicModel.hot];
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = AppMainThemeColor;
        [self discovery_ContentTableViewCell_setupUI];
        [self discovery_ContentTableViewCell_setupLayout];
    }
    return self;
}

- (void)discovery_ContentTableViewCell_setupUI {
    [self.contentView addSubview:self.discovery_imageView];
    [self.contentView addSubview:self.discovery_titleLabel];
    [self.contentView addSubview:self.discovery_authorLabel];
    [self.contentView addSubview:self.discovery_hotLabel];
    [self.contentView addSubview:self.lineView];
}

- (void)discovery_ContentTableViewCell_setupLayout {
    [self.discovery_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(12);
        make.top.equalTo(self.contentView.mas_top).offset(8);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-8);
        make.width.equalTo(@90);
    }];
    
    [self.discovery_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.discovery_imageView.mas_top);
        make.left.equalTo(self.discovery_imageView.mas_right).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-12);
    }];
    
    [self.discovery_authorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.discovery_imageView.mas_centerY);
        make.left.equalTo(self.discovery_imageView.mas_right).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-12);
    }];
    
    [self.discovery_hotLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.discovery_imageView.mas_bottom);
        make.left.equalTo(self.discovery_imageView.mas_right).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-12);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@(0.5));
    }];
}

#pragma mark - Getter && Setter
- (UIImageView *)discovery_imageView {
    if (!_discovery_imageView) {
        _discovery_imageView = [[UIImageView alloc] init];
        _discovery_imageView.image = [UIImage imageNamed:@"comic_placeholder1"];
        _discovery_imageView.layer.cornerRadius = 5;
        _discovery_imageView.layer.masksToBounds = true;
    }
    return _discovery_imageView;
}

- (UILabel *)discovery_titleLabel {
    if (!_discovery_titleLabel) {
        _discovery_titleLabel = [[UILabel alloc] init];
//        _discovery_titleLabel.text = @"无意间向她告了白";
        _discovery_titleLabel.font = [UIFont systemFontOfSize:14];
        _discovery_titleLabel.textColor = [UIColor whiteColor];
    }
    return _discovery_titleLabel;
}

- (UILabel *)discovery_authorLabel {
    if (!_discovery_authorLabel) {
        _discovery_authorLabel = [[UILabel alloc] init];
//        _discovery_authorLabel.text = @"作者: あたる午前の绿茶";
        _discovery_authorLabel.font = [UIFont systemFontOfSize:14];
        _discovery_authorLabel.textColor = [UIColor whiteColor];
    }
    return _discovery_authorLabel;
}

- (UILabel *)discovery_hotLabel {
    if (!_discovery_hotLabel) {
        _discovery_hotLabel = [[UILabel alloc] init];
//        _discovery_hotLabel.text = @"热度: 6348757";
        _discovery_hotLabel.font = [UIFont systemFontOfSize:14];
        _discovery_hotLabel.textColor = [UIColor whiteColor];
    }
    return _discovery_hotLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor lightGrayColor];
    }
    return _lineView;
}

@end
