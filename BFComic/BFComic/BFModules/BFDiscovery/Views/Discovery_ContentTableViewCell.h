//
//  Discovery_ContentTableViewCell.h
//  BFComic
//
//  Created by TokapoNzld on 2022/3/9.
//

#import <UIKit/UIKit.h>
#import "Discovery_ComicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Discovery_ContentTableViewCell : UITableViewCell

@property (nonatomic,strong) Discovery_ComicModel *discovery_cellComicModel;

@end

NS_ASSUME_NONNULL_END
